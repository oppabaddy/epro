/*
 Navicat Premium Data Transfer

 Source Server         : 156.67.219.148
 Source Server Type    : MySQL
 Source Server Version : 100148
 Source Host           : 156.67.219.148:3306
 Source Schema         : epro

 Target Server Type    : MySQL
 Target Server Version : 100148
 File Encoding         : 65001

 Date: 24/03/2021 16:08:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for faq
-- ----------------------------
DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of faq
-- ----------------------------

-- ----------------------------
-- Table structure for log_import
-- ----------------------------
DROP TABLE IF EXISTS `log_import`;
CREATE TABLE `log_import`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of log_import
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2020_01_08_011445_create_persons_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for persons
-- ----------------------------
DROP TABLE IF EXISTS `persons`;
CREATE TABLE `persons`  (
  `id_person` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `photo_profile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `birth_place` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `birth_date` date NULL DEFAULT NULL,
  `id_divisi` int NULL DEFAULT NULL,
  `id_jabatan` int NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` int NULL DEFAULT 1,
  `status_user` int NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_person`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of persons
-- ----------------------------
INSERT INTO `persons` VALUES (73, 'aUeRdKA0CV.png', 'Admin', 'Super', 'Laki-Laki', 'Makkah', '1994-01-21', NULL, NULL, 'Jl. Menuju Kemuliaan', 1, 1, NULL, '2021-01-06 12:14:11');
INSERT INTO `persons` VALUES (83, 'pD2Gdg2F5S.png', 'Uul', 'Maulana', NULL, 'Semarang', '1994-06-07', 1, 4, NULL, 1, 1, NULL, '2020-10-12 13:24:45');
INSERT INTO `persons` VALUES (84, 'DpAPNC2pQK.png', 'Risky', 'Maul', NULL, 'Semarang', '1991-02-05', 1, 6, NULL, 1, 1, NULL, '2020-10-12 13:48:15');
INSERT INTO `persons` VALUES (90, 'ZUAtEbVdGI.png', 'Achmad', 'Maul', 'Laki-Laki', 'Semarang', '1992-05-06', 5, 2, 'Jl. Oke', 1, 1, NULL, NULL);
INSERT INTO `persons` VALUES (91, 'mqrh7LAz6n.png', 'Faisal', 'Maulana', 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL);
INSERT INTO `persons` VALUES (93, 'knknkn2872.png', 'knknkn', '', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL);
INSERT INTO `persons` VALUES (96, 'n2Ff64YDNC.png', 'Achmad Maul', 'Calon DIRUT', 'Laki-Laki', 'Majapahit, Kasta Budak', '0819-12-11', NULL, NULL, NULL, 1, 1, NULL, '2020-12-28 11:28:55');
INSERT INTO `persons` VALUES (97, 'admins0101.png', 'Bapak', 'ABC', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for tb_assessment
-- ----------------------------
DROP TABLE IF EXISTS `tb_assessment`;
CREATE TABLE `tb_assessment`  (
  `id_kop` int NULL DEFAULT NULL,
  `pelaporan_keuangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `membiayai_kegiatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengelolaan_dana` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `dapat_berjalan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengelolaan_pengurus` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `peningkatan_kapasitas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengelolaan_relawan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `mendukung_peningkatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `memahami_nilai` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `efektivitas_program` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `efektivitas_program_setahun` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `cakupan_program` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengukuran_intervensi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `menjalankan_program` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `mencapai_visi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `perencanaan_program` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `komunikasikan_program` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `mendukung_kemitraan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `kemitraan_komunitas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `kemitraan_pemerintah` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `memecahkan_masalah` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `mendukung_inovasi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `memberikan_ruang` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pendapatan_utama` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `total_dana` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengurus_kop` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `relawan_kop` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `jumlah_kemitraan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `melakukan_komunitas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `teknologi_komunitas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `berapa_kegiatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `nama_media_kegiatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `masalah_utama` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `pengembangan_komunitas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_assessment
-- ----------------------------
INSERT INTO `tb_assessment` VALUES (9, 'Belum memiliki laporan keuangan', 'Belum melakukan/membutuhkan penggalangan dana', 'Hanya untuk program rutin', 'kurang 6 bulan', 'Belum memiliki prosedur standar', 'Belum membutuhkan training', 'Tidak memiliki relawan atau belum memiliki prosedur standar', 'Tidak memiliki relawan atau belum membutuhkan training', 'Tidak memiliki relawan atau belum menyampaikan nilai-nilai komunitas/organisasi', 'Belum melakukan monitoring dan evaluasi program', 'Tidak melakukan monitoring dan evaluasi', 'Berada pada tingkat unit (sekolah/universitas, taman bacaan, perpustakaan kota)', 'Activities (contoh: jumlah kegiatan, jumlah peserta kegiatan)', NULL, 'Belum memiliki rancangan kegiatan atau memiliki rancangan < 6 bulan ke depan', 'Sasaran activities (contoh: jumlah kegiatan, jumlah peserta kegiatan)', 'Belum memiliki strategi komunikasi', NULL, 'Tidak memiliki kebutuhan untuk bermitra', 'Belum memiliki hubungan kemitraan', 'Hanya menggunakan cara yang sebelumnya biasa dipakai', 'Belum memanfaatkan teknologi', 'Tidak memberikan ruang', 'Iuran/uang kas anggota', 'Rp 50 juta – Rp 250 juta', '5-15 orang', 'Tidak memiliki relawan', 'kurang 5 mitra', 'Memberikan laporan kegiatan organisasi', 'Penggunaan teknologi dalam pengorganisasian data (Google Drive, Airtable, dll)', 'kurang 10 kali', 'ascacascacadc', 'Finansial', 'Proses internal', '2020-06-02 06:34:17', '2020-08-26 14:03:57');
INSERT INTO `tb_assessment` VALUES (10, 'Belum memiliki laporan keuangan', 'Belum melakukan/membutuhkan penggalangan dana', 'Hanya untuk program rutin', 'kurang 6 bulan', 'Belum memiliki prosedur standar', 'Belum membutuhkan training', 'Tidak memiliki relawan atau belum memiliki prosedur standar', 'Tidak memiliki relawan atau belum membutuhkan training', 'Tidak memiliki relawan atau belum menyampaikan nilai-nilai komunitas/organisasi', 'Belum melakukan monitoring dan evaluasi program', 'Tidak melakukan monitoring dan evaluasi', 'Berada pada tingkat unit (sekolah/universitas, taman bacaan, perpustakaan kota)', 'Activities (contoh: jumlah kegiatan, jumlah peserta kegiatan)', NULL, 'Belum memiliki rancangan kegiatan atau memiliki rancangan < 6 bulan ke depan', 'Sasaran activities (contoh: jumlah kegiatan, jumlah peserta kegiatan)', 'Belum memiliki strategi komunikasi', NULL, 'Tidak memiliki kebutuhan untuk bermitra', 'Belum memiliki hubungan kemitraan', 'Hanya menggunakan cara yang sebelumnya biasa dipakai', 'Belum memanfaatkan teknologi', 'Tidak memberikan ruang', 'Iuran/uang kas anggota', 'kurang Rp 50 juta', 'kurang 5 orang', 'Tidak memiliki relawan', 'kurang 5 mitra', 'Memberikan laporan kegiatan organisasi', 'Penggunaan teknologi dalam project management (Trello, Asana, dll)', 'kurang 10 kali', 'dsvsavsvsafv', 'Proses internal', 'Dampak (Impact)', '2020-08-06 10:30:49', NULL);

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_post` int NULL DEFAULT NULL,
  `id_kop` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `comment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `flag` int NULL DEFAULT NULL,
  `edited_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
INSERT INTO `tb_comment` VALUES (2, 13, NULL, 14, 'Admin Super', 'airbat.exiglosi@gmail.com', 'Berita', 'test okay', 1, NULL, '2020-10-06 14:53:56', NULL);
INSERT INTO `tb_comment` VALUES (4, 21, NULL, 14, 'Admin Super', 'airbat.exiglosi@gmail.com', 'Pengumuman', 'mantap oy', 1, 'Admin', '2020-11-11 00:59:21', '2021-01-06 13:44:08');

-- ----------------------------
-- Table structure for tb_committee
-- ----------------------------
DROP TABLE IF EXISTS `tb_committee`;
CREATE TABLE `tb_committee`  (
  `id_committee` int NOT NULL AUTO_INCREMENT,
  `title_committee` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_user` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `valid_from` date NULL DEFAULT NULL,
  `valid_to` date NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_committee`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_committee
-- ----------------------------
INSERT INTO `tb_committee` VALUES (6, 'Panitia 2021', '21,22', '2021-01-06', '2021-12-31', 2, '2021-01-06 14:00:21', '2021-01-06 14:06:29');

-- ----------------------------
-- Table structure for tb_content_arewe
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_arewe`;
CREATE TABLE `tb_content_arewe`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `foto_arewe` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_arewe` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `posisi_arewe` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `urutan` int NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_arewe
-- ----------------------------
INSERT INTO `tb_content_arewe` VALUES (23, '/uploads/arewe/im4vYlli2k.png', 'Ir. Anton Santosa, M.T.', 'Commissioner', 2, 1, '2020-10-13 22:59:54', '2021-01-20 09:28:13');
INSERT INTO `tb_content_arewe` VALUES (24, '/uploads/arewe/zn2HrCod4L.png', 'Ir. Rachmadi Nugroho, M.T.', 'President Director', 3, 1, '2020-10-13 23:02:47', '2021-01-20 09:33:24');
INSERT INTO `tb_content_arewe` VALUES (25, '/uploads/arewe/dOGi0uwuVO.png', 'Joseph Sondang Tobing, M.Sc.', 'Director of Finance', 4, 1, '2020-10-13 23:03:20', '2021-01-20 09:34:16');
INSERT INTO `tb_content_arewe` VALUES (26, '/uploads/arewe/AafShxGgHR.png', 'Dr. Prasetyo Aribowo, S.H., M.Soc, Sc.', 'President Commissioner', 1, 1, '2020-10-14 13:12:58', '2021-01-20 09:27:04');
INSERT INTO `tb_content_arewe` VALUES (28, '/uploads/arewe/tgLmzxrjo4.png', 'Ahmad Fauzie Nur, S.E., M.Business.', 'Director of Operations', 5, 1, '2021-01-20 09:35:06', NULL);
INSERT INTO `tb_content_arewe` VALUES (29, '/uploads/arewe/r2BviEBPFg.png', 'Pratiknya, S.H., M.H.', 'Head of Corporate Secretariat', 6, 1, '2021-01-20 09:36:23', '2021-01-20 09:36:39');
INSERT INTO `tb_content_arewe` VALUES (30, '/uploads/arewe/Z5zerc36jp.png', 'Agus Santoso, S.E.', 'Human Capital & GA Division Head', 7, 1, '2021-01-20 09:37:35', NULL);
INSERT INTO `tb_content_arewe` VALUES (31, '/uploads/arewe/2V7VV5D2xL.png', 'Bambang Setiyawan, S.T.', 'Head of Internal Audit', 8, 1, '2021-01-20 09:38:24', NULL);
INSERT INTO `tb_content_arewe` VALUES (32, '/uploads/arewe/TFgESZnwYr.png', 'Kuniyanti Hadiatmaja, S.E., Akt.', 'Marketing Division Head', 9, 1, '2021-01-20 09:39:10', NULL);
INSERT INTO `tb_content_arewe` VALUES (33, '/uploads/arewe/VNsKLXAoAw.png', 'Danang Agung Indarto, S.E.', 'Account and Finance Division Head', 10, 1, '2021-01-20 09:39:59', NULL);
INSERT INTO `tb_content_arewe` VALUES (34, '/uploads/arewe/WGwlSwgVrY.png', 'M. Nicho Setyawan, S.T.', 'Business Development Division Head', 11, 1, '2021-01-20 09:41:03', NULL);
INSERT INTO `tb_content_arewe` VALUES (35, '/uploads/arewe/SIkrC7whAv.png', 'San Agitato Ganda P, S.T.', 'Engineering Division Head', 12, 1, '2021-01-20 09:42:53', NULL);

-- ----------------------------
-- Table structure for tb_content_banner
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_banner`;
CREATE TABLE `tb_content_banner`  (
  `id_banner` int NOT NULL AUTO_INCREMENT,
  `foto_banner` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul_banner` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_banner` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan_banner` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tanggaldurasi_awal` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggaldurasi_akhir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_banner`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_banner
-- ----------------------------
INSERT INTO `tb_content_banner` VALUES (3, '/uploads/banner/IBVOiD3bYg.png', 'Banner 2', NULL, 'Corona', NULL, NULL, 1, '2021-03-12 15:18:53', NULL);
INSERT INTO `tb_content_banner` VALUES (4, '/uploads/banner/FnTXqKLE5e.png', 'Banner 3', NULL, 'Jaga NKRI', NULL, NULL, 1, '2021-03-12 15:19:34', NULL);
INSERT INTO `tb_content_banner` VALUES (5, '/uploads/banner/782MfTtBKm.png', 'Banner 4', NULL, 'Ayo pakai masker', NULL, NULL, 1, '2021-03-12 15:20:14', NULL);

-- ----------------------------
-- Table structure for tb_content_berita
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_berita`;
CREATE TABLE `tb_content_berita`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_kop` int UNSIGNED NOT NULL,
  `judul` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `gambar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_fb` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `link_instagram` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `link_youtube` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `flag` int NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `tanggal_publish` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_berita
-- ----------------------------
INSERT INTO `tb_content_berita` VALUES (13, 14, 'Test Lagi 2', '<p>Test Lagi 2</p>', '', 'E2NJODzQYt9fva8j.png', NULL, NULL, NULL, '2020-09-16 15:25:03', 2, '2020-10-06 15:28:01', '2020-09-16 15:25:03');
INSERT INTO `tb_content_berita` VALUES (14, 14, 'Test 2', '<p>Test Kedua</p>', '', 'rdJCezculmXt8mvI.png', NULL, NULL, NULL, '2021-03-18 14:59:19', 2, NULL, '2021-03-18 14:59:19');
INSERT INTO `tb_content_berita` VALUES (15, 14, 'Test 3', '<p>Test Ketiga</p>', '', 'Q42hqRWIlG4Mjik2.png', NULL, NULL, NULL, '2021-03-18 14:59:45', 2, NULL, '2021-03-18 14:59:45');

-- ----------------------------
-- Table structure for tb_content_counter
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_counter`;
CREATE TABLE `tb_content_counter`  (
  `id` int NOT NULL,
  `rekanan` int NULL DEFAULT NULL,
  `barang` int NULL DEFAULT NULL,
  `kontruksi` int NULL DEFAULT NULL,
  `badanusaha` int NULL DEFAULT NULL,
  `perorangan` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_counter
-- ----------------------------
INSERT INTO `tb_content_counter` VALUES (1, 6, 3, 4, 1, 100, NULL, '2021-03-18 14:09:44');

-- ----------------------------
-- Table structure for tb_content_email
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_email`;
CREATE TABLE `tb_content_email`  (
  `id` int NOT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_email
-- ----------------------------
INSERT INTO `tb_content_email` VALUES (1, 'Register', '<p><br></p><p>Hai,</p><p><br></p><p>Selamat Sudah Registrasi, mohon tunggu konfirmasi selanjutnya</p>');
INSERT INTO `tb_content_email` VALUES (2, 'Approve', '<p>Approve</p>');
INSERT INTO `tb_content_email` VALUES (3, 'NotApprove', '<p>Not Approve</p>');

-- ----------------------------
-- Table structure for tb_content_footer
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_footer`;
CREATE TABLE `tb_content_footer`  (
  `id` int NOT NULL,
  `instagram` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `twitter` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `katamutiara` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `katamutiaraEN` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_footer
-- ----------------------------
INSERT INTO `tb_content_footer` VALUES (1, 'https://www.instagram.com/', 'asdasdasd', 'https://www.facebook.com/', 'pemasaran@kiw.co.id', 'Kawasan Industri Wijayakusuma', 'Jalan Raya Semarang - Kendal KM. 12, Jl. Tugu Industri I No.1, Randu Garut, Kec. Tugu, Kota Semarang, Jawa Tengah 50153', 'Hanya KIW tempat terbaik untuk investasi anda. Karena hanya KIW yang memberikan layanan dan fasilitas terlengkap, harga terbaik, infrastruktur yang memadai serta tim yang profesional. Tumbuh dan berkembanglah bersama KIW. The Best Location for Better Investment.', 'Only KIW is the best place for your investment. Because only KIW provides the most complete services and facilities, the best prices, adequate infrastructure and a professional team. Grow and develop with KIW. The Best Location for Better Investment.', NULL, '2021-01-04 15:06:49');

-- ----------------------------
-- Table structure for tb_content_landing
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_landing`;
CREATE TABLE `tb_content_landing`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `tagline1` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tagline2` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tagline3` varchar(160) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gambar` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_landing
-- ----------------------------
INSERT INTO `tb_content_landing` VALUES (1, 'PT. Kawasan Industri Wijayakusuma', 'e-Procurement', 'The Best Location for Better Investment', '/var/www/eproc/public/uploads/landing/iYUgz09wjC.png', '2021-03-12 15:02:09');

-- ----------------------------
-- Table structure for tb_content_pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_pengumuman`;
CREATE TABLE `tb_content_pengumuman`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_kop` int NOT NULL,
  `judul` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `gambar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL,
  `keterangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `waktu_event` time(0) NULL DEFAULT NULL,
  `jam_mulai` time(0) NULL DEFAULT NULL,
  `jam_selesai` time(0) NULL DEFAULT NULL,
  `link_fb` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_youtube` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_instagram` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `tanggal_publish` datetime(0) NULL DEFAULT NULL,
  `jam_publish` time(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `flag` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_pengumuman
-- ----------------------------
INSERT INTO `tb_content_pengumuman` VALUES (19, 14, 'svdjksdbvskdbv 222 333', '<p>andbckndabvkjsdbv 222 333</p>', '-6.9746846,110.325941', 'KuaoAer21UOnERmp.png', '2020-10-10 00:00:00', NULL, 'Publish', NULL, '10:00:00', '12:00:00', NULL, NULL, NULL, '2020-10-07 14:31:09', '2020-10-09 10:00:00', '10:00:00', '2020-10-07 14:41:52', 2);
INSERT INTO `tb_content_pengumuman` VALUES (20, 14, 'nbkbkb', '<p>bkbkbkb</p>', '-7.090910999999999,107.668887', 'o4Sb4m8iYhdLEqC1.png', '2020-10-30 00:00:00', NULL, 'Publish', NULL, '12:00:00', '13:00:00', NULL, NULL, NULL, '2020-10-29 10:25:43', '2020-10-29 09:00:00', '09:00:00', NULL, 2);
INSERT INTO `tb_content_pengumuman` VALUES (21, 14, 'bkb n', '<p>nm m m mn</p>', '-7.090910999999999,107.668887', 'BYc46XnZ0gJUpqGh.png', '2020-10-31 00:00:00', NULL, 'Publish', NULL, '11:00:00', '12:00:00', NULL, NULL, NULL, '2020-10-29 10:26:39', '2020-10-30 08:00:00', '08:00:00', NULL, 2);

-- ----------------------------
-- Table structure for tb_content_tentangkami
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_tentangkami`;
CREATE TABLE `tb_content_tentangkami`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `tentangkami_full` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tentangkami_fullEN` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tentangkami_thumb` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tentangkami_thumbEN` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_tentangkami
-- ----------------------------
INSERT INTO `tb_content_tentangkami` VALUES (1, '<h1 class=\"ql-align-center\"><strong>PT KIW (PERSERO)</strong></h1><p class=\"ql-align-center\"><strong><em>“ HANYA KIW TEMPAT TERBAIK UNTUK INVESTASI ANDA. KARENA HANYA KIW YANG MEMBERIKAN LAYANAN DAN FASILITAS TERLENGKAP, HARGA TERBAIK, INFRASTRUKTUR YANG MEMADAI SERTA TIM YANG PROFESIONAL. TUMBUH DAN BERKEMBANGLAH BERSAMA KIW.&nbsp;THE BEST LOCATION FOR BETTER INVESTMENT. ”</em></strong></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">PT. Kawasan Industri Wijayakusuma (Persero) yang biasa disingkat dengan PT. KIW (Persero) merupakan perusahaan berstatus Badan Usaha Milik Negara (BUMN) yang bergerak di bidang pengembangan dan pengelolaan kawasan industri.</span></p><p><br></p><h2 class=\"ql-align-center\"><span style=\"color: rgb(136, 136, 136);\">Kepemilikan saham PT. KIW (Persero) terbagi dalam 51.09% milik Kementerian BUMN; 40.19% milik Pemerintah Provinsi Jawa Tengah dan 8.52% milik Pemerintah Kabupaten Cilacap.</span></h2><p class=\"ql-align-center\"><br></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW di Semarang, Jawa Tengah yang memiliki infrastruktur lengkap dan terus berkembang serta ketersediaan tenaga kerja yang kompeten dan kompetitif yang sangat mendukung untuk investasi di bidang industri.KIW terletak jalur utama lintas provinsi yang strategis dengan akses yang sangat dekat dengan Jalan Tol, Pelabuhan, Bandara, Stasiun Kereta Api, terminal bus, pusat pemerintahan Provinsi Jawa Tengah, Rumah Sakit, Pusat Kota, serta Pusat Perbelanjaan.</span></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW merupakan kawasan industri terbaik untuk investasi dengan lahan siap bangun dan bebas banjir seluas 250Ha serta Bangunan Pabrik Siap Pakai (BPSP) seluas 48.338 m2. KIW didukung dengan layanan yang lengkap seperti WTP, WWTP, Low Cost Service Charge, Pengelolaan Parkir dan Penyewaan Bangunan Kantor, serta dilengkapi fasilitas terbaik seperti fasilitas KLIK, kemudahan bisnis, keamanan 24 jam, pemadam kebakaran dan lain sebagainya.</span></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW juga telah memiliki banyak prestasi dengan menerima berbagai penghargaan bergengsi. Hal ini menunjukkan profesionalisme KIW dalam mengembangkan dan mengelola Kawasan Industri. KIW akan semakin lincah dan berkembang dengan telah memiliki anak perusahaan, PT. Putra Wijayakusuma Sakti.</span></p><p class=\"ql-align-center\"><strong style=\"color: rgb(68, 68, 68);\">BERGABUNGLAH BERSAMA KAMI, KAWASAN INDUSTRI WIJAYAKUSUMA. THE BEST LOCATION FOR BETTER INVESTMENT.</strong></p>', '<h1 class=\"ql-align-center\"><strong>PT KIW (PERSERO)</strong></h1><p class=\"ql-align-center\"><strong><em>“KIW IS JUST THE BEST PLACE FOR YOUR INVESTMENT. BECAUSE ONLY KIW PROVIDES THE COMPLETE SERVICES AND FACILITIES, BEST PRICES, ADEQUATE INFRASTRUCTURE AND PROFESSIONAL TEAM. GROW AND GROWTH WITH KIW. THE BEST LOCATION FOR BETTER INVESTMENT.\"</em></strong></p><p><br></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">PT. Wijayakusuma Industrial Estate (Persero) which is commonly abbreviated as PT. KIW (Persero) is a state-owned company (BUMN) which is engaged in the development and management of industrial estates.</span></p><p><br></p><h2 class=\"ql-align-center\"><span style=\"color: rgb(136, 136, 136);\">Share ownership of PT. KIW (Persero) is divided into 51.09% owned by the Ministry of BUMN; 40.19% belong to the Central Java Provincial Government and 8.52% belong to the Cilacap Regency Government.</span></h2><p><br></p><p><br></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW in Semarang, Central Java, which has complete and growing infrastructure and the availability of a competent and competitive workforce that is very supportive for investment in the industrial sector. KIW is located a strategic cross-provincial main route with very close access to toll roads, ports, airports , Train Stations, bus terminals, Central Java Provincial government centers, Hospitals, City Centers, and Shopping Centers.</span></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW is the best industrial area for investment with 250ha of ready to build and flood-free land and a ready-to-use factory building (BPSP) of 48,338 m2. KIW is supported by complete services such as WTP, WWTP, Low Cost Service Charge, Parking Management and Office Building Rentals, and is equipped with the best facilities such as KLIK facilities, business conveniences, 24-hour security, fire brigade and so on.</span></p><p class=\"ql-align-justify\"><span style=\"color: rgb(68, 68, 68);\">KIW has also had many achievements by receiving various prestigious awards. This shows KIW\'s professionalism in developing and managing Industrial Estates. KIW will be more agile and develop by having a subsidiary, PT. Son of Wijayakusuma Sakti.</span></p><p class=\"ql-align-center\"><strong style=\"color: rgb(68, 68, 68);\">JOIN WITH US, KAWASAN INDUSTRI WIJAYAKUSUMA. THE BEST LOCATION FOR BETTER INVESTMENT</strong></p>', 'PT. Kawasan Industri Wijayakusuma (Persero) yang biasa disingkat dengan PT. KIW (Persero) merupakan perusahaan berstatus Badan Usaha Milik Negara (BUMN) yang bergerak di bidang pengembangan dan pengelolaan kawasan industri.', 'PT. Wijayakusuma Industrial Estate (Persero) which is commonly abbreviated as PT. KIW (Persero) is a state-owned company (BUMN) which is engaged in the development and management of industrial estates.', NULL, '2021-01-05 15:25:18');

-- ----------------------------
-- Table structure for tb_content_visimisi
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_visimisi`;
CREATE TABLE `tb_content_visimisi`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `isi_visimisi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `isi_visimisiEN` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_content_visimisi
-- ----------------------------
INSERT INTO `tb_content_visimisi` VALUES (1, 'Menjadi Pengembang dan Pengelola Kawasan Industri, Properti dan Bisnis yang Andal dan Modern.;Menjalankan bisnis pengembang dan Pengelola Properti, Kawasan Industri dan Bisnis secara terintegrasi.;Menumbuhkembangkan korporasi serta memberi kontribusi positif terhadap perekonomian Daerah dan Nasional.;Konsisten menjaga kesinambungan usaha dan menjaga harmoni sosial dan kelestarian lingkungan hidup.;Mengkonsolidasikan PT PWS sebagai anak perusahaan penopang induk perusahaan.;', 'Become a Reliable and Modern Developer and Manager of Industrial, Property and Business Estates.;Running the business of developer and property management, industrial estates and businesses in an integrated manner.;Develop corporations and make a positive contribution to the Regional and National economy.;Consistently maintaining business continuity and maintaining social harmony and environmental sustainability.;Consolidating PT PWS as the supporting subsidiary of the parent company.;', '2020-05-19 00:00:00', '2020-09-16 11:05:06');

-- ----------------------------
-- Table structure for tb_divisi
-- ----------------------------
DROP TABLE IF EXISTS `tb_divisi`;
CREATE TABLE `tb_divisi`  (
  `id_divisi` int NOT NULL AUTO_INCREMENT,
  `divisi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_divisi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_divisi
-- ----------------------------
INSERT INTO `tb_divisi` VALUES (1, 'Bussines Development', '2020-09-10 12:12:12', '2020-10-14 19:45:30');
INSERT INTO `tb_divisi` VALUES (3, 'Marketing', '2020-10-14 19:48:19', NULL);
INSERT INTO `tb_divisi` VALUES (4, 'Enginering', '2020-10-14 19:48:30', NULL);
INSERT INTO `tb_divisi` VALUES (5, 'Direksi', '2020-10-19 19:22:45', NULL);
INSERT INTO `tb_divisi` VALUES (6, NULL, '2021-01-05 14:17:45', '2021-01-05 14:40:44');

-- ----------------------------
-- Table structure for tb_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `tb_jabatan`;
CREATE TABLE `tb_jabatan`  (
  `id_jabatan` int NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_jabatan
-- ----------------------------
INSERT INTO `tb_jabatan` VALUES (1, 'Direktur Utama', '2020-10-19 20:15:27', NULL);
INSERT INTO `tb_jabatan` VALUES (2, 'Direktur Operasional', '2020-10-19 20:15:42', NULL);
INSERT INTO `tb_jabatan` VALUES (3, 'Direktur Keuangan', '2020-10-19 20:16:05', NULL);
INSERT INTO `tb_jabatan` VALUES (4, 'Kepala Divisi', '2020-10-19 20:16:20', NULL);
INSERT INTO `tb_jabatan` VALUES (6, 'Staff', '2020-10-19 20:17:01', NULL);
INSERT INTO `tb_jabatan` VALUES (7, NULL, '2021-01-05 15:18:30', '2021-01-05 15:18:43');

-- ----------------------------
-- Table structure for tb_jatuh_tempo
-- ----------------------------
DROP TABLE IF EXISTS `tb_jatuh_tempo`;
CREATE TABLE `tb_jatuh_tempo`  (
  `id` int NOT NULL,
  `tanggal_donasi` int NULL DEFAULT NULL,
  `bulan_donasi` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_jatuh_tempo
-- ----------------------------
INSERT INTO `tb_jatuh_tempo` VALUES (1, 7, 9, NULL, '2020-09-07 10:36:57');

-- ----------------------------
-- Table structure for tb_jenis_vendor
-- ----------------------------
DROP TABLE IF EXISTS `tb_jenis_vendor`;
CREATE TABLE `tb_jenis_vendor`  (
  `id_jenis_vendor` int NOT NULL AUTO_INCREMENT,
  `jenis_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_jenis_vendor`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_jenis_vendor
-- ----------------------------
INSERT INTO `tb_jenis_vendor` VALUES (1, 'Konstruksi', '2020-09-10 12:12:12', NULL);
INSERT INTO `tb_jenis_vendor` VALUES (3, 'Konsultan', '2020-10-14 20:07:46', '2020-10-14 20:08:25');

-- ----------------------------
-- Table structure for tb_kategori_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori_pekerjaan`;
CREATE TABLE `tb_kategori_pekerjaan`  (
  `id_kategori_pekerjaan` int NOT NULL AUTO_INCREMENT,
  `kategori_pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori_pekerjaan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_kategori_pekerjaan
-- ----------------------------
INSERT INTO `tb_kategori_pekerjaan` VALUES (1, 'Pengadaan Barang', '2020-09-10 12:12:12', NULL);
INSERT INTO `tb_kategori_pekerjaan` VALUES (2, 'Pekerjaan Konstruksi', '2020-10-14 20:18:33', NULL);
INSERT INTO `tb_kategori_pekerjaan` VALUES (3, 'Jasa Konsultasi Badan Usaha', '2020-10-14 20:18:56', NULL);
INSERT INTO `tb_kategori_pekerjaan` VALUES (4, 'Jasa Konsultasi Perorangan', '2020-10-14 20:19:18', NULL);
INSERT INTO `tb_kategori_pekerjaan` VALUES (5, 'Jasa Lainnya', '2020-10-14 20:19:32', NULL);

-- ----------------------------
-- Table structure for tb_log_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_log_activity`;
CREATE TABLE `tb_log_activity`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `log` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 394 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_log_activity
-- ----------------------------
INSERT INTO `tb_log_activity` VALUES (1, 'admins0101', 'Admin Super', '114.124.161.153', 'Memberikan rating 0 pada jbkjbkjb', '2021-01-04 10:30:36', NULL);
INSERT INTO `tb_log_activity` VALUES (2, 'admins0101', 'Admin Super', '114.124.161.153', 'Memberikan rating 1 pada jbkjbkjb', '2021-01-04 10:31:01', NULL);
INSERT INTO `tb_log_activity` VALUES (3, 'admins0101', 'Admin Super', '114.124.161.153', 'Memberikan rating 0 pada PT. Oke', '2021-01-04 10:31:42', NULL);
INSERT INTO `tb_log_activity` VALUES (4, 'admins0101', 'Admin Super', '114.124.161.153', 'Memberikan rating 1 pada PT. Oke', '2021-01-04 10:32:46', NULL);
INSERT INTO `tb_log_activity` VALUES (9, 'admins0101', 'Admin Super', '114.124.161.153', 'Membuat baru role akses R005', '2021-01-04 11:17:35', NULL);
INSERT INTO `tb_log_activity` VALUES (10, 'admins0101', 'Admin Super', '114.124.161.153', 'Menghapus baru role akses R005', '2021-01-04 11:17:41', NULL);
INSERT INTO `tb_log_activity` VALUES (11, 'admins0101', 'Admin Super', '114.124.161.153', 'Membuat baru menu landing test', '2021-01-04 11:47:25', NULL);
INSERT INTO `tb_log_activity` VALUES (12, 'admins0101', 'Admin Super', '114.124.161.153', 'Memperbarui menu landing test 2', '2021-01-04 11:47:50', NULL);
INSERT INTO `tb_log_activity` VALUES (13, 'admins0101', 'Admin Super', '114.124.161.153', 'Memperbarui menu landing test 2', '2021-01-04 11:48:26', NULL);
INSERT INTO `tb_log_activity` VALUES (14, 'admins0101', 'Admin Super', '114.124.161.153', 'Memperbarui menu landing test 2', '2021-01-04 11:48:26', NULL);
INSERT INTO `tb_log_activity` VALUES (15, 'admins0101', 'Admin Super', '114.124.161.153', 'menghapus menu landing child', '2021-01-04 11:52:19', NULL);
INSERT INTO `tb_log_activity` VALUES (16, 'admins0101', 'Admin Super', '114.124.161.153', 'Membuat baru menu landing vdvdsv', '2021-01-04 11:52:34', NULL);
INSERT INTO `tb_log_activity` VALUES (17, 'admins0101', 'Admin Super', '114.124.161.153', 'Memperbarui menu landing vdvdsv 2', '2021-01-04 11:53:23', NULL);
INSERT INTO `tb_log_activity` VALUES (18, 'admins0101', 'Admin Super', '114.124.161.153', 'menghapus menu landing child', '2021-01-04 11:53:40', NULL);
INSERT INTO `tb_log_activity` VALUES (19, 'admins0101', 'Admin Super', '114.124.161.153', 'menghapus menu landing parent', '2021-01-04 11:53:49', NULL);
INSERT INTO `tb_log_activity` VALUES (21, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui content email Register', '2021-01-04 14:19:41', NULL);
INSERT INTO `tb_log_activity` VALUES (22, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui content email Approve', '2021-01-04 14:24:18', NULL);
INSERT INTO `tb_log_activity` VALUES (23, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui content email NotApprove', '2021-01-04 14:24:24', NULL);
INSERT INTO `tb_log_activity` VALUES (24, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui content footer', '2021-01-04 15:06:49', NULL);
INSERT INTO `tb_log_activity` VALUES (26, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui content main banner', '2021-01-04 15:09:07', NULL);
INSERT INTO `tb_log_activity` VALUES (28, 'admins0101', 'Admin Super', '182.253.8.51', 'Memperbaharui divisi ', '2021-01-05 14:40:44', NULL);
INSERT INTO `tb_log_activity` VALUES (32, 'admins0101', 'Admin Super', '182.253.8.51', 'Membuat Kualifikasi TEST', '2021-01-05 15:05:24', NULL);
INSERT INTO `tb_log_activity` VALUES (33, 'admins0101', 'Admin Super', '117.102.65.29', 'Memperbarui Kualifikasi TEST 2', '2021-01-05 15:05:35', NULL);
INSERT INTO `tb_log_activity` VALUES (34, 'admins0101', 'Admin Super', '182.253.8.51', 'Memperbarui Kualifikasi ', '2021-01-05 15:05:40', NULL);
INSERT INTO `tb_log_activity` VALUES (35, 'admins0101', 'Admin Super', '117.102.65.29', 'Membuat Klasifikasi BG TEST', '2021-01-05 15:05:53', NULL);
INSERT INTO `tb_log_activity` VALUES (36, 'admins0101', 'Admin Super', '117.102.65.29', 'Memperbarui Kualifikasi BG TEST 2', '2021-01-05 15:06:08', NULL);
INSERT INTO `tb_log_activity` VALUES (37, 'admins0101', 'Admin Super', '117.102.65.29', 'Memperbarui Kualifikasi ', '2021-01-05 15:06:19', NULL);
INSERT INTO `tb_log_activity` VALUES (38, 'admins0101', 'Admin Super', '182.253.8.51', 'Menambahkan Jabatan fvbfsbdgsbg', '2021-01-05 15:18:30', NULL);
INSERT INTO `tb_log_activity` VALUES (39, 'admins0101', 'Admin Super', '117.102.65.29', 'MEmperbarui Jabatan fvbfsbdgsbg 2', '2021-01-05 15:18:37', NULL);
INSERT INTO `tb_log_activity` VALUES (40, 'admins0101', 'Admin Super', '117.102.65.29', 'MEmperbarui Jabatan ', '2021-01-05 15:18:43', NULL);
INSERT INTO `tb_log_activity` VALUES (41, 'admins0101', 'Admin Super', '117.102.65.29', 'Memperbarui content tentang kami', '2021-01-05 15:25:18', NULL);
INSERT INTO `tb_log_activity` VALUES (42, 'admins0101', 'Admin Super', '182.253.8.51', 'Menambahkan Banner', '2021-01-05 15:49:06', NULL);
INSERT INTO `tb_log_activity` VALUES (43, 'admins0101', 'Admin Super', '182.253.8.51', 'Memperbarui Banner', '2021-01-05 16:00:12', NULL);
INSERT INTO `tb_log_activity` VALUES (44, 'admins0101', 'Admin Super', '182.253.8.51', 'Menghapus Banner', '2021-01-05 16:00:23', NULL);
INSERT INTO `tb_log_activity` VALUES (47, 'admins0101', 'Admin Super', '182.253.8.51', 'Menghapus 1 Are We', '2021-01-05 16:36:18', NULL);
INSERT INTO `tb_log_activity` VALUES (48, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah Pengumuman baru test', '2021-01-06 09:45:55', NULL);
INSERT INTO `tb_log_activity` VALUES (49, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Pengumuman test', '2021-01-06 09:54:30', NULL);
INSERT INTO `tb_log_activity` VALUES (50, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Pengumuman', '2021-01-06 09:55:03', NULL);
INSERT INTO `tb_log_activity` VALUES (51, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Berita kbksldjvnsbldvsdjv', '2021-01-06 10:01:55', NULL);
INSERT INTO `tb_log_activity` VALUES (52, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Berita kbksldjvnsbldvsdjv 2', '2021-01-06 10:02:39', NULL);
INSERT INTO `tb_log_activity` VALUES (53, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Berita', '2021-01-06 10:02:48', NULL);
INSERT INTO `tb_log_activity` VALUES (54, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Counter', '2021-01-06 10:04:58', NULL);
INSERT INTO `tb_log_activity` VALUES (57, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-06 10:51:06', NULL);
INSERT INTO `tb_log_activity` VALUES (58, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-06 12:00:04', NULL);
INSERT INTO `tb_log_activity` VALUES (59, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui baru role akses R000', '2021-01-06 12:05:15', NULL);
INSERT INTO `tb_log_activity` VALUES (60, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui User admins0101', '2021-01-06 12:09:23', NULL);
INSERT INTO `tb_log_activity` VALUES (61, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui User admins0101', '2021-01-06 12:11:22', NULL);
INSERT INTO `tb_log_activity` VALUES (62, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui User admins0101', '2021-01-06 12:12:36', NULL);
INSERT INTO `tb_log_activity` VALUES (64, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Profile', '2021-01-06 12:14:11', NULL);
INSERT INTO `tb_log_activity` VALUES (65, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Comment', '2021-01-06 13:44:08', NULL);
INSERT INTO `tb_log_activity` VALUES (66, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Comment', '2021-01-06 13:44:43', NULL);
INSERT INTO `tb_log_activity` VALUES (69, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Committee', '2021-01-06 14:04:39', NULL);
INSERT INTO `tb_log_activity` VALUES (70, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Committee', '2021-01-06 14:06:04', NULL);
INSERT INTO `tb_log_activity` VALUES (71, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Committee', '2021-01-06 14:06:29', NULL);
INSERT INTO `tb_log_activity` VALUES (72, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-07 09:20:23', NULL);
INSERT INTO `tb_log_activity` VALUES (73, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Test', '2021-01-07 10:09:17', NULL);
INSERT INTO `tb_log_activity` VALUES (74, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-01-07 10:12:04', NULL);
INSERT INTO `tb_log_activity` VALUES (75, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Approve Pekerjaan', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (76, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-07 jam 11:00', '2021-01-07 10:13:52', NULL);
INSERT INTO `tb_log_activity` VALUES (77, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-01-07 10:42:41', NULL);
INSERT INTO `tb_log_activity` VALUES (78, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Pakta Integritas', '2021-01-07 10:45:15', NULL);
INSERT INTO `tb_log_activity` VALUES (79, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Akta Pendirian', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (80, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Akta Perubahan', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (81, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan NPWP', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (82, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan PKP', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (83, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan IUJK / SIUP', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (84, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan SBU', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (85, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-01-07 11:10:44', NULL);
INSERT INTO `tb_log_activity` VALUES (86, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Pakta Integritas', '2021-01-07 11:11:21', NULL);
INSERT INTO `tb_log_activity` VALUES (87, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Akta Pendirian', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (88, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Akta Perubahan', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (89, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan NPWP', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (90, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan IUJK / SIUP', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (91, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan PKP', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (92, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan SBU', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (93, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-01-07 11:12:55', NULL);
INSERT INTO `tb_log_activity` VALUES (94, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Dokumen Kualifikasi Pekerjaan Pakta Integritas', '2021-01-07 11:13:08', NULL);
INSERT INTO `tb_log_activity` VALUES (95, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-01-07 jam 11:00 dan berakhir pada 2021-01-07 jam 12:00', '2021-01-07 11:21:09', NULL);
INSERT INTO `tb_log_activity` VALUES (96, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memberi Pertanyaan', '2021-01-07 11:44:18', NULL);
INSERT INTO `tb_log_activity` VALUES (97, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memberi Pertanyaan', '2021-01-07 11:44:26', NULL);
INSERT INTO `tb_log_activity` VALUES (98, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-07 11:52:28', NULL);
INSERT INTO `tb_log_activity` VALUES (99, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-07 11:52:37', NULL);
INSERT INTO `tb_log_activity` VALUES (100, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-07 11:55:17', NULL);
INSERT INTO `tb_log_activity` VALUES (101, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-07 12:03:59', NULL);
INSERT INTO `tb_log_activity` VALUES (102, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-07 12:06:59', NULL);
INSERT INTO `tb_log_activity` VALUES (103, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-01-07 13:26:17', NULL);
INSERT INTO `tb_log_activity` VALUES (106, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-01-07 jam 13:00 dan berakhir pada 2021-01-07 jam 15:00', '2021-01-07 13:32:27', NULL);
INSERT INTO `tb_log_activity` VALUES (107, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Berhasil Login', '2021-01-07 14:45:33', NULL);
INSERT INTO `tb_log_activity` VALUES (111, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-01-07 15:06:37', NULL);
INSERT INTO `tb_log_activity` VALUES (112, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Menambah Penawaran Surat Penawaran', '2021-01-07 15:06:37', NULL);
INSERT INTO `tb_log_activity` VALUES (113, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Menambah Penawaran Rekapitulasi RAB', '2021-01-07 15:06:37', NULL);
INSERT INTO `tb_log_activity` VALUES (114, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Menambah Penawaran Rincian RAB', '2021-01-07 15:07:09', NULL);
INSERT INTO `tb_log_activity` VALUES (115, 'admins0101', 'Admin Super', '117.102.65.29', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-01-07 jam 15:00 dan berakhir pada 2021-01-07 jam 16:00', '2021-01-07 15:10:32', NULL);
INSERT INTO `tb_log_activity` VALUES (116, 'admins0101', 'admins0101', '182.253.8.51', 'Menambahkan Evaluasi Penawaran', '2021-01-07 15:18:39', NULL);
INSERT INTO `tb_log_activity` VALUES (117, 'admins0101', 'Admin Super', '182.253.8.51', 'Membuat Berita Acara Penawaran', '2021-01-07 15:21:16', NULL);
INSERT INTO `tb_log_activity` VALUES (118, 'faisal7342', 'Faisal Maulana', '182.253.8.51', 'Menandatangani Berita Acara Penawaran', '2021-01-07 15:21:56', NULL);
INSERT INTO `tb_log_activity` VALUES (119, 'admins0101', 'Admin Super', '182.253.8.51', 'Menandatangani Berita Acara Penawaran', '2021-01-07 15:22:20', NULL);
INSERT INTO `tb_log_activity` VALUES (120, 'admins0101', 'Admin Super', '182.253.8.51', 'Menandatangani Berita Acara Penawaran', '2021-01-07 15:23:15', NULL);
INSERT INTO `tb_log_activity` VALUES (121, 'uul2183', 'Uul Maulana', '182.253.8.51', 'Berhasil Login', '2021-01-07 15:24:44', NULL);
INSERT INTO `tb_log_activity` VALUES (122, 'admins0101', 'Admin Super', '117.102.65.29', 'Memperbarui baru role akses R003', '2021-01-07 15:26:50', NULL);
INSERT INTO `tb_log_activity` VALUES (123, 'uul2183', 'Uul Maulana', '182.253.8.51', 'Menandatangani Berita Acara Penawaran', '2021-01-07 15:27:38', NULL);
INSERT INTO `tb_log_activity` VALUES (124, 'risky3745', 'Risky Maul', '182.253.8.51', 'Berhasil Login', '2021-01-07 15:28:15', NULL);
INSERT INTO `tb_log_activity` VALUES (125, 'risky3745', 'Risky Maul', '117.102.65.29', 'Menandatangani Berita Acara Penawaran', '2021-01-07 15:29:36', NULL);
INSERT INTO `tb_log_activity` VALUES (126, 'faisal7342', 'Faisal Maulana', '117.102.65.29', 'Berhasil Login', '2021-01-07 15:30:08', NULL);
INSERT INTO `tb_log_activity` VALUES (127, 'admins0101', 'Admin Super', '182.253.8.51', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-01-07 jam 15:31 dan berakhir pada 2021-01-07 jam 16:00', '2021-01-07 15:31:10', NULL);
INSERT INTO `tb_log_activity` VALUES (128, 'admins0101', 'Admin Super', '182.253.8.51', 'Membuat Berita Acara Negosiasi', '2021-01-07 15:34:37', NULL);
INSERT INTO `tb_log_activity` VALUES (131, 'admins0101', 'Admin Super', '182.253.8.51', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-01-07 jam 15:30 dan berakhir pada 2021-01-07 jam 16:00', '2021-01-07 15:37:27', NULL);
INSERT INTO `tb_log_activity` VALUES (132, 'admins0101', 'Admin Super', '117.102.65.29', 'Menetapkan Pemenang', '2021-01-07 15:42:58', NULL);
INSERT INTO `tb_log_activity` VALUES (133, 'admins0101', 'Admin Super', '182.253.8.51', 'Menandatangani Penetapan Pemenang', '2021-01-07 15:43:35', NULL);
INSERT INTO `tb_log_activity` VALUES (134, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-12 10:05:42', NULL);
INSERT INTO `tb_log_activity` VALUES (135, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-13 10:19:44', NULL);
INSERT INTO `tb_log_activity` VALUES (136, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Pekerjaan', '2021-01-13 10:20:11', NULL);
INSERT INTO `tb_log_activity` VALUES (137, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-14 10:12:43', NULL);
INSERT INTO `tb_log_activity` VALUES (138, 'admins0101', 'Admin Super', '36.67.157.65', 'Approve Vendor', '2021-01-14 10:31:54', NULL);
INSERT INTO `tb_log_activity` VALUES (139, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Berhasil Login', '2021-01-14 10:39:42', NULL);
INSERT INTO `tb_log_activity` VALUES (140, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Pekerjaan Pembuatan Jembatan', '2021-01-14 10:42:21', NULL);
INSERT INTO `tb_log_activity` VALUES (141, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Approve Pekerjaan', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_log_activity` VALUES (142, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-14 jam 11:00', '2021-01-14 10:47:02', NULL);
INSERT INTO `tb_log_activity` VALUES (143, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Menambahkan Dokumen Kualifikasi Pekerjaan Pakta Integritas', '2021-01-14 10:50:11', NULL);
INSERT INTO `tb_log_activity` VALUES (144, 'bapak9915', 'Bapak ABC', '182.253.8.51', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-01-14 10:50:13', NULL);
INSERT INTO `tb_log_activity` VALUES (145, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-01-14 10:51:42', NULL);
INSERT INTO `tb_log_activity` VALUES (146, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-01-14 jam 10:00 dan berakhir pada 2021-01-14 jam 11:00', '2021-01-14 10:54:35', NULL);
INSERT INTO `tb_log_activity` VALUES (147, 'bapak9915', 'Bapak ABC', '182.253.8.51', 'Memberi Pertanyaan', '2021-01-14 10:56:17', NULL);
INSERT INTO `tb_log_activity` VALUES (148, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Memberi Pertanyaan', '2021-01-14 10:56:47', NULL);
INSERT INTO `tb_log_activity` VALUES (149, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 10:57:27', NULL);
INSERT INTO `tb_log_activity` VALUES (150, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 10:57:35', NULL);
INSERT INTO `tb_log_activity` VALUES (151, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 10:58:05', NULL);
INSERT INTO `tb_log_activity` VALUES (152, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 10:58:34', NULL);
INSERT INTO `tb_log_activity` VALUES (153, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-01-14 11:04:11', NULL);
INSERT INTO `tb_log_activity` VALUES (154, 'bapak9915', 'Bapak ABC', '182.253.8.51', 'Menandatangani Berita Acara Aanwijzing', '2021-01-14 11:04:56', NULL);
INSERT INTO `tb_log_activity` VALUES (155, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Aanwijzing', '2021-01-14 11:05:33', NULL);
INSERT INTO `tb_log_activity` VALUES (156, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', '2021-01-14 11:05:58', NULL);
INSERT INTO `tb_log_activity` VALUES (157, 'bapak9915', 'Bapak ABC', '182.253.8.51', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-01-14 11:16:12', NULL);
INSERT INTO `tb_log_activity` VALUES (158, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', '2021-01-14 11:17:52', NULL);
INSERT INTO `tb_log_activity` VALUES (159, 'admins0101', 'admins0101', '36.67.157.65', 'Menambahkan Evaluasi Penawaran', '2021-01-14 11:19:17', NULL);
INSERT INTO `tb_log_activity` VALUES (160, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Penawaran', '2021-01-14 11:20:31', NULL);
INSERT INTO `tb_log_activity` VALUES (161, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Menandatangani Berita Acara Penawaran', '2021-01-14 11:21:36', NULL);
INSERT INTO `tb_log_activity` VALUES (162, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-01-14 11:22:33', NULL);
INSERT INTO `tb_log_activity` VALUES (163, 'uul2183', 'Uul Maulana', '36.67.157.65', 'Berhasil Login', '2021-01-14 11:23:51', NULL);
INSERT INTO `tb_log_activity` VALUES (164, 'risky3745', 'Risky Maul', '182.253.8.51', 'Berhasil Login', '2021-01-14 11:24:08', NULL);
INSERT INTO `tb_log_activity` VALUES (165, 'risky3745', 'Risky Maul', '117.102.65.29', 'Menandatangani Berita Acara Penawaran', '2021-01-14 11:24:57', NULL);
INSERT INTO `tb_log_activity` VALUES (166, 'uul2183', 'Uul Maulana', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-01-14 11:25:15', NULL);
INSERT INTO `tb_log_activity` VALUES (167, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', '2021-01-14 11:26:05', NULL);
INSERT INTO `tb_log_activity` VALUES (168, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-01-14 11:26:41', NULL);
INSERT INTO `tb_log_activity` VALUES (169, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Menandatangani Berita Acara Negosiasi', '2021-01-14 11:27:09', NULL);
INSERT INTO `tb_log_activity` VALUES (170, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-01-14 11:27:28', NULL);
INSERT INTO `tb_log_activity` VALUES (171, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', '2021-01-14 11:27:53', NULL);
INSERT INTO `tb_log_activity` VALUES (172, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-01-14 11:28:42', NULL);
INSERT INTO `tb_log_activity` VALUES (173, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-01-14 11:28:52', NULL);
INSERT INTO `tb_log_activity` VALUES (174, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan test 2', '2021-01-14 11:40:57', NULL);
INSERT INTO `tb_log_activity` VALUES (175, 'bapak9915', 'Bapak ABC', '182.253.8.51', 'Approve Pekerjaan', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_log_activity` VALUES (176, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-14 jam 12:00', '2021-01-14 11:43:16', NULL);
INSERT INTO `tb_log_activity` VALUES (177, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-01-14 11:43:55', NULL);
INSERT INTO `tb_log_activity` VALUES (178, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-01-14 jam 10:00 dan berakhir pada 2021-01-14 jam 15:00', '2021-01-14 11:44:18', NULL);
INSERT INTO `tb_log_activity` VALUES (179, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Memberi Pertanyaan', '2021-01-14 11:44:31', NULL);
INSERT INTO `tb_log_activity` VALUES (180, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Memberi Pertanyaan', '2021-01-14 11:44:41', NULL);
INSERT INTO `tb_log_activity` VALUES (181, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Memberi Pertanyaan', '2021-01-14 11:44:48', NULL);
INSERT INTO `tb_log_activity` VALUES (182, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 11:54:14', NULL);
INSERT INTO `tb_log_activity` VALUES (183, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 11:54:21', NULL);
INSERT INTO `tb_log_activity` VALUES (184, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-01-14 11:54:30', NULL);
INSERT INTO `tb_log_activity` VALUES (185, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-18 09:24:02', NULL);
INSERT INTO `tb_log_activity` VALUES (186, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-20 09:24:06', NULL);
INSERT INTO `tb_log_activity` VALUES (187, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Dr. Prasetyo Aribowo, S.H., M.Soc, Sc.', '2021-01-20 09:27:04', NULL);
INSERT INTO `tb_log_activity` VALUES (188, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Ir. Anton Santosa, M.T.', '2021-01-20 09:27:44', NULL);
INSERT INTO `tb_log_activity` VALUES (189, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Ir. Anton Santosa, M.T.', '2021-01-20 09:28:13', NULL);
INSERT INTO `tb_log_activity` VALUES (190, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Ir. Rachmadi Nugroho, M.T.', '2021-01-20 09:33:24', NULL);
INSERT INTO `tb_log_activity` VALUES (191, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Joseph Sondang Tobing, M.Sc.', '2021-01-20 09:34:16', NULL);
INSERT INTO `tb_log_activity` VALUES (192, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Ahmad Fauzie Nur, S.E., M.Business.', '2021-01-20 09:35:06', NULL);
INSERT INTO `tb_log_activity` VALUES (193, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Pratiknya, S.H., M.H.', '2021-01-20 09:36:23', NULL);
INSERT INTO `tb_log_activity` VALUES (194, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui 1 Are We Pratiknya, S.H., M.H.', '2021-01-20 09:36:39', NULL);
INSERT INTO `tb_log_activity` VALUES (195, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Agus Santoso, S.E.', '2021-01-20 09:37:35', NULL);
INSERT INTO `tb_log_activity` VALUES (196, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Bambang Setiyawan, S.T.', '2021-01-20 09:38:24', NULL);
INSERT INTO `tb_log_activity` VALUES (197, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Kuniyanti Hadiatmaja, S.E., Akt.', '2021-01-20 09:39:10', NULL);
INSERT INTO `tb_log_activity` VALUES (198, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We Danang Agung Indarto, S.E.', '2021-01-20 09:39:59', NULL);
INSERT INTO `tb_log_activity` VALUES (199, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We M. Nicho Setyawan, S.T.', '2021-01-20 09:41:03', NULL);
INSERT INTO `tb_log_activity` VALUES (200, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambah 1 Are We San Agitato Ganda P, S.T.', '2021-01-20 09:42:53', NULL);
INSERT INTO `tb_log_activity` VALUES (201, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Pekerjaan Test', '2021-01-20 10:46:41', NULL);
INSERT INTO `tb_log_activity` VALUES (202, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-01-26 09:25:29', NULL);
INSERT INTO `tb_log_activity` VALUES (203, 'admins0101', 'Admin Super', '182.253.8.58', 'Berhasil Login', '2021-02-10 11:16:07', NULL);
INSERT INTO `tb_log_activity` VALUES (204, 'admins0101', 'Admin Super', '117.102.65.29', 'Berhasil Login', '2021-02-10 14:19:15', NULL);
INSERT INTO `tb_log_activity` VALUES (205, 'admins0101', 'Admin Super', '117.102.65.29', 'Berhasil Login', '2021-02-10 14:57:20', NULL);
INSERT INTO `tb_log_activity` VALUES (206, 'admins0101', 'Admin Super', '182.253.8.58', 'Berhasil Login', '2021-02-10 15:06:37', NULL);
INSERT INTO `tb_log_activity` VALUES (207, 'admins0101', 'Admin Super', '114.124.130.30', 'Berhasil Login', '2021-02-11 09:21:11', NULL);
INSERT INTO `tb_log_activity` VALUES (208, 'admins0101', 'Admin Super', '117.102.65.29', 'Berhasil Login', '2021-02-11 16:26:13', NULL);
INSERT INTO `tb_log_activity` VALUES (209, 'admins0101', 'Admin Super', '114.124.130.53', 'Berhasil Login', '2021-02-15 09:47:02', NULL);
INSERT INTO `tb_log_activity` VALUES (210, 'admins0101', 'Admin Super', '114.124.130.6', 'Berhasil Login', '2021-02-16 15:07:52', NULL);
INSERT INTO `tb_log_activity` VALUES (211, 'admins0101', 'Admin Super', '117.102.65.29', 'Berhasil Login', '2021-02-22 09:57:01', NULL);
INSERT INTO `tb_log_activity` VALUES (212, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-02-24 14:45:57', NULL);
INSERT INTO `tb_log_activity` VALUES (213, 'admins0101', 'Admin Super', '114.124.130.22', 'Berhasil Login', '2021-02-25 10:40:02', NULL);
INSERT INTO `tb_log_activity` VALUES (214, 'admins0101', 'Admin Super', '182.253.8.58', 'Berhasil Login', '2021-02-25 16:09:44', NULL);
INSERT INTO `tb_log_activity` VALUES (215, 'admins0101', 'Admin Super', '114.124.130.21', 'Gagal Login. Password atau Username, Salah !', '2021-02-26 14:45:33', NULL);
INSERT INTO `tb_log_activity` VALUES (216, 'admins0101', 'Admin Super', '114.124.130.21', 'Berhasil Login', '2021-02-26 14:45:46', NULL);
INSERT INTO `tb_log_activity` VALUES (217, 'admins0101', 'Admin Super', '114.124.130.21', 'Memberikan rating 4 pada Perusahan ABC', '2021-02-26 15:17:39', NULL);
INSERT INTO `tb_log_activity` VALUES (218, 'admins0101', 'Admin Super', '114.124.130.21', 'Memberikan rating 4 pada PT. Oke', '2021-02-26 15:17:53', NULL);
INSERT INTO `tb_log_activity` VALUES (219, 'admins0101', 'Admin Super', '114.124.130.21', 'Memberikan rating 3 pada Perusahan ABC', '2021-02-26 15:18:05', NULL);
INSERT INTO `tb_log_activity` VALUES (220, 'admins0101', 'Admin Super', '182.253.38.11', 'Berhasil Login', '2021-02-26 15:34:28', NULL);
INSERT INTO `tb_log_activity` VALUES (221, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-01 09:20:36', NULL);
INSERT INTO `tb_log_activity` VALUES (222, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui baru role akses R000', '2021-03-01 09:52:49', NULL);
INSERT INTO `tb_log_activity` VALUES (223, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-01 15:07:12', NULL);
INSERT INTO `tb_log_activity` VALUES (224, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Person Achmad Maul Calon DIRUT', '2021-03-01 15:08:24', NULL);
INSERT INTO `tb_log_activity` VALUES (225, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Person Achmad Maul Calon DIRUT', '2021-03-01 15:08:42', NULL);
INSERT INTO `tb_log_activity` VALUES (226, 'admins0101', 'Admin Super', '36.67.157.65', 'Memperbarui Person Achmad Maul Calon DIRUT', '2021-03-01 15:09:03', NULL);
INSERT INTO `tb_log_activity` VALUES (227, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-02 10:47:05', NULL);
INSERT INTO `tb_log_activity` VALUES (228, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-02 14:21:08', NULL);
INSERT INTO `tb_log_activity` VALUES (229, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-03 09:48:30', NULL);
INSERT INTO `tb_log_activity` VALUES (230, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-03 13:17:28', NULL);
INSERT INTO `tb_log_activity` VALUES (231, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-04 09:26:05', NULL);
INSERT INTO `tb_log_activity` VALUES (232, 'admins0101', 'Admin Super', '116.254.117.222', 'Berhasil Login', '2021-03-04 14:58:54', NULL);
INSERT INTO `tb_log_activity` VALUES (233, 'admins0101', 'Admin Super', '116.254.117.222', 'Membuat Berita Acara Aanwijzing', '2021-03-04 15:29:50', NULL);
INSERT INTO `tb_log_activity` VALUES (234, 'admins0101', 'Admin Super', '114.124.230.172', 'Berhasil Login', '2021-03-08 09:44:00', NULL);
INSERT INTO `tb_log_activity` VALUES (235, 'admins0101', 'Admin Super', '182.253.8.52', 'Berhasil Login', '2021-03-08 13:25:32', NULL);
INSERT INTO `tb_log_activity` VALUES (236, 'admins0101', 'Admin Super', '117.102.65.29', 'Menandatangani Berita Acara Aanwijzing', '2021-03-08 14:13:42', NULL);
INSERT INTO `tb_log_activity` VALUES (237, 'admins0101', 'Admin Super', '117.102.65.29', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-08 jam 12:00 dan berakhir pada 2021-03-08 jam 16:00', '2021-03-08 14:14:20', NULL);
INSERT INTO `tb_log_activity` VALUES (238, 'bapak9915', 'Bapak ABC', '117.102.65.29', 'Gagal Login. Password atau Username, Salah !', '2021-03-08 14:19:50', NULL);
INSERT INTO `tb_log_activity` VALUES (239, 'bapak9915', 'Bapak ABC', '182.253.8.52', 'Berhasil Login', '2021-03-08 14:20:02', NULL);
INSERT INTO `tb_log_activity` VALUES (240, 'bapak9915', 'Bapak ABC', '182.253.8.52', 'Menambah Penawaran Surat Penawaran', '2021-03-08 14:42:42', NULL);
INSERT INTO `tb_log_activity` VALUES (241, 'bapak9915', 'Bapak ABC', '182.253.8.52', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-03-08 14:44:18', NULL);
INSERT INTO `tb_log_activity` VALUES (242, 'admins0101', 'Admin Super', '117.102.65.29', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-03-08 jam 14:58 dan berakhir pada 2021-03-08 jam 16:00', '2021-03-08 14:58:12', NULL);
INSERT INTO `tb_log_activity` VALUES (243, 'admins0101', 'admins0101', '117.102.65.29', 'Menambahkan Evaluasi Penawaran', '2021-03-08 15:00:22', NULL);
INSERT INTO `tb_log_activity` VALUES (244, 'admins0101', 'Admin Super', '116.254.117.222', 'Membuat Berita Acara Penawaran', '2021-03-08 15:11:31', NULL);
INSERT INTO `tb_log_activity` VALUES (245, 'admins0101', 'Admin Super', '116.254.117.222', 'Menandatangani Berita Acara Penawaran', '2021-03-08 16:18:43', NULL);
INSERT INTO `tb_log_activity` VALUES (246, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-10 09:27:25', NULL);
INSERT INTO `tb_log_activity` VALUES (247, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 09:39 dan berakhir pada 2021-03-10 jam 15:45', '2021-03-10 09:39:18', NULL);
INSERT INTO `tb_log_activity` VALUES (248, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-03-10 10:56:30', NULL);
INSERT INTO `tb_log_activity` VALUES (249, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-03-10 11:05:58', NULL);
INSERT INTO `tb_log_activity` VALUES (250, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-03-10 jam 11:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 11:06:15', NULL);
INSERT INTO `tb_log_activity` VALUES (251, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-03-10 11:18:51', NULL);
INSERT INTO `tb_log_activity` VALUES (252, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 11:23:29', NULL);
INSERT INTO `tb_log_activity` VALUES (253, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan test 2', '2021-03-10 11:26:07', NULL);
INSERT INTO `tb_log_activity` VALUES (254, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-10 11:26:40', NULL);
INSERT INTO `tb_log_activity` VALUES (255, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Approve Pekerjaan', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_log_activity` VALUES (256, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:27:22', NULL);
INSERT INTO `tb_log_activity` VALUES (257, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Pakta Integritas', '2021-03-10 11:27:55', NULL);
INSERT INTO `tb_log_activity` VALUES (258, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-03-10 11:27:56', NULL);
INSERT INTO `tb_log_activity` VALUES (259, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-03-10 11:28:18', NULL);
INSERT INTO `tb_log_activity` VALUES (260, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:28:36', NULL);
INSERT INTO `tb_log_activity` VALUES (261, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memberi Pertanyaan', '2021-03-10 11:28:50', NULL);
INSERT INTO `tb_log_activity` VALUES (262, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-03-10 11:29:06', NULL);
INSERT INTO `tb_log_activity` VALUES (263, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-03-10 11:32:34', NULL);
INSERT INTO `tb_log_activity` VALUES (264, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Aanwijzing', '2021-03-10 11:33:53', NULL);
INSERT INTO `tb_log_activity` VALUES (265, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:35:05', NULL);
INSERT INTO `tb_log_activity` VALUES (266, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:38:48', NULL);
INSERT INTO `tb_log_activity` VALUES (267, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-10 jam 11:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:40:40', NULL);
INSERT INTO `tb_log_activity` VALUES (268, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-03-10 11:42:29', NULL);
INSERT INTO `tb_log_activity` VALUES (269, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambah Penawaran Surat Penawaran', '2021-03-10 11:42:34', NULL);
INSERT INTO `tb_log_activity` VALUES (270, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:43:12', NULL);
INSERT INTO `tb_log_activity` VALUES (271, 'admins0101', 'admins0101', '36.67.157.65', 'Menambahkan Evaluasi Penawaran', '2021-03-10 11:44:54', NULL);
INSERT INTO `tb_log_activity` VALUES (272, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Penawaran', '2021-03-10 11:46:55', NULL);
INSERT INTO `tb_log_activity` VALUES (273, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-03-10 11:48:16', NULL);
INSERT INTO `tb_log_activity` VALUES (274, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:49:00', NULL);
INSERT INTO `tb_log_activity` VALUES (275, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-03-10 11:49:24', NULL);
INSERT INTO `tb_log_activity` VALUES (276, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-03-10 11:49:32', NULL);
INSERT INTO `tb_log_activity` VALUES (277, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:49:46', NULL);
INSERT INTO `tb_log_activity` VALUES (278, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-03-10 11:50:31', NULL);
INSERT INTO `tb_log_activity` VALUES (279, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 11:52:31', NULL);
INSERT INTO `tb_log_activity` VALUES (280, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Test 3', '2021-03-10 11:54:06', NULL);
INSERT INTO `tb_log_activity` VALUES (281, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Approve Pekerjaan', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_log_activity` VALUES (282, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', '2021-03-10 11:54:36', NULL);
INSERT INTO `tb_log_activity` VALUES (283, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-03-10 11:57:58', NULL);
INSERT INTO `tb_log_activity` VALUES (284, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-03-10 13:21:05', NULL);
INSERT INTO `tb_log_activity` VALUES (285, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 13:21:25', NULL);
INSERT INTO `tb_log_activity` VALUES (286, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memberi Pertanyaan', '2021-03-10 13:21:43', NULL);
INSERT INTO `tb_log_activity` VALUES (287, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-03-10 13:21:55', NULL);
INSERT INTO `tb_log_activity` VALUES (288, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-03-10 13:22:46', NULL);
INSERT INTO `tb_log_activity` VALUES (289, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Aanwijzing', '2021-03-10 13:23:02', NULL);
INSERT INTO `tb_log_activity` VALUES (290, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 13:23:20', NULL);
INSERT INTO `tb_log_activity` VALUES (291, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-10 13:27:44', NULL);
INSERT INTO `tb_log_activity` VALUES (292, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-03-10 13:28:41', NULL);
INSERT INTO `tb_log_activity` VALUES (293, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 13:32:19', NULL);
INSERT INTO `tb_log_activity` VALUES (294, 'admins0101', 'admins0101', '36.67.157.65', 'Menambahkan Evaluasi Penawaran', '2021-03-10 13:35:33', NULL);
INSERT INTO `tb_log_activity` VALUES (295, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Penawaran', '2021-03-10 13:36:54', NULL);
INSERT INTO `tb_log_activity` VALUES (296, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-03-10 13:39:25', NULL);
INSERT INTO `tb_log_activity` VALUES (297, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 13:39:51', NULL);
INSERT INTO `tb_log_activity` VALUES (298, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-03-10 13:44:24', NULL);
INSERT INTO `tb_log_activity` VALUES (299, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-03-10 13:54:09', NULL);
INSERT INTO `tb_log_activity` VALUES (300, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', '2021-03-10 13:54:28', NULL);
INSERT INTO `tb_log_activity` VALUES (301, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-03-10 13:55:37', NULL);
INSERT INTO `tb_log_activity` VALUES (302, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 14:00:35', NULL);
INSERT INTO `tb_log_activity` VALUES (303, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 14:00:56', NULL);
INSERT INTO `tb_log_activity` VALUES (304, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 14:00:59', NULL);
INSERT INTO `tb_log_activity` VALUES (305, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Test Terbuka 1', '2021-03-10 15:15:33', NULL);
INSERT INTO `tb_log_activity` VALUES (306, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Pekerjaan', '2021-03-10 15:15:33', NULL);
INSERT INTO `tb_log_activity` VALUES (307, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Approve Pekerjaan', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_log_activity` VALUES (308, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', '2021-03-10 15:19:55', NULL);
INSERT INTO `tb_log_activity` VALUES (309, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-03-10 15:21:59', NULL);
INSERT INTO `tb_log_activity` VALUES (310, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-03-10 15:22:25', NULL);
INSERT INTO `tb_log_activity` VALUES (311, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', '2021-03-10 15:22:46', NULL);
INSERT INTO `tb_log_activity` VALUES (312, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-03-10 15:28:49', NULL);
INSERT INTO `tb_log_activity` VALUES (313, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Aanwijzing', '2021-03-10 15:29:12', NULL);
INSERT INTO `tb_log_activity` VALUES (314, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-10 jam 15:09 dan berakhir pada 2021-03-10 jam 18:00', '2021-03-10 15:29:40', NULL);
INSERT INTO `tb_log_activity` VALUES (315, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-03-10 15:34:40', NULL);
INSERT INTO `tb_log_activity` VALUES (316, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', '2021-03-10 15:35:13', NULL);
INSERT INTO `tb_log_activity` VALUES (317, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Evaluasi Penawaran', '2021-03-10 15:35:39', NULL);
INSERT INTO `tb_log_activity` VALUES (318, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Penawaran', '2021-03-10 15:36:11', NULL);
INSERT INTO `tb_log_activity` VALUES (319, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-03-10 15:37:28', NULL);
INSERT INTO `tb_log_activity` VALUES (320, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', '2021-03-10 15:37:52', NULL);
INSERT INTO `tb_log_activity` VALUES (321, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-03-10 15:43:03', NULL);
INSERT INTO `tb_log_activity` VALUES (322, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-03-10 15:43:12', NULL);
INSERT INTO `tb_log_activity` VALUES (323, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', '2021-03-10 15:43:32', NULL);
INSERT INTO `tb_log_activity` VALUES (324, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-03-10 15:48:46', NULL);
INSERT INTO `tb_log_activity` VALUES (325, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-10 15:48:56', NULL);
INSERT INTO `tb_log_activity` VALUES (326, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Approve Pekerjaan', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_log_activity` VALUES (327, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Berita Test Berita 2', '2021-03-10 16:07:42', NULL);
INSERT INTO `tb_log_activity` VALUES (328, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-12 09:36:53', NULL);
INSERT INTO `tb_log_activity` VALUES (329, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-12 09:39:14', NULL);
INSERT INTO `tb_log_activity` VALUES (330, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-12 jam 16:00', '2021-03-12 09:40:36', NULL);
INSERT INTO `tb_log_activity` VALUES (331, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambahkan Dokumen Kualifikasi Pekerjaan Surat Pernyataan Minat', '2021-03-12 09:59:25', NULL);
INSERT INTO `tb_log_activity` VALUES (332, 'admins0101', 'Admin Super', '36.67.157.65', 'Menerima Dokumen Kualifikasi Pekerjaan', '2021-03-12 10:00:47', NULL);
INSERT INTO `tb_log_activity` VALUES (333, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pelaksanaan Aanwijzing dimulai pada 2021-03-12 jam 09:00 dan berakhir pada 2021-03-12 jam 14:00', '2021-03-12 10:01:09', NULL);
INSERT INTO `tb_log_activity` VALUES (334, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memberi Pertanyaan', '2021-03-12 10:03:56', NULL);
INSERT INTO `tb_log_activity` VALUES (335, 'admins0101', 'Admin Super', '36.67.157.65', 'Memberi Jawaban', '2021-03-12 10:04:09', NULL);
INSERT INTO `tb_log_activity` VALUES (336, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Aanwijzing', '2021-03-12 10:05:34', NULL);
INSERT INTO `tb_log_activity` VALUES (337, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Aanwijzing', '2021-03-12 10:07:26', NULL);
INSERT INTO `tb_log_activity` VALUES (338, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Pemasukan Penawaran dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', '2021-03-12 10:14:57', NULL);
INSERT INTO `tb_log_activity` VALUES (339, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-12 10:18:59', NULL);
INSERT INTO `tb_log_activity` VALUES (340, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Menambah Penawaran Foto copy atau asli surat undangan', '2021-03-12 10:19:39', NULL);
INSERT INTO `tb_log_activity` VALUES (341, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Evaluasi Penawaran dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', '2021-03-12 10:23:48', NULL);
INSERT INTO `tb_log_activity` VALUES (342, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Evaluasi Penawaran', '2021-03-12 10:24:10', NULL);
INSERT INTO `tb_log_activity` VALUES (343, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Penawaran', '2021-03-12 10:36:16', NULL);
INSERT INTO `tb_log_activity` VALUES (344, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Penawaran', '2021-03-12 10:36:38', NULL);
INSERT INTO `tb_log_activity` VALUES (345, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Klarifikasi & Negosiasi dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', '2021-03-12 10:36:55', NULL);
INSERT INTO `tb_log_activity` VALUES (346, 'admins0101', 'Admin Super', '36.67.157.65', 'Membuat Berita Acara Negosiasi', '2021-03-12 10:37:25', NULL);
INSERT INTO `tb_log_activity` VALUES (347, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Berita Acara Negosiasi', '2021-03-12 10:37:35', NULL);
INSERT INTO `tb_log_activity` VALUES (348, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Jadwal Pekerjaan Penetapan Pemenang dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', '2021-03-12 10:37:49', NULL);
INSERT INTO `tb_log_activity` VALUES (349, 'admins0101', 'Admin Super', '36.67.157.65', 'Menetapkan Pemenang', '2021-03-12 10:38:35', NULL);
INSERT INTO `tb_log_activity` VALUES (350, 'admins0101', 'Admin Super', '36.67.157.65', 'Menandatangani Penetapan Pemenang', '2021-03-12 10:38:47', NULL);
INSERT INTO `tb_log_activity` VALUES (351, 'admins0101', 'Admin Super', '36.67.157.65', 'Mengupload Dokumen Kontrak', '2021-03-12 11:17:34', NULL);
INSERT INTO `tb_log_activity` VALUES (352, 'admins0101', 'Admin Super', '36.67.157.65', 'Mengupload Dokumen Kontrak', '2021-03-12 11:46:31', NULL);
INSERT INTO `tb_log_activity` VALUES (353, 'admins0101', 'Admin Super', '36.67.157.65', 'Mengupload Dokumen Kontrak', '2021-03-12 11:46:32', NULL);
INSERT INTO `tb_log_activity` VALUES (354, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-12 13:07:24', NULL);
INSERT INTO `tb_log_activity` VALUES (355, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Memperbarui Vendor', '2021-03-12 13:09:11', NULL);
INSERT INTO `tb_log_activity` VALUES (356, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 5 pada PT. Uul Mencari S1', '2021-03-12 14:10:52', NULL);
INSERT INTO `tb_log_activity` VALUES (357, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 1 pada PT. Oke', '2021-03-12 14:14:11', NULL);
INSERT INTO `tb_log_activity` VALUES (358, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 2 pada Perusahan ABC', '2021-03-12 14:14:20', NULL);
INSERT INTO `tb_log_activity` VALUES (359, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 1 pada PT Pencari Janda Tbk', '2021-03-12 14:14:28', NULL);
INSERT INTO `tb_log_activity` VALUES (360, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 4 pada Perusahan ABC', '2021-03-12 14:14:36', NULL);
INSERT INTO `tb_log_activity` VALUES (361, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 2 pada Perusahan ABC', '2021-03-12 14:14:41', NULL);
INSERT INTO `tb_log_activity` VALUES (362, 'admins0101', 'Admin Super', '114.124.228.99', 'Memberikan rating 0 pada PT Pencari Janda Tbk', '2021-03-12 14:15:21', NULL);
INSERT INTO `tb_log_activity` VALUES (363, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 14:48:11', NULL);
INSERT INTO `tb_log_activity` VALUES (364, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 14:50:52', NULL);
INSERT INTO `tb_log_activity` VALUES (365, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 14:53:36', NULL);
INSERT INTO `tb_log_activity` VALUES (366, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 14:57:33', NULL);
INSERT INTO `tb_log_activity` VALUES (367, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 15:00:08', NULL);
INSERT INTO `tb_log_activity` VALUES (368, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 15:01:10', NULL);
INSERT INTO `tb_log_activity` VALUES (369, 'admins0101', 'Admin Super', '114.124.228.99', 'Memperbarui content main banner', '2021-03-12 15:02:09', NULL);
INSERT INTO `tb_log_activity` VALUES (370, 'admins0101', 'Admin Super', '114.124.228.99', 'Menghapus Banner', '2021-03-12 15:16:11', NULL);
INSERT INTO `tb_log_activity` VALUES (371, 'admins0101', 'Admin Super', '114.124.228.99', 'Menambahkan Banner', '2021-03-12 15:18:53', NULL);
INSERT INTO `tb_log_activity` VALUES (372, 'admins0101', 'Admin Super', '114.124.228.99', 'Menambahkan Banner', '2021-03-12 15:19:34', NULL);
INSERT INTO `tb_log_activity` VALUES (373, 'admins0101', 'Admin Super', '114.124.228.99', 'Menambahkan Banner', '2021-03-12 15:20:14', NULL);
INSERT INTO `tb_log_activity` VALUES (374, 'admins0101', 'Admin Super', '114.124.228.99', 'Menambahkan Banner', '2021-03-12 15:23:54', NULL);
INSERT INTO `tb_log_activity` VALUES (375, 'admins0101', 'Admin Super', '114.124.228.99', 'Menghapus Banner', '2021-03-12 15:25:36', NULL);
INSERT INTO `tb_log_activity` VALUES (376, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-16 09:28:00', NULL);
INSERT INTO `tb_log_activity` VALUES (377, 'admins0101', 'Admin Super', '36.67.157.65', 'Menghapus Berita', '2021-03-16 09:28:41', NULL);
INSERT INTO `tb_log_activity` VALUES (378, 'admins0101', 'Admin Super', '36.67.157.65', 'Mengirim Email pada Vendor', '2021-03-16 11:28:27', NULL);
INSERT INTO `tb_log_activity` VALUES (379, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-17 16:18:01', NULL);
INSERT INTO `tb_log_activity` VALUES (380, 'admins0101', 'Admin Super', '114.124.130.44', 'Berhasil Login', '2021-03-18 09:03:15', NULL);
INSERT INTO `tb_log_activity` VALUES (381, 'admins0101', 'Admin Super', '114.124.130.44', 'Berhasil Login', '2021-03-18 11:06:53', NULL);
INSERT INTO `tb_log_activity` VALUES (382, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Pekerjaan Test XX', '2021-03-18 14:09:44', NULL);
INSERT INTO `tb_log_activity` VALUES (383, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-18 14:10:57', NULL);
INSERT INTO `tb_log_activity` VALUES (384, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Berita Test 2', '2021-03-18 14:59:19', NULL);
INSERT INTO `tb_log_activity` VALUES (385, 'admins0101', 'Admin Super', '36.67.157.65', 'Menambahkan Berita Test 3', '2021-03-18 14:59:45', NULL);
INSERT INTO `tb_log_activity` VALUES (386, 'admins0101', 'Admin Super', '182.2.40.247', 'Berhasil Login', '2021-03-18 15:47:18', NULL);
INSERT INTO `tb_log_activity` VALUES (387, 'faisal7342', 'Faisal Maulana', '36.67.157.65', 'Berhasil Login', '2021-03-18 16:13:55', NULL);
INSERT INTO `tb_log_activity` VALUES (388, 'admins0101', 'Admin Super', '125.163.224.133', 'Berhasil Login', '2021-03-18 17:48:43', NULL);
INSERT INTO `tb_log_activity` VALUES (389, 'admins0101', 'Admin Super', '114.124.240.126', 'Berhasil Login', '2021-03-22 09:17:26', NULL);
INSERT INTO `tb_log_activity` VALUES (390, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-22 14:56:11', NULL);
INSERT INTO `tb_log_activity` VALUES (391, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-23 11:48:26', NULL);
INSERT INTO `tb_log_activity` VALUES (392, 'admins0101', 'Admin Super', '36.67.157.65', 'Berhasil Login', '2021-03-23 14:49:57', NULL);
INSERT INTO `tb_log_activity` VALUES (393, 'admins0101', 'Admin Super', '182.2.39.212', 'Berhasil Login', '2021-03-24 10:08:50', NULL);

-- ----------------------------
-- Table structure for tb_mail_history
-- ----------------------------
DROP TABLE IF EXISTS `tb_mail_history`;
CREATE TABLE `tb_mail_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_kop` int NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `body_mail` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `send` datetime(0) NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_mail_history
-- ----------------------------
INSERT INTO `tb_mail_history` VALUES (64, 9, 'fak', '<p>agung sejagad</p>', '2020-08-25 10:59:08', 1, '2020-08-25 10:59:27', NULL);
INSERT INTO `tb_mail_history` VALUES (65, 10, 'fak', '<p>agung sejagad</p>', '2020-08-25 10:59:27', 1, '2020-08-25 10:59:45', NULL);
INSERT INTO `tb_mail_history` VALUES (66, 11, 'fak', '<p>agung sejagad</p>', '2020-08-25 10:59:46', 1, '2020-08-25 11:00:04', NULL);
INSERT INTO `tb_mail_history` VALUES (67, 12, 'fak', '<p>agung sejagad</p>', '2020-08-25 11:00:04', 1, '2020-08-25 11:00:08', NULL);
INSERT INTO `tb_mail_history` VALUES (68, 10, 'Wew', '<p>Okey Mantul</p>', '2020-08-25 11:08:49', 1, '2020-08-25 11:08:49', NULL);

-- ----------------------------
-- Table structure for tb_member_premier
-- ----------------------------
DROP TABLE IF EXISTS `tb_member_premier`;
CREATE TABLE `tb_member_premier`  (
  `id_kop` int NOT NULL AUTO_INCREMENT,
  `nama_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_komunitas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nama_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_komunitas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo_komunitas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `klaster_kerja` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `lingkup_komunitas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `provinsi_kerja` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `provinsi_kerja_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota_kerja` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota_kerja_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kop`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_member_premier
-- ----------------------------
INSERT INTO `tb_member_premier` VALUES (9, 'sdcdavdv', 'dcacas', 'Yepy Uhuy2', 'yepyuhuy3@gmail.com', '3114131', 'Pendidikan', 'ascac@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/stblsN1zHm.png', 'Pendidikan Keluarga, Homeschooling & PAUD', 'Beasiswa', 'JAWA TENGAH', '33', 'KAB. CILACAP', '33.01', 2, 'ewfffdfwfwfcwfc', '2020-06-02 06:34:17', '2020-08-26 16:18:35');
INSERT INTO `tb_member_premier` VALUES (10, 'sfasfasdf', 'safasdfsadf', 'sadfsadfsadf', 'test@gmail.com', '634653463465', 'Pendidikan', 'airbatbady@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/GORX9Gs45o.png', 'Pengembangan Guru', 'Beasiswa', 'SUMATERA BARAT', '13', 'KAB. SOLOK', '13.02', 2, 'mantul', '2020-08-06 10:30:49', '2020-08-26 16:18:19');
INSERT INTO `tb_member_premier` VALUES (11, 'dsvsavs', 'vasvasv', 'asfvadfbasfvasfv', 'test@gmail.com', '4242343', 'Pendidikan', 'test@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/Nz3VUHqpB9.png', 'Pendidikan Keluarga, Homeschooling & PAUD', 'Disabilitas', 'RIAU', '14', 'KAB. KAMPAR', '14.01', 2, 'okey', '2020-08-06 10:41:14', '2020-08-26 16:18:03');
INSERT INTO `tb_member_premier` VALUES (12, 'wewewewewe', 'wewewewewe', 'wewewewewe', 'wewe@gmail.com', '0831674647474', 'Non Pendidikan', 'wewe@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/GRP10SGjZq.png', 'Pengembangan Guru;Pendidikan Keluarga, Homeschooling & PAUD', 'Antikorupsi;Beasiswa', 'ACEH', '11', 'KAB. ACEH UTARA', '11.08', 2, 'mantul anjg', '2020-08-07 15:22:27', '2020-08-26 16:19:04');
INSERT INTO `tb_member_premier` VALUES (13, 'vfsav', 'kbkbkb', 'jkbkbkb', 'hhsh@gmail.com', '6457347367367', 'Pendidikan', 'hhsh@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/d8ITDTCtgC.png', 'Pengembangan Anak Muda', 'Homeschooling', 'SUMATERA BARAT', '13', 'KAB. TANAH DATAR', '13.04', 1, NULL, '2020-08-13 11:50:42', NULL);
INSERT INTO `tb_member_premier` VALUES (14, 'vasfvasfvas', 'vasvasvsavsv', 'savasvsavsav', 'svsavasv@gmail.com', '242323324', 'Pendidikan', 'svsavasv@gmail.com', 'D:\\smsg\\public/uploads/logo_komunitas/06kkbShbB0.png', 'Pendidikan Karakter (termasuk pendidikan lingkungan, kesehatan dan kesehatan mental)', 'Beasiswa', 'SUMATERA BARAT', '13', 'KAB. TANAH DATAR', '13.04', 1, NULL, '2020-08-14 12:11:41', NULL);

-- ----------------------------
-- Table structure for tb_member_sekunder
-- ----------------------------
DROP TABLE IF EXISTS `tb_member_sekunder`;
CREATE TABLE `tb_member_sekunder`  (
  `id_kop` int NULL DEFAULT NULL,
  `web_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `twitter_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `facebook_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instagram_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `youtube_komunitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_berdiri_kop` date NULL DEFAULT NULL,
  `jml_pengurus_kop` int NULL DEFAULT NULL,
  `jml_relawan_kop` int NULL DEFAULT NULL,
  `jml_kemitraan` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_member_sekunder
-- ----------------------------
INSERT INTO `tb_member_sekunder` VALUES (9, 'cascascasc', 'ascascasc', 'acascas', 'cascascasc', 'ascascasc', '2020-06-04', NULL, NULL, NULL, '2020-06-02 06:34:17', '2020-08-26 16:18:35');
INSERT INTO `tb_member_sekunder` VALUES (10, 'dbfdfdbfdb', NULL, NULL, NULL, NULL, '2020-08-15', NULL, NULL, NULL, '2020-08-06 10:30:49', '2020-08-26 16:18:19');
INSERT INTO `tb_member_sekunder` VALUES (11, NULL, NULL, NULL, NULL, NULL, '2020-08-20', NULL, NULL, NULL, '2020-08-06 10:41:14', '2020-08-26 16:18:04');

-- ----------------------------
-- Table structure for tb_member_tersier
-- ----------------------------
DROP TABLE IF EXISTS `tb_member_tersier`;
CREATE TABLE `tb_member_tersier`  (
  `id_kop` int NULL DEFAULT NULL,
  `pendapatan_utama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_dana` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `masalah_utama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kegiatan_pengembang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `uodated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_member_tersier
-- ----------------------------

-- ----------------------------
-- Table structure for tb_menu_detail_a
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_detail_a`;
CREATE TABLE `tb_menu_detail_a`  (
  `MenuID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ClassId` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Link` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Icon` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Alias` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Active` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MenuID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_menu_detail_a
-- ----------------------------
INSERT INTO `tb_menu_detail_a` VALUES ('A01', 'nav-dashboard', 'Dashboard', '/dashboard', 'feather icon-home', 'dashboard', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A02', 'nav-master', 'Master', '#', 'feather icon-grid', 'master', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A03', 'nav-user', 'User', '#', 'feather icon-user', 'user', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A04', 'nav-persons', 'Person', '/persons', 'feather icon-users', 'person', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A07', 'nav-profile', 'Profile', '/profile', 'feather icon-github', 'profile', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A08', 'nav-manage', 'Manage', '#', 'feather icon-layers', 'manage', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A09', 'nav-vendor', 'Vendor', '#', 'feather icon-link', 'vendor', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A10', 'nav-pelelangan', 'Paket Lelang', '#', 'feather icon-bar-chart-2', 'paketlelang', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A11', 'nav-rekap', 'Rekap Lelang', '#', 'feather \r\nicon-clipboard', 'rekaplelang', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A12', 'nav-kontrak', 'Dokumen Kontrak', '#', 'feather icon-folder', 'dokumenkontrak', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A13', 'nav-addedum', 'Perubahan Schedule', '#', 'feather icon-edit', 'perubahanschedule', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A14', 'nav-beritaacara', 'Berita Acara', '#', 'feather icon-printer', 'beritaacara', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A15', 'nav-penilaian', 'Penilaian Vendor', '#', 'feather icon-alert-octagon', 'penilaianvendor', 'false');
INSERT INTO `tb_menu_detail_a` VALUES ('A16', 'nav-logactivity', 'Log Activity', '#', 'feather \r\nicon-sunset', 'logaktifitas', 'false');

-- ----------------------------
-- Table structure for tb_menu_detail_b
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_detail_b`;
CREATE TABLE `tb_menu_detail_b`  (
  `ID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `MenuID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `DetailID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ClassId` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Link` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Alias` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Active` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_menu_detail_b
-- ----------------------------
INSERT INTO `tb_menu_detail_b` VALUES ('1', 'A02', 'B00', 'nav-master2', 'All For User', '#', 'allforuser', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('2', 'A03', 'B00', 'nav-user1', 'Data User', '/users', 'datauser', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('3', 'A03', 'B01', 'nav-user2', 'User to Vendor', '/userstovendor', 'usertovendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('6', 'A02', 'B01', 'nav-master3', 'Setting', '#', 'setting', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('10', 'A02', 'B02', 'nav-master4', 'Content', '#', 'content', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('15', 'A08', 'B00', 'nav-mng0', 'Manage Comment', '/managecomment', 'managecomment', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('16', 'A08', 'B01', 'nav-mng1', 'Manage Employee', '/manageemployee', 'manageemployee', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('17', 'A08', 'B02', 'nav-mng2', 'Manage Committee', '/managecommittee', 'managecommittee', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('18', 'A08', 'B03', 'nav-mng3', 'Manage Vendor', '/managevendor', 'managevendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('19', 'A09', 'B00', 'nav-ven0', 'Data Vendor', '/datavendor', 'datavendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('20', 'A09', 'B01', 'nav-ven1', 'Request Vendor', '/reqvendor', 'requestvendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('21', 'A09', 'B02', 'nav-ven2', 'Email To Vendor', '/emailtovendor', 'emailtovendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('22', 'A10', 'B00', 'nav-lel0', 'Penunjukan', '#', 'penunjukanLangsung', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('23', 'A11', 'B00', 'nav-rek0', 'Penunjukan Langsung', '/repenunjukanlangsung', 'repenunjukanlangsung', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('24', 'A11', 'B01', 'nav-rek1', 'Pelelangan Terbatas', '/repelelanganterbatas', 'repelelanganterbatas', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('25', 'A11', 'B02', 'nav-rek2', 'Pelelangan Umum', '/repelelanganumum', 'repelelanganumum', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('26', 'A12', 'B00', 'nav-kon0', 'Data Kontrak', '/datakontrak', 'datakontrak', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('27', 'A13', 'B00', 'nav-add0', 'Data Perubahan', '/dataperubahanschedule', 'dataperubahanschedule', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('28', 'A14', 'B00', 'nav-ber0', 'BA Penunjukan', '/bapenunjukan', 'bapenunjukan', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('29', 'A14', 'B01', 'nav-ber1', 'BA Terbatas', '/baterbatas', 'baterbatas', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('30', 'A14', 'B02', 'nav-ber2', 'BA Umum', '/baumum', 'baumum', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('31', 'A15', 'B00', 'nav-kom0', 'Data Penilaian Vendor', '/datapenilaianvendor', 'datapenilaianvendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('32', 'A09', 'B03', 'nav-ven3', 'My Data Vendor', '/mydatavendor', 'mydatavendor', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('33', 'A10', 'B01', 'nav-lel1', 'Terbatas', '#', 'pelelanganterbatas', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('34', 'A10', 'B02', 'nav-lel2', 'Umum', '#', 'pelalanganumum', 'false');
INSERT INTO `tb_menu_detail_b` VALUES ('35', 'A16', 'B00', 'nav-log0', 'Data Log Activity', '/datalogaktifitas', 'datalogaktifitas', 'false');

-- ----------------------------
-- Table structure for tb_menu_detail_c
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_detail_c`;
CREATE TABLE `tb_menu_detail_c`  (
  `ID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `MenuID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `DetailID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `DetailID2` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ClassId` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Alias` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Active` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_menu_detail_c
-- ----------------------------
INSERT INTO `tb_menu_detail_c` VALUES ('1', 'A02', 'B00', 'C00', 'Data Role', 'nav-role', '/role', 'datarole', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('2', 'A02', 'B00', 'C01', 'Menu Landing', 'nav-menulanding', '/menulanding', 'menulanding', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('3', 'A02', 'B01', 'C00', 'Email', 'nav-email', '/settingemail', 'settingemail', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('4', 'A02', 'B02', 'C00', 'Tentang Kami', 'nav-kami', '/tentangkami', 'tentangkami', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('5', 'A02', 'B02', 'C01', 'Banner', 'nav-banner', '/banner', 'banner', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('6', 'A02', 'B02', 'C02', 'Visi & Misi', 'nav-visi', '/visimisi', 'visimisi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('8', 'A02', 'B02', 'C04', 'Who Are We', 'nav-arewe', '/whoarewe', 'whoarewe', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('11', 'A02', 'B02', 'C07', 'Pengumuman', 'nav-pengumuman', '/mpengumuman', 'mpengumuman', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('12', 'A02', 'B02', 'C08', 'Berita', 'nav-berita', '/mberita', 'mberita', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('13', 'A02', 'B01', 'C01', 'Footer', 'nav-footer', '/mfooter', 'mfooter', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('14', 'A02', 'B02', 'C09', 'Counter', 'nav-counter', '/mcounter', 'mcounter', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('15', 'A02', 'B01', 'C02', 'Main Banner', 'nav-mainbanner', '/mainbanner', 'mainbanner', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('28', 'A10', 'B00', 'C00', 'Info Pekerjaan', 'nav-infopekerjaan', '/pl_infopekerjaan', 'pl_infopekerjaan', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('29', 'A10', 'B00', 'C03', 'Aanwijzing\r\n', 'nav-aanwijzing', '/pl_aanwijzing', 'pl_aanwijzing', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('30', 'A10', 'B00', 'C04', 'Penawaran', 'nav-penawaran', '/pl_penawaran', 'pl_penawaran', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('31', 'A10', 'B00', 'C06', 'Klarifikasi & Negosiasi', 'nav-negosiasi', '/pl_negosiasi', 'pl_negosiasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('32', 'A10', 'B00', 'C07', 'Penetapan Pemenang', 'nav-pemenang', '/pl_pemenang', 'pl_pemenang', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('21', 'A02', 'B01', 'C03', 'Jenis Vendor', 'nav-jenisvendor', '/mjenisvendor', 'mjenisvendor', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('22', 'A02', 'B01', 'C04', 'Divisi', 'nav-divisi', '/mdivisi', 'mdivisi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('23', 'A02', 'B01', 'C05', 'Kategori Pekerjaan', 'nav-kategoripekerjaan', '/mkategoripekerjaan', 'mkategoripekerjaan', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('24', 'A02', 'B01', 'C06', 'Sub Klasifikasi', 'nav-subklasifikasi', '/msubklasifikasi', 'msubklasifikasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('25', 'A02', 'B01', 'C07', 'Jabatan', 'nav-jabatan', '/mjabatan', 'mjabatan', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('33', 'A10', 'B00', 'C02', 'Dokumen Kualifikasi', 'nav-dokumenkualifikasi', '/pl_dokumenkualifikasi', 'pl_dokumenkualifikasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('34', 'A10', 'B00', 'C05', 'Evaluasi', 'nav-evaliasi', '/pl_evaluasi', 'pl_evaluasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('42', 'A10', 'B02', 'C00', 'Info Pekerjaan', 'nav-infopekerjaanum', '/um_infopekerjaan', 'um_infopekerjaan', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('43', 'A10', 'B02', 'C03', 'Aanwijzing\r\n', 'nav-aanwijzingum', '/um_aanwijzing', 'um_aanwijzing', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('44', 'A10', 'B02', 'C04', 'Penawaran', 'nav-penawaranum', '/um_penawaran', 'um_penawaran', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('45', 'A10', 'B02', 'C06', 'Klarifikasi & Negosiasi', 'nav-negosiasium', '/um_negosiasi', 'um_negosiasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('46', 'A10', 'B02', 'C07', 'Penetapan Pemenang', 'nav-pemenangum', '/um_pemenang', 'um_pemenang', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('47', 'A10', 'B02', 'C02', 'Dokumen Kualifikasi', 'nav-dokumenkualifikasium', '/um_dokumenkualifikasi', 'um_dokumenkualifikasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('48', 'A10', 'B02', 'C05', 'Evaluasi', 'nav-evaliasium', '/um_evaluasi', 'um_evaluasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('35', 'A10', 'B01', 'C00', 'Info Pekerjaan', 'nav-infopekerjaanpt', '/pt_infopekerjaan', 'pt_infopekerjaan', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('36', 'A10', 'B01', 'C03', 'Aanwijzing\r\n', 'nav-aanwijzingpt', '/pt_aanwijzing', 'pt_aanwijzing', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('37', 'A10', 'B01', 'C04', 'Penawaran', 'nav-penawaranpt', '/pt_penawaran', 'pt_penawaran', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('38', 'A10', 'B01', 'C06', 'Klarifikasi & Negosiasi', 'nav-negosiasipt', '/pt_negosiasi', 'pt_negosiasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('39', 'A10', 'B01', 'C07', 'Penetapan Pemenang', 'nav-pemenangpt', '/pt_pemenang', 'pt_pemenang', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('40', 'A10', 'B01', 'C02', 'Dokumen Kualifikasi', 'nav-dokumenkualifikasipt', '/pt_dokumenkualifikasi', 'pt_dokumenkualifikasi', 'false');
INSERT INTO `tb_menu_detail_c` VALUES ('41', 'A10', 'B01', 'C05', 'Evaluasi', 'nav-evaliasipt', '/pt_evaluasi', 'pt_evaluasi', 'false');

-- ----------------------------
-- Table structure for tb_menu_landing_a
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_landing_a`;
CREATE TABLE `tb_menu_landing_a`  (
  `MenuID` int NOT NULL,
  `ClassId` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `NameEn` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Alias` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Active` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MenuID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_menu_landing_a
-- ----------------------------
INSERT INTO `tb_menu_landing_a` VALUES (1, 'nav-home', 'Beranda', 'Home', '/', 'home', 'false');
INSERT INTO `tb_menu_landing_a` VALUES (2, 'nav-aboutus', 'Program', 'Programs', '#', 'aboutus', 'false');
INSERT INTO `tb_menu_landing_a` VALUES (3, 'nav-activity', 'Jaringan', 'Networks', '#', 'activity', 'false');
INSERT INTO `tb_menu_landing_a` VALUES (5, 'nav-community', 'Tentang Kami', 'About Us', '#', 'community', 'false');

-- ----------------------------
-- Table structure for tb_menu_landing_b
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_landing_b`;
CREATE TABLE `tb_menu_landing_b`  (
  `ID` int NOT NULL,
  `MenuID` int NOT NULL,
  `DetailID` int NOT NULL,
  `ClassId` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `NameEn` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Alias` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Active` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_menu_landing_b
-- ----------------------------
INSERT INTO `tb_menu_landing_b` VALUES (7, 3, 1, 'nav-profilekop*1', 'Profile Vendor*', 'Profile Vendor*', '/profilevendor', 'profilekop*', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (12, 5, 0, 'nav-sejarahsingkat0', 'Sejarah Singkat', 'A brief History', '/landtentangkami', 'sejarahsingkat', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (13, 5, 1, 'nav-visi&misi1', 'Visi & Misi', 'Vision & Mission', '/visi&misi', 'visi&misi', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (14, 5, 2, 'nav-faq2', 'FAQ', 'FAQ', '/faq', 'faq', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (15, 5, 3, 'nav-kepengurusan3', 'Kepengurusan', 'Management', '/kepengurusan', 'kepengurusan', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (1, 2, 0, 'nav-pengumuman1', 'Pengumuman', 'Announcement', '/pengumuman', 'pengumuman', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (2, 2, 1, 'nav-berita2', 'Berita', 'News', '/berita', 'berita', 'false');
INSERT INTO `tb_menu_landing_b` VALUES (16, 2, 2, 'nav-paketlelang2', 'Paket Lelang', 'Auction Package', '/paketlelang', 'paketlelang', 'false');

-- ----------------------------
-- Table structure for tb_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_message`;
CREATE TABLE `tb_message`  (
  `id_message` int NOT NULL AUTO_INCREMENT,
  `id_pekerjaan` int NULL DEFAULT NULL,
  `jenis` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_message`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_message
-- ----------------------------
INSERT INTO `tb_message` VALUES (7, 7, 'Penunjukan Langsung', '2021-01-07 10:09:17', NULL);
INSERT INTO `tb_message` VALUES (8, 8, 'Penunjukan Langsung', '2021-01-14 10:42:21', NULL);
INSERT INTO `tb_message` VALUES (9, 9, 'Penunjukan Langsung', '2021-01-14 11:40:57', NULL);
INSERT INTO `tb_message` VALUES (10, 10, 'Pelelangan Umum', '2021-01-20 10:46:41', NULL);
INSERT INTO `tb_message` VALUES (11, 11, 'Penunjukan Langsung', '2021-03-10 11:26:07', NULL);
INSERT INTO `tb_message` VALUES (12, 12, 'Penunjukan Langsung', '2021-03-10 11:54:06', NULL);
INSERT INTO `tb_message` VALUES (13, 13, 'Pelelangan Terbatas', '2021-03-10 15:15:32', NULL);
INSERT INTO `tb_message` VALUES (14, 14, 'Pelelangan Umum', '2021-03-18 14:09:44', NULL);

-- ----------------------------
-- Table structure for tb_message_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_message_detail`;
CREATE TABLE `tb_message_detail`  (
  `id_message` int NOT NULL,
  `id_sender` int NULL DEFAULT NULL,
  `id_receiver` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_message_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tb_notifikasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_notifikasi`;
CREATE TABLE `tb_notifikasi`  (
  `id_notif` int NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_sender` int NULL DEFAULT NULL,
  `id_receiver` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_notif`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 500 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_notifikasi
-- ----------------------------
INSERT INTO `tb_notifikasi` VALUES (19, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor bkbkb', 2, '2020-10-21 22:30:18', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (20, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jbkjbkb', 2, '2020-10-21 22:48:51', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (21, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jhkhkjh', 2, '2020-10-22 16:26:00', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (22, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jlnljnjn', 2, '2020-10-22 17:39:12', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (23, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor kjljljlj', 2, '2020-10-22 18:06:43', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (24, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor kllkj', 2, '2020-10-22 18:11:24', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (25, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jlnljnkj', 2, '2020-10-22 18:16:21', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (26, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor PT. Uul Mencari S1', 2, '2020-10-22 18:23:56', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (27, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor ljljnjl', 2, '2020-10-22 18:27:22', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (53, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor ljnjnj', 2, '2020-10-26 22:10:42', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (54, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jbkjbkjb', 2, '2020-10-26 22:19:23', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (59, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor sdsd', 2, '2020-12-22 18:39:33', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (60, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor nfd', 2, '2020-12-23 08:27:19', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (61, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor njnjn', 2, '2020-12-23 09:49:18', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (62, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor kjnkjnkj', 2, '2020-12-23 09:56:04', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (63, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor bhhbhjb', 2, '2020-12-23 10:01:05', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (64, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor hbjbjhbjh', 2, '2020-12-23 10:16:48', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (65, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor njknjkn', 2, '2020-12-23 10:22:24', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (66, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor PT Pencari Janda Tbk', 2, '2020-12-28 09:51:42', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (145, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor Putra Wijayakusuma Sakti, PT', 2, '2021-01-05 10:14:40', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (146, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jhkjbj', 2, '2021-01-05 10:31:47', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (147, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor jbkjbkb', 2, '2021-01-05 10:52:22', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (148, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor kjkjbkjbjkb', 2, '2021-01-05 10:58:48', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (149, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor hgvkhbvkh', 2, '2021-01-05 11:07:31', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (150, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor kbvkjbjb', 2, '2021-01-05 11:15:10', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (151, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor ljnljljnl', 2, '2021-01-05 11:25:02', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (152, 'Pekerjaan', 7, 14, '29', 'Penunjukan Langsung kepada PT. Uul Mencari S1 untuk pekerjaan Test', 2, '2021-01-07 10:09:17', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (153, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-01-07 10:12:55', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (154, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-01-07 10:12:55', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (155, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-01-07 10:12:55', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (156, 'Pekerjaan', 7, 14, '29', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-07 jam 11:00', 2, '2021-01-07 10:13:52', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (157, 'Pekerjaan', 7, 14, '29', 'Dokumen Kualifikasi Di Terima', 2, '2021-01-07 11:17:44', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (158, 'Pekerjaan', 7, 14, '29', 'Pelaksanaan Aanwijzing dimulai pada 2021-01-07 jam 11:00 dan berakhir pada 2021-01-07 jam 12:00', 2, '2021-01-07 11:21:09', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (159, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:18', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (160, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:18', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (161, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:18', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (162, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:26', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (163, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:26', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (164, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test', 2, '2021-01-07 11:44:26', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (165, 'Pekerjaan', 7, 14, '29', 'Panitia memberi jawaban', 2, '2021-01-07 11:52:28', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (166, 'Pekerjaan', 7, 14, '29', 'Panitia memberi jawaban', 2, '2021-01-07 11:52:37', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (167, 'Pekerjaan', 7, 14, '29', 'Panitia memberi jawaban', 2, '2021-01-07 11:55:17', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (168, 'Pekerjaan', 7, 14, '29', 'Panitia memberi jawaban', 2, '2021-01-07 12:03:59', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (169, 'Pekerjaan', 7, 14, '29', 'Panitia memberi jawaban', 2, '2021-01-07 12:06:59', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (170, 'Pekerjaan', 7, 14, '29', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-01-07 13:26:17', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (171, 'Pekerjaan', 7, 14, '21', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-01-07 13:26:17', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (172, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:33', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (173, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:33', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (174, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:33', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (175, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:33', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (176, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:49', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (177, 'Pekerjaan', 7, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:49', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (178, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-07 13:29:49', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (179, 'Pekerjaan', 7, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-01-07 jam 13:00 dan berakhir pada 2021-01-07 jam 15:00', 2, '2021-01-07 13:32:27', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (180, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:01:06', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (181, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:01:06', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (182, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:01:06', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (183, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:07:10', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (184, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:07:10', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (185, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 900000000', 2, '2021-01-07 15:07:10', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (186, 'Pekerjaan', 7, 14, '29', 'Evaluasi Penawaran dimulai pada 2021-01-07 jam 15:00 dan berakhir pada 2021-01-07 jam 16:00', 2, '2021-01-07 15:10:32', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (187, 'Pekerjaan', 7, 14, '29', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-07 15:21:16', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (188, 'Pekerjaan', 7, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-07 15:21:16', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (189, 'Pekerjaan', 7, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-07 15:21:16', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (190, 'Pekerjaan', 7, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-07 15:21:16', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (191, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:21:56', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (192, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:21:56', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (193, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:21:56', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (194, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:21:56', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (195, 'Pekerjaan', 7, NULL, '21', 'Ketua Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:22:20', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (196, 'Pekerjaan', 7, NULL, '22', 'Ketua Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:22:20', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (197, 'Pekerjaan', 7, NULL, '21', 'Ketua Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:22:20', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (198, 'Pekerjaan', 7, NULL, '14', 'Ketua Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:22:20', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (199, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:23:15', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (200, 'Pekerjaan', 7, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:23:15', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (201, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:23:15', '2021-01-07 15:27:30');
INSERT INTO `tb_notifikasi` VALUES (202, 'Pekerjaan', 7, 21, '22', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:27:38', '2021-01-07 15:29:26');
INSERT INTO `tb_notifikasi` VALUES (203, 'Pekerjaan', 7, 21, '14', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:27:38', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (204, 'Pekerjaan', 7, 22, '21', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 1, '2021-01-07 15:29:36', NULL);
INSERT INTO `tb_notifikasi` VALUES (205, 'Pekerjaan', 7, 22, '21', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 1, '2021-01-07 15:29:36', NULL);
INSERT INTO `tb_notifikasi` VALUES (206, 'Pekerjaan', 7, 22, '14', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-07 15:29:36', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (207, 'Pekerjaan', 7, 14, '29', 'Klarifikasi & Negosiasi dimulai pada 2021-01-07 jam 15:31 dan berakhir pada 2021-01-07 jam 16:00', 2, '2021-01-07 15:31:10', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (208, 'Pekerjaan', 7, 14, '29', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 2, '2021-01-07 15:34:37', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (209, 'Pekerjaan', 7, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-01-07 15:34:37', NULL);
INSERT INTO `tb_notifikasi` VALUES (210, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:36:36', NULL);
INSERT INTO `tb_notifikasi` VALUES (211, 'Pekerjaan', 7, 29, '22', 'PT. Uul Mencari S1 Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-07 15:36:36', '2021-01-14 11:45:30');
INSERT INTO `tb_notifikasi` VALUES (212, 'Pekerjaan', 7, 29, '21', 'PT. Uul Mencari S1 Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:36:36', NULL);
INSERT INTO `tb_notifikasi` VALUES (213, 'Pekerjaan', 7, 29, '14', 'PT. Uul Mencari S1 Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-07 15:36:36', '2021-02-11 16:27:34');
INSERT INTO `tb_notifikasi` VALUES (214, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:37:01', NULL);
INSERT INTO `tb_notifikasi` VALUES (215, 'Pekerjaan', 7, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-07 15:37:01', '2021-01-14 11:45:31');
INSERT INTO `tb_notifikasi` VALUES (216, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:37:01', NULL);
INSERT INTO `tb_notifikasi` VALUES (217, 'Pekerjaan', 7, 14, '29', 'Penetapan Pemenang dimulai pada 2021-01-07 jam 15:30 dan berakhir pada 2021-01-07 jam 16:00', 2, '2021-01-07 15:37:27', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (218, 'Pekerjaan', 7, 14, '29', 'Pemenang Lelang PT. Uul Mencari S1', 2, '2021-01-07 15:42:58', '2021-01-07 15:43:25');
INSERT INTO `tb_notifikasi` VALUES (219, 'Pekerjaan', 7, 14, NULL, 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-01-07 15:42:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (220, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:43:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (221, 'Pekerjaan', 7, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-07 15:43:35', '2021-01-14 11:45:33');
INSERT INTO `tb_notifikasi` VALUES (222, 'Pekerjaan', 7, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-07 15:43:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (223, 'Register', NULL, NULL, '14', 'Request Registrasi Vendor Perusahan ABC', 2, '2021-01-14 10:28:55', '2021-03-18 16:12:12');
INSERT INTO `tb_notifikasi` VALUES (224, 'Pekerjaan', 8, 14, '35', 'Penunjukan Langsung kepada Perusahan ABC untuk pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:42:21', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (225, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menerima Pekerjaan', 2, '2021-01-14 10:45:04', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (226, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Menerima Pekerjaan', 2, '2021-01-14 10:45:04', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (227, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Menerima Pekerjaan', 2, '2021-01-14 10:45:04', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (228, 'Pekerjaan', 8, 14, '35', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-14 jam 11:00', 2, '2021-01-14 10:47:02', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (229, 'Pekerjaan', 8, 14, '35', 'Dokumen Kualifikasi Di Terima', 2, '2021-01-14 10:51:42', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (230, 'Pekerjaan', 8, 14, '35', 'Pelaksanaan Aanwijzing dimulai pada 2021-01-14 jam 10:00 dan berakhir pada 2021-01-14 jam 11:00', 2, '2021-01-14 10:54:35', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (231, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:17', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (232, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:17', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (233, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:17', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (234, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:47', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (235, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:47', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (236, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan Pekerjaan Pembuatan Jembatan', 2, '2021-01-14 10:56:47', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (237, 'Pekerjaan', 8, 14, '35', 'Panitia memberi jawaban', 2, '2021-01-14 10:57:27', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (238, 'Pekerjaan', 8, 14, '35', 'Panitia memberi jawaban', 2, '2021-01-14 10:57:35', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (239, 'Pekerjaan', 8, 14, '35', 'Panitia memberi jawaban', 2, '2021-01-14 10:58:05', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (240, 'Pekerjaan', 8, 14, '35', 'Panitia memberi jawaban', 2, '2021-01-14 10:58:34', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (241, 'Pekerjaan', 8, 14, '35', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-01-14 11:04:11', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (242, 'Pekerjaan', 8, 14, '21', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-01-14 11:04:11', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (243, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:04:56', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (244, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:04:56', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (245, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:04:56', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (246, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:04:56', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (247, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:05:33', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (248, 'Pekerjaan', 8, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:05:33', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (249, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 2, '2021-01-14 11:05:33', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (250, 'Pekerjaan', 8, 14, '35', 'Pemasukan Penawaran dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', 2, '2021-01-14 11:05:58', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (251, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 1000000000', 2, '2021-01-14 11:16:13', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (252, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 1000000000', 2, '2021-01-14 11:16:13', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (253, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 1000000000', 2, '2021-01-14 11:16:13', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (254, 'Pekerjaan', 8, 14, '35', 'Evaluasi Penawaran dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', 2, '2021-01-14 11:17:52', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (255, 'Pekerjaan', 8, 14, '35', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-14 11:20:31', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (256, 'Pekerjaan', 8, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-14 11:20:31', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (257, 'Pekerjaan', 8, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-14 11:20:31', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (258, 'Pekerjaan', 8, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-01-14 11:20:31', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (259, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:21:36', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (260, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:21:36', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (261, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:21:36', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (262, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:21:36', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (263, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:22:33', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (264, 'Pekerjaan', 8, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:22:33', '2021-01-14 11:24:30');
INSERT INTO `tb_notifikasi` VALUES (265, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:22:33', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (266, 'Pekerjaan', 8, 22, '21', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:24:57', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (267, 'Pekerjaan', 8, 22, '21', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:24:57', '2021-01-14 11:25:06');
INSERT INTO `tb_notifikasi` VALUES (268, 'Pekerjaan', 8, 22, '14', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:24:57', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (269, 'Pekerjaan', 8, 21, '22', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:25:15', '2021-01-14 11:45:40');
INSERT INTO `tb_notifikasi` VALUES (270, 'Pekerjaan', 8, 21, '14', 'Anggota Panitia Menandatangani Berita Acara Penawaran', 2, '2021-01-14 11:25:15', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (271, 'Pekerjaan', 8, 14, '35', 'Klarifikasi & Negosiasi dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', 2, '2021-01-14 11:26:05', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (272, 'Pekerjaan', 8, 14, '35', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 2, '2021-01-14 11:26:41', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (273, 'Pekerjaan', 8, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-01-14 11:26:41', NULL);
INSERT INTO `tb_notifikasi` VALUES (274, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:27:09', NULL);
INSERT INTO `tb_notifikasi` VALUES (275, 'Pekerjaan', 8, 35, '22', 'Perusahan ABC Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-14 11:27:09', '2021-01-14 11:45:34');
INSERT INTO `tb_notifikasi` VALUES (276, 'Pekerjaan', 8, 35, '21', 'Perusahan ABC Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:27:09', NULL);
INSERT INTO `tb_notifikasi` VALUES (277, 'Pekerjaan', 8, 35, '14', 'Perusahan ABC Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-14 11:27:09', '2021-03-10 11:05:40');
INSERT INTO `tb_notifikasi` VALUES (278, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:27:28', NULL);
INSERT INTO `tb_notifikasi` VALUES (279, 'Pekerjaan', 8, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-14 11:27:28', '2021-01-14 11:45:19');
INSERT INTO `tb_notifikasi` VALUES (280, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:27:28', NULL);
INSERT INTO `tb_notifikasi` VALUES (281, 'Pekerjaan', 8, 14, '35', 'Penetapan Pemenang dimulai pada 2021-01-14 jam 11:00 dan berakhir pada 2021-01-14 jam 12:00', 2, '2021-01-14 11:27:53', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (282, 'Pekerjaan', 8, 14, '35', 'Pemenang Lelang Perusahan ABC', 2, '2021-01-14 11:28:42', '2021-01-14 11:44:08');
INSERT INTO `tb_notifikasi` VALUES (283, 'Pekerjaan', 8, 14, NULL, 'Pemenang Lelang Perusahan ABC', 1, '2021-01-14 11:28:42', NULL);
INSERT INTO `tb_notifikasi` VALUES (284, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:28:52', NULL);
INSERT INTO `tb_notifikasi` VALUES (285, 'Pekerjaan', 8, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-01-14 11:28:52', '2021-01-14 11:45:42');
INSERT INTO `tb_notifikasi` VALUES (286, 'Pekerjaan', 8, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-01-14 11:28:52', NULL);
INSERT INTO `tb_notifikasi` VALUES (287, 'Pekerjaan', 9, 14, '35', 'Penunjukan Langsung kepada Perusahan ABC untuk pekerjaan test 2', 2, '2021-01-14 11:40:57', '2021-01-14 11:44:22');
INSERT INTO `tb_notifikasi` VALUES (288, 'Pekerjaan', 9, 35, '21', 'Perusahan ABC Menerima Pekerjaan', 1, '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_notifikasi` VALUES (289, 'Pekerjaan', 9, 35, '22', 'Perusahan ABC Menerima Pekerjaan', 2, '2021-01-14 11:43:04', '2021-01-14 11:45:44');
INSERT INTO `tb_notifikasi` VALUES (290, 'Pekerjaan', 9, 35, '14', 'Perusahan ABC Menerima Pekerjaan', 2, '2021-01-14 11:43:04', '2021-03-10 11:53:00');
INSERT INTO `tb_notifikasi` VALUES (291, 'Pekerjaan', 9, 14, '35', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-01-14 jam 12:00', 2, '2021-01-14 11:43:16', '2021-01-14 11:44:22');
INSERT INTO `tb_notifikasi` VALUES (292, 'Pekerjaan', 9, 14, '35', 'Dokumen Kualifikasi Di Terima', 2, '2021-01-14 11:43:55', '2021-01-14 11:44:22');
INSERT INTO `tb_notifikasi` VALUES (293, 'Pekerjaan', 9, 14, '35', 'Pelaksanaan Aanwijzing dimulai pada 2021-01-14 jam 10:00 dan berakhir pada 2021-01-14 jam 15:00', 2, '2021-01-14 11:44:18', '2021-01-14 11:44:22');
INSERT INTO `tb_notifikasi` VALUES (294, 'Pekerjaan', 9, 35, '21', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 1, '2021-01-14 11:44:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (295, 'Pekerjaan', 9, 35, '22', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:31', '2021-01-14 11:45:45');
INSERT INTO `tb_notifikasi` VALUES (296, 'Pekerjaan', 9, 35, '14', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:31', '2021-03-10 11:53:00');
INSERT INTO `tb_notifikasi` VALUES (297, 'Pekerjaan', 9, 35, '21', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 1, '2021-01-14 11:44:41', NULL);
INSERT INTO `tb_notifikasi` VALUES (298, 'Pekerjaan', 9, 35, '22', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:41', '2021-01-14 11:45:36');
INSERT INTO `tb_notifikasi` VALUES (299, 'Pekerjaan', 9, 35, '14', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:41', '2021-03-10 11:53:00');
INSERT INTO `tb_notifikasi` VALUES (300, 'Pekerjaan', 9, 35, '21', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 1, '2021-01-14 11:44:48', NULL);
INSERT INTO `tb_notifikasi` VALUES (301, 'Pekerjaan', 9, 35, '22', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:48', '2021-01-14 11:45:38');
INSERT INTO `tb_notifikasi` VALUES (302, 'Pekerjaan', 9, 35, '14', 'Perusahan ABC Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-01-14 11:44:48', '2021-03-10 11:53:00');
INSERT INTO `tb_notifikasi` VALUES (303, 'Pekerjaan', 9, 14, '35', 'Panitia memberi jawaban', 1, '2021-01-14 11:54:14', NULL);
INSERT INTO `tb_notifikasi` VALUES (304, 'Pekerjaan', 9, 14, '35', 'Panitia memberi jawaban', 1, '2021-01-14 11:54:21', NULL);
INSERT INTO `tb_notifikasi` VALUES (305, 'Pekerjaan', 9, 14, '35', 'Panitia memberi jawaban', 1, '2021-01-14 11:54:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (306, 'Pekerjaan', 9, 14, '35', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-04 15:29:50', NULL);
INSERT INTO `tb_notifikasi` VALUES (307, 'Pekerjaan', 9, 14, '22', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-04 15:29:50', NULL);
INSERT INTO `tb_notifikasi` VALUES (308, 'Pekerjaan', 9, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-08 14:13:42', NULL);
INSERT INTO `tb_notifikasi` VALUES (309, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-08 14:13:42', NULL);
INSERT INTO `tb_notifikasi` VALUES (310, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-08 14:13:42', NULL);
INSERT INTO `tb_notifikasi` VALUES (311, 'Pekerjaan', 9, 14, '35', 'Pemasukan Penawaran dimulai pada 2021-03-08 jam 12:00 dan berakhir pada 2021-03-08 jam 16:00', 1, '2021-03-08 14:14:19', NULL);
INSERT INTO `tb_notifikasi` VALUES (312, 'Pekerjaan', 9, 35, '21', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 10000000', 1, '2021-03-08 14:44:18', NULL);
INSERT INTO `tb_notifikasi` VALUES (313, 'Pekerjaan', 9, 35, '22', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 10000000', 1, '2021-03-08 14:44:18', NULL);
INSERT INTO `tb_notifikasi` VALUES (314, 'Pekerjaan', 9, 35, '14', 'Perusahan ABC Memasukan Penawaran baru senilai Rp. 10000000', 2, '2021-03-08 14:44:18', '2021-03-10 11:53:00');
INSERT INTO `tb_notifikasi` VALUES (315, 'Pekerjaan', 9, 14, '35', 'Evaluasi Penawaran dimulai pada 2021-03-08 jam 14:58 dan berakhir pada 2021-03-08 jam 16:00', 1, '2021-03-08 14:58:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (316, 'Pekerjaan', 9, 14, '35', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-08 15:11:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (317, 'Pekerjaan', 9, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-08 15:11:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (318, 'Pekerjaan', 9, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-08 15:11:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (319, 'Pekerjaan', 9, 14, '28', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-08 15:11:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (320, 'Pekerjaan', 9, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-08 16:18:43', NULL);
INSERT INTO `tb_notifikasi` VALUES (321, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-08 16:18:43', NULL);
INSERT INTO `tb_notifikasi` VALUES (322, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-08 16:18:43', NULL);
INSERT INTO `tb_notifikasi` VALUES (323, 'Pekerjaan', 9, 14, '35', 'Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 09:39 dan berakhir pada 2021-03-10 jam 15:45', 1, '2021-03-10 09:39:18', NULL);
INSERT INTO `tb_notifikasi` VALUES (324, 'Pekerjaan', 9, 14, '35', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 10:56:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (325, 'Pekerjaan', 9, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 10:56:30', NULL);
INSERT INTO `tb_notifikasi` VALUES (326, 'Pekerjaan', 9, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:05:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (327, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:05:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (328, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:05:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (329, 'Pekerjaan', 9, 14, '35', 'Penetapan Pemenang dimulai pada 2021-03-10 jam 11:00 dan berakhir pada 2021-03-10 jam 16:00', 1, '2021-03-10 11:06:15', NULL);
INSERT INTO `tb_notifikasi` VALUES (330, 'Pekerjaan', 9, 14, '35', 'Pemenang Lelang Perusahan ABC', 1, '2021-03-10 11:18:51', NULL);
INSERT INTO `tb_notifikasi` VALUES (331, 'Pekerjaan', 9, 14, NULL, 'Pemenang Lelang Perusahan ABC', 1, '2021-03-10 11:18:51', NULL);
INSERT INTO `tb_notifikasi` VALUES (332, 'Pekerjaan', 9, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:23:29', NULL);
INSERT INTO `tb_notifikasi` VALUES (333, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:23:29', NULL);
INSERT INTO `tb_notifikasi` VALUES (334, 'Pekerjaan', 9, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:23:29', NULL);
INSERT INTO `tb_notifikasi` VALUES (335, 'Pekerjaan', 11, 14, '29', 'Penunjukan Langsung kepada PT. Uul Mencari S1 untuk pekerjaan test 2', 2, '2021-03-10 11:26:07', '2021-03-10 11:28:44');
INSERT INTO `tb_notifikasi` VALUES (336, 'Pekerjaan', 11, 29, '21', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (337, 'Pekerjaan', 11, 29, '22', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_notifikasi` VALUES (338, 'Pekerjaan', 11, 29, '14', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-03-10 11:26:58', '2021-03-10 11:52:19');
INSERT INTO `tb_notifikasi` VALUES (339, 'Pekerjaan', 11, 14, '29', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', 2, '2021-03-10 11:27:22', '2021-03-10 11:28:44');
INSERT INTO `tb_notifikasi` VALUES (340, 'Pekerjaan', 11, 14, '29', 'Dokumen Kualifikasi Di Terima', 2, '2021-03-10 11:28:18', '2021-03-10 11:28:44');
INSERT INTO `tb_notifikasi` VALUES (341, 'Pekerjaan', 11, 14, '29', 'Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', 2, '2021-03-10 11:28:36', '2021-03-10 11:28:44');
INSERT INTO `tb_notifikasi` VALUES (342, 'Pekerjaan', 11, 29, '21', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan test 2', 1, '2021-03-10 11:28:50', NULL);
INSERT INTO `tb_notifikasi` VALUES (343, 'Pekerjaan', 11, 29, '22', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan test 2', 1, '2021-03-10 11:28:50', NULL);
INSERT INTO `tb_notifikasi` VALUES (344, 'Pekerjaan', 11, 29, '14', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan test 2', 2, '2021-03-10 11:28:50', '2021-03-10 11:52:19');
INSERT INTO `tb_notifikasi` VALUES (345, 'Pekerjaan', 11, 14, '29', 'Panitia memberi jawaban', 1, '2021-03-10 11:29:06', NULL);
INSERT INTO `tb_notifikasi` VALUES (346, 'Pekerjaan', 11, 14, '29', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-10 11:32:34', NULL);
INSERT INTO `tb_notifikasi` VALUES (347, 'Pekerjaan', 11, 14, '21', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-10 11:32:34', NULL);
INSERT INTO `tb_notifikasi` VALUES (348, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 11:33:53', NULL);
INSERT INTO `tb_notifikasi` VALUES (349, 'Pekerjaan', 11, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 11:33:53', NULL);
INSERT INTO `tb_notifikasi` VALUES (350, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 11:33:53', NULL);
INSERT INTO `tb_notifikasi` VALUES (351, 'Pekerjaan', 11, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:35:05', NULL);
INSERT INTO `tb_notifikasi` VALUES (352, 'Pekerjaan', 11, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:38:48', NULL);
INSERT INTO `tb_notifikasi` VALUES (353, 'Pekerjaan', 11, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-10 jam 11:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:40:40', NULL);
INSERT INTO `tb_notifikasi` VALUES (354, 'Pekerjaan', 11, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1200000000', 1, '2021-03-10 11:42:34', NULL);
INSERT INTO `tb_notifikasi` VALUES (355, 'Pekerjaan', 11, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1200000000', 1, '2021-03-10 11:42:34', NULL);
INSERT INTO `tb_notifikasi` VALUES (356, 'Pekerjaan', 11, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1200000000', 2, '2021-03-10 11:42:34', '2021-03-10 11:52:19');
INSERT INTO `tb_notifikasi` VALUES (357, 'Pekerjaan', 11, 14, '29', 'Evaluasi Penawaran dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:43:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (358, 'Pekerjaan', 11, 14, '29', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 11:46:55', NULL);
INSERT INTO `tb_notifikasi` VALUES (359, 'Pekerjaan', 11, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 11:46:55', NULL);
INSERT INTO `tb_notifikasi` VALUES (360, 'Pekerjaan', 11, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 11:46:55', NULL);
INSERT INTO `tb_notifikasi` VALUES (361, 'Pekerjaan', 11, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 11:46:55', NULL);
INSERT INTO `tb_notifikasi` VALUES (362, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 11:48:16', NULL);
INSERT INTO `tb_notifikasi` VALUES (363, 'Pekerjaan', 11, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 11:48:16', NULL);
INSERT INTO `tb_notifikasi` VALUES (364, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 11:48:16', NULL);
INSERT INTO `tb_notifikasi` VALUES (365, 'Pekerjaan', 11, 14, '29', 'Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:49:00', NULL);
INSERT INTO `tb_notifikasi` VALUES (366, 'Pekerjaan', 11, 14, '29', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 11:49:24', NULL);
INSERT INTO `tb_notifikasi` VALUES (367, 'Pekerjaan', 11, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 11:49:24', NULL);
INSERT INTO `tb_notifikasi` VALUES (368, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:49:32', NULL);
INSERT INTO `tb_notifikasi` VALUES (369, 'Pekerjaan', 11, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:49:32', NULL);
INSERT INTO `tb_notifikasi` VALUES (370, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:49:32', NULL);
INSERT INTO `tb_notifikasi` VALUES (371, 'Pekerjaan', 11, 14, '29', 'Penetapan Pemenang dimulai pada 2021-03-10 jam 10:00 dan berakhir pada 2021-03-10 jam 15:00', 1, '2021-03-10 11:49:46', NULL);
INSERT INTO `tb_notifikasi` VALUES (372, 'Pekerjaan', 11, 14, '29', 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-10 11:50:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (373, 'Pekerjaan', 11, 14, NULL, 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-10 11:50:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (374, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:52:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (375, 'Pekerjaan', 11, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:52:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (376, 'Pekerjaan', 11, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 11:52:31', NULL);
INSERT INTO `tb_notifikasi` VALUES (377, 'Pekerjaan', 12, 14, '29', 'Penunjukan Langsung kepada PT. Uul Mencari S1 untuk pekerjaan Test 3', 2, '2021-03-10 11:54:06', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (378, 'Pekerjaan', 12, 29, '21', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_notifikasi` VALUES (379, 'Pekerjaan', 12, 29, '22', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_notifikasi` VALUES (380, 'Pekerjaan', 12, 29, '14', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-03-10 11:54:20', '2021-03-17 16:20:03');
INSERT INTO `tb_notifikasi` VALUES (381, 'Pekerjaan', 12, 14, '29', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', 2, '2021-03-10 11:54:36', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (382, 'Pekerjaan', 12, 14, '29', 'Dokumen Kualifikasi Di Terima', 2, '2021-03-10 13:21:05', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (383, 'Pekerjaan', 12, 14, '29', 'Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', 2, '2021-03-10 13:21:25', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (384, 'Pekerjaan', 12, 29, '21', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test 3', 1, '2021-03-10 13:21:43', NULL);
INSERT INTO `tb_notifikasi` VALUES (385, 'Pekerjaan', 12, 29, '22', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test 3', 1, '2021-03-10 13:21:43', NULL);
INSERT INTO `tb_notifikasi` VALUES (386, 'Pekerjaan', 12, 29, '14', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Test 3', 2, '2021-03-10 13:21:43', '2021-03-17 16:20:03');
INSERT INTO `tb_notifikasi` VALUES (387, 'Pekerjaan', 12, 14, '29', 'Panitia memberi jawaban', 2, '2021-03-10 13:21:55', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (388, 'Pekerjaan', 12, 14, '29', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-03-10 13:22:46', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (389, 'Pekerjaan', 12, 14, '21', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-10 13:22:46', NULL);
INSERT INTO `tb_notifikasi` VALUES (390, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 13:23:02', NULL);
INSERT INTO `tb_notifikasi` VALUES (391, 'Pekerjaan', 12, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 13:23:02', NULL);
INSERT INTO `tb_notifikasi` VALUES (392, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 13:23:02', NULL);
INSERT INTO `tb_notifikasi` VALUES (393, 'Pekerjaan', 12, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', 2, '2021-03-10 13:23:20', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (394, 'Pekerjaan', 12, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1000000000', 1, '2021-03-10 13:28:41', NULL);
INSERT INTO `tb_notifikasi` VALUES (395, 'Pekerjaan', 12, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1000000000', 1, '2021-03-10 13:28:41', NULL);
INSERT INTO `tb_notifikasi` VALUES (396, 'Pekerjaan', 12, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 1000000000', 2, '2021-03-10 13:28:41', '2021-03-17 16:20:03');
INSERT INTO `tb_notifikasi` VALUES (397, 'Pekerjaan', 12, 14, '29', 'Evaluasi Penawaran dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', 2, '2021-03-10 13:32:19', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (398, 'Pekerjaan', 12, 14, '29', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-03-10 13:36:54', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (399, 'Pekerjaan', 12, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 13:36:54', NULL);
INSERT INTO `tb_notifikasi` VALUES (400, 'Pekerjaan', 12, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 13:36:54', NULL);
INSERT INTO `tb_notifikasi` VALUES (401, 'Pekerjaan', 12, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 13:36:54', NULL);
INSERT INTO `tb_notifikasi` VALUES (402, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 13:39:25', NULL);
INSERT INTO `tb_notifikasi` VALUES (403, 'Pekerjaan', 12, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 13:39:25', NULL);
INSERT INTO `tb_notifikasi` VALUES (404, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 13:39:25', NULL);
INSERT INTO `tb_notifikasi` VALUES (405, 'Pekerjaan', 12, 14, '29', 'Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', 2, '2021-03-10 13:39:51', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (406, 'Pekerjaan', 12, 14, '29', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 2, '2021-03-10 13:44:24', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (407, 'Pekerjaan', 12, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 13:44:24', NULL);
INSERT INTO `tb_notifikasi` VALUES (408, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 13:54:09', NULL);
INSERT INTO `tb_notifikasi` VALUES (409, 'Pekerjaan', 12, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 13:54:09', NULL);
INSERT INTO `tb_notifikasi` VALUES (410, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 13:54:09', NULL);
INSERT INTO `tb_notifikasi` VALUES (411, 'Pekerjaan', 12, 14, '29', 'Penetapan Pemenang dimulai pada 2021-03-10 jam 12:00 dan berakhir pada 2021-03-10 jam 16:00', 2, '2021-03-10 13:54:28', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (412, 'Pekerjaan', 12, 14, '29', 'Pemenang Lelang PT. Uul Mencari S1', 2, '2021-03-10 13:55:37', '2021-03-12 10:51:53');
INSERT INTO `tb_notifikasi` VALUES (413, 'Pekerjaan', 12, 14, NULL, 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-10 13:55:37', NULL);
INSERT INTO `tb_notifikasi` VALUES (414, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (415, 'Pekerjaan', 12, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (416, 'Pekerjaan', 12, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (417, 'Pekerjaan', 12, NULL, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (418, 'Pekerjaan', 12, NULL, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (419, 'Pekerjaan', 12, NULL, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (420, 'Pekerjaan', 12, NULL, '14', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-03-10 14:00:56', '2021-03-17 16:20:03');
INSERT INTO `tb_notifikasi` VALUES (421, 'Pekerjaan', 12, NULL, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:59', NULL);
INSERT INTO `tb_notifikasi` VALUES (422, 'Pekerjaan', 12, NULL, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:59', NULL);
INSERT INTO `tb_notifikasi` VALUES (423, 'Pekerjaan', 12, NULL, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 14:00:59', NULL);
INSERT INTO `tb_notifikasi` VALUES (424, 'Pekerjaan', 12, NULL, '14', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 2, '2021-03-10 14:00:59', '2021-03-17 16:20:03');
INSERT INTO `tb_notifikasi` VALUES (425, 'Pekerjaan', 13, 14, '29', 'Pelelangan Terbatas kepada PT. Uul Mencari S1 untuk pekerjaan Test Terbuka 1', 2, '2021-03-10 15:15:32', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (426, 'Pekerjaan', 13, 29, '21', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_notifikasi` VALUES (427, 'Pekerjaan', 13, 29, '22', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_notifikasi` VALUES (428, 'Pekerjaan', 13, 29, '14', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-03-10 15:18:45', '2021-03-10 15:48:59');
INSERT INTO `tb_notifikasi` VALUES (429, 'Pekerjaan', 13, 14, '29', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-10 jam 15:00', 2, '2021-03-10 15:19:55', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (430, 'Pekerjaan', 13, 14, '29', 'Dokumen Kualifikasi Di Terima', 2, '2021-03-10 15:22:25', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (431, 'Pekerjaan', 13, 14, '29', 'Pelaksanaan Aanwijzing dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', 2, '2021-03-10 15:22:46', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (432, 'Pekerjaan', 13, 14, '29', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-03-10 15:28:49', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (433, 'Pekerjaan', 13, 14, '22', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-10 15:28:49', NULL);
INSERT INTO `tb_notifikasi` VALUES (434, 'Pekerjaan', 13, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 15:29:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (435, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 15:29:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (436, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-10 15:29:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (437, 'Pekerjaan', 13, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-10 jam 15:09 dan berakhir pada 2021-03-10 jam 18:00', 2, '2021-03-10 15:29:40', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (438, 'Pekerjaan', 13, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 1, '2021-03-10 15:34:40', NULL);
INSERT INTO `tb_notifikasi` VALUES (439, 'Pekerjaan', 13, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 1, '2021-03-10 15:34:40', NULL);
INSERT INTO `tb_notifikasi` VALUES (440, 'Pekerjaan', 13, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 2, '2021-03-10 15:34:40', '2021-03-10 15:48:59');
INSERT INTO `tb_notifikasi` VALUES (441, 'Pekerjaan', 13, 14, '29', 'Evaluasi Penawaran dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', 2, '2021-03-10 15:35:13', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (442, 'Pekerjaan', 13, 14, '29', 'Berita Acara Penawaran mohon ditandatangani', 2, '2021-03-10 15:36:11', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (443, 'Pekerjaan', 13, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 15:36:11', NULL);
INSERT INTO `tb_notifikasi` VALUES (444, 'Pekerjaan', 13, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 15:36:11', NULL);
INSERT INTO `tb_notifikasi` VALUES (445, 'Pekerjaan', 13, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-10 15:36:11', NULL);
INSERT INTO `tb_notifikasi` VALUES (446, 'Pekerjaan', 13, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 15:37:28', NULL);
INSERT INTO `tb_notifikasi` VALUES (447, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 15:37:28', NULL);
INSERT INTO `tb_notifikasi` VALUES (448, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-10 15:37:28', NULL);
INSERT INTO `tb_notifikasi` VALUES (449, 'Pekerjaan', 13, 14, '29', 'Klarifikasi & Negosiasi dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', 2, '2021-03-10 15:37:52', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (450, 'Pekerjaan', 13, 14, '29', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 2, '2021-03-10 15:43:03', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (451, 'Pekerjaan', 13, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-10 15:43:03', NULL);
INSERT INTO `tb_notifikasi` VALUES (452, 'Pekerjaan', 13, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:43:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (453, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:43:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (454, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:43:12', NULL);
INSERT INTO `tb_notifikasi` VALUES (455, 'Pekerjaan', 13, 14, '29', 'Penetapan Pemenang dimulai pada 2021-03-10 jam 15:00 dan berakhir pada 2021-03-10 jam 18:00', 2, '2021-03-10 15:43:32', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (456, 'Pekerjaan', 13, 14, '29', 'Pemenang Lelang PT. Uul Mencari S1', 2, '2021-03-10 15:48:46', '2021-03-10 15:57:45');
INSERT INTO `tb_notifikasi` VALUES (457, 'Pekerjaan', 13, 14, NULL, 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-10 15:48:46', NULL);
INSERT INTO `tb_notifikasi` VALUES (458, 'Pekerjaan', 13, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:48:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (459, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:48:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (460, 'Pekerjaan', 13, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-10 15:48:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (461, 'Pekerjaan', 10, 29, '21', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_notifikasi` VALUES (462, 'Pekerjaan', 10, 29, '22', 'PT. Uul Mencari S1 Menerima Pekerjaan', 1, '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_notifikasi` VALUES (463, 'Pekerjaan', 10, 29, '14', 'PT. Uul Mencari S1 Menerima Pekerjaan', 2, '2021-03-10 16:03:14', '2021-03-12 10:38:55');
INSERT INTO `tb_notifikasi` VALUES (464, 'Pekerjaan', 10, 14, '29', 'Pemasukan Dokumen Kualifikasi berakhir pada 2021-03-12 jam 16:00', 2, '2021-03-12 09:40:36', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (465, 'Pekerjaan', 10, 14, '29', 'Dokumen Kualifikasi Di Terima', 2, '2021-03-12 10:00:47', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (466, 'Pekerjaan', 10, 14, '29', 'Pelaksanaan Aanwijzing dimulai pada 2021-03-12 jam 09:00 dan berakhir pada 2021-03-12 jam 14:00', 2, '2021-03-12 10:01:09', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (467, 'Pekerjaan', 10, 29, '21', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Pekerjaan Test', 1, '2021-03-12 10:03:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (468, 'Pekerjaan', 10, 29, '22', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Pekerjaan Test', 1, '2021-03-12 10:03:56', NULL);
INSERT INTO `tb_notifikasi` VALUES (469, 'Pekerjaan', 10, 29, '14', 'PT. Uul Mencari S1 Memberi Pertanyaan pada pekerjaan Pekerjaan Test', 2, '2021-03-12 10:03:56', '2021-03-12 10:38:55');
INSERT INTO `tb_notifikasi` VALUES (470, 'Pekerjaan', 10, 14, '29', 'Panitia memberi jawaban', 2, '2021-03-12 10:04:09', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (471, 'Pekerjaan', 10, 14, '29', 'Berita Acara Aanwijzing mohon ditandatangani', 2, '2021-03-12 10:05:34', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (472, 'Pekerjaan', 10, 14, '21', 'Berita Acara Aanwijzing mohon ditandatangani', 1, '2021-03-12 10:05:34', NULL);
INSERT INTO `tb_notifikasi` VALUES (473, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-12 10:07:26', NULL);
INSERT INTO `tb_notifikasi` VALUES (474, 'Pekerjaan', 10, 14, '22', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-12 10:07:26', NULL);
INSERT INTO `tb_notifikasi` VALUES (475, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Aanwizjing', 1, '2021-03-12 10:07:26', NULL);
INSERT INTO `tb_notifikasi` VALUES (476, 'Pekerjaan', 10, 14, '29', 'Pemasukan Penawaran dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', 2, '2021-03-12 10:14:56', '2021-03-12 10:20:26');
INSERT INTO `tb_notifikasi` VALUES (477, 'Pekerjaan', 10, 29, '21', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 1, '2021-03-12 10:19:39', NULL);
INSERT INTO `tb_notifikasi` VALUES (478, 'Pekerjaan', 10, 29, '22', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 1, '2021-03-12 10:19:39', NULL);
INSERT INTO `tb_notifikasi` VALUES (479, 'Pekerjaan', 10, 29, '14', 'PT. Uul Mencari S1 Memasukan Penawaran baru senilai Rp. 100000000', 2, '2021-03-12 10:19:39', '2021-03-12 10:38:55');
INSERT INTO `tb_notifikasi` VALUES (480, 'Pekerjaan', 10, 14, '29', 'Evaluasi Penawaran dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', 1, '2021-03-12 10:23:48', NULL);
INSERT INTO `tb_notifikasi` VALUES (481, 'Pekerjaan', 10, 14, '29', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-12 10:36:15', NULL);
INSERT INTO `tb_notifikasi` VALUES (482, 'Pekerjaan', 10, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-12 10:36:15', NULL);
INSERT INTO `tb_notifikasi` VALUES (483, 'Pekerjaan', 10, 14, '22', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-12 10:36:15', NULL);
INSERT INTO `tb_notifikasi` VALUES (484, 'Pekerjaan', 10, 14, '21', 'Berita Acara Penawaran mohon ditandatangani', 1, '2021-03-12 10:36:15', NULL);
INSERT INTO `tb_notifikasi` VALUES (485, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-12 10:36:37', NULL);
INSERT INTO `tb_notifikasi` VALUES (486, 'Pekerjaan', 10, 14, '22', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-12 10:36:38', NULL);
INSERT INTO `tb_notifikasi` VALUES (487, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Penawaran', 1, '2021-03-12 10:36:38', NULL);
INSERT INTO `tb_notifikasi` VALUES (488, 'Pekerjaan', 10, 14, '29', 'Klarifikasi & Negosiasi dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', 1, '2021-03-12 10:36:54', NULL);
INSERT INTO `tb_notifikasi` VALUES (489, 'Pekerjaan', 10, 14, '29', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-12 10:37:25', NULL);
INSERT INTO `tb_notifikasi` VALUES (490, 'Pekerjaan', 10, 14, '21', 'Berita Acara Klarifikasi & Negosiasi mohon ditandatangani', 1, '2021-03-12 10:37:25', NULL);
INSERT INTO `tb_notifikasi` VALUES (491, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:37:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (492, 'Pekerjaan', 10, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:37:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (493, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:37:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (494, 'Pekerjaan', 10, 14, '29', 'Penetapan Pemenang dimulai pada 2021-03-12 jam 10:00 dan berakhir pada 2021-03-12 jam 16:00', 1, '2021-03-12 10:37:49', NULL);
INSERT INTO `tb_notifikasi` VALUES (495, 'Pekerjaan', 10, 14, '29', 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-12 10:38:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (496, 'Pekerjaan', 10, 14, NULL, 'Pemenang Lelang PT. Uul Mencari S1', 1, '2021-03-12 10:38:35', NULL);
INSERT INTO `tb_notifikasi` VALUES (497, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:38:47', NULL);
INSERT INTO `tb_notifikasi` VALUES (498, 'Pekerjaan', 10, 14, '22', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:38:47', NULL);
INSERT INTO `tb_notifikasi` VALUES (499, 'Pekerjaan', 10, 14, '21', 'Admin Menandatangani Berita Acara Klarifikasi & Negosiasi', 1, '2021-03-12 10:38:47', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan`;
CREATE TABLE `tb_pekerjaan`  (
  `id_pekerjaan` int NOT NULL AUTO_INCREMENT,
  `kode_pekerjaan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dokumen_pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_vendor` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_committee` int NULL DEFAULT NULL,
  `id_kategori_pekerjaan` int NULL DEFAULT NULL,
  `metode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `anggaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kualifikasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nilai_pagu` bigint NULL DEFAULT NULL,
  `id_divisi` int NULL DEFAULT NULL,
  `valid_from` date NULL DEFAULT NULL,
  `valid_to` date NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pekerjaan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan
-- ----------------------------
INSERT INTO `tb_pekerjaan` VALUES (7, 'GWTFWN43CT', 'Test', 'WTW9YiiPmB-informasi-pekerjaan.pdf', 'dtnLyvKV8v-dokumen-pekerjaan.pdf', 'Test', '31', 6, 1, 'Penunjukan Langsung', '2021', 'Semarang', 'Perusahaan Besar', 1000000000, 1, '2021-01-01', '2021-01-31', 3, '2021-01-07 10:09:17', '2021-01-07 11:17:44');
INSERT INTO `tb_pekerjaan` VALUES (8, '9OCFJA2DPB', 'Pekerjaan Pembuatan Jembatan', 'nJRU8bUn1B-informasi-pekerjaan.pdf', 'xmUJCHxsoR-dokumen-pekerjaan.pdf', 'Jembatan dengan batu koral', '123', 6, 2, 'Penunjukan Langsung', 'APBN 2021', 'Semarangt', 'Perusahaan Menengah', 120000000, 1, '2021-01-07', '2021-01-29', 3, '2021-01-14 10:42:21', '2021-01-14 10:51:42');
INSERT INTO `tb_pekerjaan` VALUES (9, 'IZJWWLEQ0X', 'test 2', 'SvlJgCk3EV-informasi-pekerjaan.pdf', 'c12pZFyNZ9-dokumen-pekerjaan.pdf', 'test 2', '123', 6, 1, 'Penunjukan Langsung', '2021', 'Semarang', 'Perusahaan Menengah', 100000000, 1, '2021-01-13', '2021-01-30', 3, '2021-01-14 11:40:57', '2021-01-14 11:43:55');
INSERT INTO `tb_pekerjaan` VALUES (10, 'TECZOZVATB', 'Pekerjaan Test', 'xsxmTrR2fM-informasi-pekerjaan.pdf', 'DqPZNdEdlC-dokumen-pekerjaan.pdf', 'Test Pekerjaan Lelang Umum', '31', 6, 2, 'Pelelangan Umum', 'APBN 2021', 'Semarang', 'Perusahaan Menengah', 125000000, 1, '2021-01-19', '2021-01-31', 3, '2021-01-20 10:46:41', '2021-03-12 10:00:47');
INSERT INTO `tb_pekerjaan` VALUES (11, 'WML7VK7YX0', 'test 2', '2JdFsKBpiX-informasi-pekerjaan.pdf', 'HfdsMh0ypP-dokumen-pekerjaan.pdf', 'njkkjnkjn', '31', 6, 1, 'Penunjukan Langsung', '2020', 'jdnkfjansdf', 'Perusahaan Menengah', 1000000000, 1, '2021-03-10', '2021-04-10', 3, '2021-03-10 11:26:07', '2021-03-10 11:28:18');
INSERT INTO `tb_pekerjaan` VALUES (12, 'O5XQ1NFUMQ', 'Test 3', 'mnEQsOIuax-informasi-pekerjaan.pdf', '3giKtOAAhi-dokumen-pekerjaan.pdf', 'hjhgbjhbvvjhv', '31', 6, 2, 'Penunjukan Langsung', '2021', 'kjbkbkb', 'Perusahaan Menengah', 10000000, 1, '2021-03-10', '2021-04-01', 3, '2021-03-10 11:54:06', '2021-03-10 13:21:05');
INSERT INTO `tb_pekerjaan` VALUES (13, 'ROPOPVHZOG', 'Test Terbuka 1', 'wMd7VZfo0n-informasi-pekerjaan.pdf', 'cLo9IoGeNi-dokumen-pekerjaan.pdf', 'vsflkvnslnvsv', '31', 6, 2, 'Pelelangan Terbatas', '2020', 'Semarang', 'Perusahaan Menengah', 1000000000, 1, '2021-03-01', '2021-04-10', 3, '2021-03-10 15:15:32', '2021-03-10 15:22:25');
INSERT INTO `tb_pekerjaan` VALUES (14, 'VQJFZGLPHW', 'Test XX', '1yKB6iqvl9-informasi-pekerjaan.pdf', 'ri9yar9myn-dokumen-pekerjaan.pdf', 'Oke sip', NULL, 6, 3, 'Pelelangan Umum', '2021', 'Semarang', 'Perusahaan Kecil', 200000000, 1, '2021-03-01', '2021-03-25', 0, '2021-03-18 14:09:44', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_aanwijzing
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_aanwijzing`;
CREATE TABLE `tb_pekerjaan_aanwijzing`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_message` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `pertanyaan` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `jawaban` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_aanwijzing
-- ----------------------------
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (7, 7, 31, '<p>test tanya</p>', '<p>okey</p>', '2021-01-07 11:44:18', '2021-01-07 12:06:59');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (7, 7, 31, '<p>bolehkan tanya</p>', '<p>boleh</p>', '2021-01-07 11:44:26', '2021-01-07 11:52:37');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (8, 8, 123, '<p>Pertanyaan 1?</p>', '<p>Jawaban 4</p>', '2021-01-14 10:56:17', '2021-01-14 10:58:34');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (8, 8, 123, '<p>Pertanyaan 2?</p>', '<p>Jawaban 2</p>', '2021-01-14 10:56:47', '2021-01-14 10:57:35');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (9, 9, 123, '<p>pertanyaan 1 ?</p>', '<p>Jawaban 1</p>', '2021-01-14 11:44:31', '2021-01-14 11:54:14');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (9, 9, 123, '<p>Pertanyaan 2?</p>', '<p>Jawaban 2</p>', '2021-01-14 11:44:41', '2021-01-14 11:54:21');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (9, 9, 123, '<p>Pertanyaan 3?</p>', '<p>Jawaban 3</p>', '2021-01-14 11:44:48', '2021-01-14 11:54:30');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (11, 11, 31, '<p>test ahh</p>', '<p>Okay</p>', '2021-03-10 11:28:50', '2021-03-10 11:29:06');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (12, 12, 31, '<p>test</p>', '<p>ya</p>', '2021-03-10 13:21:43', '2021-03-10 13:21:55');
INSERT INTO `tb_pekerjaan_aanwijzing` VALUES (10, 10, 31, '<p>oi oi</p>', '<p>ya</p>', '2021-03-12 10:03:56', '2021-03-12 10:04:09');

-- ----------------------------
-- Table structure for tb_pekerjaan_aanwijzing_ba
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_aanwijzing_ba`;
CREATE TABLE `tb_pekerjaan_aanwijzing_ba`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `no_ba` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pimpinan_rapat` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `jabatan_pimpinan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dimulai_jam` time(0) NULL DEFAULT NULL,
  `hasil` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `attachment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_aanwijzing_ba
-- ----------------------------
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (7, '001', 'Ruang Rapat', 21, 'dDAW0VZolm.png', 2, 'Ketua Panitia', '12:00:00', '<p>cnsdlvnsv</p><p>sv</p><p>sdv</p><p>svd</p><p>dsvsdvsdv</p><p><br></p><p><br></p><p><br></p><p>sdvsdvs</p><p>dvsd</p><p>v</p><p>sdv</p><p>sdv</p><p>sdv</p><p><br></p><p><br></p><p><br></p><p>sdvsdvsdvs</p><p>dv</p><p>sdv</p><p>sdv</p><p>sdv</p><p>sdv</p><p>sdv</p><p><br></p><p><br></p><p><br></p><p><br></p><p>sdvdsv</p><p>sdvsdvsdvsdvsdv</p><p>sv</p><p>sdv</p><p>sdv</p><p>sv</p><p>s</p><p>vsdvsdvsdvsdv</p><p><br></p><p><br></p><p>svsvdsdvsdvsdvs</p><p>vs</p><p>dv</p><p>sdv</p><p>sv</p><p>s</p><p>vs</p><p>v</p>', NULL, 2, '2021-01-07 13:26:17', '2021-01-07 13:29:49');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (8, '123', 'Ruang Rapat', 21, 'bqWhon5yW9.png', 2, 'Ketua Panitia', '12:00:00', '<p>Hasil Rapat isi disini</p>', NULL, 2, '2021-01-14 11:04:11', '2021-01-14 11:05:33');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (9, '0020/H/003839', 'Ruang Rapat PT. KIW', 22, '7xrtZgdcpr.png', 2, 'Ketua Panitia', '12:00:00', '<p>TEst Aja Sih INi</p>', 'arLjPzFeO6G1QIAl.pdf,MAdW3WaJeVwnylJi.pdf', 2, '2021-03-04 15:29:50', '2021-03-08 14:13:42');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (10, 'hdjha880898', 'Ruang Rapat', 21, '803WwekBTD.png', 2, 'Ketua', '12:00:00', '<p>ksdjsdhfjadhfsd</p><p>adasd</p><p>a</p><p>dasd</p><p>asd</p>', 'z2S2xYXbi9HwfKME.png,xhlDY4kt5j4cTeTT.png', 2, '2021-03-12 10:05:34', '2021-03-12 10:07:26');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (11, 'jnacjncaljn', 'jnjnjknj', 21, 'sSNBNiMcSV.png', 2, 'jkncskjnvsknv', '12:00:00', '<p>kn kcn kaca</p>', '3J2vZ6ZyEGZHMIWi.png,Gj7fxut6FHuGkjwv.png,hmzG87yr5ONvdQRT.pdf', 2, '2021-03-10 11:32:34', '2021-03-10 11:33:53');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (12, 'h jhbjhv', 'jhhjbjhbjhb', 21, 'AzmYeq0Enh.png', 2, 'jkbkjbkb', '12:00:00', '<p>khbhkvjhvv</p>', 'KXeCkcAhDopt66I5.png,tkUM5YmgOtEFqh3L.png,t2CIUR2fRqMXbn42.png,VH31gBXflrbaVQ8y.pdf', 2, '2021-03-10 13:22:46', '2021-03-10 13:23:02');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (13, 'bkhjhbhb', 'jhbjbjb', 22, '56m87od4L2.png', 2, 'jkjbkb', '12:00:00', '<p>kjbkbkbjkb</p>', 'Rku8hkE98ygD35VR.png,j0h9TUWpi9b5jkfm.png,9oR8LowCPMdutRNa.pdf', 2, '2021-03-10 15:28:49', '2021-03-10 15:29:12');
INSERT INTO `tb_pekerjaan_aanwijzing_ba` VALUES (14, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '2021-03-18 14:09:44', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_aanwijzing_ba_penyedia
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_aanwijzing_ba_penyedia`;
CREATE TABLE `tb_pekerjaan_aanwijzing_ba_penyedia`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_aanwijzing_ba_penyedia
-- ----------------------------
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (7, 31, 29, 'z7alTWBlnO.png', 2, '2021-01-07 13:26:17', '2021-01-07 13:29:33');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (8, 123, 35, 'WqT1Sme5N1.png', 2, '2021-01-14 11:04:11', '2021-01-14 11:04:56');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (9, 123, 35, '7xrtZgdcpr.png', 2, '2021-03-04 15:29:50', '2021-03-08 14:13:42');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (11, 31, 29, 'sSNBNiMcSV.png', 2, '2021-03-10 11:32:34', '2021-03-10 11:33:53');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (12, 31, 29, 'AzmYeq0Enh.png', 2, '2021-03-10 13:22:46', '2021-03-10 13:23:02');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (13, 31, 29, '56m87od4L2.png', 2, '2021-03-10 15:28:49', '2021-03-10 15:29:12');
INSERT INTO `tb_pekerjaan_aanwijzing_ba_penyedia` VALUES (10, 31, 29, '803WwekBTD.png', 2, '2021-03-12 10:05:34', '2021-03-12 10:07:26');

-- ----------------------------
-- Table structure for tb_pekerjaan_aanwijzing_ba_peserta
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_aanwijzing_ba_peserta`;
CREATE TABLE `tb_pekerjaan_aanwijzing_ba_peserta`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_aanwijzing_ba_peserta
-- ----------------------------
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (7, 'bkhbkbjk', 'jbkjbkjb', '2021-01-07 13:26:17', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (7, 'bkhkjkj', 'kjkbkjb', '2021-01-07 13:26:17', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (7, 'kjbkbkb', 'kjbkbkb', '2021-01-07 13:26:17', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (8, 'Anggota 1', 'Panitia', '2021-01-14 11:04:11', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (8, 'Anggota 2', 'Panitia', '2021-01-14 11:04:11', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (9, 'Test 1', 'Anggota', '2021-03-04 15:29:50', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (9, 'Test 2', 'Anggota', '2021-03-04 15:29:50', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (11, 'jnkjn', 'knkjnk', '2021-03-10 11:32:34', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (12, 'bhbb', 'hjbjhbjhb', '2021-03-10 13:22:46', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (13, 'kjnkj', 'kjkjbkjb', '2021-03-10 15:28:49', NULL);
INSERT INTO `tb_pekerjaan_aanwijzing_ba_peserta` VALUES (10, 'TEst', 'fyfhfj', '2021-03-12 10:05:34', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_dokumen_kontrak
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_dokumen_kontrak`;
CREATE TABLE `tb_pekerjaan_dokumen_kontrak`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `dokumen_kontrak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_dokumen_kontrak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_dokumen_kontrak
-- ----------------------------
INSERT INTO `tb_pekerjaan_dokumen_kontrak` VALUES (13, 'Test', 'VbPuHmkeyO-Test.pdf', '2021-03-12 11:17:34', NULL);
INSERT INTO `tb_pekerjaan_dokumen_kontrak` VALUES (13, 'Test 2', 'Brg9UkkcKZ-Test 2.pdf', '2021-03-12 11:46:32', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_evaluasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_evaluasi`;
CREATE TABLE `tb_pekerjaan_evaluasi`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `notulen` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pemenang` int NULL DEFAULT NULL,
  `file_notulen` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_evaluasi` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_evaluasi
-- ----------------------------
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (7, '<p>kgkdb</p><p>dsvs</p><p>vs</p><p>fvs</p><p>v</p><p>sv</p><p>sdv</p><p>sdv</p>', 31, 'doabwJMWDg-notulen-evaluasi-penawaran.pdf', 2, '2021-01-07 15:10:32', '2021-01-07 15:21:16');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (8, '<p>isi Notulen</p>', 123, 'IlnUqDPRrM-notulen-evaluasi-penawaran.pdf', 2, '2021-01-14 11:17:52', '2021-01-14 11:20:31');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (9, '<p>hwfjwekfjwhefkjwehf</p>', 123, 'dwLXgyaY5M-notulen-evaluasi-penawaran.pdf', 2, '2021-03-08 14:58:12', '2021-03-08 15:11:30');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (11, '<p>jkvnksvkjsdvndjsn</p>', 31, 'PWxqnL61K2-notulen-evaluasi-penawaran.pdf', 2, '2021-03-10 11:43:12', '2021-03-10 11:46:55');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (12, '<p>jhvbjhjhv</p>', 31, 'cqfS50tc7Z-notulen-evaluasi-penawaran.pdf', 2, '2021-03-10 13:32:19', '2021-03-10 13:36:54');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (13, '<p>kjbkbkbkjbkjb</p>', 31, 'BpcIk19RNj-notulen-evaluasi-penawaran.pdf', 2, '2021-03-10 15:35:13', '2021-03-10 15:36:11');
INSERT INTO `tb_pekerjaan_evaluasi` VALUES (10, '<p>lkdamflkdmf</p>', 31, 'BS2cHFmpPH-notulen-evaluasi-penawaran.pdf', 2, '2021-03-12 10:23:48', '2021-03-12 10:36:15');

-- ----------------------------
-- Table structure for tb_pekerjaan_jadwal
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_jadwal`;
CREATE TABLE `tb_pekerjaan_jadwal`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `nama_jadwal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_mulai` date NULL DEFAULT NULL,
  `jam_mulai` time(0) NULL DEFAULT NULL,
  `tgl_selesai` date NULL DEFAULT NULL,
  `jam_selesai` time(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_jadwal
-- ----------------------------
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Pemasukan Dokumen Kualifikasi', '2021-01-07', '10:12:55', '2021-01-07', '11:30:00', '2021-01-07 10:12:55', '2021-01-07 10:13:52');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Pelaksanaan Aanwijzing', '2021-01-07', '11:00:00', '2021-01-07', '14:00:00', '2021-01-07 11:21:09', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Pemasukan Penawaran', '2021-01-07', '13:00:00', '2021-01-07', '15:30:00', '2021-01-07 13:32:27', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Evaluasi Penawaran', '2021-01-07', '15:00:00', '2021-01-07', '16:00:00', '2021-01-07 15:10:32', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Klarifikasi & Negosiasi', '2021-01-07', '15:31:00', '2021-01-07', '16:00:00', '2021-01-07 15:31:10', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (7, 'Penetapan Pemenang', '2021-01-07', '15:30:00', '2021-01-07', '16:00:00', '2021-01-07 15:37:27', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Pemasukan Dokumen Kualifikasi', '2021-01-14', '10:45:04', '2021-01-14', '11:00:00', '2021-01-14 10:45:04', '2021-01-14 10:47:02');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Pelaksanaan Aanwijzing', '2021-01-14', '10:00:00', '2021-01-14', '11:00:00', '2021-01-14 10:54:35', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Pemasukan Penawaran', '2021-01-14', '11:00:00', '2021-01-14', '12:00:00', '2021-01-14 11:05:58', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Evaluasi Penawaran', '2021-01-14', '11:00:00', '2021-01-14', '12:00:00', '2021-01-14 11:17:52', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Klarifikasi & Negosiasi', '2021-01-14', '11:00:00', '2021-01-14', '12:00:00', '2021-01-14 11:26:05', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (8, 'Penetapan Pemenang', '2021-01-14', '11:00:00', '2021-01-14', '12:00:00', '2021-01-14 11:27:53', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Pemasukan Dokumen Kualifikasi', '2021-01-14', '11:43:04', '2021-01-14', '12:00:00', '2021-01-14 11:43:04', '2021-01-14 11:43:15');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Pelaksanaan Aanwijzing', '2021-01-14', '10:00:00', '2021-01-14', '15:00:00', '2021-01-14 11:44:18', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Pemasukan Penawaran', '2021-03-08', '12:00:00', '2021-03-08', '16:00:00', '2021-03-08 14:14:19', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Evaluasi Penawaran', '2021-03-08', '14:58:00', '2021-03-08', '16:00:00', '2021-03-08 14:58:12', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Klarifikasi & Negosiasi', '2021-03-10', '09:39:00', '2021-03-10', '15:45:00', '2021-03-10 09:39:18', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (9, 'Penetapan Pemenang', '2021-03-10', '11:00:00', '2021-03-10', '16:00:00', '2021-03-10 11:06:15', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Pemasukan Dokumen Kualifikasi', '2021-03-10', '11:26:58', '2021-03-10', '15:00:00', '2021-03-10 11:26:58', '2021-03-10 11:27:22');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Pelaksanaan Aanwijzing', '2021-03-10', '10:00:00', '2021-03-10', '15:00:00', '2021-03-10 11:28:36', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Pemasukan Penawaran', '2021-03-10', '11:00:00', '2021-03-10', '15:00:00', '2021-03-10 11:35:05', '2021-03-10 11:40:40');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Evaluasi Penawaran', '2021-03-10', '10:00:00', '2021-03-10', '15:00:00', '2021-03-10 11:43:12', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Klarifikasi & Negosiasi', '2021-03-10', '10:00:00', '2021-03-10', '15:00:00', '2021-03-10 11:49:00', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (11, 'Penetapan Pemenang', '2021-03-10', '10:00:00', '2021-03-10', '15:00:00', '2021-03-10 11:49:46', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Pemasukan Dokumen Kualifikasi', '2021-03-10', '11:54:20', '2021-03-10', '15:00:00', '2021-03-10 11:54:20', '2021-03-10 11:54:36');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Pelaksanaan Aanwijzing', '2021-03-10', '12:00:00', '2021-03-10', '16:00:00', '2021-03-10 13:21:25', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Pemasukan Penawaran', '2021-03-10', '12:00:00', '2021-03-10', '16:00:00', '2021-03-10 13:23:20', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Evaluasi Penawaran', '2021-03-10', '12:00:00', '2021-03-10', '16:00:00', '2021-03-10 13:32:19', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Klarifikasi & Negosiasi', '2021-03-10', '12:00:00', '2021-03-10', '16:00:00', '2021-03-10 13:39:51', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (12, 'Penetapan Pemenang', '2021-03-10', '12:00:00', '2021-03-10', '16:00:00', '2021-03-10 13:54:28', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Pemasukan Dokumen Kualifikasi', '2021-03-10', '15:18:45', '2021-03-10', '18:00:00', '2021-03-10 15:18:45', '2021-03-10 15:19:55');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Pelaksanaan Aanwijzing', '2021-03-10', '15:00:00', '2021-03-10', '18:00:00', '2021-03-10 15:22:46', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Pemasukan Penawaran', '2021-03-10', '15:09:00', '2021-03-10', '18:00:00', '2021-03-10 15:29:40', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Evaluasi Penawaran', '2021-03-10', '15:00:00', '2021-03-10', '18:00:00', '2021-03-10 15:35:13', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Klarifikasi & Negosiasi', '2021-03-10', '15:00:00', '2021-03-10', '18:00:00', '2021-03-10 15:37:52', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (13, 'Penetapan Pemenang', '2021-03-10', '15:00:00', '2021-03-10', '18:00:00', '2021-03-10 15:43:32', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Pemasukan Dokumen Kualifikasi', '2021-03-10', '16:03:14', '2021-03-12', '16:00:00', '2021-03-10 16:03:14', '2021-03-12 09:40:36');
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Pelaksanaan Aanwijzing', '2021-03-12', '09:00:00', '2021-03-12', '14:00:00', '2021-03-12 10:01:09', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Pemasukan Penawaran', '2021-03-12', '10:00:00', '2021-03-12', '16:00:00', '2021-03-12 10:14:56', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Evaluasi Penawaran', '2021-03-12', '10:00:00', '2021-03-12', '16:00:00', '2021-03-12 10:23:48', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Klarifikasi & Negosiasi', '2021-03-12', '10:00:00', '2021-03-12', '16:00:00', '2021-03-12 10:36:54', NULL);
INSERT INTO `tb_pekerjaan_jadwal` VALUES (10, 'Penetapan Pemenang', '2021-03-12', '10:00:00', '2021-03-12', '16:00:00', '2021-03-12 10:37:49', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_jadwal_addendum
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_jadwal_addendum`;
CREATE TABLE `tb_pekerjaan_jadwal_addendum`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `nama_jadwal_addendum` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_mulai_old` date NULL DEFAULT NULL,
  `jam_mulai_old` time(0) NULL DEFAULT NULL,
  `tgl_selesai_old` date NULL DEFAULT NULL,
  `jam_selesai_old` time(0) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_jadwal_addendum
-- ----------------------------

-- ----------------------------
-- Table structure for tb_pekerjaan_ketahui
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_ketahui`;
CREATE TABLE `tb_pekerjaan_ketahui`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `ketahui` int NULL DEFAULT NULL,
  `approve` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_ketahui
-- ----------------------------
INSERT INTO `tb_pekerjaan_ketahui` VALUES (7, 28, 0, '2021-01-07 10:09:17', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (8, 28, 0, '2021-01-14 10:42:21', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (9, 28, 0, '2021-01-14 11:40:57', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (10, 28, 0, '2021-01-20 10:46:41', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (11, 28, 0, '2021-03-10 11:26:07', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (12, 28, 0, '2021-03-10 11:54:06', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (13, 28, 0, '2021-03-10 15:15:32', NULL);
INSERT INTO `tb_pekerjaan_ketahui` VALUES (14, 28, 0, '2021-03-18 14:09:44', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_kualifikasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_kualifikasi`;
CREATE TABLE `tb_pekerjaan_kualifikasi`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `dokumen_kualifikasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_dokumen_kualifikasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_kualifikasi
-- ----------------------------
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'Akta Pendirian', 'P6bDkq8WQG-akta-pendirian.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'Akta Perubahan', 'KtQwvkxKWr-akta-perubahan.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'NPWP', 'BIxvItgSzX-npwp.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'PKP', 'TedJUoiRZa-pkp.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'IUJK / SIUP', 'RJBdNNFXFS-iujksiup.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'SBU', '1TIvudVvME-sbu.pdf', '2021-01-07 10:12:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'Surat Pernyataan Minat', 'WIv6Zo0xdr-Surat Pernyataan Minat.pdf', '2021-01-07 10:42:41', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (7, 31, 'Pakta Integritas', '2zHR9H1ouW-Pakta Integritas.pdf', '2021-01-07 11:13:08', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'Akta Pendirian', 'hPShGbASty-akta-pendirian.pdf', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'NPWP', 'KugJZcLVpv-npwp.pdf', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'PKP', 'a0vmYzDj5t-pkp.pdf', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'IUJK / SIUP', '123456_iujksiup.pdf', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'SBU', '987654_sbu.pdf', '2021-01-14 10:45:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'Pakta Integritas', 'o2WS98f8rK-Pakta Integritas.pdf', '2021-01-14 10:50:11', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (8, 123, 'Surat Pernyataan Minat', 'g4rGfvNW3l-Surat Pernyataan Minat.pdf', '2021-01-14 10:50:13', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (9, 123, 'Akta Pendirian', 'hPShGbASty-akta-pendirian.pdf', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (9, 123, 'NPWP', 'KugJZcLVpv-npwp.pdf', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (9, 123, 'PKP', 'a0vmYzDj5t-pkp.pdf', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (9, 123, 'IUJK / SIUP', '123456_iujksiup.pdf', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (9, 123, 'SBU', '987654_sbu.pdf', '2021-01-14 11:43:04', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'Akta Pendirian', 'P6bDkq8WQG-akta-pendirian.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'Akta Perubahan', 'KtQwvkxKWr-akta-perubahan.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'NPWP', 'BIxvItgSzX-npwp.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'PKP', 'TedJUoiRZa-pkp.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'IUJK / SIUP', 'RJBdNNFXFS-iujksiup.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'SBU', '1TIvudVvME-sbu.pdf', '2021-03-10 11:26:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'Pakta Integritas', 'OSScqVbLNo-Pakta Integritas.pdf', '2021-03-10 11:27:55', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (11, 31, 'Surat Pernyataan Minat', 'ikhVSgMO9z-Surat Pernyataan Minat.pdf', '2021-03-10 11:27:56', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'Akta Pendirian', 'P6bDkq8WQG-akta-pendirian.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'Akta Perubahan', 'KtQwvkxKWr-akta-perubahan.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'NPWP', 'BIxvItgSzX-npwp.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'PKP', 'TedJUoiRZa-pkp.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'IUJK / SIUP', 'RJBdNNFXFS-iujksiup.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'SBU', '1TIvudVvME-sbu.pdf', '2021-03-10 11:54:20', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (12, 31, 'Surat Pernyataan Minat', 'tf0SYyb7Eo-Surat Pernyataan Minat.pdf', '2021-03-10 11:57:58', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'Akta Pendirian', 'P6bDkq8WQG-akta-pendirian.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'Akta Perubahan', 'KtQwvkxKWr-akta-perubahan.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'NPWP', 'BIxvItgSzX-npwp.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'PKP', 'TedJUoiRZa-pkp.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'IUJK / SIUP', 'RJBdNNFXFS-iujksiup.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'SBU', '1TIvudVvME-sbu.pdf', '2021-03-10 15:18:45', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (13, 31, 'Surat Pernyataan Minat', 'O3qk8IcA9R-Surat Pernyataan Minat.pdf', '2021-03-10 15:21:59', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'Akta Pendirian', 'P6bDkq8WQG-akta-pendirian.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'Akta Perubahan', 'KtQwvkxKWr-akta-perubahan.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'NPWP', 'BIxvItgSzX-npwp.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'PKP', 'TedJUoiRZa-pkp.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'IUJK / SIUP', 'RJBdNNFXFS-iujksiup.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'SBU', '1TIvudVvME-sbu.pdf', '2021-03-10 16:03:14', NULL);
INSERT INTO `tb_pekerjaan_kualifikasi` VALUES (10, 31, 'Surat Pernyataan Minat', 'nvvt9lAs4y-Surat Pernyataan Minat.pdf', '2021-03-12 09:59:25', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_negosiasi_ba
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_negosiasi_ba`;
CREATE TABLE `tb_pekerjaan_negosiasi_ba`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `no_ba` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pimpinan_rapat` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `jabatan_pimpinan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dimulai_jam` time(0) NULL DEFAULT NULL,
  `hasil` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `attachment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_negosiasi_ba
-- ----------------------------
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (7, '986988787', 'Ruang Rapat', 21, 'mGcPQNfKCj.png', 2, 'Ketua Panitia', '12:00:00', '<p>ljblksfbjlsvblsjvnbsljvn</p><p>sv</p><p>sd</p><p>vsdvsd</p><p>v</p><p>sdv</p><p>sd</p><p>vs</p><p>dv</p>', NULL, 2, '2021-01-07 15:34:37', '2021-01-07 15:37:01');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (8, '3634543', 'Ruang Rapat', 21, 'P1QGnNg2uM.png', 2, 'Ketua Panitia', '12:00:00', '<p>Isi Hasil</p>', NULL, 2, '2021-01-14 11:26:41', '2021-01-14 11:27:28');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (9, 'hjbhjdc786ac98a6c', 'kbscvk bvjs', 21, 'pJ1ipDder4.png', 2, 'ksnbksjbvsdjbv', '12:00:00', '<p>kbsvksbv</p>', '', 2, '2021-03-10 10:56:30', '2021-03-10 11:05:58');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (11, 'kjbkbkb', 'kjbkjbkb', 21, 'jbbnML89QV.png', 2, 'kjbkhbb', '12:00:00', '<p>kjbjkbkbkb</p>', '', 2, '2021-03-10 11:49:24', '2021-03-10 11:49:32');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (12, 'bkjbkbkjb', 'jkbkjbjkb', 21, 'BzMRcywxta.png', 2, 'jnjnjn', '12:00:00', '<p>kjbkbkbkb</p>', 'gBse73KY9pJRqVrY.png,6NVAYYSDBOYcJCIY.png,z1l3OpG5qrLYscSp.png', 2, '2021-03-10 13:44:24', '2021-03-10 13:54:09');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (13, 'jksfvnkjsfv', 'kjnkbkjnkjb', 21, 'Muo2KvJtmR.png', 2, 'jnvljsdnvsnv', '12:00:00', '<p>,m nln lnlnlknlkn</p>', 'FQF1ysGG0LjmyjZk.png,VPqiz1zxicH1PVd2.png,8XQnrYDNc448tqOE.pdf', 2, '2021-03-10 15:43:03', '2021-03-10 15:43:12');
INSERT INTO `tb_pekerjaan_negosiasi_ba` VALUES (10, 'lnadfakfnafl', 'klnldsflsnf', 21, 'myBrilhMKh.png', 2, 'dsflkfksdnfdsf', '12:00:00', '<p>ljdsfnslfnsdlf</p>', 'u2NfzCcp76UFkvEd.png,qZBssU8ZHZQXYVPs.pdf', 2, '2021-03-12 10:37:25', '2021-03-12 10:37:35');

-- ----------------------------
-- Table structure for tb_pekerjaan_negosiasi_ba_penyedia
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_negosiasi_ba_penyedia`;
CREATE TABLE `tb_pekerjaan_negosiasi_ba_penyedia`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_negosiasi_ba_penyedia
-- ----------------------------
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (7, 31, 29, 'PopFcB7RQO.png', 2, '2021-01-07 15:34:37', '2021-01-07 15:36:36');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (8, 123, 35, 'SulHUJ2NuJ.png', 2, '2021-01-14 11:26:41', '2021-01-14 11:27:09');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (9, 123, 35, 'pJ1ipDder4.png', 2, '2021-03-10 10:56:30', '2021-03-10 11:05:58');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (11, 31, 29, 'jbbnML89QV.png', 2, '2021-03-10 11:49:24', '2021-03-10 11:49:32');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (12, 31, 29, 'BzMRcywxta.png', 2, '2021-03-10 13:44:24', '2021-03-10 13:54:09');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (13, 31, 29, 'Muo2KvJtmR.png', 2, '2021-03-10 15:43:03', '2021-03-10 15:43:12');
INSERT INTO `tb_pekerjaan_negosiasi_ba_penyedia` VALUES (10, 31, 29, 'myBrilhMKh.png', 2, '2021-03-12 10:37:25', '2021-03-12 10:37:35');

-- ----------------------------
-- Table structure for tb_pekerjaan_negosiasi_ba_peserta
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_negosiasi_ba_peserta`;
CREATE TABLE `tb_pekerjaan_negosiasi_ba_peserta`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_negosiasi_ba_peserta
-- ----------------------------
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (7, 21, '2021-01-07 15:34:37', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (7, 22, '2021-01-07 15:34:37', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (8, 21, '2021-01-14 11:26:41', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (8, 22, '2021-01-14 11:26:41', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (9, 21, '2021-03-10 10:56:30', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (9, 22, '2021-03-10 10:56:30', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (11, 21, '2021-03-10 11:49:24', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (11, 22, '2021-03-10 11:49:24', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (12, 21, '2021-03-10 13:44:24', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (12, 22, '2021-03-10 13:44:24', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (13, 21, '2021-03-10 15:43:03', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (13, 22, '2021-03-10 15:43:03', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (10, 21, '2021-03-12 10:37:25', NULL);
INSERT INTO `tb_pekerjaan_negosiasi_ba_peserta` VALUES (10, 22, '2021-03-12 10:37:25', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_pemenang
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_pemenang`;
CREATE TABLE `tb_pekerjaan_pemenang`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `no_surat_dir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_surat_dir` date NULL DEFAULT NULL,
  `no_surat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `harga_sepakat` bigint NULL DEFAULT NULL,
  `pelaksanaan` int NULL DEFAULT NULL,
  `pemeliharaan` int NULL DEFAULT NULL,
  `ketua_panitia` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `attachment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_pemenang
-- ----------------------------
INSERT INTO `tb_pekerjaan_pemenang` VALUES (7, '978690798', '2021-01-07', '0878797098', 31, 1000000000, 120, 190, 21, 'bFZ6snzWk6.png', 2, NULL, 2, '2021-01-07 15:42:58', '2021-01-07 15:43:35');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (8, '345353453', '2021-01-07', '3242424', 123, 100000000000, 130, 200, 21, 'NNbS99oXeH.png', 2, NULL, 2, '2021-01-14 11:28:42', '2021-01-14 11:28:52');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (9, 'bkbcadkcb', '2021-03-01', 'n djlnsv098s9-v809s8v', 123, 100000000, 120, 120, 21, 'JM8AZ68hwf.png', 2, '', 2, '2021-03-10 11:18:51', '2021-03-10 11:23:29');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (11, 'bkhbkbjkb', '2021-03-01', 'bkkjbkjb', 31, 12000000, 120, 100, 21, 'Rofl5dlLnb.png', 2, '7BhWoChmwtKp6QKg.png,EE3EJsljLYTo5G9h.png,EIa2N10NpJ9iCgwH.png,3wJbfsAoR3y8RRmB.pdf,fA8Vh7T5830m0I4t.pdf', 2, '2021-03-10 11:50:31', '2021-03-10 11:52:31');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (12, 'bjbhvhb', '2021-03-01', 'jdbf9wfy8wuf', 31, 100000000, 120, 100, 21, 'CExdqoOnJd.png', 2, 'L7LZ43IYxHlFvgyp.png,yZGfULTRKj570t3e.png,f966TkCvfLuQBm9x.png,rO8Sr01TrMP5fr7h.pdf,Z94zk6AODIXIO5TF.pdf', 2, '2021-03-10 13:55:37', '2021-03-10 14:00:59');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (13, 'uhjbkb', '2021-03-01', 'nkjb98u8u', 31, 1000000000, 130, 564, 21, 'Tb436VWrAa.png', 2, 'csh0HXyPEBJmuTTO.png,d30bZe35IKkxtawv.pdf,fChVhr6oOBYr3hzl.pdf', 2, '2021-03-10 15:48:46', '2021-03-10 15:48:56');
INSERT INTO `tb_pekerjaan_pemenang` VALUES (10, 'bkkbkjbuou070', '2021-03-03', 'hbukh977y987987', 31, 100000000, 120, 987, 21, '2k4EiEKtm1.png', 2, '3aNFReccqOoqm0q8.png,pF9iZc18ppl5t7x6.png,HlZhTApC4xKzQLWH.png,mM7No3k52RcdK8qc.pdf,uZU0E536fxZfsQRw.pdf', 2, '2021-03-12 10:38:35', '2021-03-12 10:38:47');

-- ----------------------------
-- Table structure for tb_pekerjaan_penawaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_penawaran`;
CREATE TABLE `tb_pekerjaan_penawaran`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `penawaran` bigint NULL DEFAULT NULL,
  `jaminan_penawaran` bigint NULL DEFAULT NULL,
  `status_penawaran` int NULL DEFAULT NULL,
  `catatan_penawaran` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `validasi_penawaran` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_penawaran
-- ----------------------------
INSERT INTO `tb_pekerjaan_penawaran` VALUES (7, 31, 900000000, 100000000, 2, 'oke bagus', 2, '2021-01-07 13:29:49', '2021-01-07 15:21:16');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (8, 123, 1000000000, 1000000000, 2, 'Oke saya terima', 2, '2021-01-14 11:05:33', '2021-01-14 11:20:31');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (9, 123, 10000000, 200000, 2, 'sip oke', 2, '2021-03-08 14:13:42', '2021-03-08 15:11:30');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (11, 31, 1200000000, 2000000, 2, NULL, NULL, '2021-03-10 11:33:53', '2021-03-10 11:46:55');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (12, 31, 1000000000, 100000, 2, 'oke', 2, '2021-03-10 13:23:02', '2021-03-10 13:36:54');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (13, 31, 100000000, 1000000, 2, 'oke', 2, '2021-03-10 15:29:12', '2021-03-10 15:36:11');
INSERT INTO `tb_pekerjaan_penawaran` VALUES (10, 31, 100000000, 20000000, 2, 'oke', 2, '2021-03-12 10:07:26', '2021-03-12 10:36:15');

-- ----------------------------
-- Table structure for tb_pekerjaan_penawaran_ba
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_penawaran_ba`;
CREATE TABLE `tb_pekerjaan_penawaran_ba`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `no_ba` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pimpinan_rapat` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `jabatan_pimpinan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dimulai_jam` time(0) NULL DEFAULT NULL,
  `hasil` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `attachment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_penawaran_ba
-- ----------------------------
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (7, '769676987', 'Ruang Rapat', 21, 'XQHuXxk6ag.png', 2, 'Ketua Panitia', '12:00:00', '<p>lnb</p><p>fdb</p><p>fsb</p><p>dfb</p><p>dfb</p><p>dfb</p><p>dfb</p><p>df</p><p>bdf</p><p>bfd</p><p>b</p>', NULL, 2, '2021-01-07 15:21:16', '2021-01-07 15:29:36');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (8, '45435', 'Ruang Rapat', 21, 'fVAqlqcgwJ.png', 2, 'Ketua Panitia', '12:00:00', '<p>isi Hasil</p>', NULL, 2, '2021-01-14 11:20:31', '2021-01-14 11:25:15');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (9, 'fuyg8779877987', 'jhvjhcvjvhj', 28, 'MyIXcMZ3n1.png', 2, 'ketua', '14:00:00', '<p>jbkbkb</p><p><br></p><p>bljb</p><p>ljb</p><p>jb</p><p>jblb</p><p>lb</p><p>b</p><p>lbjl</p>', 'WjY5K5Ac9DCPtMDJ.png,dDTx4wKmkZZ83BKJ.pdf,Bw2V2WwD01d38Z3x.pdf', 2, '2021-03-08 15:11:30', '2021-03-08 16:18:43');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (11, 'ssfdsffsdf', 'sdfsdfsdf', 21, 'xgi9WrlQpT.png', 2, 'fvbsvsfvsbv', '12:00:00', '<p>vbsfvsbsvdsvsv</p>', '7XYMhe8Wbl94ZJm7.png,GWaHu6Q2kzNQt1bX.png,LvB9oJORALKH0pXj.pdf', 2, '2021-03-10 11:46:55', '2021-03-10 11:48:16');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (12, 'jhjhvjhvjhv', 'kbkhbkhbh', 22, 'eZ5PLJboBq.png', 2, 'kjbkjbkb', '12:00:00', '<p>kjbkjbkb</p>', 've9obd96utQ0Gp46.png,8RI3Oykcq9g8Yv8y.png,OaKn4o1ycCZMMBfK.png', 2, '2021-03-10 13:36:54', '2021-03-10 13:39:25');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (13, 'kbkhbkbjk', 'jkbkjbkjbkbj', 21, 'o15mfmn4CJ.png', 2, 'jlnlnlnl', '12:00:00', '<p>kbkbkbkvb </p>', 'G8URAK39F3Gtb0Yk.png,zqfaaWwu9cVLJFLt.png,qtgnnHaxCwqKAP26.pdf', 2, '2021-03-10 15:36:11', '2021-03-10 15:37:28');
INSERT INTO `tb_pekerjaan_penawaran_ba` VALUES (10, 'sffdf', 'sdfsdfdsf', 21, 'uILWNeSMx9.png', 2, 'sfddffsf', '12:00:00', '<p>sdfsfasdfsdf</p>', 'Tg2BBXgekmWvSKw7.png,prHd2FVlvJZ7QQ2q.png', 2, '2021-03-12 10:36:15', '2021-03-12 10:36:37');

-- ----------------------------
-- Table structure for tb_pekerjaan_penawaran_ba_penyedia
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_penawaran_ba_penyedia`;
CREATE TABLE `tb_pekerjaan_penawaran_ba_penyedia`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_penawaran_ba_penyedia
-- ----------------------------
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (7, 31, 29, 'xZ4eC0tp8k.png', 2, '2021-01-07 15:21:16', '2021-01-07 15:21:56');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (8, 123, 35, 'J4VU7lOpUp.png', 2, '2021-01-14 11:20:31', '2021-01-14 11:21:36');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (9, 123, 35, 'MyIXcMZ3n1.png', 2, '2021-03-08 15:11:30', '2021-03-08 16:18:43');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (11, 31, 29, 'xgi9WrlQpT.png', 2, '2021-03-10 11:46:55', '2021-03-10 11:48:16');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (12, 31, 29, 'eZ5PLJboBq.png', 2, '2021-03-10 13:36:54', '2021-03-10 13:39:25');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (13, 31, 29, 'o15mfmn4CJ.png', 2, '2021-03-10 15:36:11', '2021-03-10 15:37:28');
INSERT INTO `tb_pekerjaan_penawaran_ba_penyedia` VALUES (10, 31, 29, 'uILWNeSMx9.png', 2, '2021-03-12 10:36:15', '2021-03-12 10:36:37');

-- ----------------------------
-- Table structure for tb_pekerjaan_penawaran_ba_peserta
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_penawaran_ba_peserta`;
CREATE TABLE `tb_pekerjaan_penawaran_ba_peserta`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `signature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sts_signature` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_penawaran_ba_peserta
-- ----------------------------
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (7, 21, 'eA7Dt1trAa.png', 2, '2021-01-07 15:21:16', '2021-01-07 15:27:38');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (7, 22, 'xPzBxXeJtI.png', 2, '2021-01-07 15:21:16', '2021-01-07 15:29:36');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (8, 21, 'gr3qk35GRH.png', 2, '2021-01-14 11:20:31', '2021-01-14 11:25:15');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (8, 22, 'ONMDwm5Wcd.png', 2, '2021-01-14 11:20:31', '2021-01-14 11:24:57');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (9, 21, 'MyIXcMZ3n1.png', 2, '2021-03-08 15:11:30', '2021-03-08 16:18:43');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (9, 22, 'MyIXcMZ3n1.png', 2, '2021-03-08 15:11:30', '2021-03-08 16:18:43');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (11, 21, 'xgi9WrlQpT.png', 2, '2021-03-10 11:46:55', '2021-03-10 11:48:16');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (11, 22, 'xgi9WrlQpT.png', 2, '2021-03-10 11:46:55', '2021-03-10 11:48:16');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (12, 21, 'eZ5PLJboBq.png', 2, '2021-03-10 13:36:54', '2021-03-10 13:39:25');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (12, 22, 'eZ5PLJboBq.png', 2, '2021-03-10 13:36:54', '2021-03-10 13:39:25');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (13, 21, 'o15mfmn4CJ.png', 2, '2021-03-10 15:36:11', '2021-03-10 15:37:28');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (13, 22, 'o15mfmn4CJ.png', 2, '2021-03-10 15:36:11', '2021-03-10 15:37:28');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (10, 21, 'uILWNeSMx9.png', 2, '2021-03-12 10:36:15', '2021-03-12 10:36:37');
INSERT INTO `tb_pekerjaan_penawaran_ba_peserta` VALUES (10, 22, 'uILWNeSMx9.png', 2, '2021-03-12 10:36:15', '2021-03-12 10:36:37');

-- ----------------------------
-- Table structure for tb_pekerjaan_penawaran_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_penawaran_file`;
CREATE TABLE `tb_pekerjaan_penawaran_file`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `dokumen_penawaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_dokumen_penawaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_penawaran_file
-- ----------------------------
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (7, 31, 'Foto copy atau asli surat undangan', '7-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-01-07 15:00:31', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (7, 31, 'Surat Penawaran', '7-Surat Penawaran-penawaran-pekerjaan.pdf', '2021-01-07 15:00:51', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (7, 31, 'Rekapitulasi RAB', '7-Rekapitulasi RAB-penawaran-pekerjaan.pdf', '2021-01-07 15:01:06', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (7, 31, 'Rincian RAB', '7-Rincian RAB-penawaran-pekerjaan.pdf', '2021-01-07 15:07:09', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (8, 123, 'Foto copy atau asli surat undangan', '8-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-01-14 11:16:12', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (9, 123, 'Surat Penawaran', '9-Surat Penawaran-penawaran-pekerjaan.pdf', '2021-03-08 14:42:42', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (9, 123, 'Foto copy atau asli surat undangan', '9-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-03-08 14:44:18', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (11, 31, 'Foto copy atau asli surat undangan', '11-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-03-10 11:42:29', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (11, 31, 'Surat Penawaran', '11-Surat Penawaran-penawaran-pekerjaan.pdf', '2021-03-10 11:42:34', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (12, 31, 'Foto copy atau asli surat undangan', '12-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-03-10 13:28:41', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (13, 31, 'Foto copy atau asli surat undangan', '13-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-03-10 15:34:40', NULL);
INSERT INTO `tb_pekerjaan_penawaran_file` VALUES (10, 31, 'Foto copy atau asli surat undangan', '10-Foto copy atau asli surat undangan-penawaran-pekerjaan.pdf', '2021-03-12 10:19:39', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_teruskan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_teruskan`;
CREATE TABLE `tb_pekerjaan_teruskan`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `teruskan` int NULL DEFAULT NULL,
  `approve` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_teruskan
-- ----------------------------
INSERT INTO `tb_pekerjaan_teruskan` VALUES (7, 21, 0, '2021-01-07 10:09:17', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (8, 21, 0, '2021-01-14 10:42:21', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (9, 21, 0, '2021-01-14 11:40:57', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (10, 21, 0, '2021-01-20 10:46:41', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (11, 21, 0, '2021-03-10 11:26:07', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (12, 21, 0, '2021-03-10 11:54:06', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (13, 21, 0, '2021-03-10 15:15:32', NULL);
INSERT INTO `tb_pekerjaan_teruskan` VALUES (14, 21, 0, '2021-03-18 14:09:44', NULL);

-- ----------------------------
-- Table structure for tb_pekerjaan_vendor
-- ----------------------------
DROP TABLE IF EXISTS `tb_pekerjaan_vendor`;
CREATE TABLE `tb_pekerjaan_vendor`  (
  `id_pekerjaan` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  `approve_pekerjaan` int NULL DEFAULT NULL,
  `signature` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approve_catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `validasi` int NULL DEFAULT NULL,
  `validasi_catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_pekerjaan_vendor
-- ----------------------------
INSERT INTO `tb_pekerjaan_vendor` VALUES (7, 31, 2, 'fvkREhVZ61.png', 'Oke kami ikut', 2, 'okay', '2021-01-07 10:09:17', '2021-01-07 11:17:44');
INSERT INTO `tb_pekerjaan_vendor` VALUES (8, 123, 2, 'z42amypVJt.png', 'Catatan Saya', 2, 'Oke Dokumen', '2021-01-14 10:42:21', '2021-01-14 10:51:42');
INSERT INTO `tb_pekerjaan_vendor` VALUES (9, 123, 2, 'f4XEeW5jmm.png', 'hadir', 2, 'okey', '2021-01-14 11:40:57', '2021-01-14 11:43:55');
INSERT INTO `tb_pekerjaan_vendor` VALUES (11, 31, 2, 'JNDIJaHneL.png', 'okay', 2, 'okey', '2021-03-10 11:26:07', '2021-03-10 11:28:18');
INSERT INTO `tb_pekerjaan_vendor` VALUES (12, 31, 2, '25eVfPPYHZ.png', 'key', 2, 'okey', '2021-03-10 11:54:06', '2021-03-10 13:21:05');
INSERT INTO `tb_pekerjaan_vendor` VALUES (10, 31, 2, 'n6GjIkOJtH.png', 'oke ikut', 2, 'vwvw', '2021-03-10 15:00:05', '2021-03-12 10:00:47');
INSERT INTO `tb_pekerjaan_vendor` VALUES (13, 31, 2, '6yqmC20HlD.png', 'oke', 2, 'oke', '2021-03-10 15:15:32', '2021-03-10 15:22:25');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `kd_role` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES ('R000', 'AdminSuper');
INSERT INTO `tb_role` VALUES ('R002', 'Vendor');
INSERT INTO `tb_role` VALUES ('R003', 'Committee');
INSERT INTO `tb_role` VALUES ('R004', 'Direksi');

-- ----------------------------
-- Table structure for tb_role_master
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_master`;
CREATE TABLE `tb_role_master`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `kd_role` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `MenuID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `DetailID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `DetailID2` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Action` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8197 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_role_master
-- ----------------------------
INSERT INTO `tb_role_master` VALUES (4942, 'R002', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_master` VALUES (4943, 'R002', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_master` VALUES (4944, 'R002', 'A09', 'A09B03', 'A09B03C00', '/mydatavendor', 'Insert,Update,View');
INSERT INTO `tb_role_master` VALUES (4945, 'R002', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4946, 'R002', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4947, 'R002', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4948, 'R002', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_master` VALUES (4949, 'R002', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_master` VALUES (4950, 'R002', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_master` VALUES (4951, 'R002', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4952, 'R002', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4953, 'R002', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4954, 'R002', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (4955, 'R002', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (4956, 'R002', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (4957, 'R002', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4958, 'R002', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Approve,View');
INSERT INTO `tb_role_master` VALUES (4959, 'R002', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (4960, 'R002', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_master` VALUES (4961, 'R002', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (4962, 'R002', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (7030, 'R004', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'View');
INSERT INTO `tb_role_master` VALUES (7031, 'R004', 'A07', 'A07B00', 'A07B00C00', '/profile', 'Update,View');
INSERT INTO `tb_role_master` VALUES (7032, 'R004', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7033, 'R004', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7034, 'R004', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7035, 'R004', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7036, 'R004', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7037, 'R004', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (7038, 'R004', 'A15', 'A15B00', 'A15B00C00', '/datapenilaianvendor', 'View');
INSERT INTO `tb_role_master` VALUES (8059, 'R003', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'View,Insert,Delete,Update');
INSERT INTO `tb_role_master` VALUES (8060, 'R003', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_master` VALUES (8061, 'R003', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve,Update');
INSERT INTO `tb_role_master` VALUES (8062, 'R003', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8063, 'R003', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8064, 'R003', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_master` VALUES (8065, 'R003', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8066, 'R003', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8067, 'R003', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_master` VALUES (8068, 'R003', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8069, 'R003', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8070, 'R003', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_master` VALUES (8071, 'R003', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8072, 'R003', 'A10', 'A10B01', 'A10B01C05', '/pt_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8073, 'R003', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8074, 'R003', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8075, 'R003', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8076, 'R003', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8077, 'R003', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,Approve,View,Update');
INSERT INTO `tb_role_master` VALUES (8078, 'R003', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Update,Insert,View,Approve');
INSERT INTO `tb_role_master` VALUES (8079, 'R003', 'A10', 'A10B02', 'A10B02C05', '/um_evaluasi', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_master` VALUES (8080, 'R003', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_master` VALUES (8081, 'R003', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_master` VALUES (8139, 'R000', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_master` VALUES (8140, 'R000', 'A02', 'A02B00', 'A02B00C00', '/role', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_master` VALUES (8141, 'R000', 'A03', 'A03B00', 'A03B00C00', '/users', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_master` VALUES (8142, 'R000', 'A07', 'A07B00', 'A07B00C00', '/profile', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_master` VALUES (8143, 'R000', 'A09', 'A09B00', 'A09B00C00', '/datavendor', 'Insert,Update,Delete,View,Approve,Import,Export,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8144, 'R000', 'A02', 'A02B00', 'A02B00C01', '/menulanding', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8145, 'R000', 'A02', 'A02B01', 'A02B01C00', '/settingemail', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8146, 'R000', 'A02', 'A02B01', 'A02B01C01', '/mfooter', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8147, 'R000', 'A02', 'A02B01', 'A02B01C02', '/mainbanner', 'Insert,Approve,Update,Import,Export,Delete,View');
INSERT INTO `tb_role_master` VALUES (8148, 'R000', 'A02', 'A02B02', 'A02B02C00', '/tentangkami', 'Insert,Approve,Update,Delete,View,Export,Import');
INSERT INTO `tb_role_master` VALUES (8149, 'R000', 'A02', 'A02B02', 'A02B02C01', '/banner', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8150, 'R000', 'A02', 'A02B02', 'A02B02C02', '/visimisi', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8151, 'R000', 'A02', 'A02B02', 'A02B02C04', '/whoarewe', 'Insert,Approve,Import,Update,Delete,Export,View');
INSERT INTO `tb_role_master` VALUES (8152, 'R000', 'A02', 'A02B02', 'A02B02C07', '/mpengumuman', 'Insert,Approve,Update,Import,Delete,Export,View');
INSERT INTO `tb_role_master` VALUES (8153, 'R000', 'A02', 'A02B02', 'A02B02C08', '/mberita', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8154, 'R000', 'A02', 'A02B02', 'A02B02C09', '/mcounter', 'Insert,Delete,Update,View,Export,Import,Approve');
INSERT INTO `tb_role_master` VALUES (8155, 'R000', 'A03', 'A03B01', 'A03B01C00', '/userstovendor', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_master` VALUES (8156, 'R000', 'A04', 'A04B00', 'A04B00C00', '/persons', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_master` VALUES (8157, 'R000', 'A08', 'A08B00', 'A08B00C00', '/managecomment', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_master` VALUES (8158, 'R000', 'A08', 'A08B02', 'A08B02C00', '/managecommittee', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_master` VALUES (8159, 'R000', 'A09', 'A09B01', 'A09B01C00', '/reqvendor', 'Insert,Update,Delete,View,Approve,Import,Export,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8160, 'R000', 'A09', 'A09B02', 'A09B02C00', '/emailtovendor', 'Insert,Update,Delete,View,Approve,Export,Import,Pdf');
INSERT INTO `tb_role_master` VALUES (8161, 'R000', 'A12', 'A12B00', 'A12B00C00', '/datakontrak', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_master` VALUES (8162, 'R000', 'A02', 'A02B01', 'A02B01C03', '/mjenisvendor', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_master` VALUES (8163, 'R000', 'A02', 'A02B01', 'A02B01C04', '/mdivisi', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_master` VALUES (8164, 'R000', 'A11', 'A11B00', 'A11B00C00', '/repenunjukanlangsung', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_master` VALUES (8165, 'R000', 'A11', 'A11B01', 'A11B01C00', '/repelelanganterbatas', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_master` VALUES (8166, 'R000', 'A11', 'A11B02', 'A11B02C00', '/repelelanganumum', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_master` VALUES (8167, 'R000', 'A02', 'A02B01', 'A02B01C05', '/mkategoripekerjaan', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_master` VALUES (8168, 'R000', 'A02', 'A02B01', 'A02B01C06', '/msubklasifikasi', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_master` VALUES (8169, 'R000', 'A02', 'A02B01', 'A02B01C07', '/mjabatan', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_master` VALUES (8170, 'R000', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8171, 'R000', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_master` VALUES (8172, 'R000', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8173, 'R000', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8174, 'R000', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8175, 'R000', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8176, 'R000', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_master` VALUES (8177, 'R000', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8178, 'R000', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,Update,View,Delete,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8179, 'R000', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8180, 'R000', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8181, 'R000', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8182, 'R000', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8183, 'R000', 'A10', 'A10B01', 'A10B01C05', '/pt_evaluasi', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_master` VALUES (8184, 'R000', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_master` VALUES (8185, 'R000', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Update,View,Delete,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8186, 'R000', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8187, 'R000', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8188, 'R000', 'A10', 'A10B02', 'A10B02C05', '/um_evaluasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_master` VALUES (8189, 'R000', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_master` VALUES (8190, 'R000', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_master` VALUES (8191, 'R000', 'A14', 'A14B00', 'A14B00C00', '/bapenunjukan', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_master` VALUES (8192, 'R000', 'A14', 'A14B01', 'A14B01C00', '/baterbatas', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_master` VALUES (8193, 'R000', 'A14', 'A14B02', 'A14B02C00', '/baumum', 'Insert,Delete,Update,View,Pdf');
INSERT INTO `tb_role_master` VALUES (8194, 'R000', 'A15', 'A15B00', 'A15B00C00', '/datapenilaianvendor', 'Insert,Update,Delete,View');
INSERT INTO `tb_role_master` VALUES (8195, 'R000', 'A16', 'A16B00', 'A16B00C00', '/datalogaktifitas', 'View');
INSERT INTO `tb_role_master` VALUES (8196, 'R000', 'A13', 'A13B00', 'A13B00C00', '/dataperubahanschedule', 'Insert,Update,Delete,View');

-- ----------------------------
-- Table structure for tb_role_user_access
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_user_access`;
CREATE TABLE `tb_role_user_access`  (
  `kd_role` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `UserID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `MenuID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `DetailID` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `DetailID2` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `Action` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_role_user_access
-- ----------------------------
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'View,Insert,Delete,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A09', 'A09B03', 'A09B03C00', '/mydatavendor', 'Insert,Update,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A09', 'A09B03', 'A09B03C00', '/mydatavendor', 'Insert,Update,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'View');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A07', 'A07B00', 'A07B00C00', '/profile', 'Update,View');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '29', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '31', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R004', '28', 'A15', 'A15B00', 'A15B00C00', '/datapenilaianvendor', 'View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A09', 'A09B03', 'A09B03C00', '/mydatavendor', 'Insert,Update,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '34', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'View,Insert,Delete,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A03', 'A03B00', 'A03B00C00', '/users', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B00', 'A02B00C00', '/role', 'Insert,Update,Delete,View,Approve,Export,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A07', 'A07B00', 'A07B00C00', '/profile', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B00', 'A02B00C01', '/menulanding', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C00', '/settingemail', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C01', '/mfooter', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C02', '/mainbanner', 'Insert,Approve,Update,Import,Export,Delete,View');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C00', '/tentangkami', 'Insert,Approve,Update,Delete,View,Export,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C01', '/banner', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C02', '/visimisi', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C04', '/whoarewe', 'Insert,Approve,Import,Update,Delete,Export,View');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C07', '/mpengumuman', 'Insert,Approve,Update,Import,Delete,Export,View');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C08', '/mberita', 'Insert,Update,Delete,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B02', 'A02B02C09', '/mcounter', 'Insert,Delete,Update,View,Export,Import,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A03', 'A03B01', 'A03B01C00', '/userstovendor', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A04', 'A04B00', 'A04B00C00', '/persons', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A08', 'A08B00', 'A08B00C00', '/managecomment', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A08', 'A08B02', 'A08B02C00', '/managecommittee', 'Insert,Update,Delete,View,Approve,Import,Export');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A09', 'A09B02', 'A09B02C00', '/emailtovendor', 'Insert,Update,Delete,View,Approve,Export,Import,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A09', 'A09B00', 'A09B00C00', '/datavendor', 'Insert,Update,Delete,View,Approve,Import,Export,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A09', 'A09B01', 'A09B01C00', '/reqvendor', 'Insert,Update,Delete,View,Approve,Import,Export,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A12', 'A12B00', 'A12B00C00', '/datakontrak', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A16', 'A16B00', 'A16B00C00', '/datalogaktifitas', 'View');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C05', '/pt_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C05', '/pt_evaluasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,Approve,View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,Approve,View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Update,Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Update,Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C05', '/um_evaluasi', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C05', '/um_evaluasi', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R003', '21', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R003', '22', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Approve,View,Update,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A01', 'A01B00', 'A01B00C00', '/dashboard', 'Insert,Update,Delete,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A07', 'A07B00', 'A07B00C00', '/profile', 'View,Update');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A09', 'A09B03', 'A09B03C00', '/mydatavendor', 'Insert,Update,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,View,Update,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Update,View,Approve,Insert');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Approve,View');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R002', '35', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Update,View,Approve');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C04', '/mdivisi', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C05', '/mkategoripekerjaan', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C07', '/mjabatan', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C06', '/msubklasifikasi', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C00', '/pl_infopekerjaan', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C02', '/pl_dokumenkualifikasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C03', '/pl_aanwijzing', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C04', '/pl_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C05', '/pl_evaluasi', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C06', '/pl_negosiasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B00', 'A10B00C07', '/pl_pemenang', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C00', '/pt_infopekerjaan', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C02', '/pt_dokumenkualifikasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C03', '/pt_aanwijzing', 'Insert,Update,View,Delete,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C04', '/pt_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C05', '/pt_evaluasi', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C06', '/pt_negosiasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B01', 'A10B01C07', '/pt_pemenang', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C00', '/um_infopekerjaan', 'Insert,Update,Delete,View,Approve,Download,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C02', '/um_dokumenkualifikasi', 'Insert,Update,View,Delete,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C03', '/um_aanwijzing', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C04', '/um_penawaran', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C05', '/um_evaluasi', 'Insert,Update,Delete,View,Approve,Pdf,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C06', '/um_negosiasi', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A10', 'A10B02', 'A10B02C07', '/um_pemenang', 'Insert,Download,Update,Delete,View,Approve,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A11', 'A11B00', 'A11B00C00', '/repenunjukanlangsung', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A11', 'A11B01', 'A11B01C00', '/repelelanganterbatas', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A11', 'A11B02', 'A11B02C00', '/repelelanganumum', 'Insert,Update,Delete,View,Approve,Download');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A02', 'A02B01', 'A02B01C03', '/mjenisvendor', 'Insert,Update,Delete,View,Approve,Import');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A14', 'A14B00', 'A14B00C00', '/bapenunjukan', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A14', 'A14B01', 'A14B01C00', '/baterbatas', 'Insert,Update,Delete,View,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A14', 'A14B02', 'A14B02C00', '/baumum', 'Insert,Delete,Update,View,Pdf');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A15', 'A15B00', 'A15B00C00', '/datapenilaianvendor', 'Insert,Update,Delete,View');
INSERT INTO `tb_role_user_access` VALUES ('R000', '14', 'A13', 'A13B00', 'A13B00C00', '/dataperubahanschedule', 'Insert,Update,Delete,View');

-- ----------------------------
-- Table structure for tb_subklasifikasi1
-- ----------------------------
DROP TABLE IF EXISTS `tb_subklasifikasi1`;
CREATE TABLE `tb_subklasifikasi1`  (
  `id_subklasifikasi1` int NOT NULL AUTO_INCREMENT,
  `subklasifikasi1` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_subklasifikasi1`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_subklasifikasi1
-- ----------------------------
INSERT INTO `tb_subklasifikasi1` VALUES (1, 'K1', '2020-10-19 16:38:31', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (2, 'K2', '2020-10-19 16:43:31', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (4, 'K3', '2020-10-19 17:06:36', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (5, 'M1', '2020-10-19 17:21:46', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (6, 'M2', '2020-10-19 17:26:43', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (7, 'B1', '2020-10-19 17:26:52', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (8, 'B2', '2020-10-19 17:28:05', NULL);
INSERT INTO `tb_subklasifikasi1` VALUES (9, NULL, '2021-01-05 15:05:24', '2021-01-05 15:05:40');

-- ----------------------------
-- Table structure for tb_subklasifikasi2
-- ----------------------------
DROP TABLE IF EXISTS `tb_subklasifikasi2`;
CREATE TABLE `tb_subklasifikasi2`  (
  `id_subklasifikasi2` int NOT NULL AUTO_INCREMENT,
  `subklasifikasi2` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_subklasifikasi2`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_subklasifikasi2
-- ----------------------------
INSERT INTO `tb_subklasifikasi2` VALUES (2, 'BG001 - Jasa Pelaksana Konstruksi Bangunan Hunian Tunggal & Kopel', '2020-10-19 17:25:40', '2020-10-19 17:26:30');
INSERT INTO `tb_subklasifikasi2` VALUES (3, 'BG002 - Jasa Pelaksana Konstruksi Bangunan Multi atau Banyak Hunian', '2020-10-19 17:28:29', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (4, 'BG003 - Jasa Pelaksana Konstruksi Bangunan Gudang & Industri', '2020-10-19 17:29:40', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (5, 'BG004 - Jasa Pelaksana Konstruksi Bangunan Komersial', '2020-10-19 17:29:58', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (6, 'BG005 - Jasa Pelaksana Konstruksi Bangunan Hiburan Publik', '2020-10-19 17:30:22', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (7, 'BG006 - Jasa Pelaksana Konstruksi Bangunan Hotel, Restoran, dan Bangunan Serupa Lainnya', '2020-10-19 17:30:43', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (8, 'BG007 - Jasa Pelaksana Konstruksi Bangunan Pendidikan', '2020-10-19 17:31:01', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (9, 'BG008 - Jasa Pelaksana Konstruksi Bangunan Kesehatan', '2020-10-19 17:31:19', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (10, 'BG009 - Jasa Pelaksana Untuk Konstruksi Bangunan Gedung Lainnya', '2020-10-19 17:31:36', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (11, 'SI001 - Jasa Pelaksana Konstruksi Saluran Air, Pelabuhan, Dam, dan Prasarana Sumber Daya Air Lainnya', '2020-10-19 17:32:47', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (12, 'SI002 - Jasa Pelaksana Konstruksi Instalasi Pengolahan Air Minum dan Air Limbah serta Bangunan Pengolahan Sampah', '2020-10-19 17:33:05', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (13, 'SI003 - Jasa Pelaksana Konstruksi Jalan Raya (kecuali Jalan Layang), Jalan, Rel Kereta Api, dan Landas Pacu Bandara', '2020-10-19 17:33:22', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (14, 'SI004 - Jasa Pelaksana Konstruksi Jembatan, Jalan Layang, Terowongan dan Subway', '2020-10-19 17:33:39', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (15, 'SI005 - Jasa Pelaksana Konstruksi Perpipaan Air Minum Jarak Jauh', '2020-10-19 17:33:56', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (16, 'SI006 - Jasa Pelaksana Konstruksi Perpipaan Air Limbah Jarak Jauh', '2020-10-19 17:34:15', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (17, 'SI007 - Jasa Pelaksana Konstruksi Perpipaan Minyak dan Gas Jarak Jauh', '2020-10-19 17:34:33', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (18, 'SI008 - Jasa Pelaksana Konstruksi Perpipaan Air Minum Lokal', '2020-10-19 17:34:49', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (19, 'SI009 - Jasa Pelaksana Konstruksi Perpipaan Air Limbah Lokal', '2020-10-19 17:35:14', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (20, 'SI010 - Jasa Pelaksana Konstruksi Perpipaan Minyak dan Gas Lokal', '2020-10-19 17:35:30', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (21, 'SI011 - Jasa Pelaksana Pekerja Bangunan Stadion Untuk Olah Raga Outdoor', '2020-10-19 17:35:51', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (22, 'SI012 - Jasa Pelaksana Konstruksi Bangunan Fasilitas Olah Raga Indoor dan Fasilitas Rekreasi', '2020-10-19 17:36:10', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (23, 'K001 - Jasa Pelaksana Konstruksi Pemasangan Pendingin Udara (Air Conditioner), Pemanas dan Ventilasi', '2020-10-19 17:37:48', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (24, 'MK002 - Jasa Pelaksana Konstruksi Pemasangan Pipa Air (Plumbing) Dalam Bangunan Dan Salurannya', '2020-10-19 17:38:05', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (25, 'MK003 - Jasa Pelaksana Konstruksi Pemasangan Pipa Gas Dalam Bangunan', '2020-10-19 17:38:24', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (26, 'MK004 - Jasa Pelaksana Konstruksi Insulasi Dalam Bangunan', '2020-10-19 17:38:38', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (27, 'MK005 - Jasa Pelaksana Konstruksi Pemasangan Lift Dan Tangga Berjalan', '2020-10-19 17:38:54', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (28, 'MK006 - Jasa Pelaksana Konstruksi Pertambangan dan Manufaktur', '2020-10-19 17:39:09', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (29, 'MK007 - Jasa Pelaksana Konstruksi Instalasi Thermal, Bertekanan, Minyak, Gas, Geothermal (Pekerjaan Rekayasa)', '2020-10-19 17:39:27', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (30, 'MK008 - Jasa Pelaksana Konstruksi Instalasi Alat Angkut & Alat Angkat', '2020-10-19 17:39:44', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (31, 'MK009 - Jasa Pelaksana Instalasi Fasilitas Produksi, Penyimpanan Minyak & Gas (Pekerjaan Rekayasa)', '2020-10-19 17:40:06', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (32, 'EL001 - Jasa Pelaksana Konstruksi Instalasi Pembangkit Tenaga Listrik Semua Daya', '2020-10-19 17:40:25', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (33, 'EL002 - Jasa Pelaksana Konstruksi Instalasi Pembangkit Tenaga Listrik Daya Maksimum 10 MW', '2020-10-19 17:40:43', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (34, 'EL003 - Jasa Pelaksana Konstruksi Instalasi Pembangkit Tenaga Listrik Energi Baru & Terbarukan', '2020-10-19 17:41:03', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (35, 'L004 - Jasa Pelaksana Konstruksi Instalasi Jaringan Transmisi Tenaga Listrik Tegangan Tinggi / Ekstra Tegangan Tinggi', '2020-10-19 17:41:21', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (36, 'EL005 - Jasa Pelaksana Konstruksi Jaringan Transmisi Telekomunikasi dan/atau Telepon', '2020-10-19 17:41:37', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (37, 'EL006 - Jasa Pelaksana Konstruksi Jaringan Distribusi Tenaga Listrik Tegangan Menengah', '2020-10-19 17:41:52', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (38, 'EL007 - Jasa Pelaksana Konstruksi Instalasi Jaringan Distribusi Tenaga Listrik Tegangan Rendah', '2020-10-19 17:42:07', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (39, 'EL008 - Jasa Pelaksana Konstruksi Instalasi Jaringan Distribusi Telekomunikasi dan/ atau Telepon', '2020-10-19 17:42:27', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (40, 'EL009 - Jasa Pelaksana Konstruksi Instalasi Sistem Kontrol dan Instrumentasi', '2020-10-19 17:42:46', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (41, 'EL010 - Jasa Pelaksana Konstruksi Instalasi Tenaga Listrik Gedung & Pabrik', '2020-10-19 17:43:03', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (42, 'EL011 - Jasa Pelaksana Konstruksi Intalasi Elektrikal Lainnya', '2020-10-19 17:43:23', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (43, 'PL001 - Jasa Penyewaan Alat Konstruksi dan Pembongkaran Bangunan atau Pekerjaan Sipil Lainnya Dengan Operator', '2020-10-19 17:43:49', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (44, 'PL002 - Jasa Pelaksana Perakitan dan Pemasangan Konstruksi Prafabrikasi untuk Konstruksi Bangunan Gedung', '2020-10-19 17:44:10', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (45, 'PL003 - Jasa Pelaksana Perakitan dan Pemasangan Konstruksi Prafabrikasi untuk Konstruksi Jalan dan Jembatan serta Rel Kereta Api', '2020-10-19 17:44:27', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (46, 'PL004 - Jasa Pelaksana Perakitan dan Pemasangan Kanstruksi Prafabrikasi untuk Konstruksi Prasarana Sumber Daya Air, Irigasi, Dermaga, Pelabuhan, Persungaian serta Bangunan Air Bersih, Limbah dan Sampah (Insinerator)', '2020-10-19 17:44:43', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (47, 'SP001 - Pekerjaan Penyelidikan Lapangan', '2020-10-19 17:45:37', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (48, 'SP002 - Pekerjaan Pembongkaran', '2020-10-19 17:45:52', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (49, 'SP003 - Pekerjaan Penyiapan dan Pematangan Tanah/Lokasi', '2020-10-19 17:46:12', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (50, 'SP004 - Pekerjaan Tanah, Galian dan Timbunan', '2020-10-19 17:46:26', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (51, 'SP005 - Pekerjaan Persiapan Lapangan untuk Pertambangan', '2020-10-19 17:46:39', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (52, 'SP006 - Pekerjaan Perancah', '2020-10-19 17:46:54', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (53, 'SP007 - Pekerjaan Pondasi, termasuk Pemancangannya', '2020-10-19 17:47:07', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (54, 'SP008 - Pekerjaan Pengeboran Sumur Air Tanah Dalam', '2020-10-19 17:47:24', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (55, 'SP009 - Pekerjaan Atap dan Kedap Air (Waterproofing)', '2020-10-19 17:47:39', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (56, 'SP010 - Pekerjaan Beton', '2020-10-19 17:47:53', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (57, 'SP011 - Pekerjaan Baja dan Pemasangannya, termasuk Pengelasan', '2020-10-19 17:48:07', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (58, 'P012 - Pekerjaan Pemasangan Batu', '2020-10-19 17:48:21', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (59, 'SP013 - Pekerjaan Konstruksi Khusus Lainnya', '2020-10-19 17:48:34', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (60, 'SP014 - Pekerjaan Pengaspalan dengan Rangkaian Peralatan Khusus', '2020-10-19 17:48:47', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (61, 'SP015 - Pekerjaan Lansekap/Pertamanan', '2020-10-19 17:49:00', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (62, 'SP016 - Pekerjaan Perawatan Bangunan Gedung', '2020-10-19 17:49:13', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (63, 'KT001 - Pekerjaan Kaca dan Pemasangan Kaca Jendela', '2020-10-19 17:49:28', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (64, 'KT002 - Pekerjaan Plesteran', '2020-10-19 17:49:41', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (65, 'KT003 - Pekerjaan Peengecatan', '2020-10-19 17:49:55', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (66, 'KT004 - Pekerjaan Pemasangan Keramik Lantai dan Dinding', '2020-10-19 17:50:09', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (67, 'KT005 - Pekerjaan Pemasangan Lantai Lain, Penutup Dinding dan Pemasangan Wallpaper', '2020-10-19 17:50:25', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (68, 'KT006 - Pekerjaan Kayu dan/atau Pemyambungan Kayu dengan Material Lain', '2020-10-19 17:50:39', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (69, 'KT007 - Pekerjaan Dekorasi dan Pemasangan Interior', '2020-10-19 17:50:54', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (70, 'KT008 - Pekerjaan Pemasangan Ornamen', '2020-10-19 17:51:09', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (71, 'T009 - Pekerjaan Pemasangan Gipsun', '2020-10-19 17:51:22', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (72, 'KT010 - Pekerjaan Pemasangan Platfon Akustik (Akustik Celling)', '2020-10-19 17:51:37', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (73, 'KT011 - Pemasangan Curtain Wall', '2020-10-19 17:51:55', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (74, 'AR101 - Jasa Nasihat dan Pradesain Arsitektural', '2020-10-19 18:02:49', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (75, 'AR102 - Jasa Desain Arsitektural', '2020-10-19 18:03:07', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (76, 'AR103 - Jasa Penilai Perawatan dan Kelayakan Bangunan', '2020-10-19 18:03:28', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (77, 'AR104 - Jasa Desain Interior', '2020-10-19 18:03:44', '2020-10-19 18:04:15');
INSERT INTO `tb_subklasifikasi2` VALUES (78, 'AR105 - Jasa Arsitektural Lainnya', '2020-10-19 18:04:34', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (79, 'RE101 - Jasa Nasehat dan Konsultansi Rekayasa Teknik', '2020-10-19 19:09:29', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (80, 'RE102 - Jasa Desain Rekayasa untuk Konstruksi Pondasi serta Struktur Bangunan', '2020-10-19 19:09:56', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (81, 'RE103 - Jasa Desain Rekayasa untuk Pekerjaan Teknik Sipil Air', '2020-10-19 19:10:11', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (82, 'RE104 - Jasa Desain Rekayasa untuk Pekerjaan Teknik Sipil Transportasi', '2020-10-19 19:10:30', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (83, 'RE105 - Jasa Desain Rekayasa untuk Pekerjaan Mekanikal dan Elektrikal Dalam Bangunan', '2020-10-19 19:10:51', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (84, 'RE106 - Jasa Desain Rekayasa untuk Proses Industrial dan Produksi', '2020-10-19 19:11:09', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (85, 'RE107 - Jasa Nasehat dan Konsultasi Jasa Rekayasa Konstruksi', '2020-10-19 19:11:25', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (86, 'RE108 - Jasa Desain Rekayasa Lainnya', '2020-10-19 19:11:42', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (87, 'PR101 - Jasa Perencanaan dan Perancangan Perkotaan', '2020-10-19 19:11:58', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (88, 'PR102 - Jasa Perencanaan Wilayah', '2020-10-19 19:12:10', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (89, 'PR103 - Jasa Perencanaan dan Perancangan Lingkungan Bangunan dan Lansekap', '2020-10-19 19:12:26', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (90, 'PR104 - Jasa Pengembangan Pemanfaatan Ruang', '2020-10-19 19:12:42', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (91, 'AR201 - Jasa Pengawas Administrasi Kontrak', '2020-10-19 19:13:00', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (92, 'RE201 - Jasa Pengawas Pekerjaan Konstruksi Bangunan Gedung', '2020-10-19 19:13:30', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (93, 'RE202 - Jasa Pengawas Pekerjaan Konstruksi Teknik Sipil Transportasi', '2020-10-19 19:13:45', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (94, 'RE203 - Jasa Pengawas Pekerjaan Konstruksi Teknik Sipil Air', '2020-10-19 19:14:03', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (95, 'RE204 - Jasa Pengawas Pekerjaan Konstruksi dan Instalasi Proses dan Fasilitas Industri', '2020-10-19 19:14:20', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (96, 'PR201 - Jasa Pengawas dan Pengendali Penataan Ruang', '2020-10-19 19:14:36', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (97, 'SP301 - Jasa Pembuatan Prospektus Geologi dan Geofisika', '2020-10-19 19:14:51', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (98, 'SP302 - Jasa Survey Bawah Tanah', '2020-10-19 19:15:07', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (99, 'SP303 - Jasa Survey Permukaan Tanah', '2020-10-19 19:15:21', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (100, 'SP304 - Jasa Pembuatan Peta', '2020-10-19 19:15:36', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (101, 'SP305 - Jasa Pengujian dan Analisa Komposisi dan Tingkat Kemurnian', '2020-10-19 19:15:52', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (102, 'SP306 - Jasa Pengujian dan Analisa Parameter Fisikal', '2020-10-19 19:16:19', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (103, 'SP307 - Jasa Pengujian dan Analisa Sistem Mekanikal dan Elektrikal', '2020-10-19 19:16:40', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (104, 'SP308 - Jasa Inspeksi Teknikal', '2020-10-19 19:16:59', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (105, 'KL401 - Jasa Konsultansi Lingkungan', '2020-10-19 19:17:14', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (106, 'KL402 - Jasa Konsultansi Estimasi Nilai Lahan dan Bangunan', '2020-10-19 19:18:23', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (107, 'KL403 - Jasa manajemen Proyek Terkait Konstruksi Bangunan', '2020-10-19 19:18:37', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (108, 'KL404 - Jasa manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Transportasi', '2020-10-19 19:18:54', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (109, 'KL405 - Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Keairan', '2020-10-19 19:19:15', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (110, 'KL406 - Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Lainnya', '2020-10-19 19:19:47', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (111, 'KL407 - Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Konstruksi Proses dan Fasilitas Industrial', '2020-10-19 19:20:05', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (112, 'KL408 - Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Sistem Kendali Lalu Lintas', '2020-10-19 19:20:27', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (113, 'TI501 - Jasa Terintegrasi untuk Infrastruktur Transportasi', '2020-10-19 19:20:42', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (114, 'TI502 - Jasa Terintegrasi untuk Konstruksi Prasarana dan Sarana Sumber Daya Air, Penyaluran Air dan Pekerjaan Sanitasi', '2020-10-19 19:21:06', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (115, 'TI503 - Jasa Terintegrasi untuk Konstruksi Manufaktur', '2020-10-19 19:21:21', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (116, 'TI504 - Jasa Terintegrasi untuk Konstruksi Fasilitas Minyak dan Gas', '2020-10-19 19:21:36', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (117, 'TI505 - Jasa Terintegrasi untuk Konstruksi Bangunan Gedung', '2020-10-19 19:21:51', NULL);
INSERT INTO `tb_subklasifikasi2` VALUES (118, NULL, '2021-01-05 15:05:53', '2021-01-05 15:06:19');

-- ----------------------------
-- Table structure for tb_vendor_dokumen
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_dokumen`;
CREATE TABLE `tb_vendor_dokumen`  (
  `id_vendor` int NULL DEFAULT NULL,
  `no_akta_diri` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `notaris_diri` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_akta_diri` date NULL DEFAULT NULL,
  `file_pendirian` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_akta_ubah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `notaris_ubah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_akta_ubah` date NULL DEFAULT NULL,
  `file_perubahan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `npwp_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_npwp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pkp_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_pkp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_dokumen
-- ----------------------------
INSERT INTO `tb_vendor_dokumen` VALUES (31, '12', 'H. Bambang', '2020-10-01', 'P6bDkq8WQG-akta-pendirian.pdf', NULL, NULL, NULL, 'KtQwvkxKWr-akta-perubahan.pdf', '453532345', 'BIxvItgSzX-npwp.pdf', '32453253534', 'TedJUoiRZa-pkp.pdf', '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (33, '988', 'jhkhkjhkjh', '2020-10-01', 'SFpkqKTgbw-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '98987697', 'IOErQDpxbq-npwp.pdf', '97987987', '07rPYdPR8G-pkp.pdf', '2020-10-22 18:45:35', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (39, '7676876', 'hbjhjbjh', '2020-12-30', 'unhEWjMhN0-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '6876786', 'NyHXkLBfLE-npwp.pdf', '96876867', '4DS91LCuHF-pkp.pdf', '2020-12-22 13:48:25', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (39, '7676876', 'hbjhjbjh', '2020-12-30', 'JbEp9YTUeO-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '6876786', 'JYld7QmRcw-npwp.pdf', '96876867', 'u63ceDsBFj-pkp.pdf', '2020-12-22 13:51:56', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (39, '7676876', 'hbjhjbjh', '2020-12-30', 'wDj4ZDkSf4-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '6876786', 'HFZl9rAhQ7-npwp.pdf', '96876867', 'IoCfiFcexl-pkp.pdf', '2020-12-22 13:55:40', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (39, '7676876', 'hbjhjbjh', '2020-12-30', 'OF2D9FDaKZ-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '6876786', 'vd83hEAXS9-npwp.pdf', '96876867', 'MED3KeTu4K-pkp.pdf', '2020-12-22 14:07:41', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (39, '7676876', 'hbjhjbjh', '2020-12-30', 'mKAjHVXz71-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '6876786', 'oVoUvsA9Kz-npwp.pdf', '96876867', 'ljvJoDSVpl-pkp.pdf', '2020-12-22 14:10:00', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (64, '08098098', 'hkhkghg', '2020-12-31', 'NCsVOTPdDF-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '89887', 'T5lTrNxCgT-npwp.pdf', '870057', 'K7PXlP4eOi-pkp.pdf', '2020-12-22 15:28:33', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (64, '08098098', 'hkhkghg', '2020-12-31', 'OAYknPBCvW-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '89887', 'wW1SblGcwh-npwp.pdf', '870057', 'uogRu7ISjC-pkp.pdf', '2020-12-22 15:35:32', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (67, '3232', 'sdds', '2020-12-22', 'Pg2k9qc9np-akta-pendirian.pdf', '232308', '23232i3', NULL, NULL, '23233', '2bTVxu9Iki-npwp.pdf', '323233', 'ECUzaP3BIm-pkp.pdf', '2020-12-22 18:34:25', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (67, '3232', 'sdds', '2020-12-22', 'pLakt37oL7-akta-pendirian.pdf', '232308', '23232i3', NULL, NULL, '23233', '27yshzDSe7-npwp.pdf', '323233', 'K9YC8jTnIm-pkp.pdf', '2020-12-22 18:36:49', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (67, '3232', 'sdds', '2020-12-22', 'Nh7xj7wAH8-akta-pendirian.pdf', '232308', '23232i3', NULL, NULL, '23233', 'OkMd1HMXXH-npwp.pdf', '323233', 'JzjmD7vKr0-pkp.pdf', '2020-12-22 18:38:01', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (67, '3232', 'sdds', '2020-12-22', 'd3yhkFLUpt-akta-pendirian.pdf', '232308', '23232i3', NULL, NULL, '23233', 'o80qFUJ0uK-npwp.pdf', '323233', 'ly8zhC8nFj-pkp.pdf', '2020-12-22 18:39:33', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (101, '74837468', 'kewoi', '2020-12-09', NULL, '3483948394', 'wuiw', '2020-12-09', 'EWl2vsGSec-akta-perubahan.pdf', '7843483', 'bSFQOXrsl7-npwp.pdf', '374384', 'ZJc4kS3JWf-pkp.pdf', '2020-12-23 08:27:19', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (115, '128401942', 'ga punya tanah mas', '2020-12-18', 'JAEWl5fBKW-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '4', 'ZjxUXh4TwT-npwp.pdf', '3', 'zlF1UwJ9Aq-pkp.pdf', '2020-12-28 09:51:33', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (116, '34', 'Moch. Farchan Ali Imron, SH', '2018-10-01', 'cUvTY43erv-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '861772440503000', 'jry09cZIMU-npwp.pdf', '861772440503000', '4rTxP5anGZ-pkp.pdf', '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_dokumen` VALUES (123, '01', 'Bapak XYZ', '1999-02-06', 'hPShGbASty-akta-pendirian.pdf', NULL, NULL, NULL, NULL, '123456789', 'KugJZcLVpv-npwp.pdf', '987654321', 'a0vmYzDj5t-pkp.pdf', '2021-01-14 10:28:49', NULL);

-- ----------------------------
-- Table structure for tb_vendor_iujksiup
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_iujksiup`;
CREATE TABLE `tb_vendor_iujksiup`  (
  `id_vendor` int NULL DEFAULT NULL,
  `no_iujksiup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instansi_iujksiup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_iujksiup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_iujksiup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_iujksiup
-- ----------------------------
INSERT INTO `tb_vendor_iujksiup` VALUES (31, '010101', 'Dinas Dinas', '2020-10-31', 'RJBdNNFXFS-iujksiup.pdf', '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (33, '8767676', 'hgjhjhvjh', '2020-10-29', 'fuH9RsojVh-iujksiup.pdf', '2020-10-22 18:45:35', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (33, '121212', 'TEst', '2020-10-31', '6s8ZOmrpAI-iujksiup.pdf', '2020-10-23 13:34:15', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (33, 'bhb87y98h', 'nkjnkj', '2020-10-31', '8IQZSkC0Uf-iujksiup.pdf', '2020-10-23 19:00:00', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (115, '123', 'required', '2020-12-25', '123_iujksiup.pdf', '2020-12-28 09:51:33', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (116, '13374202367026332', 'Pemerintah Kota Semarang', '2020-01-15', '13374202367026332_iujksiup.pdf', '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_iujksiup` VALUES (123, '123456', 'Instansi ABC', '2021-02-02', '123456_iujksiup.pdf', '2021-01-14 10:28:49', NULL);

-- ----------------------------
-- Table structure for tb_vendor_klasifikasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_klasifikasi`;
CREATE TABLE `tb_vendor_klasifikasi`  (
  `id_vendor` int NULL DEFAULT NULL,
  `id_klasifikasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `klasifikasi1` int NULL DEFAULT NULL,
  `klasifikasi2` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_klasifikasi
-- ----------------------------
INSERT INTO `tb_vendor_klasifikasi` VALUES (31, '010101', 1, 2, '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (31, '010101', 1, 3, '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (31, '010201', 1, 2, '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (31, '010201', 2, 3, '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (31, '010201', 1, 4, '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '8767676', 6, 3, '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '8767676', 5, 2, '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, 'hjbjh8yg', 7, 4, '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, 'hjbjh8yg', 2, 3, '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '8767676', 6, 2, '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '121212', 1, 3, '2020-10-23 13:34:15', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '121212', 1, 2, '2020-10-23 13:34:15', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '121212', 5, 6, '2020-10-23 13:34:15', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, 'bhb87y98h', 4, 3, '2020-10-23 19:00:00', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '131313', 5, 4, '2020-10-23 19:03:00', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (33, '131313', 5, 3, '2020-10-23 19:03:00', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (111, '', 4, 3, '2020-12-23 09:55:56', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (112, '', 1, 3, '2020-12-23 10:00:55', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (115, '123', 6, 7, '2020-12-28 09:51:33', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (115, 'required', 5, 6, '2020-12-28 09:51:33', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (116, '13374202367026332', 5, 13, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (116, '13374202367026332', 5, 14, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (116, '0337406139111026322', 5, 13, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (116, '0337406139111026322', 5, 14, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (123, '123456', 5, 3, '2021-01-14 10:28:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (123, '987654', 5, 3, '2021-01-14 10:28:49', NULL);
INSERT INTO `tb_vendor_klasifikasi` VALUES (123, '987654', 7, 5, '2021-01-14 10:28:49', NULL);

-- ----------------------------
-- Table structure for tb_vendor_mail_history
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_mail_history`;
CREATE TABLE `tb_vendor_mail_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vendor` int NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `body_mail` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `send` datetime(0) NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_mail_history
-- ----------------------------
INSERT INTO `tb_vendor_mail_history` VALUES (2, 8, 'hmm test', '<p>Hmmm gitu yakkkk</p>', '2020-09-23 17:07:01', 1, '2020-09-23 17:07:01', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (3, 8, 'jmmkmkm', '<p>kjnknjknjknnjkn</p>', '2020-09-24 12:15:36', 1, '2020-09-24 12:15:36', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (4, 7, 'Test Email Vendor', '<p>test email vendor</p>', '2020-09-29 18:25:52', 1, '2020-09-29 18:25:52', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (5, 5, 'Test Email Penunjukan', '<p>Penunjukan</p>', '2020-09-29 18:26:23', 1, '2020-09-29 18:26:23', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (6, 5, 'wefwfwe', '<p>wefwefwef</p>', '2020-09-29 18:28:26', 1, '2020-09-29 18:28:26', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (7, 5, 'wefwfwe', '<p>wefwefwef</p>', '2020-09-29 18:29:23', 1, '2020-09-29 18:29:23', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (8, 5, 'kjkjkjbkjb', '<p>jlnjnjknjln</p>', '2020-09-29 18:29:53', 1, '2020-09-29 18:29:53', NULL);
INSERT INTO `tb_vendor_mail_history` VALUES (9, 123, 'test PEKA', '<p>test aja loh ini</p>', '2021-03-16 11:28:27', 1, '2021-03-16 11:28:27', NULL);

-- ----------------------------
-- Table structure for tb_vendor_primary
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_primary`;
CREATE TABLE `tb_vendor_primary`  (
  `id_vendor` int NOT NULL AUTO_INCREMENT,
  `nama_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_vendor` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nama_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp_narahubung` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fax_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_vendor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo_vendor` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `alamat_vendor` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `provinsi_vendor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota_vendor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodepos_vendor` int NULL DEFAULT NULL,
  `rating` int NULL DEFAULT 0,
  `status` int NULL DEFAULT NULL,
  `catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_vendor`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_primary
-- ----------------------------
INSERT INTO `tb_vendor_primary` VALUES (31, 'PT. Uul Mencari S1', 'Test Aja', 'Faisal Maulana', 'airbatbady@gmail.com', '082136456532', '1', '082136456532', '0821884736', 'airbatbady@gmail.com', 'TpYHqW5ZPz.png', 'Jl. Melawan Nasib', 'Jawa Tengah', 'Semarang', 121234, 5, 2, 'Oke Bagus', '2020-10-22 18:23:49', '2021-03-12 14:10:52');
INSERT INTO `tb_vendor_primary` VALUES (33, 'PT. Oke', 'jknkjnkjn', 'knknkn', 'airbat@gmail.com', '08980988', '1', '9098098', '98098098', 'airbat@gmail.com', 'GtNBSuuZtu.png', 'ojnjlnljlkj', 'nbkjbkjn', 'kjknkjbkjb', 2147483647, 1, 2, NULL, '2020-10-22 18:45:35', '2021-03-12 14:14:11');
INSERT INTO `tb_vendor_primary` VALUES (115, 'PT Pencari Janda Tbk', 'Bertugas untuk mengerjakan tugas', 'Mass UUl', 'Buddyclalue@gmail.com', '0821237556474', '3', '076452325665', '786429274724', 'Buddyclalue@rocketmail.com', 'N64tuioU7Y.png', 'Jalan Raya Semarang - Kendal KM. 12, Jl. Tugu Industri I No.1, Randu Garut, Kec. Tugu, Kota Semarang', 'Jawa Tengah Ketimuran', 'Semarang Mangkang', 59358, 0, 2, 'titik koma', '2020-12-28 09:51:33', '2021-03-12 14:15:21');
INSERT INTO `tb_vendor_primary` VALUES (116, 'Putra Wijayakusuma Sakti, PT', 'PT Putra Wijayakusuma Sakti (PWS) merupakan Anak Perusahaan Badan Usaha Milik Negara PT Kawasan Indu', 'Achmad Maulana', 'airbatbady@gmail.com', '082134546784', '1', '081326959266', '0248662156', 'airbatbady@gmail.com', 'PpGJH4Hu0C.png', 'Jalan Tugu Industri I Nomor 12 Randugarut, Tugu - Kota Semarang', 'Jawa Tengah', 'Semarang', 50151, 0, 1, NULL, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_primary` VALUES (123, 'Perusahan ABC', 'Perusahaan dibidang DEF', 'Bapak ABC', 'abc@abc.com', '085123456789', '3', '085987654321', '123456', 'rizkipeka@gmail.com', '4cP1SDpxQ8.png', 'Jalan Sesama 123', 'Jawa Tengah', 'Semarang', 55555, 2, 2, 'Dokumen Sesuai', '2021-01-14 10:28:49', '2021-03-12 14:14:41');

-- ----------------------------
-- Table structure for tb_vendor_sbu
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_sbu`;
CREATE TABLE `tb_vendor_sbu`  (
  `id_vendor` int NULL DEFAULT NULL,
  `no_sbu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instansi_sbu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_sbu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_sbu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_sbu
-- ----------------------------
INSERT INTO `tb_vendor_sbu` VALUES (31, '010201', 'Dinas Dinas Terkait', '2020-10-31', '1TIvudVvME-sbu.pdf', '2020-10-22 18:23:49', NULL);
INSERT INTO `tb_vendor_sbu` VALUES (33, 'hjbjh8yg', 'hjgbjhbhjg', '2020-10-19', 'DTAnf9iQjZ-sbu.pdf', '2020-10-22 18:45:35', NULL);
INSERT INTO `tb_vendor_sbu` VALUES (33, '131313', 'Test', '2020-10-31', 'xmV1ZbZyoU-sbu.pdf', '2020-10-23 19:03:00', NULL);
INSERT INTO `tb_vendor_sbu` VALUES (115, 'required', 'required', '2020-12-09', 'required_sbu.pdf', '2020-12-28 09:51:33', NULL);
INSERT INTO `tb_vendor_sbu` VALUES (116, '0337406139111026322', 'Lembaga Pengembangan Jasa Kontruksi', '2022-01-16', '0337406139111026322_sbu.pdf', '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_sbu` VALUES (123, '987654', 'Instansi XYZ', '2021-02-02', '987654_sbu.pdf', '2021-01-14 10:28:49', NULL);

-- ----------------------------
-- Table structure for tb_vendor_secondary
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_secondary`;
CREATE TABLE `tb_vendor_secondary`  (
  `id_vendor` int NULL DEFAULT NULL,
  `web_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `facebook_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instagram_vendor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cabang_vendor` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_pusat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email_pusat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp_pusat` int NULL DEFAULT NULL,
  `fax_pusat` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_secondary
-- ----------------------------
INSERT INTO `tb_vendor_secondary` VALUES (31, 'null', 'null', 'null', 'Yes', 'JL. Kesuksesan', 'airbatbady@gmail.com', 2147483647, 2147483647, '2020-10-22 18:23:49', '2021-03-12 13:09:11');
INSERT INTO `tb_vendor_secondary` VALUES (33, 'null', 'null', 'null', 'No', 'null', 'null', 0, 0, '2020-10-22 18:45:35', '2020-10-22 20:47:32');
INSERT INTO `tb_vendor_secondary` VALUES (115, 'https://www.youtube.com/user/airbatbaddy', 'https://www.facebook.com/user/airbatbaddy', 'https://www.instagram.com/user/airbatbaddy', 'No', 'null', 'null', 0, 0, '2020-12-28 09:51:33', '2021-01-04 08:45:58');
INSERT INTO `tb_vendor_secondary` VALUES (116, 'https://pwskiw.co.id/', NULL, NULL, 'No', NULL, NULL, NULL, NULL, '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_secondary` VALUES (123, NULL, NULL, NULL, 'No', NULL, NULL, NULL, NULL, '2021-01-14 10:28:49', NULL);

-- ----------------------------
-- Table structure for tb_vendor_third
-- ----------------------------
DROP TABLE IF EXISTS `tb_vendor_third`;
CREATE TABLE `tb_vendor_third`  (
  `id_vendor` int NULL DEFAULT NULL,
  `jenis_pengurus` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pengurus` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_ktp` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto_ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan_pengurus` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tb_vendor_third
-- ----------------------------
INSERT INTO `tb_vendor_third` VALUES (33, 'Komisaris', 'SipMAntul', '2147483647', '2147483647-SipMAntul.png', 'jhkjhkh', '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_third` VALUES (33, 'Direksi', 'MAntul', '908098098', '908098098-MAntul.png', 'jnjknkjn', '2020-10-22 20:47:32', NULL);
INSERT INTO `tb_vendor_third` VALUES (115, 'Direksi', 'Rachmadi Nugroho ', '0', '0-Rachmadi Nugroho .png', 'President Director', '2021-01-04 08:45:58', NULL);
INSERT INTO `tb_vendor_third` VALUES (116, 'Komisaris', 'Ahmad Fauzie Nur', '2147483647', '3175102904790002-Ahmad Fauzie Nur.png', 'Komisaris', '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_third` VALUES (116, 'Direksi', 'Mohamad Arifudin', '2147483647', '33740221003730003-Mohamad Arifudin.png', 'Direksi', '2021-01-05 10:14:30', NULL);
INSERT INTO `tb_vendor_third` VALUES (123, 'Komisaris', 'Bapak ABC', '33741222334450008', '337412223344500080_Bapak_ABC.png', 'Jabatan ABC', '2021-01-14 10:28:49', NULL);
INSERT INTO `tb_vendor_third` VALUES (123, 'Direksi', 'Bapak DEF', '33741222334450009', '337412223344500090_Bapak_DEF.png', 'Jabatan DEF', '2021-01-14 10:28:49', NULL);
INSERT INTO `tb_vendor_third` VALUES (31, 'Komisaris', 'Nandi', '93853475', '93853475-Nandi.png', 'Komisaris', '2021-03-12 13:09:11', NULL);
INSERT INTO `tb_vendor_third` VALUES (31, 'Direksi', 'Rizky', '974647464', '974647464-Rizky.png', 'Direktur Operasional', '2021-03-12 13:09:11', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_person` int NULL DEFAULT NULL,
  `api_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NULL DEFAULT NULL,
  `ip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ip_location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active_login` int NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`password`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (14, 73, '$2y$10$jMTDjdRXpBDAEeMR5SzdWuTuG8njYSlCQOGoLXm33OIpb4NJhlT8C', 'admins0101', '$2y$10$4KG4VKIrusWh9kfrrkEXZ.up8Slh7/NMwFYEAG9WzI7WKyrUdQdNG', 'airbat.exiglosi@gmail.com', 'R000', 1, '182.2.39.212', '0', 1, NULL, '2021-01-06 12:14:11');
INSERT INTO `users` VALUES (21, 83, NULL, 'uul2183', '$2y$10$JfoE8i.JzZwP/UMiOMnn4.jSObPY8TI0Ep4ZNhOWRXoPZ1o9NF1Lq', 'uul2@gmail.com', 'R003', 1, '36.67.157.65', '0', 1, NULL, '2021-01-06 10:41:49');
INSERT INTO `users` VALUES (22, 84, NULL, 'risky3745', '$2y$10$F8.1/ai8BBePh1JRuGoBk.a4U87xU6NH/KrzDiGWP2vni6d2shtiS', 'risky@gmail.com', 'R003', 1, '182.253.8.51', '0', 1, NULL, '2020-10-12 13:48:15');
INSERT INTO `users` VALUES (28, 90, NULL, 'achmadm0506', '$2y$10$RjwZ.ozKqIIuLU/41ArQa.zJ1kOYahmXsVpMGx4YQw.aZPQ9Bbf8u', 'airbatbady@gmail.com', 'R004', 1, '127.0.0.1', '0', 0, NULL, '2020-11-07 15:31:38');
INSERT INTO `users` VALUES (29, 91, NULL, 'faisal7342', '$2y$10$iwjIOPlWonqbVHv22.kKR.MVmqESeyq70M2Pb.XXKWyvy.Nf8YWru', 'airbatbady@gmail.com', 'R002', 1, '36.67.157.65', '0', 1, NULL, NULL);
INSERT INTO `users` VALUES (31, 93, NULL, 'knknkn2872', '$2y$10$43.v9u.YIEUv6X1H/4uG7e9zrLIvMRHlk7Fn8eGodPYHJ.7RPaKsC', 'airbat@gmail.com', 'R002', 1, '192.168.43.1', '0', 1, NULL, NULL);
INSERT INTO `users` VALUES (34, 96, NULL, 'mas8712', '$2b$10$bUZa2OWc4Zd0KBj4.2txe.gMgh0Kfc3xt5P.Vmriazy/er5PTy.NK', 'Buddyclalue@gmail.com', 'R002', 1, '36.67.157.65', '0', 1, NULL, '2020-12-28 11:28:55');
INSERT INTO `users` VALUES (35, 97, NULL, 'bapak9915', '$2y$10$LKDblq9whRzca12qiesStuouQpuRw8CT1FoQvvMGqh4E6JWZbtZO6', 'rizkipeka@gmail.com', 'R002', 1, '182.253.8.52', '0', 1, NULL, NULL);

-- ----------------------------
-- Table structure for users_to_vendor
-- ----------------------------
DROP TABLE IF EXISTS `users_to_vendor`;
CREATE TABLE `users_to_vendor`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NULL DEFAULT NULL,
  `id_vendor` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of users_to_vendor
-- ----------------------------
INSERT INTO `users_to_vendor` VALUES (1, 29, 31);
INSERT INTO `users_to_vendor` VALUES (3, 31, 33);
INSERT INTO `users_to_vendor` VALUES (6, 34, 115);
INSERT INTO `users_to_vendor` VALUES (7, 35, 123);

SET FOREIGN_KEY_CHECKS = 1;
