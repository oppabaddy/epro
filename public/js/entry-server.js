/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/App.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/App.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue */ "./resources/js/components/base/Sidebar.vue");
/* harmony import */ var _Navbar_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue */ "./resources/js/components/base/Navbar.vue");
/* harmony import */ var _Footer_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Footer.vue */ "./resources/js/components/base/Footer.vue");
/* harmony import */ var _landing_base_Navbar_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../landing/base/Navbar.vue */ "./resources/js/components/landing/base/Navbar.vue");
/* harmony import */ var _landing_base_Footer_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../landing/base/Footer.vue */ "./resources/js/components/landing/base/Footer.vue");





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Navbar: _Navbar_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Sidebar: _Sidebar_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    Footers: _Footer_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    "navbar-landing": _landing_base_Navbar_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    "footer-landing": _landing_base_Footer_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      count: 1,
      currentUrlFix: "kosong",
      session: {
        id: '',
        login: '',
        role: ''
      }
    };
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    this.loading = true;
    axios.get("/session").then(function (response) {
      _this.id = response.data.id;
      _this.role = response.data.role;
      _this.login = response.data.login;

      _this.cekURL(_this.id);
    });

    if (localStorage.getItem('lang') == undefined) {
      localStorage.setItem('lang', 'ID');
      this.$i18n.locale = 'id';
    } else {
      this.$i18n.locale = localStorage.getItem('lang').toLowerCase();
    }
  },
  methods: {
    cekURL: function cekURL(id) {
      var _this2 = this;

      var currentUrl = window.location.pathname;
      var currentUrl = currentUrl.substr(1);

      if (currentUrl == "" || currentUrl == null) {
        this.currentUrlFix == 'kosong';
        this.count = 0;
      } else {
        this.currentUrlFix = currentUrl;
      }

      if (this.currentUrlFix == 'kosong') {
        console.log(this.count);
      } else {
        axios.get("/api/getSideCount/" + this.currentUrlFix + "/" + id).then(function (response) {
          _this2.count = response.data;
          console.log(_this2.count);
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Footer.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Footer.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Footer'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Navbar.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _donasi_PopupDonasi_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../donasi/PopupDonasi.vue */ "./resources/js/components/donasi/PopupDonasi.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Navbar",
  data: function data() {
    return {
      isLoading: false,
      donation: true,
      id: "",
      user_role: '',
      message: "",
      first_name: "",
      last_name: "",
      photo_profile: "",
      role: '',
      openModal: true
    };
  },
  mounted: function mounted() {
    this.cekUserMenu();
  },
  methods: {
    openModalDonasi: function openModalDonasi() {
      this.openModal = !this.openModal;
    },
    closeModal: function closeModal() {
      this.openModal = false;
    },
    openDonationModal: function openDonationModal() {
      console.log("open modal");
    },
    cekUserMenu: function cekUserMenu() {
      var _this = this;

      this.loading = true;
      axios.get("/session").then(function (response) {
        var roles = response.data.role;
        _this.role = response.data.role;

        if (roles == "R002") {
          _this.user_role = 'Admin KOP';
          axios.get("api/cekDonasiKop/" + response.data.id).then(function (response) {
            _this.message = response.data.ket_donasi;

            if (response.data != null && response.data.flag == "1") {
              _this.donation = true;
              _this.message = response.data.ket_donasi;
            } else if (response.data.status == "false") {
              $(".insertDonasiMod").modal("show");
              _this.donation = false;
            } else {
              _this.donation = false;
            }
          });
        }

        if (roles == 'R000') {
          _this.user_role = 'Admin';
        }

        _this.id = response.data.id;
        axios.get("/api/person/" + response.data.id_person).then(function (response) {
          _this.first_name = response.data.first_name;
          _this.last_name = response.data.last_name;
          _this.photo_profile = response.data.photo_profile;
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Sidebar',
  data: function data() {
    return {
      selected: undefined,
      form: {
        id: '',
        idPerson: '',
        roleCode: '',
        firstName: '',
        lastName: '',
        userName: ''
      },
      isi: {
        bUser: [],
        bMenu: []
      },
      menuA: [],
      menuB: [],
      menuB2: [],
      menuC: []
    };
  },
  mounted: function mounted() {
    this.cekUserMenu(); // this.cekURL()
  },
  methods: {
    cekUserMenu: function cekUserMenu() {
      var _this = this;

      this.loading = true;
      axios.get("/session").then(function (response) {
        _this.form.id = response.data.id;
        _this.form.roleCode = response.data.role;
        _this.form.userName = response.data.username;
        axios.get("/api/person/" + response.data.id_person).then(function (response) {
          _this.form.idPerson = response.data.id_person;
          _this.form.firstName = response.data.first_name;
          _this.form.lastName = response.data.last_name;

          _this.getMenu();

          _this.cekURL();
        });
      });
    },
    cekURL: function cekURL() {
      var _this2 = this;

      var currentUrl = window.location.pathname;
      var currentUrl = currentUrl.substr(1);
      var sideName = '';

      if (currentUrl == "") {
        console.log('kosong');
      } else {
        axios.get("/api/getSideName/" + currentUrl).then(function (response) {
          response.data.forEach(function (puki, index) {
            if (puki.nameA !== null) {
              sideName = puki.nameA;
            }

            if (puki.nameB !== null) {
              sideName = puki.nameB;
            }

            if (puki.nameC !== null) {
              sideName = puki.nameC;
            }
          });
          _this2.selected = sideName;
        });
      }
    },
    getMenu: function getMenu() {
      var _this3 = this;

      //menuA
      axios.get("/api/sidemenuA/" + this.form.id).then(function (response) {
        _this3.menuA = response.data;
        var menu = response.data;
        menu.map(function (r) {
          if (r.Link == "#") {
            _this3.getMenuB(_this3.form.id, r.MenuID);
          }
        });
      });
    },
    getMenuB: function getMenuB(user, menu) {
      var _this4 = this;

      //menuB
      axios.get("/api/sidemenuB/" + user + "/" + menu).then(function (response) {
        // this.menuB = response.data;
        var array = _this4.menuB;
        var count = response.data.length;
        var i;

        for (i = 0; i < count; i++) {
          var newItem = response.data[i];
          var index = array.findIndex(function (e) {
            return e.Link === newItem.Link && e.Name === newItem.Name;
          });

          if (index === -1) {
            array.push(newItem);
          } else {
            array[index] = newItem;
          }
        }

        var menu = _this4.menuB;
        menu.map(function (r) {
          if (r.Link == "#") {
            _this4.getMenuC(_this4.form.id, r.MenuID);
          }
        });
      });
    },
    getMenuC: function getMenuC(user, menu) {
      var _this5 = this;

      //menuC
      axios.get("/api/sidemenuC/" + user + "/" + menu).then(function (response) {
        var array = _this5.menuC;
        var count = response.data.length;
        var i;

        for (i = 0; i < count; i++) {
          var newItem = response.data[i];
          var index = array.findIndex(function (e) {
            return e.Link === newItem.Link;
          });

          if (index === -1) {
            array.push(newItem);
          } else {
            array[index] = newItem;
          }
        }
      });
    },
    pushB: function pushB(user, menu) {
      var newItem = menu;
      var array = this.isi.bMenu;
      array.indexOf(newItem) === -1 ? array.push(newItem) : console.log("This item already exists");
      this.isi.bUser = user;
      var arrStr = encodeURIComponent(JSON.stringify(this.isi.bMenu));
    },
    getpushB: function getpushB() {
      console.log(this.menuA);
      console.log(this.menuB);
      console.log(this.menuC);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "modal",
  props: {
    openModal: {
      type: Boolean
    } // disabled :{
    //   type :Boolean
    // }

  },
  data: function data() {
    return {
      disabled: true,
      req: true,
      reqalasan: false,
      modalOpen: false,
      showData: false,
      value: "600",
      test: true,
      form: {
        file: "",
        filename: "",
        fileDonasi: "",
        jenisDonasi: "",
        alasanDonasi: "",
        nominalDonasi: "",
        buktiDonasi: "",
        image: ""
      } // isModalVisible: false

    };
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    this.id_kop = this.id;
    axios.get("/session").then(function (response) {
      _this.loading = true;
      _this.id = response.data.id;
      _this.id_person = response.data.id_person;
      _this.role = response.data.role;
      _this.login = response.data.login;
    });
  },
  methods: {
    onChange: function onChange(event) {
      var t = event.target.value;
      console.log(t);

      if (t == 1) {
        this.test = true;
        this.req = false;
        this.reqalasan = true;
        this.disabled = !this.disabled;
        this.form.nominalDonasi = '';
      } else if (t == 2) {
        this.test = !this.test;
        this.disabled = true;
        this.req = true;
        this.reqalasan = false;
        this.form.nominalDonasi = '';
      } else if (t == 3) {
        this.test = true;
        this.disabled = true;
        this.req = true;
        this.reqalasan = false;
        this.form.nominalDonasi = '600.000';
      } else {
        this.nominalDonasi = "";
        this.disabled = true;
        this.test = false;
        this.form.nominalDonasi = '';
      }
    },
    closeModal: function closeModal() {
      this.modalOpen = !this.modalOpen;
    },
    showModal: function showModal() {
      this.showData = true;
      console.log(this.showData);
    },
    previewFiles: function previewFiles(event) {
      console.log(event.target.files);
      this.filename = event.target.files[0].name;
      var datas = event.target.files[0];
      var files = event.target.files || event.dataTransfer.files;
      if (!files.length) return;
      this.createImage(files[0]);
    },
    createImage: function createImage(data) {
      var _this2 = this;

      var reader = new FileReader();
      var vm = this;

      reader.onload = function (e) {
        _this2.form.image = e.target.result;
      };

      reader.readAsDataURL(data);
    },
    addData: function addData() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                Swal.fire({
                  title: "Are you sure?",
                  text: "You won't be able to revert this!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#3085d6",
                  cancelButtonColor: "#d33",
                  confirmButtonText: "Yes, insert it!",
                  confirmButtonClass: "btn btn-primary",
                  cancelButtonClass: "btn btn-danger ml-1",
                  buttonsStyling: false
                }).then(function (result) {
                  var _this4 = this;

                  if (result.value) {
                    axios.post("/api/insertDonasi", {
                      bukti_donasi: this.form.image,
                      filename: this.filename,
                      id_kop: this.id,
                      donasi_tahunan: this.form.jenisDonasi,
                      ket_tdk_bersedia: this.form.alasanDonasi,
                      konfirmasi_donasi: this.form.nominalDonasi
                    }).then(function (response) {
                      if (response.status == 200) {
                        Swal.fire({
                          type: "success",
                          title: "Inserted!",
                          text: "Your file has been Insert.",
                          confirmButtonClass: "btn btn-success"
                        });
                      } else {
                        Swal.fire({
                          type: "error",
                          title: "Failed!",
                          text: "Your file has been failled to Insert.",
                          confirmButtonClass: "btn btn-danger"
                        });
                      }

                      $(".insertDonasiMod").modal("hide");
                      _this4.showData = false;

                      _this4.closeModal();

                      _this4.form.buktiDonasi = "";
                      _this4.filename = "";
                      _this4.form.image = "", _this4.id_kop = "";
                      _this4.form.jenisDonasi = "";
                      _this4.form.alasanDonasi = "";
                      _this4.form.nominalDonasi = "";
                      setTimeout(function () {
                        window.location.reload();
                      }, 1000);
                    });
                  }
                }.bind(_this3));

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'FooterLanding'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'NavbarLanding',
  data: function data() {
    return {
      selected: undefined,
      login: undefined,
      isLoading: false,
      menuA: [],
      menuB: [],
      langs: ['en', 'id'],
      lg: localStorage.getItem('lang')
    };
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    // cekUserMenu() { 
    this.loading = true;
    axios.get("/session").then(function (response) {
      // this.id = response.data.id;
      // this.id_person = response.data.id_person;
      // this.role = response.data.role;
      if (response.data.login == undefined) {
        _this.login = false;
      } else {
        _this.login = true;
      }

      console.log(_this.login);
    }); // },
  },
  computed: function computed() {
    this.displayLocale();
  },
  mounted: function mounted() {
    this.cekURL();
    this.getMenu();
  },
  methods: {
    cekURL: function cekURL() {
      var _this2 = this;

      var currentUrl = window.location.pathname;
      var currentUrl2 = "";

      if (currentUrl == '/') {
        currentUrl2 = "/kosong";
      } else {
        currentUrl2 = currentUrl;
      }

      currentUrl2 = currentUrl2.substr(1);
      var sideName = '';
      axios.get("/api/getNavName/" + currentUrl2).then(function (response) {
        response.data.forEach(function (puki, index) {
          if (puki.nameA !== null) {
            sideName = puki.nameA;
          }

          if (puki.nameB !== null) {
            sideName = puki.nameB;
          }
        });
        _this2.selected = sideName;
      });
    },
    getMenu: function getMenu() {
      var _this3 = this;

      //menuA
      axios.get("/api/navmenuA").then(function (response) {
        _this3.menuA = response.data;
        var menu = response.data;
        menu.map(function (r) {
          if (r.Link == "#") {
            _this3.getMenuB(r.MenuID);
          }
        });
      });
    },
    getMenuB: function getMenuB(menu) {
      var _this4 = this;

      //menuB
      axios.get("/api/navmenuB/" + menu).then(function (response) {
        // this.menuB = response.data;
        var array = _this4.menuB;
        var count = response.data.length;
        var i;

        for (i = 0; i < count; i++) {
          var newItem = response.data[i];
          var index = array.findIndex(function (e) {
            return e.Link === newItem.Link;
          });

          if (index === -1) {
            array.push(newItem);
          } else {
            array[index] = newItem;
          }
        }
      });
    },
    switchLocale: function switchLocale(param) {
      // if (this.$i18n.locale === 'en') {
      this.$i18n.locale = param;
      this.lg = param.toUpperCase();
      localStorage.setItem('lang', param.toUpperCase());
      location.reload(); // } else if (this.$i18n.locale === 'id'){
      //   this.$i18n.locale = 'en'
      //   this.lg = 'EN'
      //   localStorage.setItem('lang','EN')
      // }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.row-alert {\r\n    margin-left: 0px;\r\n    margin-bottom: -14px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.card-item__cover[data-v-4f1e39d0] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\n}\n.modal-default-button[data-v-4f1e39d0] {\r\n  margin-right: 22.25rem;\n}\n.vs-radio-con[data-v-4f1e39d0] {\r\n  color: black;\n}\n#fileDonasi[data-v-4f1e39d0]{\r\n  padding-bottom: 32px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/App.vue?vue&type=template&id=217084e4&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/App.vue?vue&type=template&id=217084e4& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.count == 1
        ? [
            _c("navbar"),
            _vm._v(" "),
            _c("sidebar"),
            _vm._v(" "),
            _c("router-view"),
            _vm._v(" "),
            _c("footers")
          ]
        : _vm.count == 0
        ? [
            _c("navbar-landing"),
            _vm._v(" "),
            _c("router-view"),
            _vm._v(" "),
            _c("footer-landing")
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "footer footer-static footer-light" }, [
      _c("p", { staticClass: "clearfix blue-grey lighten-2 mb-0" }, [
        _c(
          "span",
          { staticClass: "float-md-left d-block d-md-inline-block mt-25" },
          [_vm._v("COPYRIGHT © 2020 | "), _vm._v("All rights Reserved")]
        ),
        _c("span", { staticClass: "float-md-right d-none d-md-block" }, [
          _vm._v("Hand-crafted & Made with"),
          _c("i", { staticClass: "feather icon-heart pink" })
        ]),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn-primary btn-icon scroll-top",
            attrs: { type: "button" }
          },
          [_c("i", { staticClass: "feather icon-arrow-up" })]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "nav",
    {
      staticClass:
        "header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow"
    },
    [
      _c("div", { staticClass: "navbar-wrapper" }, [
        _c("div", { staticClass: "navbar-container content" }, [
          _c(
            "div",
            { staticClass: "navbar-collapse", attrs: { id: "navbar-mobile" } },
            [
              _c(
                "div",
                {
                  staticClass:
                    "mr-auto float-left bookmark-wrapper d-flex align-items-center"
                },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "ul",
                    { staticClass: "nav navbar-nav bookmark-icons" },
                    [
                      _vm.donation == false
                        ? [
                            _c(
                              "li",
                              { staticClass: "nav-item d-none d-lg-block" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "alert alert-danger",
                                    staticStyle: {
                                      "margin-bottom": "0",
                                      "margin-left": "50"
                                    },
                                    attrs: { role: "alert" }
                                  },
                                  [
                                    _c("p", { staticClass: "mb-0" }, [
                                      _vm._v(
                                        "\n                                        Maaf anda belum melakukan iuran\n                                        tahunan, mohon klik\n                                        "
                                      ),
                                      _c(
                                        "a",
                                        {
                                          staticClass: "MainNavText",
                                          attrs: {
                                            "data-target": "#modalInsertDonasi",
                                            "data-toggle": "modal",
                                            id: "MainNavHelp",
                                            href: "#modalInsertDonasi",
                                            "aria-haspopup": "true",
                                            "aria-expanded": "false"
                                          },
                                          on: { click: _vm.openModalDonasi }
                                        },
                                        [_vm._v("disini")]
                                      ),
                                      _vm._v(
                                        "\n                                        untuk melakukan iuran.\n                                        "
                                      )
                                    ])
                                  ]
                                )
                              ]
                            )
                          ]
                        : _vm._e()
                    ],
                    2
                  )
                ]
              ),
              _vm._v(" "),
              _c("ul", { staticClass: "nav navbar-nav float-right" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("li", { staticClass: "dropdown dropdown-user nav-item" }, [
                  _c(
                    "a",
                    {
                      staticClass:
                        "dropdown-toggle nav-link dropdown-user-link",
                      attrs: { href: "#", "data-toggle": "dropdown" }
                    },
                    [
                      _c("div", { staticClass: "user-nav d-sm-flex d-none" }, [
                        _c("span", { staticClass: "user-name text-bold-600" }, [
                          _vm._v(
                            _vm._s(_vm.first_name) + " " + _vm._s(_vm.last_name)
                          )
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "user-status" }, [
                          _vm._v(_vm._s(_vm.user_role))
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "span",
                        [
                          _vm.photo_profile !== ""
                            ? [
                                _c("img", {
                                  staticClass: "round",
                                  attrs: {
                                    src:
                                      "uploads/photo_profile/" +
                                      _vm.photo_profile,
                                    height: "40",
                                    width: "40",
                                    alt: "..."
                                  },
                                  on: {
                                    error: function() {
                                      return (_vm.photo_profile =
                                        "avatarDefault.png")
                                    }
                                  }
                                })
                              ]
                            : _vm._e()
                        ],
                        2
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "dropdown-menu dropdown-menu-right" },
                    [
                      _vm.role == "R002"
                        ? _c(
                            "a",
                            {
                              staticClass: "dropdown-item",
                              attrs: { href: "/profilemykop" }
                            },
                            [
                              _c("i", { staticClass: "feather icon-user" }),
                              _vm._v(
                                " Edit\n                                Profile"
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.role == "R000"
                        ? _c(
                            "a",
                            {
                              staticClass: "dropdown-item",
                              attrs: { href: "/profile" }
                            },
                            [
                              _c("i", { staticClass: "feather icon-user" }),
                              _vm._v(
                                " Edit\n                                Profile"
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm._m(2),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _vm._m(3)
                    ]
                  )
                ])
              ])
            ]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "nav navbar-nav" }, [
      _c("li", { staticClass: "nav-item mobile-menu d-xl-none mr-auto" }, [
        _c(
          "a",
          {
            staticClass: "nav-link nav-menu-main menu-toggle hidden-xs",
            attrs: { href: "#" }
          },
          [_c("i", { staticClass: "ficon feather icon-menu" })]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item d-none d-lg-block" }, [
      _c("a", { staticClass: "nav-link nav-link-expand" }, [
        _c("i", { staticClass: "ficon feather icon-maximize" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "dropdown-item", attrs: { href: "/" } }, [
      _c("i", { staticClass: "feather icon-command" }),
      _vm._v("\n                                Landing Page")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "dropdown-item", attrs: { href: "/logout" } },
      [
        _c("i", { staticClass: "feather icon-power" }),
        _vm._v("\n                                Logout")
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "main-menu menu-fixed menu-light menu-accordion menu-shadow",
      attrs: { "data-scroll-to-active": "true" }
    },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "shadow-bottom" }),
      _vm._v(" "),
      _c("div", { staticClass: "main-menu-content" }, [
        _c(
          "ul",
          {
            staticClass: "navigation navigation-main",
            attrs: {
              id: "main-menu-navigation",
              "data-menu": "menu-navigation"
            }
          },
          [
            _c(
              "li",
              {
                class: { active: _vm.selected == "Dashboard" },
                on: {
                  click: function($event) {
                    _vm.selected = "Dashboard"
                  }
                }
              },
              [
                _c("router-link", { attrs: { to: "/dashboard" } }, [
                  _c("i", { staticClass: "feather icon-home" }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "menu-title",
                      attrs: { "data-i18n": "Dashboard" }
                    },
                    [_vm._v("Dashboard")]
                  )
                ])
              ],
              1
            ),
            _vm._v(" "),
            _vm._l(_vm.menuA, function(menua) {
              return [
                menua.Link !== "#" && menua.Name !== "Dashboard"
                  ? [
                      _c(
                        "li",
                        {
                          class: { active: _vm.selected == menua.Name },
                          on: {
                            click: function($event) {
                              _vm.selected = menua.Name
                            }
                          }
                        },
                        [
                          _c("router-link", { attrs: { to: menua.Link } }, [
                            _c("i", { class: menua.Icon }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                staticClass: "menu-title",
                                attrs: { "data-i18n": menua.Name }
                              },
                              [_vm._v(_vm._s(menua.Name))]
                            )
                          ])
                        ],
                        1
                      )
                    ]
                  : menua.Link == "#"
                  ? [
                      _c("li", { staticClass: " nav-item has-sub" }, [
                        _c("a", { attrs: { href: "#" } }, [
                          _c("i", { class: menua.Icon }),
                          _c(
                            "span",
                            {
                              staticClass: "menu-title",
                              attrs: { "data-i18n": menua.Name }
                            },
                            [_vm._v(_vm._s(menua.Name))]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          { staticClass: "menu-content" },
                          [
                            _vm._l(_vm.menuB, function(menub) {
                              return [
                                menub.Link == "#" &&
                                menua.MenuID == menub.MenuID
                                  ? [
                                      _c("li", { staticClass: " has-sub" }, [
                                        _c("a", { attrs: { href: "#" } }, [
                                          _c("i", {
                                            staticClass: "feather icon-circle"
                                          }),
                                          _c(
                                            "span",
                                            {
                                              staticClass: "menu-item",
                                              attrs: { "data-i18n": menub.Name }
                                            },
                                            [_vm._v(_vm._s(menub.Name))]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "ul",
                                          { staticClass: "menu-content" },
                                          [
                                            _vm._l(_vm.menuC, function(menuc) {
                                              return [
                                                menuc.Link == "#" &&
                                                menub.DetailID == menuc.DetailID
                                                  ? void 0
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                menuc.Link !== "#" &&
                                                menub.DetailID == menuc.DetailID
                                                  ? [
                                                      _c(
                                                        "li",
                                                        {
                                                          class: {
                                                            active:
                                                              _vm.selected ==
                                                              menuc.Name
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              _vm.selected =
                                                                menuc.Name
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: menuc.Link
                                                              }
                                                            },
                                                            [
                                                              _c("i", {
                                                                staticClass:
                                                                  "feather icon-circle"
                                                              }),
                                                              _vm._v(" "),
                                                              _c(
                                                                "span",
                                                                {
                                                                  staticClass:
                                                                    "menu-item",
                                                                  attrs: {
                                                                    "data-i18n":
                                                                      menuc.Name
                                                                  }
                                                                },
                                                                [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      menuc.Name
                                                                    )
                                                                  )
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ]
                                                  : _vm._e()
                                              ]
                                            })
                                          ],
                                          2
                                        )
                                      ])
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                menub.Link !== "#" &&
                                menua.MenuID == menub.MenuID
                                  ? [
                                      _c(
                                        "li",
                                        {
                                          class: {
                                            active: _vm.selected == menub.Name
                                          },
                                          on: {
                                            click: function($event) {
                                              _vm.selected = menub.Name
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "router-link",
                                            { attrs: { to: menub.Link } },
                                            [
                                              _c("i", {
                                                staticClass:
                                                  "feather icon-circle"
                                              }),
                                              _c(
                                                "span",
                                                {
                                                  staticClass: "menu-item",
                                                  attrs: {
                                                    "data-i18n": menub.Name
                                                  }
                                                },
                                                [_vm._v(_vm._s(menub.Name))]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  : _vm._e()
                              ]
                            })
                          ],
                          2
                        )
                      ])
                    ]
                  : _vm._e()
              ]
            })
          ],
          2
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "navbar-header" }, [
      _c("ul", { staticClass: "nav navbar-nav flex-row" }, [
        _c("li", { staticClass: "nav-item mr-auto" }, [
          _c("a", { staticClass: "navbar-brand", attrs: { href: "/" } }, [
            _c("img", {
              staticStyle: { "margin-top": "-10px" },
              attrs: {
                src: "theme/app-assets/images/logo/logo-warna.png",
                height: "40",
                width: "150",
                alt: "..."
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item nav-toggle" }, [
          _c(
            "a",
            {
              staticClass: "nav-link modern-nav-toggle pr-0",
              attrs: { "data-toggle": "collapse" }
            },
            [
              _c("i", {
                staticClass:
                  "feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"
              }),
              _c("i", {
                staticClass:
                  "toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary",
                attrs: { "data-ticon": "icon-disc" }
              })
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "modal fade text-left insertDonasiMod",
      attrs: {
        id: "modalInsertDonasi",
        tabindex: "-1",
        role: "dialog",
        "aria-labelledby": "exampleModalLongTitle",
        "aria-hidden": "true"
      }
    },
    [
      _c(
        "div",
        {
          staticClass: "modal-dialog",
          attrs: { role: "dialog", "aria-setsize": "lg" }
        },
        [
          _c(
            "div",
            { staticClass: "modal-content" },
            [
              _vm._m(0),
              _vm._v(" "),
              [
                _c(
                  "form",
                  {
                    attrs: { id: "formDonasi", autocomplete: "off" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.addData()
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "modal-body" }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "form-group col-md-12" }, [
                          _c("ul", { staticClass: "list-unstyled mb-0" }, [
                            _c("li", { staticClass: "d-inline-block mr-2" }, [
                              _c("fieldset", [
                                _c("div", { staticClass: "vs-radio-con" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.jenisDonasi,
                                        expression: "form.jenisDonasi"
                                      }
                                    ],
                                    attrs: {
                                      type: "radio",
                                      id: "1",
                                      name: "konfirmasiDonasi",
                                      value: "1",
                                      required: ""
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.form.jenisDonasi, "1")
                                    },
                                    on: {
                                      change: [
                                        function($event) {
                                          return _vm.$set(
                                            _vm.form,
                                            "jenisDonasi",
                                            "1"
                                          )
                                        },
                                        function($event) {
                                          return _vm.onChange($event)
                                        }
                                      ]
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._m(1),
                                  _vm._v(" "),
                                  _c("span", {}, [_vm._v("Belum bersedia")])
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", { staticClass: "d-inline-block mr-2" }, [
                              _c("fieldset", [
                                _c("div", { staticClass: "vs-radio-con" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.jenisDonasi,
                                        expression: "form.jenisDonasi"
                                      }
                                    ],
                                    attrs: {
                                      type: "radio",
                                      id: "2",
                                      name: "konfirmasiDonasi",
                                      value: "2",
                                      required: ""
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.form.jenisDonasi, "2")
                                    },
                                    on: {
                                      change: [
                                        function($event) {
                                          return _vm.$set(
                                            _vm.form,
                                            "jenisDonasi",
                                            "2"
                                          )
                                        },
                                        function($event) {
                                          return _vm.onChange($event)
                                        }
                                      ]
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._m(2),
                                  _vm._v(" "),
                                  _c("span", {}, [_vm._v("Dibawah 600.000")])
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", { staticClass: "d-inline-block mr-2" }, [
                              _c("fieldset", [
                                _c("div", { staticClass: "vs-radio-con" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.jenisDonasi,
                                        expression: "form.jenisDonasi"
                                      }
                                    ],
                                    attrs: {
                                      type: "radio",
                                      id: "3",
                                      name: "konfirmasiDonasi",
                                      value: "3",
                                      required: ""
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.form.jenisDonasi, "3")
                                    },
                                    on: {
                                      change: [
                                        function($event) {
                                          return _vm.$set(
                                            _vm.form,
                                            "jenisDonasi",
                                            "3"
                                          )
                                        },
                                        function($event) {
                                          return _vm.onChange($event)
                                        }
                                      ]
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._m(3),
                                  _vm._v(" "),
                                  _c("span", {}, [_vm._v("600.000")])
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", { staticClass: "d-inline-block mr-2" }, [
                              _c("fieldset", [
                                _c("div", { staticClass: "vs-radio-con" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.jenisDonasi,
                                        expression: "form.jenisDonasi"
                                      }
                                    ],
                                    attrs: {
                                      type: "radio",
                                      id: "4",
                                      name: "konfirmasiDonasi",
                                      value: "4",
                                      required: ""
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.form.jenisDonasi, "4")
                                    },
                                    on: {
                                      change: [
                                        function($event) {
                                          return _vm.$set(
                                            _vm.form,
                                            "jenisDonasi",
                                            "4"
                                          )
                                        },
                                        function($event) {
                                          return _vm.onChange($event)
                                        }
                                      ]
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._m(4),
                                  _vm._v(" "),
                                  _c("span", {}, [_vm._v("Diatas 600.000")])
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group col-md-12" }, [
                          _c(
                            "label",
                            {
                              staticClass: "form-control-label",
                              attrs: { for: "" }
                            },
                            [_vm._v("Keterangan")]
                          ),
                          _vm._v(" "),
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.alasanDonasi,
                                expression: "form.alasanDonasi"
                              }
                            ],
                            staticClass: "form-control col-sm-12",
                            attrs: {
                              autocomplete: "off",
                              rows: "3",
                              disabled: _vm.disabled,
                              required: _vm.reqalasan,
                              placeholder:
                                "Tulis keterangan jika belum bersedia"
                            },
                            domProps: { value: _vm.form.alasanDonasi },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "alasanDonasi",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-12" }, [
                      _c(
                        "label",
                        {
                          staticClass: "form-control-label",
                          attrs: { for: "nominal" }
                        },
                        [_vm._v("Nominal")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.nominalDonasi,
                            expression: "form.nominalDonasi"
                          }
                        ],
                        staticClass: "form-control col-sm-12",
                        attrs: {
                          type: "textarea",
                          placeholder: "Masukan nominal",
                          id: "nominalDonasi",
                          disabled: _vm.test,
                          autocomplete: "off",
                          required: ""
                        },
                        domProps: { value: _vm.form.nominalDonasi },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "nominalDonasi",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-12" }, [
                      _c(
                        "label",
                        {
                          staticClass: "form-control-label",
                          attrs: { for: "nominal" }
                        },
                        [_vm._v("Bukti Transfer")]
                      ),
                      _vm._v(" "),
                      _vm.form.image
                        ? _c("div", { staticClass: "col-md-3" }, [
                            _c("img", {
                              staticClass: "img-responsive",
                              attrs: {
                                src: _vm.form.image,
                                height: "70",
                                width: "90"
                              }
                            })
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "file",
                          id: "fileDonasi",
                          placeholder: "Pilih file",
                          accept: "jpg/png/pdf/jpeg",
                          required: _vm.req
                        },
                        on: { change: _vm.previewFiles }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "modal-footer" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-pure",
                          attrs: { type: "button", "data-dismiss": "modal" },
                          on: { click: _vm.closeModal }
                        },
                        [_vm._v("Close")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-primary",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ])
                  ]
                )
              ]
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h4", { staticClass: "modal-title", attrs: { id: "judul_donasi" } }, [
        _vm._v("Insert Iuran.")
      ]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "vs-radio" }, [
      _c("span", { staticClass: "vs-radio--border" }),
      _vm._v(" "),
      _c("span", { staticClass: "vs-radio--circle" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "vs-radio" }, [
      _c("span", { staticClass: "vs-radio--border" }),
      _vm._v(" "),
      _c("span", { staticClass: "vs-radio--circle" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "vs-radio" }, [
      _c("span", { staticClass: "vs-radio--border" }),
      _vm._v(" "),
      _c("span", { staticClass: "vs-radio--circle" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "vs-radio" }, [
      _c("span", { staticClass: "vs-radio--border" }),
      _vm._v(" "),
      _c("span", { staticClass: "vs-radio--circle" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("footer", { staticClass: "footer-area" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row justify-content-between" }, [
        _c("div", { staticClass: "col-sm-6 col-md-4 col-xl-3" }, [
          _c("div", { staticClass: "single-footer-widget footer_1" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("p", [_vm._v(_vm._s(_vm.$t("message.sectionfooter1")))])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-sm-6 col-md-4 col-xl-4" }, [
          _c("div", { staticClass: "single-footer-widget footer_2" }, [
            _c("h4", { staticStyle: { "font-family": "'GothamNarrow'" } }, [
              _vm._v(_vm._s(_vm.$t("message.sectionfooter2")))
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                _vm._s(_vm.$t("message.sectionfooter3")) +
                  "\n                        "
              )
            ]),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _vm._m(2)
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 col-md-4" }, [
          _c("div", { staticClass: "single-footer-widget footer_2" }, [
            _c("h4", { staticStyle: { "font-family": "'GothamNarrow'" } }, [
              _vm._v(_vm._s(_vm.$t("message.sectionfooter4")))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "contact_info" }, [
              _c("p", [
                _c("span", [
                  _vm._v(" " + _vm._s(_vm.$t("message.sectionfooter5")) + " ")
                ]),
                _vm._v(" Hath of it fly signs bear be one blessed after ")
              ]),
              _vm._v(" "),
              _c("p", [
                _c("span", [
                  _vm._v(" " + _vm._s(_vm.$t("message.sectionfooter6")))
                ]),
                _vm._v(" +2 36 265 (8060)")
              ]),
              _vm._v(" "),
              _c("p", [
                _c("span", [
                  _vm._v(" " + _vm._s(_vm.$t("message.sectionfooter7")) + " ")
                ]),
                _vm._v("info@colorlib.com ")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _vm._m(3)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "/" } }, [
      _c("img", { attrs: { src: "themeLanding/img/logo2.png", alt: "" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("form", { attrs: { action: "#" } }, [
      _c("div", { staticClass: "form-group" }, [
        _c("div", { staticClass: "input-group mb-3" }, [
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "text",
              placeholder: "Enter email address",
              onfocus: "this.placeholder = ''",
              onblur: "this.placeholder = 'Enter email address'"
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "input-group-append" }, [
            _c(
              "button",
              { staticClass: "btn btn_1", attrs: { type: "button" } },
              [_c("i", { staticClass: "ti-angle-right" })]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "social_icon" }, [
      _c("a", { attrs: { href: "#" } }, [
        _c("i", { staticClass: "ti-facebook" })
      ]),
      _vm._v(" "),
      _c("a", { attrs: { href: "#" } }, [
        _c("i", { staticClass: "ti-twitter-alt" })
      ]),
      _vm._v(" "),
      _c("a", { attrs: { href: "#" } }, [
        _c("i", { staticClass: "ti-instagram" })
      ]),
      _vm._v(" "),
      _c("a", { attrs: { href: "#" } }, [_c("i", { staticClass: "ti-skype" })])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container-fluid" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "copyright_part_text text-center" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-12" }, [
                _c("p", { staticClass: "footer-text m-0" }, [
                  _vm._v(
                    "\n\t\t\t\t\t\t\t\tCopyright ©2020 All rights reserved\n\t\t\t\t\t\t\t\t"
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("header", { staticClass: "main_menu home_menu" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row align-items-center" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("nav", { staticClass: "navbar navbar-expand-lg navbar-light" }, [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "collapse navbar-collapse main-menu-item justify-content-end",
                attrs: { id: "navbarSupportedContent" }
              },
              [
                _vm.lg == "ID"
                  ? [
                      _c(
                        "ul",
                        { staticClass: "navbar-nav align-items-center" },
                        [
                          _c(
                            "li",
                            {
                              class: { active: _vm.selected == "Home" },
                              on: {
                                click: function($event) {
                                  _vm.selected = "Home"
                                }
                              }
                            },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "nav-link",
                                  staticStyle: {
                                    "font-family": "'GothamBold'"
                                  },
                                  attrs: { to: "/" }
                                },
                                [_vm._v("Home\n                              ")]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _vm._l(_vm.menuA, function(menua) {
                            return [
                              menua.Link !== "#" && menua.Name !== "Home"
                                ? [
                                    _c(
                                      "li",
                                      {
                                        class: {
                                          active: _vm.selected == menua.Name
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.selected = menua.Name
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "nav-link",
                                            staticStyle: {
                                              "font-family": "'GothamBold'"
                                            },
                                            attrs: { to: menua.Link }
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(menua.Name) +
                                                "\n                                  "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                : menua.Link == "#"
                                ? [
                                    _c(
                                      "li",
                                      {
                                        staticClass: "nav-item dropdown",
                                        class: {
                                          active: _vm.selected == menua.Name
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.selected = menua.Name
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "a",
                                          {
                                            staticClass:
                                              "nav-link dropdown-toggle",
                                            staticStyle: {
                                              "font-family": "'GothamBold'"
                                            },
                                            attrs: {
                                              href: "#",
                                              id: "navbarDropdown",
                                              role: "button",
                                              "data-toggle": "dropdown",
                                              "aria-haspopup": "true",
                                              "aria-expanded": "false"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                        " +
                                                _vm._s(menua.Name) +
                                                "\n                                    "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass: "dropdown-menu",
                                            attrs: {
                                              "aria-labelledby":
                                                "navbarDropdown"
                                            }
                                          },
                                          [
                                            _vm._l(_vm.menuB, function(menub) {
                                              return [
                                                _vm.login == true
                                                  ? [
                                                      menub.Link ==
                                                        "/profilekop" &&
                                                      menua.MenuID ==
                                                        menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.Name
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : menub.Link !== "#" &&
                                                          menua.MenuID ==
                                                            menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.Name
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : _vm._e()
                                                    ]
                                                  : _vm.login == false
                                                  ? [
                                                      menub.Link ==
                                                        "/profilekop" &&
                                                      menua.MenuID ==
                                                        menub.MenuID
                                                        ? [
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  href: "/login"
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.Name
                                                                  )
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : menub.Link !== "#" &&
                                                          menua.MenuID ==
                                                            menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.Name
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : _vm._e()
                                                    ]
                                                  : _vm._e()
                                              ]
                                            })
                                          ],
                                          2
                                        )
                                      ]
                                    )
                                  ]
                                : _vm._e()
                            ]
                          }),
                          _vm._v(" "),
                          _c("li", { staticClass: "nav-item dropdown" }, [
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-toggle nav-link",
                                staticStyle: { "font-family": "'GothamBold'" },
                                attrs: {
                                  id: "dropdown-flag",
                                  href: "#",
                                  "data-toggle": "dropdown",
                                  "aria-haspopup": "true",
                                  "aria-expanded": "false"
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "flag-icon flag-icon-us"
                                }),
                                _c("span", {
                                  staticClass: "selected-language"
                                }),
                                _vm._v(_vm._s(_vm.lg))
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "dropdown-menu",
                                attrs: { "aria-labelledby": "dropdown-flag" }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    staticClass: "dropdown-item",
                                    staticStyle: {
                                      "font-family": "'GothamBold'"
                                    },
                                    attrs: { href: "#", "data-language": "en" },
                                    on: {
                                      click: function($event) {
                                        return _vm.switchLocale("en")
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "flag-icon flag-icon-us"
                                    }),
                                    _vm._v(" EN")
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "dropdown-item",
                                    staticStyle: {
                                      "font-family": "'GothamBold'"
                                    },
                                    attrs: { href: "#", "data-language": "id" },
                                    on: {
                                      click: function($event) {
                                        return _vm.switchLocale("id")
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "flag-icon flag-icon-us"
                                    }),
                                    _vm._v(" ID")
                                  ]
                                )
                              ]
                            )
                          ])
                        ],
                        2
                      )
                    ]
                  : [
                      _c(
                        "ul",
                        { staticClass: "navbar-nav align-items-center" },
                        [
                          _c(
                            "li",
                            {
                              class: { active: _vm.selected == "Home" },
                              on: {
                                click: function($event) {
                                  _vm.selected = "Home"
                                }
                              }
                            },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "nav-link",
                                  staticStyle: {
                                    "font-family": "'GothamBold'"
                                  },
                                  attrs: { to: "/" }
                                },
                                [_vm._v("Home\n                              ")]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _vm._l(_vm.menuA, function(menua) {
                            return [
                              menua.Link !== "#" && menua.Name !== "Home"
                                ? [
                                    _c(
                                      "li",
                                      {
                                        class: {
                                          active: _vm.selected == menua.Name
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.selected = menua.Name
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "nav-link",
                                            staticStyle: {
                                              "font-family": "'GothamBold'"
                                            },
                                            attrs: { to: menua.Link }
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(menua.NameEn) +
                                                "\n                                  "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                : menua.Link == "#"
                                ? [
                                    _c(
                                      "li",
                                      {
                                        staticClass: "nav-item dropdown",
                                        class: {
                                          active: _vm.selected == menua.Name
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.selected = menua.Name
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "a",
                                          {
                                            staticClass:
                                              "nav-link dropdown-toggle",
                                            staticStyle: {
                                              "font-family": "'GothamBold'"
                                            },
                                            attrs: {
                                              href: "#",
                                              id: "navbarDropdown",
                                              role: "button",
                                              "data-toggle": "dropdown",
                                              "aria-haspopup": "true",
                                              "aria-expanded": "false"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                        " +
                                                _vm._s(menua.NameEn) +
                                                "\n                                    "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass: "dropdown-menu",
                                            attrs: {
                                              "aria-labelledby":
                                                "navbarDropdown"
                                            }
                                          },
                                          [
                                            _vm._l(_vm.menuB, function(menub) {
                                              return [
                                                _vm.login == true
                                                  ? [
                                                      menub.Link ==
                                                        "/profilekop" &&
                                                      menua.MenuID ==
                                                        menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.NameEn
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : menub.Link !== "#" &&
                                                          menua.MenuID ==
                                                            menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.NameEn
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : _vm._e()
                                                    ]
                                                  : _vm.login == false
                                                  ? [
                                                      menub.Link ==
                                                        "/profilekop" &&
                                                      menua.MenuID ==
                                                        menub.MenuID
                                                        ? [
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  href: "/login"
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.NameEn
                                                                  )
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : menub.Link !== "#" &&
                                                          menua.MenuID ==
                                                            menub.MenuID
                                                        ? [
                                                            _c(
                                                              "router-link",
                                                              {
                                                                staticClass:
                                                                  "dropdown-item",
                                                                staticStyle: {
                                                                  "font-family":
                                                                    "'GothamBold'"
                                                                },
                                                                attrs: {
                                                                  to: menub.Link
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    menub.NameEn
                                                                  ) +
                                                                    "\n                                            "
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        : _vm._e()
                                                    ]
                                                  : _vm._e()
                                              ]
                                            })
                                          ],
                                          2
                                        )
                                      ]
                                    )
                                  ]
                                : _vm._e()
                            ]
                          }),
                          _vm._v(" "),
                          _c("li", { staticClass: "nav-item dropdown" }, [
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-toggle nav-link",
                                staticStyle: { "font-family": "'GothamBold'" },
                                attrs: {
                                  id: "dropdown-flag",
                                  href: "#",
                                  "data-toggle": "dropdown",
                                  "aria-haspopup": "true",
                                  "aria-expanded": "false"
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "flag-icon flag-icon-us"
                                }),
                                _c("span", {
                                  staticClass: "selected-language"
                                }),
                                _vm._v(_vm._s(_vm.lg))
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "dropdown-menu",
                                attrs: { "aria-labelledby": "dropdown-flag" }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    staticClass: "dropdown-item",
                                    staticStyle: {
                                      "font-family": "'GothamBold'"
                                    },
                                    attrs: { href: "#", "data-language": "en" },
                                    on: {
                                      click: function($event) {
                                        return _vm.switchLocale("en")
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "flag-icon flag-icon-us"
                                    }),
                                    _vm._v(" EN")
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "dropdown-item",
                                    staticStyle: {
                                      "font-family": "'GothamBold'"
                                    },
                                    attrs: { href: "#", "data-language": "id" },
                                    on: {
                                      click: function($event) {
                                        return _vm.switchLocale("id")
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "flag-icon flag-icon-us"
                                    }),
                                    _vm._v(" ID")
                                  ]
                                )
                              ]
                            )
                          ])
                        ],
                        2
                      )
                    ]
              ],
              2
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "navbar-brand", attrs: { href: "/" } }, [
      _c("img", { attrs: { src: "themeLanding/img/logo2.png", alt: "logo" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#navbarSupportedContent",
          "aria-controls": "navbarSupportedContent",
          "aria-expanded": "false",
          "aria-label": "Toggle navigation"
        }
      },
      [_c("span", { staticClass: "navbar-toggler-icon" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/base/App.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/base/App.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=217084e4& */ "./resources/js/components/base/App.vue?vue&type=template&id=217084e4&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./resources/js/components/base/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/App.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/base/App.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/App.vue?vue&type=template&id=217084e4&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/base/App.vue?vue&type=template&id=217084e4& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=217084e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/App.vue?vue&type=template&id=217084e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_217084e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/base/Footer.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/base/Footer.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Footer.vue?vue&type=template&id=707d9d3e& */ "./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e&");
/* harmony import */ var _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Footer.vue?vue&type=script&lang=js& */ "./resources/js/components/base/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/Footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/Footer.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/base/Footer.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=template&id=707d9d3e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Footer.vue?vue&type=template&id=707d9d3e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_707d9d3e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/base/Navbar.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/base/Navbar.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=0f2fc133& */ "./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133&");
/* harmony import */ var _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue?vue&type=script&lang=js& */ "./resources/js/components/base/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Navbar.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/Navbar.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/base/Navbar.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=0f2fc133& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Navbar.vue?vue&type=template&id=0f2fc133&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_0f2fc133___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/base/Sidebar.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/base/Sidebar.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=template&id=47e086c9& */ "./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9&");
/* harmony import */ var _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=script&lang=js& */ "./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/Sidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=template&id=47e086c9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Sidebar.vue?vue&type=template&id=47e086c9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_47e086c9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/donasi/PopupDonasi.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/donasi/PopupDonasi.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true& */ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true&");
/* harmony import */ var _PopupDonasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopupDonasi.vue?vue&type=script&lang=js& */ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& */ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PopupDonasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4f1e39d0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/donasi/PopupDonasi.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopupDonasi.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=style&index=0&id=4f1e39d0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_style_index_0_id_4f1e39d0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donasi/PopupDonasi.vue?vue&type=template&id=4f1e39d0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopupDonasi_vue_vue_type_template_id_4f1e39d0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/landing/base/Footer.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/landing/base/Footer.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Footer.vue?vue&type=template&id=7d686f74& */ "./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74&");
/* harmony import */ var _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Footer.vue?vue&type=script&lang=js& */ "./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/landing/base/Footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=template&id=7d686f74& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Footer.vue?vue&type=template&id=7d686f74&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_7d686f74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/landing/base/Navbar.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/landing/base/Navbar.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=5ffdec3b& */ "./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b&");
/* harmony import */ var _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue?vue&type=script&lang=js& */ "./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/landing/base/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=5ffdec3b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/base/Navbar.vue?vue&type=template&id=5ffdec3b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_5ffdec3b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/entry-server.js":
/*!**************************************!*\
  !*** ./resources/js/entry-server.js ***!
  \**************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_base_App_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/base/App.vue */ "./resources/js/components/base/App.vue");

renderVueComponentToString(_components_base_App_vue__WEBPACK_IMPORTED_MODULE_0__["default"], function (err, res) {
  print(res);
});

/***/ }),

/***/ 5:
/*!********************************************!*\
  !*** multi ./resources/js/entry-server.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\smsg\resources\js\entry-server.js */"./resources/js/entry-server.js");


/***/ })

/******/ });