require('./bootstrap');

window.Vue = require('vue');


// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';

Vue.use(VueRouter,VueAxios,Axios);

// import file yang dibuat tadi
import Navbar from './components/Navbar.vue';
import Sidebar from './components/Sidebar.vue';
import Footers from './components/Footer.vue';

import Dashboard from './components/Dashboard.vue';
import App from './components/App.vue';
import Create from './components/Create.vue';
import Read from './components/Read.vue';
import ReadDT from './components/ReadDT.vue';
import ReadDTA from './components/ReadDTA.vue';
import Update from './components/Update.vue';

import VueTableDynamic from 'vue-table-dynamic';

// membuat router
const routes = [
    {
        name: 'dashboard',
        path: '/',
        component: Dashboard
    },
    {
        name: 'sidebar',
        path: '/sidebar',
        component: Sidebar
    },
    {
        name: 'read',
        path: '/read',
        component: Read
    },
    {
        name: 'readDT',
        path: '/readDT',
        component: ReadDT
    },
    {
        name: 'readDTA',
        path: '/readDTA',
        component: ReadDTA
    },
    {
        name: 'create',
        path: '/create',
        component: Create
    },
    {
        name: 'update',
        path: '/detail/:id',
        component: Update
    }
]

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount("#app");