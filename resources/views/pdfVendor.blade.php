<!DOCTYPE html>
<html>
 <head>
  <!-- <title> Surat Keterangan </title> -->
 </head>

 <body bgcolor="white">
  
  <font face="Arial" color="black" size="3"> 
  	<p align="center"> 
  		<font face="Arial" color="black" size="6"><b>PT. Kawasan Industri Wijaya Kusuma</b> <br></font>
  		Jl. Raya Semarang – Kendal KM 12, Semarang – Indonesia 50151 | Phone : +6224 8662156 | Fax : +6224 8661476 <br>Email : pemasaran@kiw.co.id 
  	</p>
  </font>
  <hr>

  <font face="Arial" color="black" size="4"> 
  	<p align="center"> 
  		<b>DATA VENDOR</b><br>
  		No. {{ $pdf->id_vendor }} - {{ $pdf->created_at }}
      <table border="1" width="100%">
        <tbody>
          <tr>
            <td rowspan="1" style="text-align: center;">
              <img src="{{ public_path('uploads/logo_vendor/'.$pdf->logo_vendor) }}" style="width:100px; height:auto; margin:5px;">
            </td>
            <td>
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Nama
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->nama_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Nama PIC
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->nama_narahubung }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Email PIC
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->email_narahubung }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Telpon PIC
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->tlp_narahubung }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      No. NPWP
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->npwp_vendor }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="1">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      No. PKP
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->pkp_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Email 
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->email_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Telpon
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->tlp_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Fax
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->fax_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Provinsi
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->provinsi_vendor }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td colspan="1">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Kota
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->kota_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Kode Pos
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->kodepos_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Website
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->web_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Facebook
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->facebook_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Instagram
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->instagram_vendor }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td width="20%">
                      Alamat
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->alamat_vendor }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td width="20%">
                      Deskripsi
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->deskripsi_vendor }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td width="20%">
                      Kantor Cabang
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->cabang_vendor }}
                    </td>
                  </tr>
                  <tr>
                    <td width="20%">
                      Email Pusat 
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->email_pusat }}
                    </td>
                  </tr>
                  <tr>
                    <td width="20%">
                      Telpon Pusat
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->tlp_pusat }}
                    </td>
                  </tr>
                  <tr>
                    <td width="20%">
                       Pusat
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->fax_pusat }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td width="20%">
                      Alamat Pusat
                    </td>
                    <td width="5%"> 
                      :
                    </td>
                    <td width="75%">
                      {{ $pdf->alamat_pusat }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
  	</p>
  </font>

  <div style="page-break-before:always">&nbsp;</div> 

  <font face="Arial" color="black" size="4"> 
    <p align="center"> 
      <b>DOKUMEN VENDOR</b><br>
      <table border="1" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Notaris
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->notaris_diri }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Akta Pendirian
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->no_akta_diri }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Tanggal Pendirian
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->tgl_akta_diri }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      File Akta Pendirian
                    <td> 
                      :
                    </td>
                    <td>
                      Lampiran Zip
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Notaris Perubahan
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->notaris_ubah }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Akta Perubahan
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->no_akta_ubah }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Tanggal Perubahan
                    <td> 
                      :
                    </td>
                    <td>
                      {{ $pdf->tgl_akta_ubah }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      File Akta Perubahan
                    <td> 
                      :
                    </td>
                    <td>
                      Lampiran Zip
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </p>
  </font>

  <font face="Arial" color="black" size="4"> 
    <p align="center"> 
      <b>DATA PEMILIK</b><br>
      <table border="1" width="100%">
        <tbody>
          <tr>
            <td rowspan="1" style="text-align: center;">

            </td>
            <td>
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Nama Pemilik
                    </td>
                    <td> 
                      :
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                      
                    <td> 
                      :
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table border="1" width="100%">
                <tbody>
                  <tr>
                    <td>
                      Alamat Pemilik
                    <td> 
                      :
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </p>
  </font>

 </body>
</html>