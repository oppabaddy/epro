<!DOCTYPE html>
<html>
 <head>
    <style>
      @page {
            size: 250mm 337mm;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
      #watermark { position: fixed; bottom: 0px; right: 0px; width: 100%; height: 100%; z-index: -1; }
      .page-break {
          page-break-after: always;
      }
    </style>
  </head>

 <body bgcolor="white">
  <div id="watermark">
    <img src="uploads/kopSuratLow.png" alt="" style="height:auto; width:100%;"/>
  </div>

  <!-- <script type="text/javascript">
      (function() {

      var img = document.getElementById('watermark').firstChild;
      img.onload = function() {
          if(img.height > img.width) {
              img.height = 'auto';
              img.width = '100%';
          }
      };

      }());
  </script> -->

  <!-- <div id="watermark" ><img src="uploads/kopSuratLow.png" height="auto" width="100%" style="border:5px solid #555"></div> -->
  
  <div style="margin:75px75px75px75px !important;">
    <!-- <img src="uploads/kopSuratLow.png" height="auto" width="100%"> -->
    <font face="Arial" color="black" size="3"> 
    	<p align="center"> 
        <br><br><br><br><br>
    		<b>BERITA ACARA </b><br>
    		Nomor.  {{ $dataFix->id_pekerjaan }} -  {{ $dataFix->no_ba }}


          <table border="0" width="100%">
            <tbody>
              <tr>
                <td></td>
                <td style="text-align: center; vertical-align: middle;">TENTANG</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td style="text-align: center; vertical-align: middle;">{{ $dataFix->nama_pekerjaan1 }}</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Pada hari ini {{ $dataFix->hari }} tanggal {{ $dataFix->tanggal }} bulan {{ $dataFix->bulan }} tahun {{ $dataFix->tahun }} telah dilakukan penjelasan/Aanwijzing untuk:</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Pekerjaan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->nama_pekerjaan2}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Lokasi</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->lokasi}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Rapat Dipimpin</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->pimpinan_rapat}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Jabatan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->jabatan_pimpinan}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Dimulai jam</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->dimulai_jam}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="20%">Peserta Rapat :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>

          @if (count($dataFix->peserta) > 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">PT. KIW (Persero) :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              @foreach ($dataFix->peserta as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;" width="20%">{{$row->nama}}</td>
                  <td width="5%">:</td>
                  <td style="text-align: justify; vertical-align: middle;"width="75%">{{$row->jabatan}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @if (count($dataFix->penyedia) > 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="20%">Penyedia jasa :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              @foreach ($dataFix->penyedia as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;" width="20%">{{$row->nama_vendor}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @if (substr_count($dataFix->hasil,'</p>') >= 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="50%">Pokok - pokok penjelasan :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="45%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td width="100%">{!! $dataFix->hasil !!}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Demikian berita acara ini dibuat dengan sesungguhnya untuk dapat dipergunakan seperlunya.</td>
              </tr>
            </tbody>
          </table>

          @if (substr_count($dataFix->hasil,'</p>') <= 10 || substr_count($dataFix->hasil,'</p>') >= 15)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br><br>
          @else
            <br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: left; vertical-align: top;" width="50%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: justify; vertical-align: middle;" width="30%">Penyedia jasa :</td>
                        <td width="5%"></td>
                        <td style="text-align: justify; vertical-align: middle;" width="65%"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" width="100%">
                    <tbody>
                      @foreach ($dataFix->penyedia as $key => $row)
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%">{{$key+1}}. {{$row->nama_vendor}}</td>
                        </tr>
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%" height="100px">
                            <img class="round" src="uploads/signature/{{$row->signature}}" width="150" height="150" alt="...">
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </td>
                <td style="text-align: justify; vertical-align: middle;" width="50%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">Panitia Pengadaan :</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">PT. Kawasan Industri Wijayakusuma (Persero)</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">Semarang, Tahun {{$dataFix->tahun2}}</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%" height="100px">
                          <img class="round" src="uploads/signature/{{$dataFix->signature}}" width="150" height="150" alt="...">
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">{{$dataFix->pimpinan_rapat}}</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">{{$dataFix->jabatan_pimpinan}}</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

    	</p>
    </font>
  </div>
 </body>
</html>