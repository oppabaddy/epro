<!doctype html>
<html lang="{{ app()->getLocale() }}" class="loading" data-textdirection="ltr">

<head>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>KIW - ePro</title>
    <link rel="shortcut icon" type="image/x-icon" href="theme/app-assets/images/logo/logo-warna2.png"> 
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/vendors.min.css">
        <!-- BEGIN: Analytic -->
        <!-- <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/charts/apexcharts.css"> -->
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/tether-theme-arrows.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/tether.min.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/shepherd-theme-default.css">
        <!-- END: Analitic -->
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="theme/app-assets/css/core/colors/palette-gradient.css"> 
        <!-- BEGIN: Analytic -->
        <link rel="stylesheet" type="text/css" href="theme/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/css/pages/dashboard-analytics.css">
        <link rel="stylesheet" type="text/css" href="theme/app-assets/css/pages/card-analytics.css">
        <!-- END: Analitic -->
        <link rel="stylesheet" type="text/css" href="theme/app-assets/css/pages/app-chat.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="theme/assets/css/style.css">
    <!-- END: Custom CSS-->
    <style type="text/css" media="screen">
        .ql-align-justify {
            white-space: normal !important; 
            text-align: justify !important; 
        }
        .ql-align-left {
            text-align: left !important; 
        }
        .ql-align-right {
            text-align: right !important; 
        }
        .ql-align-center {
            text-align: center !important; 
        }
        .dropdown2 i {
            margin-right: 0px !important; 
        }    
        html body .content .content-wrapper {
            min-height: 520px !important; 
        }
        .vgt-responsive {
            min-height: 300px !important;
            position: unset !important;
        }
        .vgt-table.bordered td, .vgt-table.bordered th {
            border: 1px solid #dcdfe6 !important; 
        }
        .vgt-table thead th {
            color: #606266 !important;
            vertical-align: middle !important; 
            border-bottom: 1px solid #dcdfe6 !important;
            background: none !important;
            background-color: #137bbf1f !important;
            padding-right: 1.5em !important;
            font-weight: 500 !important;
        }
        .vgt-table th.line-numbers, .vgt-table th.vgt-checkbox-col {
            padding: 0 .75em 0 .75em !important; 
            color: #606266 !important; 
            word-wrap: break-word !important; 
            width: 25px !important; 
            text-align: center !important; 
            /*background: none !important;*/
        }
        table.vgt-table {
            font-size: 14px !important;
            border-radius: 0.5rem !important;
            white-space:nowrap !important;
            /*border: 1px solid #f8f8f8 !important; */
        }
        .vgt-global-search {
            padding: 5px 0 !important;
            display: -webkit-box !important;
            display: flex !important;
            flex-wrap: nowrap !important;
            -webkit-box-align: stretch !important;
            align-items: stretch !important;
            border: 0px solid #dcdfe6 !important;
            background: none !important; 
            margin-bottom: 15px !important;
            width: 30% !important;
            margin-left: 70% !important;
            float: right !important;
        }
        table.vgt-table td {
            vertical-align: inherit !important; 
            /*border-bottom: 1px solid #f8f8f8 !important; */
        }
        .vgt-wrap__footer {
            border: 0px solid #dcdfe6 !important; 
            background: none !important; 
        }
        .accordion .collapse-border-item.card:first-child {
             border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important; 
        }
        .accordion .collapse-border-item.card {
            margin-bottom: 20px !important; 
        }
        .accordion .collapse-border-item.card .card-header {
            background-color: rgba(0, 0, 0, 0.03) !important; 
        }
        .ql-editor {
            min-height: 350px !important;
        }
        .contentIsi p {
            margin: 0px !important;
        }
        .dropdown-notification .dropdown-menu-header {
            background: #137BBF !important;
        }
        .dropdown-notification .dropdown-menu.dropdown-menu-right::before {
            background: #137BBF !important;
            border-color: #137BBF !important;
        }
        .badge.badge-primary {
            background-color: #bf1313 !important;
        }
    </style>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="vertical-layout vertical-menu-modern 2-columns chat-application navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

<!-- <body class="vertical-layout vertical-menu-modern content-left-sidebar chat-application navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar"> -->

    <!-- Component Vue -->
    <div id="app">      
    </div>

    <script src="{{ mix('js/app.js') }}" type="text/javascript" ></script> 
    <!-- /js/app.js?id=140353998a11de4d8357 -->
    <!-- BEGIN: Vendor JS-->
    <script src="theme/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
        <!-- BEGIN: Analytic -->
        <!-- <script src="theme/app-assets/vendors/js/charts/apexcharts.min.js"></script> -->
        <script src="theme/app-assets/vendors/js/extensions/tether.min.js"></script>
        <script src="theme/app-assets/vendors/js/extensions/shepherd.min.js"></script>
        <!-- END: Analytic -->
        <script src="theme/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="theme/app-assets/vendors/js/extensions/polyfill.min.js"></script>
        <script src="theme/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
        <script src="theme/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>

        <script src="theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="theme/app-assets/js/core/app-menu.js"></script>
    <script src="theme/app-assets/js/core/app.js"></script>
    <script src="theme/app-assets/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
        <!-- BEGIN: Analytic -->
        <!-- <script src="theme/app-assets/js/scripts/pages/dashboard-analytics.js"></script> -->
        <!-- <script src="theme/app-assets/js/scripts/pages/dashboard-ecommerce.js"></script> -->
        <script src="theme/app-assets/js/scripts/extensions/sweet-alerts.js"></script>
        <script src="theme/app-assets/js/scripts/navs/navs.js"></script>
        <script src="theme/app-assets/js/scripts/pages/app-chat.js"></script>
        <!-- <script src="theme/app-assets/js/scripts/datatables/datatable.js"></script> -->
        <!-- END: Analytic -->
    <!-- END: Page JS-->

</body>

</html>