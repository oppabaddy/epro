<!DOCTYPE html>
<html>
 <head>
  <title> Laporan Harian Satwa </title>

  <style type="text/css" media="screen">
    @page { margin: 10px; }
    body { margin: 10px; } 

    table {
      border-collapse: collapse;
      width:100%;
    }

    table, th, td {
      border: 1px solid black;
    } 
  </style>
 </head> 

 <body bgcolor="white">
  
<!--   <font face="Arial" color="black" size="3"> 
  	<p align="center"> 
  		<font face="Arial" color="black" size="6"><b>GEMBIRA LOKA ZOO</b> <br></font>
  		Jl. Kebun Raya No.2 Yogyakarta 55171 | Phone : 0274-373861 | Fax : 0274-384666 <br>Email : Info@Gembiralokazoo.Com 
  	</p>
  </font>
  <hr> -->
  <table border="1" align="center">
    <tbody>
      <tr align="center">
        <td colspan="11">
            LAPORAN HARIAN SATWA
        </td>
      </tr>
      <tr align="center">
        <td colspan="11">
            Gembira Loka Zoo
        </td>
      </tr>
      <tr>
        <td align="center">
            Tanggal
        </td>
        <td colspan="2" align="center">
            <?php echo $pdf->tgl_inspeksi?>
        </td>
        <td align="center">
            Zona
        </td>
        <td colspan="3" align="center">
            <?php echo $pdf->zona_inspeksi?>
        </td>
        <td align="center">
            Nama Keeper
        </td>
        <td colspan = "3" align="center">
            <?php echo $pdf->nama_keeper_tugas_new?>
        </td>
      </tr>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Pergerakan Satwa
        </td>
        <td colspan="4" align="center" >
            Zoo ID
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            Ayah
        </td>
        <td align="center">
            Ibu
        </td>
        <td align="center">
            Jumlah Terkini
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            
        </td>
        <td align="center" >
            Jantan
        </td>
        <td align="center">
            Betina
        </td>
        <td align="center">
            Tidak Diketahui
        </td>
        <td align="center">
            Tidak Ada
        </td>
        <td align="center">
            Catatan
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Lahir
        </td>
        <?php if($pdf2->lahir_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lahir_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lahir_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lahir_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
            <?php echo $pdf2->ket_lahir_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Mati
        </td>
        <?php if($pdf2->mati_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->mati_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->mati_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->mati_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
            <?php echo $pdf2->ket_mati_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Pindah
        </td>
        <?php if($pdf2->pindah_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->pindah_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->pindah_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->pindah_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
            <?php echo $pdf2->ket_pindah_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Baru Datang
        </td>
        <?php if($pdf2->datang_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->datang_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->datang_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->datang_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
          <?php echo $pdf2->ket_datang_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Lepas/Hilang/Ditemukan Kembali
        </td>
        <?php if($pdf2->lepas_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lepas_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lepas_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->lepas_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
          <?php echo $pdf2->ket_lepas_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center" >
            Sakit
        </td>
        <?php if($pdf2->sakit_satwa == "Male"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->sakit_satwa == "Female"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->sakit_satwa == "Unknown"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <?php if($pdf2->sakit_satwa == "Nothing"){?>
          <td align="center" >
              OK
          </td>
        <?php } else {?>
          <td align="center" >
              
          </td>
        <?php }?>
        <td align="center">
          <?php echo $pdf2->ket_sakit_satwa?>
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
        <td align="center">
            
        </td>
      </tr>
      <tr>
        <td colspan="11" style="vertical-align: top; text-align: left;" height="100">
            Observasi Umum : <?php echo $pdf2->observasi_satwa?>
        </td>
      </tr>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          Pest
        </td>
        <td colspan="3" align="center">
          Kuantitas
        </td>
        <td colspan="5" align="center">
          Lokasi
        </td>
      </tr>
      <?php foreach($pdf3 as $pest){ ?>
        <tr>
          <td colspan="3" align="center">
            <?php echo $pest->nama_master?>
          </td>
          <td colspan="3" align="center">
            <?php echo $pest->kuantitas_pest?>
          </td>
          <td colspan="5" align="center">
            <?php echo $pest->lokasi?>
          </td>
        </tr>
      <?php } ?>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          Pakan
        </td>
        <td colspan="2" align="center">
          Kualitas
        </td>
        <td colspan="2" align="center">
          Kuantitas
        </td>
        <td colspan="5" align="center">
          Catatan
        </td>
      </tr>
      <?php foreach($pdf4 as $pakan){ ?>
        <tr>
          <td colspan="2" align="center">
            <?php echo $pakan->nama_master?>
          </td>
          <?php if(preg_replace("/\-.+/", "", $pakan->kualitas_pakan) == "Good"){?>
            <td align="center" >
                Baik
            </td>
          <?php } else {?>
            <td align="center" >
                
            </td>
          <?php }?>
          <?php if(preg_replace("/\-.+/", "", $pakan->kualitas_pakan) == "Bad"){?>
            <td align="center" >
                Buruk
            </td>
          <?php } else {?>
            <td align="center" >
                
            </td>
          <?php }?>
          <?php if(preg_replace("/\-.+/", "", $pakan->kuantitas_pakan) == "Enough"){?>
            <td align="center" >
                Cukup
            </td>
          <?php } else {?>
            <td align="center" >
                
            </td>
          <?php }?>
          <?php if(preg_replace("/\-.+/", "", $pakan->kuantitas_pakan) == "Less"){?>
            <td align="center" >
                Kurang
            </td>
          <?php } else {?>
            <td align="center" >
                
            </td>
          <?php }?>
          <td colspan="5" align="center">
            <?php echo $pakan->catatan?>
          </td>
        </tr>
      <?php } ?>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          Infrastruktur
        </td>
        <td colspan="4" align="center">
          Kualitas
        </td>
        <td colspan="4" align="center">
          Catatan
        </td>
      </tr>
      <?php foreach($pdf5 as $infra){ ?>
        <tr>
          <td colspan="3" align="center">
            <?php echo $infra->nama_master?>
          </td>
          <?php if(preg_replace("/\-.+/", "", $infra->kualitas_infra) == "Good"){?>
            <td colspan="2" align="center" >
                Baik
            </td>
          <?php } else {?>
            <td colspan="2" align="center" >
                
            </td>
          <?php }?>
          <?php if(preg_replace("/\-.+/", "", $infra->kualitas_infra) == "Bad"){?>
            <td colspan="2"align="center" >
                Buruk
            </td>
          <?php } else {?>
            <td colspan="2"align="center" >
                
            </td>
          <?php }?>
          <td colspan="4" align="center">
            <?php echo $infra->catatan?>
          </td>
        </tr>
      <?php } ?>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <tr>
        <td colspan="11" style="vertical-align: top; text-align: left;" height="100">
          Catatan : <?php echo $pdf->catatan?>
        </td>
      </tr>
      <tr>
        <td colspan="11" align="center">
        </td>
      </tr>
      <?php if($pdf->kabid_acc == 0 && $pdf->kanit_acc == 0 && $pdf->keeper_acc == 0 && $pdf->status == 2) {?>
        <tr>
          <td colspan="7" style="vertical-align: top; text-align: left;" height="100">

          </td>
          <td colspan="4" style="vertical-align: top; text-align: left;" height="100">
            Admin
            <br/>
            <br/>
              <img src="theme/assets/images/approved.png" style="width:100px;padding-left: 20px" alt="...">
            <br/>
            Tanggal: <?php echo $pdf->kabid_tgl?>
          </td>
        </tr>
      <?php } else { ?>
        <tr>
          <td colspan="4" style="vertical-align: top; text-align: left;" height="100">
            Kepala Bidang
            <br/>
            <br/>
            <?php if($pdf->kabid_acc == 2) {?>
              <img src="theme/assets/images/approved.png" style="width:100px;padding-left: 20px" alt="...">
            <?php } ?>
            <br/>
            Tanggal: <?php echo $pdf->kabid_tgl?>
          </td>
          <td colspan="3" style="vertical-align: top; text-align: left;" height="100">
            Kepala Unit
            <br/>
            <br/>
            <?php if($pdf->kanit_acc == 2) {?>
              <img src="theme/assets/images/approved.png" style="width:100px;padding-left: 20px" alt="...">
            <?php } ?>
            <br/>
            Tanggal: <?php echo $pdf->kanit_tgl?>
          </td>
          <td colspan="4" style="vertical-align: top; text-align: left;" height="100">
            Keeper Pendata
            <br/>
            <br/>
            <?php if($pdf->keeper_acc == 2) {?>
              <img src="theme/assets/images/approved.png" style="width:100px;padding-left: 20px" alt="...">
            <?php } ?>
            <br/>
            Tanggal: <?php echo $pdf->keeper_tgl?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
 </body>
</html>