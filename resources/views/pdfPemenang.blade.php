<!DOCTYPE html>
<html>
 <head>
    <style>
      @page {
            size: 250mm 337mm;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
      #watermark { position: fixed; bottom: 0px; right: 0px; width: 100%; height: 100%; z-index: -1; }
      .page-break {
          page-break-after: always;
      }
    </style>
  </head>

 <body bgcolor="white">
  <div id="watermark">
    <img src="uploads/kopSuratLow.png" alt="" style="height:auto; width:100%;"/>
  </div>
  
  <div style="margin:75px75px75px75px !important;">
    <font face="Arial" color="black" size="3"> 
      <p align="center"> 
        <br><br><br><br><br><br><br>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: left; vertical-align: middle;">No  : {{$dataFix->no_surat}}</td>
                <td></td>
                <td style="text-align: right; vertical-align: middle;">Semarang, {{$dataFix->created_at}}</td>
              </tr>
              <tr>
                <td style="text-align: left; vertical-align: middle;">Hal : Pemberitahuan Pemenang Pemilihan Langsung</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: left; vertical-align: middle;" width="70%">
                  
                </td>
                <td style="text-align: right; vertical-align: middle;" width="30%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: left; vertical-align: middle;" colspan="2">Kepada Yth :</td>
                      </tr>
                      @foreach ($dataFix->penyedia as $key => $row)
                        <tr>
                          <td width="5%">{{$key+1}}.</td>
                          <td style="text-align: justify; vertical-align: middle;" width="95%">{{$row->nama_vendor}}</td>
                        </tr>
                        <tr>
                          <td width="5%"></td>
                          <td style="text-align: justify; vertical-align: middle;" width="95%">{{$row->alamat_vendor}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Berdasarkan Surat Direksi PT. KIW (Persero) Nomor: {{$dataFix->no_surat_dir}} tanggal {{$dataFix->tgl_surat_dir}} perihal Persetujuan Penetapan Pemenang Pemilihan Langsung Pekerjaan {{$dataFix->nama_pekerjaan2}}, bersama ini kami sampaikan bahwa sebagai pemenang pekerjaan tersebut adalah:</td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Nama</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->nama_vendor}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Alamat</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->alamat_vendor}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">NPWP</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->npwp_vendor}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Harga Kesepakatan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">Rp. {{number_format($dataFix->kesepakatan,2,",",".")}} ( {{$dataFix->kesepakatan2}} rupiah ) , harga sudah termasuk Jasa, pajak dan segala pengeluaran yang berkaitan dengan pekerjaan ini</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Waktu Pelaksanaan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->pelaksanaan}} ( {{$dataFix->pelaksanaan2}} ) hari kalender</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Waktu Pemeliharaan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->pemeliharaan}} ( {{$dataFix->pemeliharaan2}} ) hari kalender</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Demikian, atas perhatian dan partisipasi Saudara diucapkan terima kasih.</td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: left; vertical-align: middle;" width="60%">
                  
                </td>
                <td style="text-align: right; vertical-align: middle;" width="40%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">PT. Kawasan Industri Wijayakusuma (Persero)</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">Semarang, Tahun {{$dataFix->created_at2}}</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%" height="100px">
                          <img class="round" src="uploads/signature/{{$dataFix->signature}}" width="150" height="150" alt="...">
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">{{$dataFix->nama_ketua_panitia}}</td>
                      </tr>
                      <tr>
                        <td style="text-align: center; vertical-align: middle;" width="100%">Ketua Panitia</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
      </p>
    </font>
  </div>

 </body>
</html>