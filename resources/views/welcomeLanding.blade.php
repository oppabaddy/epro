<!doctype html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <title>Etrain</title> -->
    <!-- <link rel="icon" href="themeLanding/img/favicon.png"> -->
    <title>KIW - ePro</title>
    <link rel="icon" href="themeLanding/img/logo-warna2.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="themeLanding/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="themeLanding/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="themeLanding/css/owl.carousel.min.css">
    <!-- <link rel="stylesheet" href="themeLanding/css/owl.carousel.css"> -->
    <!-- <link rel="stylesheet" href="themeLanding/css/owl.theme.css"> -->
    <!-- themify CSS -->
    <link rel="stylesheet" href="themeLanding/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="themeLanding/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="themeLanding/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="themeLanding/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="themeLanding/css/style.css">
    
    <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <style type="text/css" media="screen">
        html {
          scroll-behavior: smooth;
        }
        .ql-align-justify {
            white-space: normal !important; 
            text-align: justify !important; 
        }
        .ql-align-left {
            text-align: left !important; 
        }
        .ql-align-right {
            text-align: right !important; 
        }
        .ql-align-center {
            text-align: center !important; 
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #007bff !important;
            border: 1px solid #007bff !important;
            color: #ffffff !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #ced4da !important; 
        }
        .learning_part .learning_member_text2 ul li {
            margin-bottom: 0px !important;
            padding-left: 0px !important;
            padding-top: 0px !important;
            font-size: 16px !important;
        }
        .feature_part .single_feature_part {
            margin-top: 20px !important;
        }
        .dropdown .dropdown-menu {
            background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item {
            color: #ffffff !important;
            background-color: #1D437D !important;
        }
        
        .dropdown .dropdown-menu .dropdown-item:hover {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item.active {
          color: #f9b700 !important; 
          background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item:active {
          color: #f9b700 !important;
          background-color: #1D437D !important; 
        }

        /*.dropdown-item.active, .dropdown-item:active {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }*/
        .main_menu .main-menu-item ul li .nav-link:hover {
          color: #f9b700 !important;
          /*color: #000000;*/
        }
        .navbar-light .navbar-nav .active>.nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show>.nav-link {
            color: #f9b700 !important;
        }
        .accordion .collapse-border-item.card:first-child {
             border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important; 
        }
        .accordion .collapse-border-item.card {
            margin-bottom: 20px !important; 
        }
        .accordion .collapse-border-item.card .card-header {
            background-color: rgba(0, 0, 0, 0.03) !important; 
        }
        .btn_2:hover, .main_menu .navbar-toggler:after, .feature_part .single_feature:hover span {
            background-image: linear-gradient(to left, #1D437D 0%, #137BBE 51%, #1668A8 100%) !important;
        }
        @font-face {
          font-family: 'GothamBold';
          src: url('fonts/Gotham-Font/GothamBold.ttf')  format('truetype');
        }
        @font-face {
          font-family: 'GothamNarrow';
          src: url('fonts/Gotham-Font/GothamMedium.ttf')  format('truetype');
        }
        .special_cource .single_special_cource .special_cource_text {
            padding: 35px 35px 20px !important;
        }
    </style>
</head>

<body>
    
    <!-- Component Vue -->
    <div id="app">      
    </div>

    <script src="{{ mix('js/app.js') }}" type="text/javascript" ></script> 

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="themeLanding/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="themeLanding/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="themeLanding/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="themeLanding/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="themeLanding/js/owl.carousel.min.js"></script>
    <!-- <script src="themeLanding/js/owl.carousel.js"></script> -->
    <script src="themeLanding/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/slick.min.js"></script>
    <script src="themeLanding/js/jquery.counterup.min.js"></script>
    <script src="themeLanding/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="themeLanding/js/custom.js"></script>

    <script src="theme/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="theme/app-assets/js/scripts/extensions/sweet-alerts.js"></script>
</body>

</html>