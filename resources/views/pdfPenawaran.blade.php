<!DOCTYPE html>
<html>
 <head>
    <style>
      @page {
            size: 250mm 337mm;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
      #watermark { position: fixed; bottom: 0px; right: 0px; width: 100%; height: 100%; z-index: -1; }
      .page-break {
          page-break-after: always;
      }
    </style>
  </head>

 <body bgcolor="white">
  <div id="watermark">
    <img src="uploads/kopSuratLow.png" alt="" style="height:auto; width:100%;"/>
  </div>
  <div style="margin:75px75px75px75px !important;">
    <font face="Arial" color="black" size="3"> 
    	<p align="center"> 
        <br><br><br><br><br>
        <b>BERITA ACARA</b><br>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td></td>
                <td style="text-align: center; vertical-align: middle;">TENTANG</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td style="text-align: center; vertical-align: middle;">{{ $dataFix->nama_pekerjaan1 }}</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Pada hari ini {{ $dataFix->hari }} tanggal {{ $dataFix->tanggal }} bulan {{ $dataFix->bulan }} tahun {{ $dataFix->tahun }}, telah mengadakan rapat pemasukan dan pembukaan penawaran untuk:</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Pekerjaan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->nama_pekerjaan2}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Lokasi</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->lokasi}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Rapat Dipimpin</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->pimpinan_rapat}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Jabatan</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->jabatan_pimpinan}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">Dimulai jam</td>
                <td width="5%">:</td>
                <td style="text-align: justify; vertical-align: middle;" width="75%">{{$dataFix->dimulai_jam}}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="20%">Peserta Rapat :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>

          @if (count($dataFix->peserta) > 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="20%">PT. KIW (Persero) :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              @foreach ($dataFix->peserta as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;" width="20%">{{$row->nama}}</td>
                  <td width="5%">:</td>
                  <td style="text-align: justify; vertical-align: middle;"width="75%">{{$row->jabatan}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @if (count($dataFix->penyedia) > 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%"> 
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="20%">Penyedia jasa :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="75%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              @foreach ($dataFix->penyedia as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;" width="20%">{{$row->nama_vendor}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @if (substr_count($dataFix->hasil,'<p>') > 10)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td style="text-align: justify; vertical-align: middle;text-decoration: underline;" width="50%">Pokok - pokok penjelasan :</td>
                <td width="5%"></td>
                <td style="text-align: justify; vertical-align: middle;" width="45%"></td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td width="100%">{!! $dataFix->hasil !!}</td>
              </tr>
              <tr>
                <td colspan="3"><br></td>
              </tr>
            </tbody>
          </table>

          @if (count($dataFix->penawaran) >= 15)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br>
          @endif
          <table border="1" width="100%" style="border-collapse: collapse;">
            <thead>
              <tr>
                <th>NO</th>
                <th>PENYEDIA JASA</th>
                <th>HARGA PENAWARAN</th>
                <th>KET</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($dataFix->penawaran as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;" width="40%">{{$row->nama_vendor}}</td>
                  <td style="text-align: justify; vertical-align: middle;" width="40%">Rp. {{number_format($row->penawaran,2,",",".")}}</td>
                  @if ($row->validasi_penawaran == 2)
                    <td style="text-align: justify; vertical-align: middle;" width="15%">SAH</td>
                  @else
                    <td style="text-align: justify; vertical-align: middle;" width="15%">TIDAK SAH</td>
                  @endif
                </tr>
              @endforeach
            </tbody>
          </table>
          <br>
          <table border="1" width="100%" style="border-collapse: collapse;">
            <thead>
              <tr>
                <th style="text-align: center; vertical-align: middle;">NO</th>
                <th style="text-align: center; vertical-align: middle;">NAMA REKANAN</th>
                <th style="text-align: center; vertical-align: middle;" colspan="14">LAMPIRAN</th>
                <th style="text-align: center; vertical-align: middle;">HARGA PENAWARAN</th>
                <th style="text-align: center; vertical-align: middle;">JAMINAN</th>
                <th style="text-align: center; vertical-align: middle;">KET</th>
              </tr>
              <tr>
                <th></th>
                <th></th>
                <th style="text-align: center; vertical-align: middle;">a</th>
                <th style="text-align: center; vertical-align: middle;">b</th>
                <th style="text-align: center; vertical-align: middle;">c</th>
                <th style="text-align: center; vertical-align: middle;">d</th>
                <th style="text-align: center; vertical-align: middle;">e</th>
                <th style="text-align: center; vertical-align: middle;">f</th>
                <th style="text-align: center; vertical-align: middle;">g</th>
                <th style="text-align: center; vertical-align: middle;">h</th>
                <th style="text-align: center; vertical-align: middle;">i</th>
                <th style="text-align: center; vertical-align: middle;">j</th>
                <th style="text-align: center; vertical-align: middle;">k</th>
                <th style="text-align: center; vertical-align: middle;">l</th>
                <th style="text-align: center; vertical-align: middle;">m</th>
                <th style="text-align: center; vertical-align: middle;">n</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($dataFix->penawaran as $key => $row)
                <tr>
                  <td width="5%">{{$key+1}}.</td>
                  <td style="text-align: justify; vertical-align: middle;">{{$row->nama_vendor}}</td>
                    @foreach ($row->dokumen_penawaran as $keys => $rows)
                      @if ($rows->dokumen_penawaran == null || $rows->dokumen_penawaran == '')
                        <td style="text-align: center; vertical-align: middle;"><i class="feather icon-minus" aria-hidden="true"></i>-</td>
                      @else
                        <td style="text-align: center; vertical-align: middle;"><i class="feather icon-check" aria-hidden="true"></i>v</td>
                      @endif
                    @endforeach
                  
                  <td style="text-align: justify; vertical-align: middle;">Rp. {{number_format($row->penawaran,2,",",".")}}</td>
                  <td style="text-align: justify; vertical-align: middle;">Rp. {{number_format($row->jaminan_penawaran,2,",",".")}}</td>
                  @if ($row->validasi_penawaran == 2)
                    <td style="text-align: justify; vertical-align: middle;" width="15%">SAH</td>
                  @else
                    <td style="text-align: justify; vertical-align: middle;" width="15%">TIDAK SAH</td>
                  @endif
                </tr>
              @endforeach
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td colspan="3"><br></td>
              </tr>
              <tr>
                <td colspan="3" style="text-align: justify; vertical-align: middle;">Demikian berita acara ini dibuat dengan sesungguhnya untuk dapat dipergunakan seperlunya.</td>
              </tr>
            </tbody>
          </table>

          @if (substr_count($dataFix->hasil,'</p>') <= 10 || substr_count($dataFix->hasil,'</p>') >= 20)
            <div class="page-break"></div>
            <br><br><br><br><br><br><br><br><br>
          @else
            <br><br>
          @endif
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: justify; vertical-align: middle;" width="50%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: justify; vertical-align: middle;" width="50%">Panitia Pengadaan :</td>
                        <td style="text-align: justify; vertical-align: middle;" width="50%"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" width="100%">
                    <tbody>
                      @foreach ($dataFix->peserta as $key => $row)
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%">{{$key+1}}. {{$row->nama}}</td>
                        </tr>
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%" height="100px">
                            <img class="round" src="uploads/signature/{{$row->signature}}" width="150" height="150" alt="...">
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </td>
                <td style="text-align: left; vertical-align: top;" width="50%">
                  <table border="0" width="100%">
                    <tbody>
                      <tr>
                        <td style="text-align: justify; vertical-align: middle;" width="30%">Penyedia jasa :</td>
                        <td width="5%"></td>
                        <td style="text-align: justify; vertical-align: middle;" width="65%"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" width="100%">
                    <tbody>
                      @foreach ($dataFix->penyedia as $key => $row)
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%">{{$key+1}}. {{$row->nama_vendor}}</td>
                        </tr>
                        <tr>
                          <td style="text-align: left; vertical-align: middle;" width="100%" height="100px">
                            <img class="round" src="uploads/signature/{{$row->signature}}" width="150" height="150" alt="...">
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table border="0" width="100%">
            <tbody>
              <tr>
                <td style="text-align: center; vertical-align: middle;" width="100%">PT. Kawasan Industri Wijayakusuma (Persero)</td>
              </tr>
              <tr>
                <td style="text-align: center; vertical-align: middle;" width="100%">Semarang, Tahun {{$dataFix->tahun2}}</td>
              </tr>
              <tr>
                <td style="text-align: center; vertical-align: middle;" width="100%" height="100px">
                  <img class="round" src="uploads/signature/{{$dataFix->signature}}" width="150" height="150" alt="...">
                </td>
              </tr>
              <tr>
                <td style="text-align: center; vertical-align: middle;" width="100%">{{$dataFix->pimpinan_rapat}}</td>
              </tr>
              <tr>
                <td style="text-align: center; vertical-align: middle;" width="100%">{{$dataFix->jabatan_pimpinan}}</td>
              </tr>
            </tbody>
          </table>

    	</p>
    </font>
  </div>
 </body>
</html>