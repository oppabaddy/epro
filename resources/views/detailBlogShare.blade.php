<!doctype html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Semua Murid Semua Guru</title>

    <meta name="twitter:title"       content="{{ $dataFix->judul }} ">
    <meta name="twitter:description" content=" {{ date('j F Y', strtotime($dataFix->tanggal_publish)) }}">
    <meta name="twitter:image"       content="https://semuamuridsemuaguru.id/uploads/kegiatan/{{ $dataFix->gambar }}">
    <meta name="twitter:card"        content="summary_large_image">

    <meta property="og:url"                content="https://semuamuridsemuaguru.id/detailBlogShare?id={{ $dataFix->id }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{ $dataFix->judul }}" />
    <meta property="og:description"        content="{{ date('j F Y', strtotime($dataFix->tanggal_publish)) }}" />
    <meta property="og:image"              content="https://semuamuridsemuaguru.id/uploads/blog/{{ $dataFix->gambar }}" />

    <link rel="icon" href="themeLanding/img/logo-warna2.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="themeLanding/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="themeLanding/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="themeLanding/css/owl.carousel.min.css">
    <!-- <link rel="stylesheet" href="themeLanding/css/owl.carousel.css"> -->
    <!-- <link rel="stylesheet" href="themeLanding/css/owl.theme.css"> -->
    <!-- themify CSS -->
    <link rel="stylesheet" href="themeLanding/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="themeLanding/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="themeLanding/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="themeLanding/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="themeLanding/css/style.css">

    <link rel="stylesheet" type="text/css" href="theme/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <style type="text/css" media="screen">
        html {
          scroll-behavior: smooth;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #007bff !important;
            border: 1px solid #007bff !important;
            color: #ffffff !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #ced4da !important;
        }
        .learning_part .learning_member_text2 ul li {
            margin-bottom: 0px !important;
            padding-left: 0px !important;
            padding-top: 0px !important;
            font-size: 16px !important;
        }
        .feature_part .single_feature_part {
            margin-top: 20px !important;
        }
        .dropdown .dropdown-menu {
            background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item {
            color: #ffffff !important;
            background-color: #1D437D !important;
        }

        .dropdown .dropdown-menu .dropdown-item:hover {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item.active {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }
        .dropdown .dropdown-menu .dropdown-item:active {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }

        /*.dropdown-item.active, .dropdown-item:active {
          color: #f9b700 !important;
          background-color: #1D437D !important;
        }*/
        .main_menu .main-menu-item ul li .nav-link:hover {
          color: #f9b700 !important;
          /*color: #000000;*/
        }
        .navbar-light .navbar-nav .active>.nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show>.nav-link {
            color: #f9b700 !important;
        }
        .accordion .collapse-border-item.card:first-child {
             border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important;
        }
        .accordion .collapse-border-item.card {
            margin-bottom: 20px !important;
        }
        .accordion .collapse-border-item.card .card-header {
            background-color: rgba(0, 0, 0, 0.03) !important;
        }
        .btn_2:hover, .main_menu .navbar-toggler:after, .feature_part .single_feature:hover span {
            background-image: linear-gradient(to left, #1D437D 0%, #137BBE 51%, #1668A8 100%) !important;
        }
        @font-face {
          font-family: 'GothamBold';
          src: url('fonts/Gotham-Font/GothamBold.ttf')  format('truetype');
        }
        @font-face {
          font-family: 'GothamNarrow';
          src: url('fonts/Gotham-Font/GothamMedium.ttf')  format('truetype');
        }
    </style>
</head>

<body>

    <div>
      <!-- breadcrumb start-->
      <section class="breadcrumb breadcrumb_bg">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="breadcrumb_iner text-center">
                          <div class="breadcrumb_iner_item">
                              <h2 style="font-family: 'GothamBold'">Detail Blog</h2>
                              <p style="font-family: 'GothamNarrow'">Program<span>/</span>Blog</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- breadcrumb start-->

      <!-- List KOP -->
      <section class="special_cource padding_top" style="padding-bottom: 50px;" id="listKop">
          <div class="container">
              <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12" >
                  <div class="row">
                    <div class="col-md-6" align="left">
                      <a type="button" class="btn_2" onclick="back()" style="background-color:#ffffff">Back</a>
                    </div>
                    <div class="form-group col-md-6" align="right">
                    </div>
                  </div>
                  <div class="card-body">
                    <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 100%; padding: 10px; width: 100%;'>
                          <div class='content' style='box-sizing: border-box; display: block; margin: 0 auto; max-width: 100%; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                            <table class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;'>
                                  <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' colspan="2">
                                        <h1>{{ $dataFix->judul }}</h1>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' colspan="2">
                                        <h4>{{ date('d-m-Y', strtotime($dataFix->tanggal_publish)) }}</h4>
                                        <hr>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        {!! html_entity_decode( $dataFix->content) !!}
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        <hr>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td align="left">
                                        @if($dataFix->link_instagram !== null)
                                            <a href="'http://www.instagram.com/'+$dataFix->link_instagram" target="_blank" style="padding-right:30px">
                                              <b><i class="ti-instagram"></i> Instagram</b>
                                            </a>
                                        @endif
                                        @if($dataFix->link_fb !== null)
                                          <a :href="'http://www.facebook.com/'+$dataFix->link_fb" target="_blank" style="padding-right:30px">
                                            <b><i class="ti-facebook"></i> Facebook</b>
                                          </a>
                                        @endif
                                        @if($dataFix->link_youtube !== null)
                                          <a :href="'http://www.youtube.com/'+$dataFix->link_youtube" target="_blank">
                                            <b><i class="ti-youtube"></i> Youtube</b>
                                          </a>
                                        @endif
                                      </td>
                                      <td align="right">
                                        <!-- <a class="btn btn-flat-dark btn-sm" href="">
                                          <b><i class="ti-share"></i> Bagikan</b>
                                        </a> -->
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            <!-- END MAIN CONTENT AREA -->
                            </table>

                            <!-- START FOOTER -->
                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                              </table>
                            <!-- END FOOTER -->
                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-sm-12 col-lg-12 col-xl-12" >
                  <div class="row">
                  </div>
                  <div class="card-body">
                    <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 100%; padding: 10px; width: 100%;'>
                          <div class='content' style='box-sizing: border-box; display: block; margin: 0 auto; max-width: 100%; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                            <table class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;'>
                                  <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>
                                        <h3>Comments</h3>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>
                                        <div class="row">
                                          <div class="col-md-6" style="padding: 10px;">
                                            <label><p>Email</p></label>
                                            <input
                                              type="textfield"
                                              class="form-control"
                                              id="email"
                                              autocomplete="off"
                                              required
                                            >
                                          </div>
                                          <div class="col-md-6" style="padding: 10px;">
                                            <label><p>Nama</p></label>
                                            <input
                                              type="textfield"
                                              class="form-control"
                                              id="nama"
                                              autocomplete="off"
                                              required
                                            >
                                          </div>
                                          <div class="col-md-12" style="padding: 10px;">
                                            <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="100" rows="5"  id="comment" required></textarea>
                                          </div>
                                          <div class="col-md-12" style="padding: 10px;">
                                            <a type="button" class="btn_2" onclick="comment()" style="float:right;">Send</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            <!-- END MAIN CONTENT AREA -->
                            </table>

                            <!-- START FOOTER -->
                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                              </table>
                            <!-- END FOOTER -->
                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                      </tr>
                    </table>

                    <input type="text" id="sessLog" value="{{Session::get('login')}}" style="display: none;"/>
                    <input type="text" id="sessFirst" value="{{Session::get('first_name')}}" style="display: none;"/>
                    <input type="text" id="sessLast" value="{{Session::get('last_name')}}" style="display: none;"/>
                    <input type="text" id="sessEmail" value="{{Session::get('email')}}" style="display: none;"/>
                    <input type="text" id="sessIdUser" value="{{Session::get('id')}}" style="display: none;"/>
                    <input type="text" id="sessUsername" value="{{Session::get('username')}}" style="display: none;"/>
                    <input type="text" id="sessRole" value="{{Session::get('role')}}" style="display: none;"/>
                    <input type="text" id="sessIdPost" value="{{$dataFix->id}}" style="display: none;"/>

                    <?php $detailCommentToShow = 5;?>

                    @for ($i = 0; $i <= $detailCommentToShow; $i++)
                        @if($i < count($dataCom))
                            <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;'>
                              <tr>
                                <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                                <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 100%; padding: 10px; width: 100%;'>
                                  <div class='content' style='box-sizing: border-box; display: block; margin: 0 auto; max-width: 100%; padding: 10px;'>

                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <table class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;'>

                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;'>
                                          <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                                            <tr>
                                              <td style='font-family: sans-serif; font-size: 14px; vertical-align: bottom;'>
                                                <h3><img src="{{$dataCom[$i]->photo_profile}}" alt="" style="width:60px;height:width:60px;border-radius: 30px;border: 1px solid #00000033;">&nbsp;&nbsp;{{$dataCom[$i]->nama}}</h3>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>
                                                <p>{{$dataCom[$i]->comment}}</p>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td style='font-family: sans-serif; font-size: 10px;' valign="bottom" align="right">
                                                <p>{{date('d-m-Y H:i', strtotime($dataCom[$i]->created_at))}}</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    <!-- END MAIN CONTENT AREA -->
                                    </table>

                                    <!-- START FOOTER -->
                                      <table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'>
                                      </table>
                                    <!-- END FOOTER -->
                                  <!-- END CENTERED WHITE CONTAINER -->
                                  </div>
                                </td>
                                <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td>
                              </tr>
                            </table>
                        @endif
                    @endfor

                    @if(count($dataCom) > 5)
                        <!-- <div class="col-sm-12 col-lg-12 col-xl-12" style="margin-bottom:50px;margin-top:50px;" align="center">
                          <a type="button" class="btn_2" onclick="$detailCommentToShow += 5" style="background-color:#ffffff">Selanjutnya</a>
                        </div> -->
                    @endif

                  </div>
                </div>
              </div>
          </div>
      </section>
      <!-- End List KOP -->

    </div>

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="themeLanding/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="themeLanding/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="themeLanding/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="themeLanding/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="themeLanding/js/owl.carousel.min.js"></script>
    <!-- <script src="themeLanding/js/owl.carousel.js"></script> -->
    <script src="themeLanding/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="themeLanding/js/slick.min.js"></script>
    <script src="themeLanding/js/jquery.counterup.min.js"></script>
    <script src="themeLanding/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="themeLanding/js/custom.js"></script>

    <script src="theme/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="theme/app-assets/js/scripts/extensions/sweet-alerts.js"></script>

    <script>
        $(document).ready(function() {
          var sessLog = $('#sessLog').val();
          if(sessLog == true){
            $('#email').prop('disabled', true);
            $('#nama').prop('disabled', true);
            $('#email').val($("#sessEmail").val());
            $('#nama').val($("#sessFirst").val()+' '+$("#sessLast").val());
          } else {
            $('#email').prop('disabled', false);
            $('#nama').prop('disabled', false);
          }
        });

        function comment(){
            var idUser = $("#sessIdUser").val();
            var username = $("#sessUsername").val();
            var role = $("#sessRole").val();
            var idPost = $("#sessIdPost").val();
            var email = $('#email').val();
            var nama = $('#nama').val();
            var comment = $('#comment').val();

            Swal.fire({
              title: "Are you sure?",
              text: "You won't be able to revert this!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, insert it!',
              confirmButtonClass: 'btn btn-primary',
              cancelButtonClass: 'btn btn-danger ml-1',
              buttonsStyling: false,
            }).then(function (result) {
              if (result.value) {
                if(comment !== ""){
                    $.post("/api/Comment", {
                      _token: "{{csrf_token()}}",
                      email: email,
                      id_post: idPost,
                      id_user: idUser,
                      role: role,
                      username: username,
                      nama: nama,
                      comment: comment,
                      tipe: 'Blog',
                    })
                    .then(response => {
                      if (response.status == true) {
                        Swal.fire({
                          type: "success",
                          title: "Inserted!",
                          text: "Your file has been Insert.",
                          confirmButtonClass: "btn btn-success"
                        });
                        location.reload();
                      } else {
                        Swal.fire({
                          type: "error",
                          title: "Failed!",
                          text: "Your file has been failled to Insert.",
                          confirmButtonClass: "btn btn-danger"
                        });
                        location.reload();
                      }
                    });
                } else {
                  Swal.fire(
                    {
                      type: "error",
                      title: "Failed!",
                      text: "Your comment failed",
                      confirmButtonClass: "btn btn-success"
                    });
                  }
                }
              }.bind(this)
            );
        }

        function back(){
            window.location.href = "kegiatan";
        }

    </script>
</body>

</html>