require('./bootstrap');



window.Vue = require('vue'); 

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';
import VueSweetalert2 from 'vue-sweetalert2';
import Moment from 'moment';
import VueI18n from 'vue-i18n';
import SocialSharing from 'vue-social-sharing';
import Meta from 'vue-meta';

Vue.use(VueRouter,VueAxios,Axios,VueSweetalert2,Moment,VueI18n,SocialSharing);
Vue.use(Meta, {
  keyName: "metaInfo",
  refreshOnceOnNavigation: true
});

// file error
import not404 from './components/404.vue';
import not404Land from './components/404Land.vue';

// file base
import App from './components/base/App.vue';

// file Master
import Role from './components/master/role/Role.vue';
import MenuLanding from './components/master/menulanding/MenuLanding.vue';
import SettingEmail from './components/master/email/SettingEmail.vue';
import TentangKami from './components/master/tentangkami/TentangKami.vue';
import Banner from './components/master/banner/Banner.vue';
import VisiMisi from './components/master/visimisi/VisiMisi.vue';
import MainBanner from './components/master/mainbanner/MainBanner.vue';
import AreWe from './components/master/arewe/AreWe.vue';
import Pengumuman from './components/master/pengumuman/Pengumuman.vue';
import Berita from './components/master/berita/Berita.vue';
import SettingFooter from './components/master/footer/SettingFooter.vue';
import SettingCounter from './components/master/counter/SettingCounter.vue';
import Divisi from './components/master/divisi/Divisi.vue';
import JenisVendor from './components/master/jenisvendor/JenisVendor.vue';
import KategoriPekerjaan from './components/master/kategoripekerjaan/KategoriPekerjaan.vue';
import SubKlasifikasi from './components/master/subklasifikasi/SubKlasifikasi.vue';
import Jabatan from './components/master/jabatan/Jabatan.vue';


// file person
import Persons from './components/persons/Persons.vue';

// file user
import Users from './components/user/User.vue';
import UserstoVendor from './components/user/UsertoVendor.vue';

// file kop 
import Kop from './components/kop/Kop.vue';
import ReqKop from './components/kop/ReqKop.vue';
import EmailToKop from './components/kop/EmailToKop.vue';

// file vendor
import Vendor from './components/vendor/Vendor.vue';
import ReqVendor from './components/vendor/ReqVendor.vue';
import EmailToVendor from './components/vendor/EmailToVendor.vue';
import MyVendor from './components/vendor/MyVendor.vue';

// file manage committee
import ManageCommittee from './components/committee/ManageCommittee.vue';

import Dashboard from './components/Dashboard.vue';

//file landing
import HomeLanding from './components/landing/Home.vue';
import RegisterLanding from './components/landing/Register.vue';
import ProfileVendorLanding from './components/landing/ProfileVendor.vue';
import TentangKamiLanding from './components/landing/landtentangkami/LandTentangKami.vue';
import TentangVisiMisi from './components/landing/landtentangkami/LandVisiMisi.vue';
import DetailPelelangan from './components/landing/paketlelang/DetailPelelangan.vue';
import ListPelelangan from './components/landing/paketlelang/ListPelelangan.vue';
import DetailPengumuman from './components/landing/pengumuman/DetailPengumuman.vue';
import ListPengumuman from './components/landing/pengumuman/ListPengumuman.vue';
import DetailBerita from './components/landing/berita/DetailBerita.vue';
import ListBerita from './components/landing/berita/ListBerita.vue';
import AssessmentKapasitas from './components/landing/assesment/AssesmentKapasitas.vue';
import AntarKlaster from './components/landing/assesment/AntarKlaster.vue';
import TiapKlaster from './components/landing/assesment/TiapKlaster.vue';
import Kepengurusan from './components/landing/kepengurusan/Kepengurusan.vue';

// file management
import ManageComment from './components/management/ManageComment.vue';

// file paketlelang
import PlInformasiPaket from './components/paketlelang/penunjukanlangsung/InformasiPekerjaan.vue';
import PlDokumenKualifikasi from './components/paketlelang/penunjukanlangsung/DokumenKualifikasi.vue';
import PlAanwijzing from './components/paketlelang/penunjukanlangsung/Aanwijzing.vue';
import PlPenawaran from './components/paketlelang/penunjukanlangsung/Penawaran.vue';
import PlEvaluasi from './components/paketlelang/penunjukanlangsung/Evaluasi.vue';
import PlKlarifikasiNegosiasi from './components/paketlelang/penunjukanlangsung/KlarifikasiNegosiasi.vue';
import PlPenetapanPemenang from './components/paketlelang/penunjukanlangsung/PenetapanPemenang.vue';

import PtInformasiPaket from './components/paketlelang/pelelanganterbatas/InformasiPekerjaan.vue';
import PtDokumenKualifikasi from './components/paketlelang/pelelanganterbatas/DokumenKualifikasi.vue';
import PtAanwijzing from './components/paketlelang/pelelanganterbatas/Aanwijzing.vue';
import PtPenawaran from './components/paketlelang/pelelanganterbatas/Penawaran.vue';
import PtEvaluasi from './components/paketlelang/pelelanganterbatas/Evaluasi.vue';
import PtKlarifikasiNegosiasi from './components/paketlelang/pelelanganterbatas/KlarifikasiNegosiasi.vue';
import PtPenetapanPemenang from './components/paketlelang/pelelanganterbatas/PenetapanPemenang.vue';

import UmInformasiPaket from './components/paketlelang/pelelanganumum/InformasiPekerjaan.vue';
import UmDokumenKualifikasi from './components/paketlelang/pelelanganumum/DokumenKualifikasi.vue';
import UmAanwijzing from './components/paketlelang/pelelanganumum/Aanwijzing.vue';
import UmPenawaran from './components/paketlelang/pelelanganumum/Penawaran.vue';
import UmEvaluasi from './components/paketlelang/pelelanganumum/Evaluasi.vue';
import UmKlarifikasiNegosiasi from './components/paketlelang/pelelanganumum/KlarifikasiNegosiasi.vue';
import UmPenetapanPemenang from './components/paketlelang/pelelanganumum/PenetapanPemenang.vue';

// file rekappaketlelang
import RePenunjukanLangsung from './components/rekaplelang/RekapPenunjukanLangsung.vue';
import RePelelanganTerbatas from './components/rekaplelang/RekapPelelanganTerbatas.vue';
import RePelelanganUmum from './components/rekaplelang/RekapPelelanganUmum.vue';

// file beritaacara
import BaPenunjukanLangsung from './components/beritaacara/BAPenunjukan.vue';
import BaTerbatas from './components/beritaacara/BATerbatas.vue';
import BaUmum from './components/beritaacara/BAUmum.vue';

// file penilaianvendor
import PenilaianVendor from './components/penilaianvendor/PenilaianVendor.vue';

// file logactivity
import LogActivity from './components/logactivity/LogActivity.vue';

// file addendum
import Addendum from './components/addendum/Addendum.vue';

// file dokumenkontrak
import DokumenKontrak from './components/dokumenkontrak/DokumenKontrak.vue';

import Profile from './components/base/Profile.vue';



// membuat router

const routes = [
    {
        name: 'home',
        path: '/',
        component: HomeLanding
    },
    {
        name: 'not404',
        path: '/not404',
        component: not404
    },
    {
        name: 'not404land',
        path :'*',
        component: not404Land
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'role',
        path: '/role',
        component: Role
    },
    {
        name: 'menulanding',
        path: '/menulanding',
        component: MenuLanding
    },
    {
        name: 'settingemail',
        path: '/settingemail',
        component: SettingEmail
    },
    {
        name: 'mfooter',
        path: '/mfooter',
        component: SettingFooter
    },
    {
        name: 'mcounter',
        path: '/mcounter',
        component: SettingCounter
    },
    {
        name: 'tentangkami',
        path: '/tentangkami',
        component: TentangKami
    },
    {
        name: 'banner',
        path: '/banner',
        component: Banner
    },
    {
        name: 'mdivisi',
        path: '/mdivisi',
        component: Divisi
    },
    {
        name: 'mjabatan',
        path: '/mjabatan',
        component: Jabatan
    },
    {
        name: 'mjenisvendor',
        path: '/mjenisvendor',
        component: JenisVendor
    },
    {
        name: 'mkategoripekerjaan',
        path: '/mkategoripekerjaan',
        component: KategoriPekerjaan
    },
    {
        name: 'msubklasifikasi',
        path: '/msubklasifikasi',
        component: SubKlasifikasi
    },
    {
        name: 'visimisi',
        path: '/visimisi',
        component: VisiMisi
    },
    {
        name: 'mainbanner',
        path: '/mainbanner',
        component: MainBanner
    },
    {
        name: 'whoarewe',
        path: '/whoarewe',
        component: AreWe
    },
    {
        name: 'mpengumuman',
        path: '/mpengumuman',
        component: Pengumuman
    },
    {
        name: 'mberita',
        path: '/mberita',
        component: Berita
    },
    {
        name: 'persons',
        path: '/persons',
        component: Persons
    },
    {
        name: 'users',
        path: '/users',
        component: Users
    },
    {
        name: 'userstovendor',
        path: '/userstovendor',
        component: UserstoVendor
    },
    {
        name: 'kop',
        path: '/kop',
        component: Kop
    },
    {
        name: 'reqkop',
        path: '/reqkop',
        component: ReqKop
    },
    {
        name: 'emailtokop',
        path: '/emailtokop',
        component: EmailToKop
    },
    {
        name: 'register',
        path: '/regIn',
        component: RegisterLanding
    },
    {
        name: 'profilevendor',
        path: '/profilevendor',
        component: ProfileVendorLanding

    },
    {
        name: 'profile',
        path: '/profile',
        component: Profile
    },
    {
        name: 'landtentangkami',
        path: '/landtentangkami',
        component: TentangKamiLanding
    },
    {
        name: 'visi&misi',
        path: '/visi&misi',
        component: TentangVisiMisi
    },
    {
        name: 'detailpelelangan',
        path: '/detailpelelangan',
        component: DetailPelelangan
    },
    {
        name: 'paketlelang',
        path: '/paketlelang',
        component: ListPelelangan
    },
    {
        name: 'detailpengumuman',
        path: '/detailpengumuman',
        component: DetailPengumuman
    },
    {
        name: 'pengumuman',
        path: '/pengumuman',
        component: ListPengumuman
    },
    {
        name: 'detailberita',
        path: '/detailberita',
        component: DetailBerita
    },
    {
        name: 'berita',
        path: '/berita',
        component: ListBerita
    },
    {
        name: 'asesmenkapasitas',
        path: '/asesmenkapasitas',
        component: AssessmentKapasitas
    },
    {
        name: 'antarklaster',
        path: '/antarklaster',
        component: AntarKlaster
    },
    {
        name: 'tiapklaster',
        path: '/tiapklaster',
        component: TiapKlaster
    },
    {
        name: 'kepengurusan',
        path: '/kepengurusan',
        component: Kepengurusan
    },
    {
        name: 'managecomment',
        path: '/managecomment',
        component: ManageComment
    },
    {
        name: 'datavendor',
        path: '/datavendor',
        component: Vendor
    },
    {
        name: 'reqvendor',
        path: '/reqvendor',
        component: ReqVendor
    },
    {
        name: 'emailtovendor',
        path: '/emailtovendor',
        component: EmailToVendor
    },
    {
        name: 'mydatavendor',
        path: '/mydatavendor',
        component: MyVendor
    },
    {
        name: 'pl_infopekerjaan',
        path: '/pl_infopekerjaan',
        component: PlInformasiPaket
    },
    {
        name: 'pl_aanwijzing',
        path: '/pl_aanwijzing',
        component: PlAanwijzing
    },
    {
        name: 'pl_dokumenkualifikasi',
        path: '/pl_dokumenkualifikasi',
        component: PlDokumenKualifikasi
    },
    {
        name: 'pl_penawaran',
        path: '/pl_penawaran',
        component: PlPenawaran
    },
    {
        name: 'pl_evaluasi',
        path: '/pl_evaluasi',
        component: PlEvaluasi
    },
    {
        name: 'pl_negosiasi',
        path: '/pl_negosiasi',
        component: PlKlarifikasiNegosiasi
    },
    {
        name: 'pl_pemenang',
        path: '/pl_pemenang',
        component: PlPenetapanPemenang
    },
    {
        name: 'pt_infopekerjaan',
        path: '/pt_infopekerjaan',
        component: PtInformasiPaket
    },
    {
        name: 'pt_dokumenkualifikasi',
        path: '/pt_dokumenkualifikasi',
        component: PtDokumenKualifikasi
    },
    {
        name: 'pt_aanwijzing',
        path: '/pt_aanwijzing',
        component: PtAanwijzing
    },
    {
        name: 'pt_penawaran',
        path: '/pt_penawaran',
        component: PtPenawaran
    },
    {
        name: 'pt_evaluasi',
        path: '/pt_evaluasi',
        component: PtEvaluasi
    },
    {
        name: 'pt_negosiasi',
        path: '/pt_negosiasi',
        component: PtKlarifikasiNegosiasi
    },
    {
        name: 'pt_pemenang',
        path: '/pt_pemenang',
        component: PtPenetapanPemenang
    },
    {
        name: 'um_infopekerjaan',
        path: '/um_infopekerjaan',
        component: UmInformasiPaket
    },
    {
        name: 'um_dokumenkualifikasi',
        path: '/um_dokumenkualifikasi',
        component: UmDokumenKualifikasi
    },
    {
        name: 'um_aanwijzing',
        path: '/um_aanwijzing',
        component: UmAanwijzing
    },
    {
        name: 'um_penawaran',
        path: '/um_penawaran',
        component: UmPenawaran
    },
    {
        name: 'um_evaluasi',
        path: '/um_evaluasi',
        component: UmEvaluasi
    },
    {
        name: 'um_negosiasi',
        path: '/um_negosiasi',
        component: UmKlarifikasiNegosiasi
    },
    {
        name: 'um_pemenang',
        path: '/um_pemenang',
        component: UmPenetapanPemenang
    },
    {
        name: 'repenunjukanlangsung',
        path: '/repenunjukanlangsung',
        component: RePenunjukanLangsung
    },
    {
        name: 'repelelanganterbatas',
        path: '/repelelanganterbatas',
        component: RePelelanganTerbatas
    },
    {
        name: 'repelelanganumum',
        path: '/repelelanganumum',
        component: RePelelanganUmum
    },
    {
        name: 'bapenunjukan',
        path: '/bapenunjukan',
        component: BaPenunjukanLangsung
    },
    {
        name: 'baterbatas',
        path: '/baterbatas',
        component: BaTerbatas
    },
    {
        name: 'baumum',
        path: '/baumum',
        component: BaUmum
    },
    {
        name: 'datapenilaianvendor',
        path: '/datapenilaianvendor',
        component: PenilaianVendor
    },
    {
        name: 'dataperubahanschedule',
        path: '/dataperubahanschedule',
        component: Addendum
    },
    {
        name: 'datakontrak',
        path: '/datakontrak',
        component: DokumenKontrak
    },
    {
        name: 'managecommittee',
        path: '/managecommittee',
        component: ManageCommittee
    },
    {
        name: 'datalogaktifitas',
        path: '/datalogaktifitas',
        component: LogActivity
    },
]

const messages = {

  en: {
    message: {
      hello: 'hello world',
      sectionbanner1: 'EVERY CHILD YEARNS TO LEARN',
      sectionbanner2: 'Making The World Of Education Better',
      sectionbanner3: 'An empowered public network that learns, moves and has meaning together because education is our responsibility',
      sectionbannerbtn1: 'REGISTER',
      sectionbannerbtn2: 'SIGN IN',
      sectionbannerbtn3: 'MY DASHBOARD',
      sectionbannerbtn4: 'SIGN OUT',
      sectionhistory1: 'History',
      sectionhistory2: 'Our History',
      sectionhistory3: 'About Us',
      sectionhistorybtn1: 'Read More',
      sectionhistory4: 'Vision & Mission',
      sectioncheck1: 'Partner',
      sectioncheck2: 'Procurement of goods',
      sectioncheck3: 'Construction work',
      sectioncheck4: 'Business Entity Consulting Services',
      sectioncheck5: 'Individual Consulting Services',
      sectionhistory5: 'Who Are We',
      sectionvideo1: 'Our',
      sectionvideo2: 'Videos',
      sectionvideo3: 'Video about activities from SMSG',
      sectionvideo4: 'See Youtube',
      sectionfooter1: 'All Students All Teachers are empowered public networks that learn, move, and have meaning together because EDUCATION IS OUR RESPONSIBILITY',
      sectionfooter2: 'Newsletter',
      sectionfooter3: 'Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.',
      sectionfooter4: 'Contact us',
      sectionfooter5: 'Address :',
      sectionfooter6: 'Phone :',
      sectionfooter7: 'Email :',
      sectionReg1: 'Registration',
      sectionReg2: '<b>Want to Register Your Community / Organization in the #SemuaMuridSemuaGuru Network?</b> <br><br>All Students All Teachers (SMSG) are educational activator networks that facilitate the process of integration, collaboration and innovation in the Indonesian education ecosystem. The SMSG network has members from the Community and Educational Organization (Vendor) and other stakeholders such as Ministries / Institutions, Media, and Corporations. In 2019, SMSG was officially incorporated as an association with Vendor representatives as its founder. SMSG activities in general are online and offline meetings of network members, joint work program implementation, and Vendor capacity building programs. <br><br> Considering the different curation processes that need to be carried out, as well as the different needs related to the nature of the services provided to students and students, SMSG membership is currently not open to institutions whose main activity is to conduct early childhood education to formal tertiary education. <br><br> Lets join #kerjabarengan for education. Register your Community / Organization now.',
      sectionReg3: 'Data Vendor',
      sectionReg4: 'Vendor Name',
      sectionReg5: 'Vendor Description',
      sectionReg6: 'Name PIC',
      sectionReg7: 'Email PIC',
      sectionReg8: 'Telephone PIC',
      sectionReg9: 'Vendor Standing Date',
      sectionReg10: 'Type Vendor',
      sectionReg11: 'Vendor Email',
      sectionReg12: 'Work Cluster',
      sectionReg13: 'Vendor Scope',
      sectionReg14: 'Province',
      sectionReg15: 'City',
      sectionReg16: 'Vendor Website',
      sectionReg17: 'Vendor Twitter',
      sectionReg18: 'Vendor Facebook',
      sectionReg19: 'Vendor Instagram',
      sectionReg20: 'Vendor Youtube',
      sectionReg21: 'Document',
      sectionReg22: 'Company Management',
      sectionReg23: 'Annual Contribution',
      sectionReg24: 'Information',
      sectionReg25: 'Number of Contributions',
      sectionReg26: 'Photo KTP',
      sectionReg27: 'No. NPWP',
      sectionReg28: 'No. PKP',
      sectionReg29: 'No. Telp Vendor',
      sectionReg30: 'No. Fax Vendor',
      sectionReg31: 'Postal Code',
      sectionReg32: 'Branch Office',
      sectionReg33: 'No. Telp Headquarters',
      sectionReg34: 'No. Fax Headquarters',
      sectionReg35: 'Email Headquarters',
      sectionReg36: 'Headquarters Address',
      sectionReg37: 'Vendor Address',
      sectionReg38: 'Deed of Incorporation',
      sectionReg39: 'Date of Stand',
      sectionReg40: 'Notary Public',
      sectionReg41: 'Notary of Change',
      sectionReg42: 'Deed of Change',
      sectionReg43: 'Change Date',
      sectionReg44: 'Name of the owner',
      sectionReg45: 'Address of the Owner',
      sectionReg46: 'No. KTP Owners',
      sectionReg47: 'No. IUJK / SIUP ',
      sectionReg48: 'No. SBU ',
      sectionReg49: 'Validity Period',
      sectionReg50: 'Giving Agency',
      sectionReg51: 'SBU classification',
      sectionReg52: 'IUJK / SIUP classification',
      sectionSejarah1: 'A brief History',
      sectionProfile1: 'Profile Vendor',
      sectionProfile2: 'Vendor Details',
      sectionProfile3: 'PIC Name',
      sectionProfile4: 'PIC Email',
      sectionProfile5: 'Vendor Email', 
      sectionProfile6: 'Vendor Type',
      sectionProfile7: 'Work Cluster',
      sectionProfile8: 'Vendor Scope',
      sectionProfile9: 'Province',
      sectionProfile10: 'City',
      sectionProfile11: 'Description',
      sectionProfilebtn1: 'Detail',
      sectionProfilebtn2: 'Load More',
      sectionPaket1: 'Program',
      sectionPaket2: 'Auction Package',
      sectionPaket3: 'Detail Auction Package',
      sectionAreWebtn1: 'See All',
      sectionPaketbtn1: 'See All Auction',
      sectionPaketbtn2: 'Detail',
      sectionPaketbtn3: 'Back',
      sectionPaketbtn4: 'Previous',
      sectionPaketbtn5: 'Next',
      sectionPaketbtn6: 'Join the auction',
      sectionPengumuman1: 'Program',
      sectionPengumuman2: 'Announcement of Auction',
      sectionPengumuman3: 'Detail Announcement of Auction',
      sectionPengumumanbtn1: 'See All Announcement of Auction',
      sectionPengumumanbtn2: 'Read',
      sectionPengumumanbtn3: 'Back',
      sectionPengumumanbtn4: 'Previous',
      sectionPengumumanbtn5: 'Next',
      sectionBerita1: 'Program',
      sectionBerita2: 'Auction News',
      sectionBerita3: 'Detail Auction News',
      sectionBeritabtn1: 'See All ',
      sectionBeritabtn2: 'Read',
      sectionBeritabtn3: 'Back',
      sectionBeritabtn4: 'Previous',
      sectionBeritabtn5: 'Next',
      sectionJaringan1: 'Network',
      sectionJaringan2: 'Good Message',
      sectionJaringan3: 'Detail Good Message',
      sectionJaringan4: 'Good Practice',
      sectionJaringan5: 'Detail Good Practice',
      sectionJaringanbtn1: 'See All',
      sectionJaringanbtn2: 'Read',
      sectionJaringanbtn3: 'Back',
      sectionKomen: 'Comments',
      sectionKomenbtn1: 'Send',
      support1: 'Supported By',
      sectionAssesment1: 'Impact Measurement',
      sectionAssesment2: 'Capacity Assesment',
      sectionAssesment3: 'ASSESSMENT OF THE CAPACITY OF THE EDUCATION ORGANIZATION ALL STUDENTS OF ALL TEACHERS 2019',
      sectionAssesment4: 'ASSESSMENT OF THE CAPACITY OF THE EDUCATION ORGANIZATION ALL STUDENTS OF ALL TEACHERS 2019',
      sectionAssesmentbtn1: 'Inter-cluster Capacity Assessment 2019',
      sectionAssesmentbtn2: 'Capacity Assessment for each 2019 cluster',
      sectionManage2: 'Management',
      sectionManage1: 'About Us',
    }
  },
  id: {
    message: {
      hello: 'Hai Dunia',
      sectionbanner1: 'Belajar, Bergerak, Bermakna',
      sectionbanner2: 'Pendidikan adalah Tanggung Jawab Kita',
      sectionbanner3: 'Semua Murid Semua Guru, jaringan penggerak pendidikan yang memfasilitasi proses integrasi, kolaborasi, dan inovasi antar pemangku kepentingan pendidikan.',
      sectionbannerbtn1: 'DAFTAR',
      sectionbannerbtn2: 'MASUK',
      sectionbannerbtn3: 'DASBOR SAYA',
      sectionbannerbtn4: 'KELUAR',
      sectionhistory1: 'Sejarah',
      sectionhistory2: 'Sejarah Kami',
      sectionhistory3: 'Tentang Kami',
      sectionhistorybtn1: 'Selengkapnya',
      sectionhistory4: 'Visi & Misi',
      sectioncheck1: 'Rekanan',
      sectioncheck2: 'Pengadaan Barang',
      sectioncheck3: 'Pekerjaan Konstruksi',
      sectioncheck4: 'Jasa Konsultasi Badan Usaha',
      sectioncheck5: 'Jasa Konsultasi Perorangan',
      sectionhistory5: 'Siapa Kami',
      sectionvideo1: 'Video',
      sectionvideo2: 'Kami',
      sectionvideo3: 'Video tentang pengumuman dari SMSG',
      sectionvideo4: 'Lihat Youtube',
      sectionfooter1: 'Semua Murid Semua Guru adalah jaringan publik berdaya yang belajar, bergerak, dan bermakna bersama karena PENDIDIKAN ADALAH TANGGUNG JAWAB KITA',
      sectionfooter2: 'Newsletter',
      sectionfooter3: 'Tetap diperbarui dengan tren terbaru kami Benih surga begitu kata tempat bersayap menghasilkan buah.',
      sectionfooter4: 'Kontak Kami',
      sectionfooter5: 'Alamat :',
      sectionfooter6: 'Telpon :',
      sectionfooter7: 'Email :',
      sectionReg1: 'Registrasi',
      sectionReg2: '<b> Ingin Mendaftar Komunitas / Organisasi Anda di Jaringan #SemuaMuridSemuaGuru? </b> <br> <br> Semua Siswa Semua Guru (SMSG) adalah jaringan penggerak pendidikan yang memfasilitasi proses integrasi, kolaborasi, dan inovasi di Indonesia ekosistem pendidikan. Jaringan SMSG memiliki anggota dari Komunitas dan Organisasi Pendidikan (Vendor) dan pemangku kepentingan lainnya seperti Kementerian / Lembaga, Media, dan Perusahaan. Pada 2019, SMSG secara resmi didirikan sebagai asosiasi dengan perwakilan Vendor sebagai pendirinya. Pengumuman SMSG secara umum adalah pertemuan anggota jaringan secara online dan offline, pelaksanaan program kerja bersama, dan program peningkatan kapasitas Vendor. <br> <br> Mempertimbangkan berbagai proses kurasi yang perlu dilakukan, serta berbagai kebutuhan terkait dengan sifat layanan yang diberikan kepada siswa dan siswa, keanggotaan SMSG saat ini tidak terbuka untuk lembaga yang pengumuman utamanya adalah untuk melakukan pendidikan anak usia dini ke pendidikan tinggi formal. <br> <br> Mari bergabung dengan #kerjabarengan untuk pendidikan. Daftarkan Komunitas / Organisasi Anda sekarang.',
      sectionReg3: 'Data Vendor',
      sectionReg4: 'Nama Vendor',
      sectionReg5: 'Deskripsi Vendor',
      sectionReg6: 'Nama PIC',
      sectionReg7: 'Email PIC',
      sectionReg8: 'Telepon PIC',
      sectionReg9: 'Tanggal Berdiri Vendor',
      sectionReg10: 'Tipe Vendor',
      sectionReg11: 'Email Vendor',
      sectionReg12: 'Klaster Kerja',
      sectionReg13: 'Lingkup Vendor',
      sectionReg14: 'Provinsi',
      sectionReg15: 'Kota',
      sectionReg16: 'Website Vendor',
      sectionReg17: 'Twitter Vendor',
      sectionReg18: 'Facebook Vendor',
      sectionReg19: 'Instagram Vendor',
      sectionReg20: 'Youtube Vendor',
      sectionReg21: 'Dokumen',
      sectionReg22: 'Pengurus Perusahaan',
      sectionReg23: 'Kontribusi Tahunan',
      sectionReg24: 'Informasi',
      sectionReg25: 'Jumlah Kontribusi',
      sectionReg26: 'Foto KTP',
      sectionReg27: 'No. NPWP',
      sectionReg28: 'No. PKP',
      sectionReg29: 'No. Telp Vendor',
      sectionReg30: 'No. Fax Vendor',
      sectionReg31: 'Kode Pos',
      sectionReg32: 'Kantor Cabang',
      sectionReg33: 'No. Telp Kantor Pusat',
      sectionReg34: 'No. Fax Kantor Pusat',
      sectionReg35: 'Email Kantor Pusat',
      sectionReg36: 'Alamat Kantor Pusat',
      sectionReg37: 'Alamat Vendor',
      sectionReg38: 'Akta Pendirian',
      sectionReg39: 'Tanggal Pendirian',
      sectionReg40: 'Notaris',
      sectionReg41: 'Notaris Perubahan',
      sectionReg42: 'Akta Perubahan',
      sectionReg43: 'Tanggal Perubahan',
      sectionReg44: 'Nama Pemilik',
      sectionReg45: 'Alamat Pemilik',
      sectionReg46: 'No. KTP Pemilik',
      sectionReg47: 'No. IUJK / SIUP',
      sectionReg48: 'No. SBU',
      sectionReg49: 'Masa Berlaku',
      sectionReg50: 'Instansi Pemberi',
      sectionReg51: 'Klasifikasi SBU',
      sectionReg52: 'Klasifikasi IUJK / SIUP',
      sectionSejarah1: 'Sejarah Singkat',
      sectionProfile1: 'Profil Vendor',
      sectionProfile2: 'Detail Vendor',
      sectionProfile3: 'Nama PIC',
      sectionProfile4: 'Email PIC',
      sectionProfile5: 'Email Vendor',
      sectionProfile6: 'Tipe Vendor',
      sectionProfile7: 'Work Cluster',
      sectionProfile8: 'Lingkup Vendor',
      sectionProfile9: 'Provinsi',
      sectionProfile10: 'Kota',
      sectionProfile11: 'Keterangan',
      sectionProfilebtn1: 'Detail',
      sectionProfilebtn2: 'Selanjutnya',
      sectionPaket1: 'Program',
      sectionPaket2: 'Paket Lelang',
      sectionPaket3: 'Detail Paket Lelang',
      sectionAreWebtn1: 'Lihat Semua',
      sectionPaketbtn1: 'Lihat Semua Lelang',
      sectionPaketbtn2: 'Detail',
      sectionPaketbtn3: 'Kembali',
      sectionPaketbtn4: 'Sebelumnya',
      sectionPaketbtn5: 'Selanjutnya',
      sectionPaketbtn6: 'Ikut Lelang',
      sectionPengumuman1: 'Program',
      sectionPengumuman2: 'Pengumuman Lelang',
      sectionPengumuman3: 'Detail Pengumuman Lelang',
      sectionPengumumanbtn1: 'Lihat Semua Pengumuman Lelang',
      sectionPengumumanbtn2: 'Baca',
      sectionPengumumanbtn3: 'Kembali',
      sectionPengumumanbtn4: 'Sebelumnya',
      sectionPengumumanbtn5: 'Selanjutnya',
      sectionBerita1: 'Program',
      sectionBerita2: 'Berita Lelang',
      sectionBerita3: 'Detail Berita Lelang',
      sectionBeritabtn1: 'Lihat Semua Berita Lelang',
      sectionBeritabtn2: 'Baca',
      sectionBeritabtn3: 'Kembali',
      sectionBeritabtn4: 'Sebelumnya',
      sectionBeritabtn5: 'Selanjutnya',
      sectionJaringan1: 'Jaringan',
      sectionJaringan2: 'Pesan Baik',
      sectionJaringan3: 'Detail Pesan Baik',
      sectionJaringan4: 'Praktik Baik',
      sectionJaringan5: 'Detail Praktik Baik',
      sectionJaringanbtn1: 'Lihat Semua',
      sectionJaringanbtn2: 'Baca',
      sectionJaringanbtn3: 'Kembali',
      sectionKomen: 'Komentar',
      sectionKomenbtn1: 'Kirim',
      support1: 'Didukung Oleh',
      sectionAssesment1: 'Pengukuran Dampak',
      sectionAssesment2: 'Asesmen Kapasitas',
      sectionAssesment3: 'ASESMEN KAPASITAS KOMUNITAS ORGANISASI PENDIDIKAN SEMUA MURID SEMUA GURU 2019',
      sectionAssesment4: 'ASSESSMENT OF THE CAPACITY OF THE EDUCATION ORGANIZATION ALL STUDENTS OF ALL TEACHERS 2019',
      sectionAssesmentbtn1: 'Asesmen Kapasitas antar klaster 2019',
      sectionAssesmentbtn2: 'Asesmen Kapasitas tiap klaster 2019',
      sectionManage2: 'Kepengurusan',
      sectionManage1: 'Tentang Kami',
    }
  },
}

// Create VueI18n instance with options
const i18n = new VueI18n({
    locale: 'id', // set locale
    messages, // set locale messages
})

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router, i18n }, App)).$mount("#app");

