<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Events\RefreshEvents;

class MasterController extends Controller {
    // mengambil semua data Menu A,B,C
    public function menuA() {
        return DB::table('tb_menu_detail_a')->orderBy('MenuID', 'ASC')->get();
    }
    public function sidemenuA($id) {
        $menu = DB::table('tb_role_user_access as m')
            ->join('tb_menu_detail_a as a', 'a.MenuID', '=', 'm.MenuID')
            ->select(DB::raw('distinct a.*, m.UserID '))
            ->where('m.UserID', '=', "{$id}")->orderBy('MenuID', 'ASC')->get();

        return $menu;
    }
    public function menuB() {
        return DB::table('tb_menu_detail_b')->orderBy('DetailID', 'ASC')->get();
    }
    public function sidemenuB($user, $menu) {
        $menu = json_decode($menu);
        $submenuz = DB::table('tb_menu_detail_b as b')
            ->Join('tb_role_user_access as m', function ($join) {
                $join->on('m.MenuID', '=', 'b.MenuID');
            })
            ->select(DB::raw('distinct b.*'))
            ->where('m.UserID', '=', "{$user}")
            ->where('m.MenuID', '=', "{$menu}")
            ->get();

        $submenux = array();
        foreach ($submenuz as $key) {
            if ($key->Link == "#") {
                $submenu = DB::table('tb_menu_detail_b as b')
                    ->Join('tb_role_user_access as m', function ($join) {
                        $join->on('m.MenuID', '=', 'b.MenuID');
                    })
                    ->select(DB::raw('distinct b.*'))
                    ->where('m.UserID', '=', "{$user}")
                    ->where('m.MenuID', '=', "{$menu}")
                    ->where('b.DetailID', '=', "{$key->DetailID}")
                    ->first();
                array_push($submenux, $submenu);
            } else {
                $DetailID = $menu . $key->DetailID;
                $submenuz2 = DB::table('tb_menu_detail_b as b')
                    ->Join('tb_role_user_access as m', function ($join) {
                        $join->on('m.MenuID', '=', 'b.MenuID');
                    })
                    ->select(DB::raw('distinct m.*'))
                    ->where('m.UserID', '=', "{$user}")
                    ->where('m.MenuID', '=', "{$menu}")
                    ->where('m.DetailID', '=', "{$DetailID}")
                    ->first();

                if (!empty($submenuz2)) {
                    $submenu = DB::table('tb_menu_detail_b as b')
                        ->Join('tb_role_user_access as m', function ($join) {
                            $join->on('m.MenuID', '=', 'b.MenuID');
                        })
                        ->select(DB::raw('distinct b.*, b.Name'))
                        ->where('m.UserID', '=', "{$user}")
                        ->where('m.MenuID', '=', "{$menu}")
                        ->where('b.Link', '=', "{$submenuz2->Link}")
                        ->first();
                    array_push($submenux, $submenu);
                }
            }
        }
        return $submenux;
    }
    public function sidemenuBNew($user,$menu)
    { 
      $menu = explode(",",$menu);
        $submenuz = DB::table('tb_menu_detail_b as b')
            ->Join('tb_role_user_access as m', function($join){
              $join->on('m.MenuID', '=', 'b.MenuID');
          })
            ->select(DB::raw('distinct b.*'))
            ->where('m.UserID', '=', "{$user}")
            ->whereIn('m.MenuID', $menu)
            ->orderBy('m.DetailID', 'asc')
            ->get();

        $submenux = array();
        foreach ($submenuz as $key) {
          if($key->Link == "#"){
            $submenu = DB::table('tb_menu_detail_b as b')
                ->Join('tb_role_user_access as m', function($join){
                  $join->on('m.MenuID', '=', 'b.MenuID');
              })
                ->select(DB::raw('distinct b.*'))
                ->where('m.UserID', '=', "{$user}")
                ->where('m.MenuID', '=', "{$key->MenuID}")
                ->where('b.DetailID', '=', "{$key->DetailID}")
                ->orderBy('m.DetailID', 'asc')
                ->first();

            array_push($submenux, $submenu);
          } else {
            $DetailID = $key->MenuID.$key->DetailID;
            $submenuz2 = DB::table('tb_menu_detail_b as b')
              ->Join('tb_role_user_access as m', function($join){
                $join->on('m.MenuID', '=', 'b.MenuID');
            })
              ->select(DB::raw('distinct m.*'))
              ->where('m.UserID', '=', "{$user}")
              ->where('m.MenuID', '=', "{$key->MenuID}")
              ->where('m.DetailID', '=', "{$DetailID}")
              ->orderBy('m.DetailID', 'asc')
              ->first();

            if(!empty($submenuz2)){
              $submenu = DB::table('tb_menu_detail_b as b')
                ->Join('tb_role_user_access as m', function($join){
                  $join->on('m.MenuID', '=', 'b.MenuID');
              })
                ->select(DB::raw('distinct b.*, b.Name'))
                ->where('m.UserID', '=', "{$user}")
                ->whereIn('m.MenuID', $menu)
                ->where('b.Link', '=', "{$submenuz2->Link}")
                ->orderBy('m.DetailID', 'asc')
                ->first();

            array_push($submenux, $submenu);
            }
          }
        }
        // print_r($submenux);
        return $submenux;
    }
    public function menuC() {
        return DB::table('tb_menu_detail_c')->orderBy('DetailID2', 'ASC')->get();
    }
    public function sidemenuC($user, $menu) {
        $submenuz2 = DB::table('tb_role_user_access as m')
            ->select(DB::raw('distinct m.*'))
            ->where('m.UserID', '=', "{$user}")
            ->where('m.MenuID', '=', "{$menu}")
            ->get();

        $submenux = array();
        foreach ($submenuz2 as $key) {
            $DetailID2 = $key->DetailID2;

            $submenuxz = DB::table('tb_menu_detail_c as c')
                ->Join('tb_role_user_access as m', function ($join) {
                    $join->on('m.MenuID', '=', 'c.MenuID');
                })
                ->select(DB::raw('distinct m.*'))
                ->where('m.UserID', '=', "{$user}")
                ->where('m.MenuID', '=', "{$menu}")
                ->where('m.DetailID2', '=', "{$DetailID2}")
                ->first();

            if (!empty($submenuz2)) {
                $submenu2 = DB::table('tb_menu_detail_c as c')
                    ->Join('tb_role_user_access as m', function ($join) {
                        $join->on('m.MenuID', '=', 'c.MenuID');
                    })
                    ->select(DB::raw('distinct c.*, c.Name'))
                    ->where('m.UserID', '=', "{$user}")
                    ->where('m.MenuID', '=', "{$menu}")
                    ->where('c.Link', '=', "{$submenuxz->Link}")
                    ->first();
                array_push($submenux, $submenu2);
            }
        }
        return $submenux;
    }
    public function sidemenuCNew($user,$menu)
    {
      $menu = explode(",",$menu);
        $submenuz2 = DB::table('tb_role_user_access as m')
          ->select(DB::raw('distinct m.*'))
          ->where('m.UserID', '=', "{$user}")
          ->whereIn('m.MenuID', $menu)
          ->orderBy('m.DetailID2', 'asc')
          ->get();

        $submenux = array();
        foreach ($submenuz2 as $key) {
          $MenuID = $key->MenuID;
          $DetailID2 = $key->DetailID2;
          $submenuxz = DB::table('tb_menu_detail_c as c')
            ->Join('tb_role_user_access as m', function($join){
              $join->on('m.MenuID', '=', 'c.MenuID');
          })
            ->select(DB::raw('distinct m.*'))
            ->where('m.UserID', '=', "{$user}")
            ->where('m.MenuID', '=', "{$MenuID}")
            ->where('m.DetailID2', '=', "{$DetailID2}")
            ->orderBy('m.DetailID2', 'asc')
            ->first();

          if(!empty($submenuz2)){
            $submenu2 = DB::table('tb_menu_detail_c as c')
                ->Join('tb_role_user_access as m', function($join){
                  $join->on('m.MenuID', '=', 'c.MenuID');
              })
                ->select(DB::raw('distinct c.*, c.Name'))
                ->where('m.UserID', '=', "{$user}")
                ->where('m.MenuID', '=', "{$submenuxz->MenuID}")
                ->where('c.Link', '=', "{$submenuxz->Link}")
                ->orderBy('m.DetailID2', 'asc')
                ->first();
            array_push($submenux, $submenu2);
          }
        }
        return $submenux;
    }
    public function action($id, $link) {
        $get = DB::table('tb_role_user_access')
            ->select('Action')
            ->where('UserID', '=', "{$id}")->where('Link', '=', "/" . $link)->orderBy('MenuID', 'ASC')->first();
        $action = (explode(",", $get->Action));
        return $action;
    }

    // menampilkan datatable
    public function getUser(Request $request) {
        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'username');
        $type = $request->input('type', 'asc');
        $users = DB::table('users')
            ->leftjoin('roles', 'roles.id_person', '=', 'users.id_person')
            ->select('users.*', 'roles.first_name', 'roles.last_name')
        // ->where( 'users.status', '=', 1 )
            ->where('users.username', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $users['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $users['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'users' => $users,
        ]);
    }

    //menambah data role
    public function addRole(Request $request) {
        $role_code = $request->role_code;
        $role_name = $request->role_name;
        $role_master = $request->role_master;
        $role_action = $request->role_action;

        DB::table('tb_role')->insert(
            [
                'kd_role' => $role_code,
                'role' => $role_name,
            ]
        );

        $action2 = array();
        foreach ($role_master as $key) {
            $split = explode("-", $key);
            $action = array();
            foreach ($role_action as $keys) {
                $split2 = explode("-", $keys);
                if ($split[3] == $split2[3]) {
                    array_push($action, $split2[4]);
                }
            }
            array_push($action2, $split[0] . "-" . $split[1] . "-" . $split[2] . "-" . $split[3] . "-" . implode(",", $action));
        }

        foreach ($action2 as $keyz) {
            $split3 = explode("-", $keyz);
            DB::table('tb_role_master')->insert(
                [
                    'kd_role' => $role_code,
                    'MenuID' => $split3[0],
                    'DetailID' => $split3[1],
                    'DetailID2' => $split3[2],
                    'Link' => $split3[3],
                    'Action' => $split3[4],
                ]
            );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat baru role akses ".$role_code,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    //menampilkan datatable role
    public function getRole(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $roles = DB::table('tb_role')->where('role', 'LIKE', '%' . $search_query . '%')
            ->orderBy('kd_role', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $roles['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $roles['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'roles' => $roles,
        ]);
    }

    //mengambil semua data role byid
    public function allRoleByID($kd_role) {
        $data = DB::table('tb_role')->where('kd_role', $kd_role)->first();
        return json_encode($data);
    }

    public function roleByID($kd_role) {
        $data = DB::table('tb_role_master')->where('kd_role', $kd_role)->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = $key->MenuID . "-" . $key->DetailID . "-" . $key->DetailID2 . "-" . $key->Link;
        }
        return $response;
    }

    public function actionByID($kd_role) {
        $data = DB::table('tb_role_master')->where('kd_role', $kd_role)->get();

        $response = array();
        foreach ($data as $key) {
            $data2 = explode(",", $key->Action);
            if ($key->Action == null || $key->Action == "") {

            } else {
                foreach ($data2 as $key2) {
                    $response[] = $key->MenuID . "-" . $key->DetailID . "-" . $key->DetailID2 . "-" . $key->Link . "-" . $key2;
                }
            }
        }
        return $response;
    }

    // mengubah data role
    public function updateRole($id, Request $request) {
        $role_code = $request->role_code;
        $role_name = $request->role_name;
        $role_master = $request->role_master;
        $role_action = $request->role_action;

        DB::table('tb_role')->where('kd_role', $role_code)->update(
            [
                'role' => $role_name,
            ]
        );
        DB::table('tb_role_master')
            ->where('kd_role', $role_code)
            ->delete();

        $action2 = array();
        foreach ($role_master as $key) {
            $split = explode("-", $key);
            $action = array();
            foreach ($role_action as $keys) {
                $split2 = explode("-", $keys);
                if ($split[3] == $split2[3]) {
                    array_push($action, $split2[4]);
                }
            }
            array_push($action2, $split[0] . "-" . $split[1] . "-" . $split[2] . "-" . $split[3] . "-" . implode(",", $action));
        }

        foreach ($action2 as $keyz) {
            $split3 = explode("-", $keyz);
            DB::table('tb_role_master')->insert(
                [
                    'kd_role' => $role_code,
                    'MenuID' => $split3[0],
                    'DetailID' => $split3[1],
                    'DetailID2' => $split3[2],
                    'Link' => $split3[3],
                    'Action' => $split3[4],
                ]
            );
        }

        $data = DB::table('tb_role_master')->select('kd_role', 'MenuID', 'DetailID', 'DetailID2', 'Link', 'Action')->where('kd_role', $role_code)->get(); //from master role
        $data2 = DB::table('tb_role_user_access')->select('kd_role', 'MenuID', 'DetailID', 'DetailID2', 'Link', 'Action')->where('kd_role', $role_code)->get(); //from user have access
        $result = array();
        foreach ($data as $va1) {
            $found = false;
            foreach ($data2 as $va2) {
                $x = array_diff((array) $va1, (array) $va2);
                if (empty($x)) {
                    $found = true;
                }
            }
            if (!$found) {
                $result[] = $va1;
            } 
        }
        foreach ($data2 as $va2) {
            $found = false;
            foreach ($data as $va1) {
                $x = array_diff((array) $va2, (array) $va1);
                if (empty($x)) {
                    $found = true;
                }
            }
            if (!$found) {
                $result[] = $va2;
            }
        }

        $fixs = array();
        foreach ($result as $res) {
            $fix = DB::table('tb_role_master')->where('kd_role', "{$res->kd_role}")
                ->where('MenuID', "{$res->MenuID}")
                ->where('DetailID', "{$res->DetailID}")
                ->where('DetailID2', "{$res->DetailID2}")
                ->where('Link', "{$res->Link}")
                ->first();

            array_push($fixs, $fix);
        }
        $fixs = array_filter($fixs);

        $countz = array();
        foreach ($result as $res) {
            $count = DB::table('tb_role_user_access')->where('kd_role', "{$res->kd_role}")
                ->where('MenuID', "{$res->MenuID}")
                ->where('DetailID', "{$res->DetailID}")
                ->where('DetailID2', "{$res->DetailID2}")
                ->where('Link', "{$res->Link}")
                ->where('Action', "{$res->Action}")
                ->count();

            array_push($countz, $count);

            echo ($count);

            if ($count < 1) {
                $data3 = DB::table('users')->select(DB::raw('id as UserID'))->where('role', $role_code)->get();
                foreach ($data3 as $key3) {
                    $set = DB::table('tb_role_master')->select('kd_role', 'MenuID', 'DetailID', 'DetailID2', 'Link', 'Action')->where('kd_role', $res->kd_role)->where('MenuID', $res->MenuID)->where('DetailID', $res->DetailID)->where('DetailID2', $res->DetailID2)->where('Link', $res->Link)->first();
                    DB::table('tb_role_user_access')->insert(
                        [
                            'kd_role' => $set->kd_role,
                            'UserID' => $key3->UserID,
                            'MenuID' => $set->MenuID,
                            'DetailID' => $set->DetailID,
                            'DetailID2' => $set->DetailID2,
                            'Link' => $set->Link,
                            'Action' => $set->Action,
                        ]
                    );
                }
            } else {
                foreach ($result as $key) {
                    DB::table('tb_role_user_access')
                        ->where('kd_role', "{$key->kd_role}")
                        ->where('MenuID', "{$key->MenuID}")
                        ->where('DetailID', "{$key->DetailID}")
                        ->where('DetailID2', "{$key->DetailID2}")
                        ->where('Link', "{$key->Link}")
                        ->delete();
                }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui baru role akses ".$role_code,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // echo json_encode($countz);
        // echo json_encode($fixs);
        try {
            broadcast(new RefreshEvents());
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
        return;
    }

    // menghapus data role
    public function deleteRole(Request $request, $kd_role) {
        $cek = DB::table('tb_role_user_access')->where('kd_role', $kd_role)->count();
        if ($cek > 0) {
            return 205;
        } else {
            DB::table('tb_role')->where('kd_role', $kd_role)->delete();

            DB::table('tb_role_master')->where('kd_role', $kd_role)->delete();

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menghapus role akses ".$kd_role,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );

            return 204;
        }
    }

    //mengambil semua data listmenu byid
    public function getSide($id) {
        $data = DB::table('tb_role_user_access as r')
            ->select('r.*', 'a.Name')
            ->join('tb_menu_detail_a as a', 'r.MenuID', '=', 'a.MenuID')
            ->where('r.UserID', $id)->get();
        return json_encode($data);
    }
    public function getSideName($link) {
        $data = DB::table('tb_role_user_access as r')
            ->select('a.Name as nameA', 'a.ClassId as ClassIdA', 'b.Name as nameB', 'b.ClassId as ClassIdB', 'c.Name as nameC', 'c.ClassId as ClassIdC',)
            ->leftjoin('tb_menu_detail_a as a', 'r.Link', '=', 'a.Link')
            ->leftjoin('tb_menu_detail_b as b', 'r.Link', '=', 'b.Link')
            ->leftjoin('tb_menu_detail_c as c', 'r.Link', '=', 'c.Link')
            ->where('r.Link', "/" . $link)->get();
        return json_encode($data);
    }
    public function getSideCount($link, $id) {
        $data = DB::table('tb_role_user_access as r')
            ->select('a.Name as nameA', 'a.ClassId as ClassIdA', 'b.Name as nameB', 'b.ClassId as ClassIdB', 'c.Name as nameC', 'c.ClassId as ClassIdC',)
            ->leftjoin('tb_menu_detail_a as a', 'r.Link', '=', 'a.Link')
            ->leftjoin('tb_menu_detail_b as b', 'r.Link', '=', 'b.Link')
            ->leftjoin('tb_menu_detail_c as c', 'r.Link', '=', 'c.Link')
            ->where('r.Link', "/" . $link)->where('r.UserID', $id)->count();
        return json_encode($data);
    }
    public function getNavName($link) {
        $linkNew = "";
        if ($link == "kosong") {

        } else {
            $linkNew = $link;
        }
        $data = DB::table('tb_menu_landing_a as a')
            ->select('a.Name as nameA', 'b.Name as nameB')
            ->leftjoin('tb_menu_landing_b as b', 'a.Link', '=', 'b.Link')
            ->where('a.Link', "/" . $linkNew)
            ->orwhere('b.Link', "/" . $linkNew)
            ->get();
        return json_encode($data);
    }

    //menampilkan datatable role
    public function getMaster(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $masters = DB::table('tb_master')->where('nama_master', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_master', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $masters['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $masters['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'masters' => $masters,
        ]);
    }

    //mengambil semua data master byid
    public function allMasterByID($id_master) {
        $data = DB::table('tb_master')->where('id_master', $id_master)->first();
        return json_encode($data);
    }

    // menghapus data master
    public function deleteMaster($id_master) {
        $cek_pest = DB::table('tb_cek_pest')->where('id_master', $id_master)->count();
        $cek_pakan = DB::table('tb_cek_pakan')->where('id_master', $id_master)->count();
        $cek_infra = DB::table('tb_cek_infra')->where('id_master', $id_master)->count();
        if ($cek_pest > 0 || $cek_pakan > 0 || $cek_infra > 0) {
            return 205;
        } else {
            DB::table('tb_master')->where('id_master', $id_master)->delete();
            return 204;
        }
    }

    //menambah data master
    public function addMaster(Request $request) {
        $nama_master = $request->nama_master;
        $type_master = $request->type_master;

        DB::table('tb_master')->insert(
            [
                'nama_master' => $nama_master,
                'type_master' => $type_master,
            ]
        );

        return;
    }

    // mengubah data master
    public function updateMaster($id_master, Request $request) {
        $nama_master = $request->nama_master;

        DB::table('tb_master')->where('id_master', $id_master)->update(
            [
                'nama_master' => $nama_master,
            ]
        );
        return;
    }

    //menambah data Menu Landing Parent
    public function addMenuLanding(Request $request) {
        $menu_name = $request->menu_name;
        $menu_nameEn = $request->menu_nameEn;
        $menu_link = $request->menu_link;
        // $menu_name = strtolower($menu_name);
        // $menu_name = ucfirst($menu_name);
        if (filter_var($menu_link, FILTER_VALIDATE_URL)) {
            $link = $menu_link;
        } else {
            $link = strtolower($request->menu_name);
            $link = str_replace(' ', '', $link);
            $link = "/" . $link;
        }

        $menuID = '';
        $classID = str_replace(' ', '', $request->menu_name);
        $classID = "nav-" . strtolower($classID);
        $alias = strtolower($request->menu_name);
        $alias = str_replace(' ', '', $alias);
        $data = DB::table('tb_menu_landing_a')->select('MenuID')->orderBy('MenuID', 'Desc')->first();
        if (empty($data)) {
            $menuID = '1';
        } else {
            $menuID = $data->MenuID + 1;
        }
        DB::table('tb_menu_landing_a')->insert(
            [
                'MenuID' => $menuID,
                'ClassId' => $classID,
                'Name' => $menu_name,
                'NameEn' => $menu_nameEn,
                'Link' => $link,
                'Alias' => $alias,
                'Active' => "false",
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat baru menu landing parent".$menu_name,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function addMenuChild(Request $request) {
        $menu_id = $request->menu_id;
        $name_child = $request->name_child;
        $name_childEn = $request->name_childEn;
        $name_childLink = $request->name_childLink;

        if (filter_var($name_childLink, FILTER_VALIDATE_URL)) {
            $link = $name_childLink;
        } else {
            $link = strtolower($request->name_child);
            $link = str_replace(' ', '', $link);
            $link = "/" . $link;
        }

        $alias = strtolower($request->name_child);
        $alias = str_replace(' ', '', $alias);

        $id = '';
        $data = DB::table('tb_menu_landing_b')->select('ID')->orderBy('ID', 'Desc')->first();
        if (empty($data)) {
            $id = '1';
        } else {
            $id = $data->ID + 1;
        }

        $detailID = '';
        $data = DB::table('tb_menu_landing_b')->select('DetailID')->where('MenuID', $menu_id)->orderBy('DetailID', 'Desc')->first();
        if (empty($data)) {
            $detailID = '0';
        } else {
            $detailID = $data->DetailID + 1;
        }
        $classID = str_replace(' ', '', $request->name_child);
        $classID = "nav-" . strtolower($classID) . $detailID;

        DB::table('tb_menu_landing_a')->where('MenuID', $menu_id)->update(
            [
                'Link' => "#",
            ]
        );

        DB::table('tb_menu_landing_b')->insert(
            [
                'ID' => $id,
                'MenuID' => $menu_id,
                'DetailID' => $detailID,
                'ClassId' => $classID,
                'Name' => $name_child,
                'NameEn' => $name_childEn,
                'Link' => $link,
                'Alias' => $alias,
                'Active' => "false",
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat baru menu landing child".$name_child,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    // mengubah data master
    public function updatemenulanding($MenuID, Request $request) {
        $menu_name = $request->menu_name;
        $menu_nameEn = $request->menu_nameEn;
        $menu_link = $request->menu_link;

        $detail_id = $request->detail_id;

        if ($detail_id !== "kosong") {
            DB::table('tb_menu_landing_b')->where('MenuID', $MenuID)->where('DetailID', $detail_id)->update(
                [
                    'Name' => $menu_name,
                    'NameEn' => $menu_nameEn,
                    'Link' => $menu_link,
                ]
            );
        } else if ($detail_id == "kosong") {
            DB::table('tb_menu_landing_a')->where('MenuID', $MenuID)->update(
                [
                    'Name' => $menu_name,
                    'NameEn' => $menu_nameEn,
                    'Link' => $menu_link,
                ]
            );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui menu landing ".$menu_name,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    //menampilkan datatable menu Parent
    public function getMenuParent(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $menuParent = DB::table('tb_menu_landing_a')->where('Name', 'LIKE', '%' . $search_query . '%')
            ->orderBy('MenuID', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $menuParent['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $menuParent['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'menuParent' => $menuParent,
        ]);
    }

    public function getMenuParentChild($id) {
        $data = DB::table('tb_menu_landing_a')->where('MenuID', $id)->first();
        return json_encode($data);
    }

    public function getMenuParentChild2($id) {
        $data = DB::table('tb_menu_landing_b')->where('ID', $id)->first();
        return json_encode($data);
    }

    public function getMenuChild(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $menuChild = DB::table('tb_menu_landing_b as b')->where('b.Name', 'LIKE', '%' . $search_query . '%')
            ->join('tb_menu_landing_a as a', 'a.MenuID', '=', 'b.MenuID')
            ->select('b.*', 'a.Name as NameA')
            ->orderBy('b.MenuID', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $menuChild['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $menuChild['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'menuChild' => $menuChild,
        ]);
    }

    public function dropdownparents() {
        $data = DB::table('tb_menu_landing_a')->orderBy('MenuID', 'asc')->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->MenuID,
                'text' => $key->Name,
            );
        }
        return $response;
    }

    // menghapus data parent
    public function deleteMenuParent(Request $request, $MenuID) {
        $cek = DB::table('tb_menu_landing_b')->where('MenuID', $MenuID)->count();
        if ($cek > 0) {
            return 205;
        } else {
            DB::table('tb_menu_landing_a')->where('MenuID', $MenuID)->delete();
            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "menghapus menu landing parent",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
            return 204;
        }
    }

    public function deleteMenuChild(Request $request, $ID) {
        // $cek = DB::table('tb_menu_landing_b')->where('MenuID', $MenuID)->count();
        // if($cek > 0){
        //   return 205;
        // } else {
        DB::table('tb_menu_landing_b')->where('ID', $ID)->delete();
        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "menghapus menu landing child",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return 204;
        // }
    }

    public function navmenuA() {
        $menu = DB::table('tb_menu_landing_a as a')
            ->select(DB::raw('a.*'))
            ->orderBy('MenuID', 'ASC')->get();

        foreach ($menu as $key) {
            if (filter_var($key->Link, FILTER_VALIDATE_URL)) {
                $key->linkhref = "Link";
            } else {
                $key->linkhref = "Not Link";
            }
        }

        return $menu;
    }

    public function navmenuB($menu) {
        $menu = json_decode($menu);
        DB::statement(DB::raw('set @MenuID=-1'));
        $submenuz = DB::table('tb_menu_landing_a as a')
            ->leftjoin('tb_menu_landing_b as b', 'a.MenuID', '=', 'b.MenuID')
            ->where('a.MenuID', $menu)->get(['a.*',
            DB::raw('@MenuID  := @MenuID  + 1 AS rownum')]);

        $submenux = array();
        foreach ($submenuz as $key) {
            if ($key->Link == "#") {
                $submenu = DB::table('tb_menu_landing_b as b')
                    ->Join('tb_menu_landing_a as a', function ($join) {
                        $join->on('a.MenuID', '=', 'b.MenuID');
                    })
                    ->select(DB::raw('distinct b.*'))
                    ->where('a.MenuID', '=', "{$menu}")
                    ->where('b.DetailID', '=', "{$key->rownum}")
                    ->first();
                array_push($submenux, $submenu);
            } else {
                $DetailID = $menu . $key->rownum;
                $submenuz2 = DB::table('tb_menu_landing_b as b')
                    ->Join('tb_menu_landing_a as a', function ($join) {
                        $join->on('a.MenuID', '=', 'b.MenuID');
                    })
                    ->select(DB::raw('distinct a.*'))
                    ->where('a.MenuID', '=', "{$menu}")
                    ->where('a.DetailID', '=', "{$DetailID}")
                    ->first();

                if (!empty($submenuz2)) {
                    $submenu = DB::table('tb_menu_landing_b as b')
                        ->Join('tb_menu_landing_a as a', function ($join) {
                            $join->on('a.MenuID', '=', 'b.MenuID');
                        })
                        ->select(DB::raw('distinct b.*, b.Name'))
                        ->where('a.MenuID', '=', "{$menu}")
                        ->where('b.Link', '=', "{$submenuz2->Link}")
                        ->first();
                    array_push($submenux, $submenu);
                }
            }
        }

        foreach ($submenux as $key) {
            if (filter_var($key->Link, FILTER_VALIDATE_URL)) {
                $key->linkhref = "Link";
            } else {
                $key->linkhref = "Not Link";
            }
        }
        return $submenux;
    }

    public function navmenuBNew($menu) {
        $menu = explode(",",$menu);
        DB::statement(DB::raw('set @MenuID=-1'));
        $submenuz = DB::table('tb_menu_landing_a as a')
            ->leftjoin('tb_menu_landing_b as b', 'a.MenuID', '=', 'b.MenuID')
            ->select(DB::raw('distinct a.*'))
            ->whereIn('a.MenuID', $menu)->get(['a.*', DB::raw('@MenuID  := @MenuID  + 1 AS rownum')]);

        $submenux = array();
        foreach ($submenuz as $key) {
            if ($key->Link == "#") {
                $submenu = DB::table('tb_menu_landing_b as b')
                    ->Join('tb_menu_landing_a as a', function ($join) {
                        $join->on('a.MenuID', '=', 'b.MenuID');
                    })
                    ->select(DB::raw('distinct b.*'))
                    ->where('a.MenuID', '=', "{$key->MenuID}")
                    ->get();

                    foreach ($submenu as $keyz) {
                        $submenu2 = DB::table('tb_menu_landing_b as b')
                            ->Join('tb_menu_landing_a as a', function ($join) {
                                $join->on('a.MenuID', '=', 'b.MenuID');
                            })
                            ->select(DB::raw('distinct b.*'))
                            ->where('a.MenuID', '=', "{$keyz->MenuID}")
                            ->where('b.DetailID', '=', "{$keyz->DetailID}")
                            ->first();

                            array_push($submenux, $submenu2);
                    }
            } 
        }

        foreach ($submenux as $key) {
            if (filter_var($key->Link, FILTER_VALIDATE_URL)) {
                $key->linkhref = "Link";
            } else {
                $key->linkhref = "Not Link";
            }
        }
        return $submenux;
    }

    public function getContent($type) {
        $data = DB::table('tb_content_email')->where('type', $type)->get();
        return $data;
    }

    public function addContent(Request $request) {
        $content = $request->content;
        $type = $request->type;
        DB::table('tb_content_email')->where('type', $type)->update(
            [
                'content' => $content,
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui content email ".$type,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function getcontentMainTentangKami() {
        $data = DB::table('tb_content_tentangkami')->where('id', 1)->get();
        return $data;
    }

    public function addContentTentangKami(Request $request) {
        $content_main = $request->content_main;
        $content_thumb = $request->content_thumb;
        $content_mainEN = $request->content_mainEN;
        $content_thumbEN = $request->content_thumbEN;
        DB::table('tb_content_tentangkami')->where('id', 1)->update(
            [
                'tentangkami_full' => $content_main,
                'tentangkami_thumb' => $content_thumb,
                'tentangkami_fullEN' => $content_mainEN,
                'tentangkami_thumbEN' => $content_thumbEN,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui content tentang kami",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function getBanner(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $banners = DB::table('tb_content_banner')
            ->where('judul_banner', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_banner', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $banners['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $banners['searchTerm'] = $search_query ? null : '';
        }

        foreach ($banners['data'] as $key => $value) {
            if (filter_var($banners['data'][$key]->foto_banner, FILTER_VALIDATE_URL)) {
                $banners['data'][$key]->foto_banner = str_replace("open", "uc", $banners['data'][$key]->foto_banner);
            } else {
                $path = $banners['data'][$key]->foto_banner;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $banners['data'][$key]->foto_banner = $src;
            }
        }

        return response()->json([
            'banners' => $banners,
        ]);
    }

    public function showBanner($id_banner) {
        $data = DB::table('tb_content_banner')->where('id_banner', $id_banner)->first();
        return json_encode($data);
    }

    public function addBanner(Request $request) {
        $photo_banner = $request->photo_banner;
        $photo_banner_link = $request->photo_banner_link;
        $judul_banner = $request->judul_banner;
        $keterangan_banner = $request->keterangan_banner;
        // $tanggal_durasi_akhir = $request->tanggal_durasi_akhir;
        $foto = '';
        // if($photo_banner == "kosong" && $photo_banner_link !== ""){
        // $foto = $photo_banner_link;
        // } else if($photo_banner !== "kosong") {
        $image = $request->photo_banner; // your base64 encoded
        if (substr($image, 0, 14) == "data:image/png") {
            $image = str_replace('data:image/png;base64,', '', $image);
        } else if (substr($image, 0, 15) == "data:image/jpeg") {
            $image = str_replace('data:image/jpeg;base64,', '', $image);
        }
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        // $destinationPath = public_path() . '/uploads/banner';
        $destinationPath = '/uploads/banner';
        \File::put(public_path().$destinationPath . '/' . $imageName, base64_decode($image));
        $foto = $destinationPath . "/" . $imageName;
        // }

        DB::table('tb_content_banner')->insert(
            [
                'foto_banner' => $foto,
                'judul_banner' => $judul_banner,
                'link_banner' => $photo_banner_link,
                'keterangan_banner' => $keterangan_banner,
                // 'tanggaldurasi_akhir' => $tanggal_durasi_akhir,
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Banner",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteBanner($id, Request $request) {
        $data = DB::table('tb_content_banner')->where('id_banner', $id)->first();

        if (!empty($data)) {
            $img2 = file(public_path().$data->foto_banner);
            if (file_exists($data->foto_banner)) {
                unlink(public_path().$data->foto_banner);
            }
        }

        DB::table('tb_content_banner')->where('id_banner', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Banner",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateBanner($id, Request $request) {
        DB::table('tb_content_banner')
            ->where('id_banner', $id)
            ->update(
                [
                    'link_banner' => $request->link_banner,
                    'judul_banner' => $request->judul_banner,
                    'keterangan_banner' => $request->keterangan_banner,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Banner",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function getcontentBanner() {
        DB::statement(DB::raw('set @id_banner=0'));
        $data = DB::table('tb_content_banner')->get(['tb_content_banner.*',
            DB::raw('@id_banner  := @id_banner  + 1 AS rownum')]);

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->foto_banner, FILTER_VALIDATE_URL)) {
                $data[$key]->foto_banner = str_replace("open", "uc", $data[$key]->foto_banner);
            } else {
                $path = $data[$key]->foto_banner;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->foto_banner = $src;
            }
        }

        return $data;
    }

    public function getcontentVisiMisi() {
        $data = DB::table('tb_content_visimisi')->where('id', 1)->first();

        $dataFix = array();
        array_push($dataFix, array(
            "id" => $data->id,
            "isi_visimisi" => explode(";", $data->isi_visimisi),
            "isi_visimisiEN" => explode(";", $data->isi_visimisiEN),
        ));

        return json_encode($dataFix[0]);
    }

    public function getcontentLanding() {
        $data = DB::table('tb_content_landing')->where('id', 1)->first();
        $src = str_replace(public_path(), url('/'), $data->gambar);

        $dataFix = array();
        array_push($dataFix, array(
            "id" => $data->id,
            "tagline1" => $data->tagline1,
            "tagline2" => $data->tagline2,
            "tagline3" => $data->tagline3,
            "gambarOld" => $data->gambar,
            "gambar" => $src,
        ));

        return json_encode($dataFix[0]);
    }

    public function addContentVisiMisi(Request $request) {
        $isiVisiMisi1 = $request->isiVisiMisi1;
        $isiVisiMisiEN1 = $request->isiVisiMisiEN1;
        $isiVisiMisi2 = $request->isiVisiMisi2;
        $isiVisiMisiEN2 = $request->isiVisiMisiEN2;
        $isiVisiMisi3 = $request->isiVisiMisi3;
        $isiVisiMisiEN3 = $request->isiVisiMisiEN3;
        $isiVisiMisi4 = $request->isiVisiMisi4;
        $isiVisiMisiEN4 = $request->isiVisiMisiEN4;
        $isiVisiMisi5 = $request->isiVisiMisi5;
        $isiVisiMisiEN5 = $request->isiVisiMisiEN5;
        $isiVisiMisi6 = $request->isiVisiMisi6;
        $isiVisiMisiEN6 = $request->isiVisiMisiEN6;

        DB::table('tb_content_visimisi')->where('id', 1)->update(
            [
                'isi_visimisi' => $isiVisiMisi1 . ";" . $isiVisiMisi2 . ";" . $isiVisiMisi3 . ";" . $isiVisiMisi4 . ";" . $isiVisiMisi5 . ";" . $isiVisiMisi6,
                'isi_visimisiEN' => $isiVisiMisiEN1 . ";" . $isiVisiMisiEN2 . ";" . $isiVisiMisiEN3 . ";" . $isiVisiMisiEN4 . ";" . $isiVisiMisiEN5 . ";" . $isiVisiMisiEN6,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Visi Misi",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function addContentLanding(Request $request) {
        $tagline1 = $request->tagline1;
        $tagline2 = $request->tagline2;
        $tagline3 = $request->tagline3;
        $gambar = $request->gambar;

        if ($request->gambar !== "kosong") {
            $image = $request->gambar; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            // $imageName = 'banner_img.png';
            $imageName = str_random(10) . '.' . 'png';
            // $destinationPath = public_path() . '/themeLanding/img';
            $destinationPath = public_path() . '/uploads/landing';
            $destinationPathOld = public_path() . '/uploads/landing/';

            if ($request->gambarOld) {
                if (file_exists($request->gambarOld)) {
                    unlink($request->gambarOld);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));

            $path = $destinationPath . '/' . $imageName;

            DB::table('tb_content_landing')->where('id', 1)->update(
                [
                    'tagline1' => $tagline1,
                    'tagline2' => $tagline2,
                    'tagline3' => $tagline3,
                    'gambar' => $path,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_content_landing')->where('id', 1)->update(
                [
                    'tagline1' => $tagline1,
                    'tagline2' => $tagline2,
                    'tagline3' => $tagline3,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui content main banner",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function getcontentFooter() {
        $data = DB::table('tb_content_footer')->where('id', 1)->first();

        return json_encode($data);
    }

    public function addContentFooter(Request $request) {
        $id = $request->id;
        $instagram = $request->instagram;
        $facebook = $request->facebook;
        $twitter = $request->twitter;
        $email = $request->email;
        $tempat = $request->tempat;
        $alamat = $request->alamat;
        $katamutiara = $request->katamutiara;
        $katamutiaraEN = $request->katamutiaraEN;

        DB::table('tb_content_footer')->where('id', 1)->update(
            [
                'instagram' => $instagram,
                'facebook' => $facebook,
                'twitter' => $twitter,
                'email' => $email,
                'tempat' => $tempat,
                'alamat' => $alamat,
                'katamutiara' => $katamutiara,
                'katamutiaraEN' => $katamutiaraEN,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui content footer",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }


    public function getAreWe(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $arewes = DB::table('tb_content_arewe')
            ->where('nama_arewe', 'LIKE', '%' . $search_query . '%')
            ->orderBy('urutan', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $arewes['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $arewes['searchTerm'] = $search_query ? null : '';
        }

        foreach ($arewes['data'] as $key => $value) {
            if (filter_var($arewes['data'][$key]->foto_arewe, FILTER_VALIDATE_URL)) {
                $arewes['data'][$key]->foto_arewe = str_replace("open", "uc", $arewes['data'][$key]->foto_arewe);
            } else {
                $path = $arewes['data'][$key]->foto_arewe;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $arewes['data'][$key]->foto_arewe = $src;
            }
        }

        return response()->json([
            'arewes' => $arewes,
        ]);
    }

    public function addAreWe(Request $request) {
        $photo_arewe = $request->photo_arewe;
        $nama_arewe = $request->nama_arewe;
        $posisi_arewe = $request->posisi_arewe;
        $foto = '';

        $count = DB::table('tb_content_arewe')->count();
        $urutan = $count + 1;
        // echo $count;
        if ($count == 100) {
            $sts = "gagal";
            return $sts;
        } else {
            $image = $request->photo_arewe; // your base64 encoded
            if (substr($image, 0, 14) == "data:image/png") {
                $image = str_replace('data:image/png;base64,', '', $image);
            } else if (substr($image, 0, 15) == "data:image/jpeg") {
                $image = str_replace('data:image/jpeg;base64,', '', $image);
            }
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            // $destinationPath = public_path() . '/uploads/arewe';
            $destinationPath = '/uploads/arewe';
            \File::put($destinationPath . '/' . $imageName, base64_decode($image));
            $foto = $destinationPath . "/" . $imageName;

            DB::table('tb_content_arewe')->insert(
                [
                    'foto_arewe' => $foto,
                    'nama_arewe' => $nama_arewe,
                    'posisi_arewe' => $posisi_arewe,
                    'urutan' => $urutan,
                    'status' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );

            $sts = "oke";

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menambah 1 Are We ".$nama_arewe,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );

            return $sts;
        }

    }

    public function getAreWeID($id) {

        $data = DB::table('tb_content_arewe')->where('id', $id)->first();
        $data->foto_areweOld = $data->foto_arewe;

        if (filter_var($data->foto_arewe, FILTER_VALIDATE_URL)) {
            $data->foto_arewe = str_replace("open", "uc", $data->foto_arewe);
        } else {
            $path = public_path() .$data->foto_arewe;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->foto_arewe = $src;
        }

        return json_encode($data);
    }

    public function updateAreWe($id, Request $request) {
        if ($request->foto_arewe !== "kosong") {
            $image = $request->foto_arewe; // your base64 encoded
            if (substr($image, 0, 14) == "data:image/png") {
                $image = str_replace('data:image/png;base64,', '', $image);
            } else if (substr($image, 0, 15) == "data:image/jpeg") {
                $image = str_replace('data:image/jpeg;base64,', '', $image);
            }
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            // $destinationPath = public_path() . '/uploads/arewe';
            $destinationPath = '/uploads/arewe';

            unlink(public_path().$request->foto_arewe_old);
            \File::put(public_path().$destinationPath . '/' . $imageName, base64_decode($image));
            $foto = $destinationPath . "/" . $imageName;

            DB::table('tb_content_arewe')->where('id', $id)
                ->update(
                    [
                        'foto_arewe' => $foto,
                        'nama_arewe' => $request->nama_arewe,
                        'posisi_arewe' => $request->posisi_arewe,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        } else {
            DB::table('tb_content_arewe')->where('id', $id)
                ->update(
                    [
                        'nama_arewe' => $request->nama_arewe,
                        'posisi_arewe' => $request->posisi_arewe,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui 1 Are We ".$request->nama_arewe,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function getcontentAreWe() {
        // $test = array();
        // array_push($test, array("id" => "",
        //                         "foto_arewe" => "",
        //                         "nama_arewe" => "",
        //                         "posisi_arewe" => ""));
        //   $data = DB::table('tb_content_arewe')->get();

        //   foreach($data as $key => $value)
        //   {
        //     if(filter_var($data[$key]->foto_arewe, FILTER_VALIDATE_URL)){
        //       $data[$key]->foto_arewe = str_replace("open","uc",$data[$key]->foto_arewe);
        //     } else {
        //       $path = $data[$key]->foto_arewe;
        //       $dataImg = file_get_contents($path);
        //       $type = pathinfo($path, PATHINFO_EXTENSION);
        //       $base64 = base64_encode($dataImg);
        //       $src = 'data:image/' . $type . ';base64,' . $base64;

        //       $data[$key]->foto_arewe = $src;
        //     }
        //   }

        //   foreach ($data as $key) {
        //     array_push($test, $key);
        //   }
        //   return $test;

        DB::statement(DB::raw('set @id=0'));
        $data = DB::table('tb_content_arewe')->orderBy('urutan', 'asc')->get(['tb_content_arewe.*',
            DB::raw('@id  := @id  + 1 AS rownum')]);

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->foto_arewe, FILTER_VALIDATE_URL)) {
                $data[$key]->foto_arewe = str_replace("open", "uc", $data[$key]->foto_arewe);
            } else {
                $path = $data[$key]->foto_arewe;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->foto_arewe = $src;
            }
        }

        return $data;
    }

    public function deleteAreWe($id, Request $request) {
        $data = DB::table('tb_content_arewe')->where('id', $id)->first();

        if (!empty($data)) {
            $img2 = file($data->foto_arewe);
            if (file_exists($data->foto_arewe)) {
                unlink($data->foto_arewe);
            }
        }

        DB::table('tb_content_arewe')->where('id', $id)->delete();

        $dataChg = DB::table('tb_content_arewe')->get();
        foreach ($dataChg as $key) {
            if ($key->urutan > $data->urutan) {
                DB::table('tb_content_arewe')->where('id', $key->id)
                    ->update(
                        [
                            'urutan' => ($key->urutan) - 1,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus 1 Are We",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function getListManageAll($limit) {
        $test = array();
        if ($limit == 0) {
            array_push($test, array(
                "id" => "",
                "foto_arewe" => "",
                "nama_arewe" => "",
                "posisi_arewe" => "",
                "urutan" => ""));
        }

        $data = DB::table('tb_content_arewe')
            ->select("id", "foto_arewe", "nama_arewe", "posisi_arewe", "urutan")
            ->offset($limit)->limit(6)->orderBy('urutan', 'asc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->foto_arewe, FILTER_VALIDATE_URL)) {
                $data[$key]->foto_arewe = str_replace("open", "uc", $data[$key]->foto_arewe);
            } else {
                // $path = $data[$key]->foto_arewe;
                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->foto_arewe = '/uploads/' . strstr($data[$key]->foto_arewe, 'arewe/');
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListManageAllCari($cari) {
        $test = array();
        array_push($test, array(
            "id" => "",
            "foto_arewe" => "",
            "nama_arewe" => "",
            "posisi_arewe" => "",
            "urutan" => ""));

        if ($cari !== "kosong") {
            $data = DB::table('tb_content_arewe')
                ->select("id", "foto_arewe", "nama_arewe", "posisi_arewe", "urutan")
                ->where('nama_arewe', 'like', '%' . $cari . '%')
                ->orderBy('urutan', 'asc')->get();
        }

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->foto_arewe, FILTER_VALIDATE_URL)) {
                $data[$key]->foto_arewe = str_replace("open", "uc", $data[$key]->foto_arewe);
            } else {
                // $path = $data[$key]->foto_arewe;
                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->foto_arewe = $src;
                $data[$key]->foto_arewe = '/uploads/' . strstr($data[$key]->foto_arewe, 'arewe/');
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function upUrutan(Request $request) {
        $id = $request->id;
        $data = DB::table('tb_content_arewe')->where('id', $id)->first();
        $oldUrutan = $data->urutan;
        $newUrutan = ($data->urutan) - 1;

        $dataChg = DB::table('tb_content_arewe')->where('urutan', $newUrutan)->first();
        $idChg = $dataChg->id;

        DB::table('tb_content_arewe')->where('id', $id)
            ->update(
                [
                    'urutan' => $newUrutan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_content_arewe')->where('id', $dataChg->id)
            ->update(
                [
                    'urutan' => $oldUrutan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        return;
    }
    public function downUrutan(Request $request) {
        $id = $request->id;
        $data = DB::table('tb_content_arewe')->where('id', $id)->first();
        $oldUrutan = $data->urutan;
        $newUrutan = ($data->urutan) + 1;

        $dataChg = DB::table('tb_content_arewe')->where('urutan', $newUrutan)->first();
        $idChg = $dataChg->id;

        DB::table('tb_content_arewe')->where('id', $id)
            ->update(
                [
                    'urutan' => $newUrutan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_content_arewe')->where('id', $dataChg->id)
            ->update(
                [
                    'urutan' => $oldUrutan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        return;
    }


    public function getcontentCounter() {
        $data = DB::table('tb_content_counter')->where('id', 1)->first();

        return json_encode($data);
    }

    public function addContentCounter(Request $request) {
        $id = $request->id;
        $rekanan = $request->rekanan;
        $barang = $request->barang;
        $kontruksi = $request->kontruksi;
        $badanusaha = $request->badanusaha;
        $perorangan = $request->perorangan;

        DB::table('tb_content_counter')->where('id', 1)->update(
            [
                'rekanan' => $rekanan,
                'barang' => $barang,
                'kontruksi' => $kontruksi,
                'badanusaha' => $badanusaha,
                'perorangan' => $perorangan,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Counter",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }


    public function getDivisi(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $divisis = DB::table('tb_divisi')
            ->where('divisi', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_divisi', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $divisis['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $divisis['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'divisis' => $divisis,
        ]);
    }

    public function showDivisi($id_divisi) {
        $data = DB::table('tb_divisi')->where('id_divisi', $id_divisi)->first();
        return json_encode($data);
    }

    public function addDivisi(Request $request) {
        $divisi = $request->divisi;

        DB::table('tb_divisi')->insert(
            [
                'divisi' => $divisi,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambah divisi ".$divisi,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteDivisi($id, Request $request) {

        DB::table('tb_divisi')->where('id_divisi', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus divisi", 
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateDivisi($id, Request $request) {
        DB::table('tb_divisi')
            ->where('id_divisi', $id)
            ->update(
                [
                    'divisi' => $request->divisi,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbaharui divisi ".$request->divisi,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function dropdownDivisi() {
        $data = DB::table('tb_divisi')->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_divisi,
                'text' => $key->divisi,
            );
        }
        return $response;
    }


    public function getJenisVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $jenisvendors = DB::table('tb_jenis_vendor')
            ->where('jenis_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_jenis_vendor', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $jenisvendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $jenisvendors['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'jenisvendors' => $jenisvendors,
        ]);
    }

    public function showJenisVendor($id_jenis_vendor) {
        $data = DB::table('tb_jenis_vendor')->where('id_jenis_vendor', $id_jenis_vendor)->first();
        return json_encode($data);
    }

    public function addJenisVendor(Request $request) {
        $jenis_vendor = $request->jenis_vendor;

        DB::table('tb_jenis_vendor')->insert(
            [
                'jenis_vendor' => $jenis_vendor,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan jenis vendor baru ".$jenis_vendor,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteJenisVendor($id, Request $request) {

        DB::table('tb_jenis_vendor')->where('id_jenis_vendor', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "menghapus jenis vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateJenisVendor($id, Request $request) {
        DB::table('tb_jenis_vendor')
            ->where('id_jenis_vendor', $id)
            ->update(
                [
                    'jenis_vendor' => $request->jenis_vendor,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui jenis vendor baru ".$request->jenis_vendor,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }
    public function dropdownJenisVendor()
    {
        $data = DB::table('tb_jenis_vendor')->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id_jenis_vendor,
                      'text' => $key->jenis_vendor,
                    );
        }
        return $response;
    } 


    public function getKategoriPekerjaan(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $kategoripekerjaans = DB::table('tb_kategori_pekerjaan')
            ->where('kategori_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_kategori_pekerjaan', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $kategoripekerjaans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $kategoripekerjaans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'kategoripekerjaans' => $kategoripekerjaans,
        ]);
    }

    public function showKategoriPekerjaan($id_kategori_pekerjaan) {
        $data = DB::table('tb_kategori_pekerjaan')->where('id_kategori_pekerjaan', $id_kategori_pekerjaan)->first();
        return json_encode($data);
    }

    public function addKategoriPekerjaan(Request $request) {
        $kategori_pekerjaan = $request->kategori_pekerjaan;

        DB::table('tb_kategori_pekerjaan')->insert(
            [
                'kategori_pekerjaan' => $kategori_pekerjaan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan kategori ".$kategori_pekerjaan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteKategoriPekerjaan($id, Request $request) {

        DB::table('tb_kategori_pekerjaan')->where('id_kategori_pekerjaan', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus kategori",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateKategoriPekerjaan($id, Request $request) {
        DB::table('tb_kategori_pekerjaan')
            ->where('id_kategori_pekerjaan', $id)
            ->update(
                [
                    'kategori_pekerjaan' => $request->kategori_pekerjaan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui kategori ".$request->kategori_pekerjaan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function dropdownSubKlasifikasi1()
    {
        $data = DB::table('tb_subklasifikasi1')->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id_subklasifikasi1,
                      'text' => $key->subklasifikasi1,
                    );
        }
        return $response;
    } 
    public function getSubKlasifikasi1(Request $request) {

            $search_query = $request->searchTerm;
            $perPage = $request->per_page;
            $subklasifikasi1s = DB::table('tb_subklasifikasi1')
                ->where('subklasifikasi1', 'LIKE', '%' . $search_query . '%')
                ->orderBy('id_subklasifikasi1', 'asc')
                ->paginate($perPage)
                ->toArray();

            if ($search_query !== null) {
                $subklasifikasi1s['searchTerm'] = $search_query ?: '';
            } else if ($search_query == null) {
                $subklasifikasi1s['searchTerm'] = $search_query ? null : '';
            }

            return response()->json([
                'subklasifikasi1s' => $subklasifikasi1s,
            ]);
    }

    public function showSubKlasifikasi1($id_subklasifikasi1) {
        $data = DB::table('tb_subklasifikasi1')->where('id_subklasifikasi1', $id_subklasifikasi1)->first();
        return json_encode($data);
    }

    public function addSubKlasifikasi1(Request $request) {
        $subklasifikasi1 = $request->subklasifikasi1;

        DB::table('tb_subklasifikasi1')->insert(
            [
                'subklasifikasi1' => $subklasifikasi1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat Kualifikasi ".$subklasifikasi1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteSubKlasifikasi1($id) {

        DB::table('tb_subklasifikasi1')->where('id_subklasifikasi1', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Kualifikasi",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateSubKlasifikasi1($id, Request $request) {
        DB::table('tb_subklasifikasi1')
            ->where('id_subklasifikasi1', $id)
            ->update(
                [
                    'subklasifikasi1' => $request->subklasifikasi1,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Kualifikasi ".$request->subklasifikasi1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function dropdownSubKlasifikasi2()
    {
        $data = DB::table('tb_subklasifikasi2')->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id_subklasifikasi2,
                      'text' => $key->subklasifikasi2,
                    );
        }
        return $response;
    } 
    public function getSubKlasifikasi2(Request $request) {

            $search_query = $request->searchTerm;
            $perPage = $request->per_page;
            $subklasifikasi2s = DB::table('tb_subklasifikasi2')
                ->where('subklasifikasi2', 'LIKE', '%' . $search_query . '%')
                ->orderBy('id_subklasifikasi2', 'asc')
                ->paginate($perPage)
                ->toArray();

            if ($search_query !== null) {
                $subklasifikasi2s['searchTerm'] = $search_query ?: '';
            } else if ($search_query == null) {
                $subklasifikasi2s['searchTerm'] = $search_query ? null : '';
            }

            return response()->json([
                'subklasifikasi2s' => $subklasifikasi2s,
            ]);
    }

    public function showSubKlasifikasi2($id_subklasifikasi2) {
        $data = DB::table('tb_subklasifikasi2')->where('id_subklasifikasi2', $id_subklasifikasi2)->first();
        return json_encode($data);
    }

    public function addSubKlasifikasi2(Request $request) {
        $subklasifikasi2 = $request->subklasifikasi2;

        DB::table('tb_subklasifikasi2')->insert(
            [
                'subklasifikasi2' => $subklasifikasi2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat Klasifikasi ".$subklasifikasi2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteSubKlasifikasi2($id) {

        DB::table('tb_subklasifikasi2')->where('id_subklasifikasi2', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Klasifikasi",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateSubKlasifikasi2($id, Request $request) {
        DB::table('tb_subklasifikasi2')
            ->where('id_subklasifikasi2', $id)
            ->update(
                [
                    'subklasifikasi2' => $request->subklasifikasi2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Kualifikasi ".$request->subklasifikasi2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }


    public function getJabatan(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $jabatans = DB::table('tb_jabatan')
            ->where('jabatan', 'LIKE', '%' . $search_query . '%')
            ->orderBy('id_jabatan', 'asc')
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $jabatans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $jabatans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'jabatans' => $jabatans,
        ]);
    }

    public function showJabatan($id_jabatan) {
        $data = DB::table('tb_jabatan')->where('id_jabatan', $id_jabatan)->first();
        return json_encode($data);
    }

    public function addJabatan(Request $request) {
        $jabatan = $request->jabatan;

        DB::table('tb_jabatan')->insert(
            [
                'jabatan' => $jabatan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Jabatan ".$jabatan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function deleteJabatan($id, Request $request) {

        DB::table('tb_jabatan')->where('id_jabatan', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Jabatan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function updateJabatan($id, Request $request) {
        DB::table('tb_jabatan')
            ->where('id_jabatan', $id)
            ->update(
                [
                    'jabatan' => $request->jabatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Jabatan ".$request->jabatan,
                'created_at' => date("Y-m-d H:i:s"),
            ]

        );
        return;
    }

    public function dropdownJabatan() {
        $data = DB::table('tb_jabatan')->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_jabatan,
                'text' => $key->jabatan,
            );
        }
        return $response;
    }

}