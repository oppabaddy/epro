<?php
namespace App\Http\Controllers\ApiMobile;

use App\Http\Controllers\Controller;
use App\Kop;
use App\Mail\ThanksRegister;
use App\Models\PesanModel;
use App\Models\PraktikModel;
use Carbon\Carbon;
use DateTime;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;

// use DateTime;

class ApiMobileController extends Controller
{
    public function get_pesan_or_praktik_baik($kategory, $cari)
    {

        if ($kategory == 'pesan') {
            $table = 'pesan_baik';
        }

        if ($kategory == 'praktik') {
            $table = 'praktik_baik';
        }
        if ($cari !== 'kosong' && $cari !== "") {
            $data = DB::table($table)
                ->where('flag', 2)
                ->where('judul', 'like', '%' . $cari . '%')->orderBy('tanggal_publish', 'desc')->get();
        } else {
            $data = DB::table($table)
                ->where('flag', 2)->orderBy('tanggal_publish', 'desc')->get();
        }

        if (count($data) > 0) {
            $test = array();
            foreach ($data as $key => $value) {
                if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                    $img = str_replace("open", "uc", $data[$key]->gambar);
                    $data[$key]->gambar = $this->cekImage($img);

                } else {
                    $path = public_path() . '/uploads/' . $table . '/' . $data[$key]->gambar;
                    $imgFile = str_replace(public_path(), url('/'), $path);
                    $data[$key]->gambar = $this->cekImage($imgFile);
                }

                if ($kategory == 'praktik') {

                    if ($data[$key]->alamat != null) {
                        $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data[$key]->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
                        $response = Curl::to($url)
                            ->withData()
                            ->get();

                        $alamat = json_decode($response, true);
                        $data[$key]->str_alamat = $alamat["Response"]["View"][0]["Result"][0]["Location"]["Address"]["Label"];
                    } else {
                        $data[$key]->str_alamat = 'Alamat belum di isi';
                    }
                }

                $data[$key]->content = preg_replace("/<img[^>]+\>/i", "", $data[$key]->content);

                $dataComment = DB::table('tb_comment')
                    ->where('id_post', $data[$key]->id)->where('tipe', 'Pesan')->count();
                $data[$key]->valComment = $dataComment;
                // $now = date('j F, Y', strtotime($data[$key]->tanggal_publish));

                // $data[$key]->tanggal_publish = $now;

// $christmas = new DateTime('25 December 2018');

            }

            foreach ($data as $key) {
                if ($key) {
                    array_push($test, $key);
                }
            }

            $res['message'] = "Success!";
            $res['values'] = $test;
            return response($res);
        } else {
            $res['message'] = "Empty!";
            return response($res);
        }
    }

    public function cekImage($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); # handles 301/2 redirects
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode == 200) {
            return $url;
        } else {
            $default = "imgDefault.png";
            $path = public_path() . '/uploads/' . $default;
            $src = str_replace(public_path(), url('/'), $path);
            return $src;
        }
    }

    public function convert_arr($data)
    {

        $satu = str_replace('[', '', $data);
        $dua = str_replace(']', '', $satu);
        return $dua;

    }

    public function insert_gambar(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $param = $request->params;

        $fileName = null;
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $fileName = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/' . $param . '/';
            // \File::put($destinationPath . '/' . $fileName, $image);
            $image->move($destinationPath, $fileName);
            return response()->json([
                'url_img' => $destinationPath . '/' . $fileName,
                'nama_file' => $param . '/' . $fileName,
                'nama_img' => $fileName,
                'status' => 'sukses',
            ]);
        }
    }

    public function deleteImage(Request $request)
    {
        $foto = $request->nama_foto;
        $part = $request->param;
        $path = public_path() . '/uploads/' . $part . '/' . $foto;
        unlink("$path");

        return response()->json(['status' => true]);

    }

    public function insert_mobile(Request $request)
    {
        $table = $request->table;

        if ($table == 'donasi') {
            if ($request->donasi_tahunan == "Belum bersedia") {
                DB::table('tb_donasi')->insert(
                    [
                        'id_kop' => $request->id_kop,
                        'donasi_tahunan' => $request->donasi_tahunan,
                        'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
                        'konfirmasi_donasi' => $request->konfirmasi_donasi,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $ret = [
                    'status' => 'sukses',
                    'msg' => 'Data berhasil disimpan',
                    'ket' => "donasi tanpa bukti",
                ];

            } else {

                $bukti_donasi = null;
                if ($request->hasFile('bukti_donasi')) {
                    $image = $request->file('bukti_donasi');
                    $bukti_donasi = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
                    $destinationPath = public_path() . './uploads/bukti_transfer/';
                    $image->move($destinationPath, $bukti_donasi);

                }

                DB::table('tb_donasi')->insert(
                    [
                        'id_kop' => $request->id_kop,
                        'donasi_tahunan' => $request->donasi_tahunan,
                        'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
                        'konfirmasi_donasi' => $request->konfirmasi_donasi,
                        'bukti_donasi' => $destinationPath . '' . $bukti_donasi,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $ret = [
                    'status' => 'sukses',
                    'msg' => 'Data berhasil disimpan',
                    'ket' => "donasi dengan bukti",
                ];
            }

        }

        if ($table == 'registrasi') {
            $this->validate($request, [
                'logo_komunitas' => 'required|image|mimes:jpeg,png,jpg,gif,svg|file|max:2000000',

            ]);

            $logo_komunitas = null;
            if ($request->hasFile('logo_komunitas')) {
                $image = $request->file('logo_komunitas');
                $logo_komunitas = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
                $destinationPath = public_path() . '/uploads/logo_komunitas/';
                $image->move($destinationPath, $logo_komunitas);
            }

            $klaster_kerja = $this->convert_arr($request->klaster_kerja);
            $lingkup_komunitas = $this->convert_arr($request->lingkup_komunitas);

            $kop = new Kop;
            $kop['nama_komunitas'] = $request->nama_komunitas;
            $kop['deskripsi_komunitas'] = $request->deskripsi_komunitas;
            $kop['nama_narahubung'] = $request->nama_narahubung;
            $kop['email_narahubung'] = $request->email_narahubung;
            $kop['tlp_narahubung'] = $request->tlp_narahubung;
            $kop['jenis_komunitas'] = $request->jenis_komunitas;
            $kop['email_komunitas'] = $request->email_komunitas;
            $kop['logo_komunitas'] = $destinationPath . '' . $logo_komunitas;
            $kop['klaster_kerja'] = $request->klaster_kerja1 . ';' . $request->klaster_kerja2;
            $kop['lingkup_komunitas'] = $request->lingkup_komunitas1 . ';' . $request->lingkup_komunitas2;
            $kop['provinsi_kerja'] = $request->provinsi_kerja;
            $kop['kota_kerja'] = $request->kota_kerja;
            $kop['status'] = 1;
            $kop['created_at'] = date("Y-m-d H:i:s");
            $kop->save();
            $id_kop = $kop->id;

            if ($id_kop) {
                DB::table('tb_member_sekunder')->insert(
                    [
                        'id_kop' => $id_kop,
                        'tgl_berdiri_kop' => $request->tgl_komunitas,
                        'web_komunitas' => $request->web_komunitas,
                        'twitter_komunitas' => $request->twitter_komunitas,
                        'facebook_komunitas' => $request->facebook_komunitas,
                        'instagram_komunitas' => $request->instagram_komunitas,
                        'youtube_komunitas' => $request->youtube_komunitas,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                // if ($request->donasi_tahunan == "Belum bersedia") {
                //     DB::table('tb_donasi')->insert(
                //         [
                //             'id_kop' => $id_kop,
                //             'donasi_tahunan' => $request->donasi_tahunan,
                //             'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
                //             'konfirmasi_donasi' => $request->konfirmasi_donasi,
                //             'created_at' => date("Y-m-d H:i:s"),
                //         ]
                //     );
                // } else {
                //     $bukti_donasi = null;
                //     if ($request->hasFile('bukti_donasi')) {
                //         $image = $request->file('bukti_donasi');
                //         $bukti_donasi = md5($image->getClientOriginalName() . time()) . "." . $image->getClientOriginalExtension();
                //         $destinationPath = public_path() . './uploads/bukti_transfer/';
                //         $image->move($destinationPath, $bukti_donasi);
                //     }

                //     DB::table('tb_donasi')->insert(
                //         [
                //             'id_kop' => $id_kop,
                //             'donasi_tahunan' => $request->donasi_tahunan,
                //             'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
                //             'konfirmasi_donasi' => $request->konfirmasi_donasi,
                //             'bukti_donasi' => $destinationPath . '' . $bukti_donasi,
                //             'created_at' => date("Y-m-d H:i:s"),
                //         ]
                //     );
                // }

                $objDemo = DB::table('tb_content_email')->where('type', 'Register')->first();

                $sendEmail = array();
                array_push($sendEmail, $request->email_narahubung, $request->email_komunitas);

                foreach ($sendEmail as $key) {
                    Mail::to($key)->send(new ThanksRegister($objDemo));
                }

                $ret = [
                    'status' => 'sukses',
                    'msg' => 'Data berhasil disimpan',
                    'tes' => $klaster_kerja,
                ];
            } else {
                $ret = [
                    'status' => 'failed',
                    'msg' => 'gagal',
                    'tes' => $klaster_kerja,

                ];
            }
        }
        return response()->json($ret);
    }

    public function insert_pesan_baik(Request $request)
    {
        $gambar = $request->gambar;
        $table = $request->table;

        if ($gambar == "" || $gambar == null || $gambar == 'null') {
            if ($table == 'pesan_baik') {
                $destinationPath = public_path() . '/uploads/pesan_baik';
            } else {
                $destinationPath = public_path() . '/uploads/praktik_baik';
            }

            $img = file($destinationPath . "/imgDefault.png");
            $imageName = str_random() . '.png';
            \File::put($destinationPath . '/' . $imageName, $img);
        } else {
            $imageName = $gambar;
        }

        if ($request->flag == 2) {
            $publish = Carbon::now();
            $status = 'Publish';
        } else {
            $publish = null;
            $status = 'Draft';
        }

        if ($table == 'pesan_baik') {
            $data = [
                'judul' => $request->judul,
                "content" => $request->content,
                "gambar" => $imageName,
                'id_kop' => $request->id_kop,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'flag' => $request->flag,
                'created_at' => Carbon::now(),
                'tanggal_publish' => $publish,
            ];
            $insert = PesanModel::insert($data);

        } else {
            $datas = [
                'judul' => $request->judul,
                "content" => $request->content,
                "gambar" => $imageName,
                'id_kop' => $request->id_kop,
                'tanggal' => $request->tanggal,
                'waktu_event' => $request->waktu_event,
                'jam_mulai' => $request->startTime,
                'jam_selesai' => $request->endTime,
                'alamat' => $request->alamat,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'flag' => $request->flag,
                'status' => $status,
                'created_at' => Carbon::now(),
                'tanggal_publish' => $publish,
            ];
            $insert = PraktikModel::insert($datas);
        }

        if ($insert) {
            return response()->json(['status' => true, 'gambar' => $imageName]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getListKegiatanAll()
    {
        $test = array();
        $data = DB::table('tb_content_kegiatan')
            ->select("id", "id_kop", "gambar", "content", "judul", "tanggal", "jam_mulai", "jam_selesai", "alamat", "updated_at")
            ->where('flag', 2)->orderBy('tanggal', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {

                $path = public_path() . '/uploads/kegiatan/' . $data[$key]->gambar;
                $imgFile = str_replace(public_path(), url('/'), $path);
                $data[$key]->gambar = $this->cekImage($imgFile);
            }

            if (date("Y-m-d") > $data[$key]->tanggal) {
                $data[$key]->publishDate = "lewat";
            } else {
                $dateold = new DateTime($data[$key]->tanggal);
                $datenow = new DateTime(date("Y-m-d"));
                $data[$key]->publishDate = $dateold->diff($datenow)->days;
            }

            if ($data[$key]->alamat !== null) {
                $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data[$key]->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
                $response = Curl::to($url)
                    ->withData()
                    ->get();

                $alamat = json_decode($response, true);
                $data[$key]->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
            } else {
                $data[$key]->alamat2 = "No Location";
            }
            $data[$key]->content = preg_replace("/<img[^>]+\>/i", "", $data[$key]->content);

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Kegiatan')->count();
            $data[$key]->valComment = $dataComment;
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListBlogAll()
    {
        $test = array();
        $data = DB::table('tb_content_blog')
            ->select("id", "id_kop", "gambar", "content", "judul", "tanggal_publish", "updated_at")
            ->where('flag', 2)->orderBy('tanggal_publish', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {

                $path = public_path() . '/uploads/blog/' . $data[$key]->gambar;
                $imgFile = str_replace(public_path(), url('/'), $path);
                $data[$key]->gambar = $this->cekImage($imgFile);

            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Blog')->count();
            $data[$key]->valComment = $dataComment;
            $data[$key]->content = preg_replace("/<img[^>]+\>/i", "", $data[$key]->content);

        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    private function get_id_primary($id)
    {
        $data = DB::table('users_to_kop as c')
            ->select('c.id_kop')
            ->where('c.id_user', $id)
            ->get();
        foreach ($data as $kop) {
            $id_kop = $kop->id_kop;
        }
        return $id_kop;
    }

    public function get_Mykop($id)
    {
        $id_kop = $this->get_id_primary($id);

        $data = DB::table('tb_member_premier as p')
            ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
            ->leftjoin('tb_assessment as a', 'p.id_kop', '=', 'a.id_kop')
            ->where('p.id_kop', $id_kop)->first();

        $logo = $data->logo_komunitas;
        if (filter_var($data->logo_komunitas, FILTER_VALIDATE_URL)) {
            $data->logo_komunitas = str_replace("open", "uc", $data->logo_komunitas);
        } else {
            // $path = public_path() . '/uploads/logo_komunitas/' . $data->logo_komunitas;
            $imgFile = str_replace(public_path(), url('/'), $data->logo_komunitas);
            $data->logo_komunitas = $this->cekImage($imgFile);

            // $path = public_path() . '/uploads/' . $table . '/' . $data[$key]->gambar;
            // $imgFile = str_replace(public_path(), url('/'), $path);
            // $data[$key]->gambar = $this->cekImage($imgFile);
        }
        $dataFix = array();
        array_push($dataFix, array(
            "id_kop" => $id_kop,
            "nama_komunitas" => $data->nama_komunitas,
            "deskripsi_komunitas" => $data->deskripsi_komunitas,
            "nama_narahubung" => $data->nama_narahubung,
            "email_narahubung" => $data->email_narahubung,
            "tlp_narahubung" => $data->tlp_narahubung,
            "jenis_komunitas" => $data->jenis_komunitas,
            "email_komunitas" => $data->email_komunitas,
            "logo_komunitas" => $data->logo_komunitas,
            "klaster_kerja" => explode(";", $data->klaster_kerja),
            "lingkup_komunitas" => explode(";", $data->lingkup_komunitas),
            "provinsi_kerja" => $data->provinsi_kerja,
            "kota_kerja" => $data->kota_kerja,
            "tgl_berdiri_kop" => $data->tgl_berdiri_kop,
            "web_komunitas" => $data->web_komunitas,
            "twitter_komunitas" => $data->twitter_komunitas,
            "facebook_komunitas" => $data->facebook_komunitas,
            "instagram_komunitas" => $data->instagram_komunitas,
            'pelaporan_keuangan' => $data->pelaporan_keuangan,
            'membiayai_kegiatan' => $data->membiayai_kegiatan,
            'pengelolaan_dana' => $data->pengelolaan_dana,
            'dapat_berjalan' => $data->dapat_berjalan,
            'pengelolaan_pengurus' => $data->pengelolaan_pengurus,
            'peningkatan_kapasitas' => $data->peningkatan_kapasitas,
            'pengelolaan_relawan' => $data->pengelolaan_relawan,
            'mendukung_peningkatan' => $data->mendukung_peningkatan,
            'memahami_nilai' => $data->memahami_nilai,
            'efektivitas_program' => $data->efektivitas_program,
            'efektivitas_program_setahun' => $data->efektivitas_program_setahun,
            'cakupan_program' => $data->cakupan_program,
            'pengukuran_intervensi' => $data->pengukuran_intervensi,
            'mencapai_visi' => $data->mencapai_visi,
            'perencanaan_program' => $data->perencanaan_program,
            'komunikasikan_program' => $data->komunikasikan_program,
            'kemitraan_komunitas' => $data->kemitraan_komunitas,
            'kemitraan_pemerintah' => $data->kemitraan_pemerintah,
            'memecahkan_masalah' => $data->memecahkan_masalah,
            'mendukung_inovasi' => $data->mendukung_inovasi,
            'memberikan_ruang' => $data->memberikan_ruang,
            'pendapatan_utama' => explode(";", $data->pendapatan_utama),
            'relawan_kop' => $data->relawan_kop,
            'jumlah_kemitraan' => $data->jumlah_kemitraan,
            'melakukan_komunitas' => explode(";", $data->melakukan_komunitas),
            'teknologi_komunitas' => explode(";", $data->teknologi_komunitas),
            'berapa_kegiatan' => $data->berapa_kegiatan,
            'nama_media_kegiatan' => $data->nama_media_kegiatan,
            'masalah_utama' => explode(";", $data->masalah_utama),
            'pengembangan_komunitas' => explode(";", $data->pengembangan_komunitas),
        ));
        return json_encode($dataFix[0]);
    }

    // $2y$10$vi3OJ9LKNI3TZJy0v.dxiuYOQprCEpiN5By01TK0OK6kvI0P0RJ/K

    public function getListKop($limit)
    {
        $test = array();
        $data = DB::table('tb_member_premier as p')
            ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')->where('p.status', 2)->offset($limit)->limit(10)->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $gambar = str_replace("open", "uc", $data[$key]->logo_komunitas);
                $url = $this->cekImage($gambar);
                $data[$key]->logo_komunitas = $gambar;
            } else {

                $imgFile = str_replace(public_path(), url('/'), $data[$key]->logo_komunitas);
                $data[$key]->logo_komunitas = $this->cekImage($imgFile);

                // $path = $data[$key]->logo_komunitas;
                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = $base64;
                // $data[$key]->logo_komunitas_upload = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return json_encode($test);
    }

    public function getListKopByJenis($category, $cluster, $scope)
    {

        if ($category !== "kosong" && $cluster == "kosong" && $scope == "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where('p.jenis_komunitas', $category)
                ->get();

        } else if ($category == "kosong" && $cluster !== "kosong" && $scope == "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where('klaster_kerja', 'like', '%' . $cluster . '%')
                ->get();

            // $data = Kop::where('status', 2)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
        } else if ($category == "kosong" && $cluster == "kosong" && $scope !== "kosong") {

            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where('lingkup_komunitas', 'like', '%' . $scope . '%')
                ->get();

            // $data = Kop::where('status', 2)->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category !== "kosong" && $cluster !== "kosong" && $scope == "kosong") {

            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where("jenis_komunitas", $category)
                ->where("klaster_kerja", 'like', '%' . $cluster . '%')
                ->get();

            // $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
        } else if ($category !== "kosong" && $cluster == "kosong" && $scope !== "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where("jenis_komunitas", $category)
                ->where('lingkup_komunitas', 'like', '%' . $scope . '%')
                ->get();

            // $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category == "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('status', 2)
                ->where('klaster_kerja', 'like', '%' . $cluster . '%')
                ->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category !== "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('status', 2)
                ->where('jenis_komunitas', $category)
                ->where('klaster_kerja', 'like', '%' . $cluster . '%')
                ->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category == "kosong" || $cluster == "kosong" || $scope == "kosong") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')->where('status', 2)->get();
        }

        $test = array();
        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_komunitas = str_replace("open", "uc", $data[$key]->logo_komunitas);
            } else {

                $imgFile = str_replace(public_path(), url('/'), $data[$key]->logo_komunitas);
                $data[$key]->logo_komunitas = $this->cekImage($imgFile);

                // $path = $data[$key]->logo_komunitas;
                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = $base64;
                // $data[$key]->logo_komunitas_upload = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }

        return json_encode($test);
        // return $test;
    }

    public function getListKopBySearch($search)
    {
        if ($search !== "kosong" || $search !== "") {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->where('nama_komunitas', 'like', '%' . $search . '%')
                ->get();

        } else {
            $data = DB::table('tb_member_premier as p')
                ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                ->where('p.status', 2)
                ->get();
        }

        $test = array();
        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_komunitas = str_replace("open", "uc", $data[$key]->logo_komunitas);
            } else {
                $imgFile = str_replace(public_path(), url('/'), $data[$key]->logo_komunitas);
                $data[$key]->logo_komunitas = $this->cekImage($imgFile);

                // $path = $data[$key]->logo_komunitas;
                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = $base64;
                // $data[$key]->logo_komunitas_upload = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    // banner

    public function getcontentBanner()
    {
        // DB::statement(DB::raw('set @id_banner=0'));
        // $data = DB::table('tb_content_banner')->get(['tb_content_banner.*',
        //     DB::raw('@id_banner  := @id_banner  + 1 AS rownum')]);

        // foreach ($data as $key => $value) {
        //     if (filter_var($data[$key]->foto_banner, FILTER_VALIDATE_URL)) {
        //         $url = str_replace("open", "uc", $data[$key]->foto_banner);
        //         $data[$key]->foto_banner = $url;

        //     } else {
        //         $path = $data[$key]->foto_banner;
        //         $dataImg = file_get_contents($path);
        //         $type = pathinfo($path, PATHINFO_EXTENSION);
        //         $base64 = base64_encode($dataImg);
        //         $src = $base64;

        //         $data[$key]->foto_banner = $src;
        //     }
        // }

        // return $data;

        $path = public_path() . '/themeLanding/img/slide/slide1.jpg';
        $dataImg = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = base64_encode($dataImg);
        $src = $base64;
        $ret = array();
        array_push($ret, array(
            "id_banner" => 0,
            "foto_banner" => $src,
            "judul_banner" => "",
            "link_banner" => "",
            "keterangan_banner" => "",
            "tanggaldurasi_awal" => null,
            "tanggaldurasi_akhir" => null,
            "status" => 1,
            "created_at" => "2020-07-23 12:23:09",
            "updated_at" => null,
            "rownum" => 1,

        ));

        DB::statement(DB::raw('set @id_banner=0'));
        $data = DB::table('tb_content_banner')->get(['tb_content_banner.*',
            DB::raw('@id_banner  := @id_banner  + 1 AS rownum')]);

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->foto_banner, FILTER_VALIDATE_URL)) {
                $url = str_replace("open", "uc", $data[$key]->foto_banner);
                $data[$key]->foto_banner = $url;

            } else {
                $path = $data[$key]->foto_banner;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = $base64;

                $data[$key]->foto_banner = $src;
            }
        }

        foreach ($data as $key) {
            array_push($ret, $key);
        }
        return $ret;
    }

    public function check_account(Request $request)
    {
        $password = $request->password;
        $data = DB::table('users')->where('id_person', $request->id)->get();
        foreach ($data as $key) {
            if (!(Hash::check($request->password, $key->password))) {
                return response()->json(['status' => 'gagal', 'pass' => $request->password]);
            } else {
                return response()->json(['status' => 'sukses', 'pass' => $request->password]);
            }
            // if (Hash::check($password, $data->password)) {

        }
    }

    public function userUpdatePass(Request $request)
    {

        $newpass = bcrypt($request->newpass);
        $update = array(
            'password' => $newpass,
            'updated_at' => date("Y-m-d H:i:s"),
        );

        if (DB::table('users')->where('id', $request->id)->update($update)) {
            return response()->json(['status' => 'sukses']);
        } else {
            return response()->json(['status' => 'gagal']);
        }

    }
    // alter db
    // ALTER TABLE `users`
    // CHANGE COLUMN `password` `password` TEXT NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci' AFTER `username`;

}
