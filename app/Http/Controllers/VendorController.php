<?php
namespace App\Http\Controllers;

use App\Events\PersonEvents;
use App\Exports\IuranExport;
use App\Exports\VendorExport;
// import file model Vendor
use App\Vendor;
use App\Mail\ApproveVendor;
use App\Mail\BulkMail;
use App\Mail\VendorMail;
use App\Mail\NotApproveVendor;
use App\Mail\ThanksRegister;
use App\Person;
use DateTime;
use DB;
use Excel;
use PDF;
use ZipArchive;
use File;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use Location;

use App\Events\VendorEvents;
use App\Events\NotifEvents;
use App\Events\RefreshEvents;

class VendorController extends Controller {
    
    // // mengambil semua data
    // public function all() {
    //     return Vendor::all();
    // }

    public function all2() {
        return DB::table('tb_vendor_primary')->where('status', 2)->get();
    }

    public function dropdownvendor() {
        $data = DB::table('tb_vendor_primary')
                ->orderBy('rating','desc')
                ->where('status', 2)
                ->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_vendor,
                'text' => $key->nama_vendor,
            );
        }
        return $response;
    }
    public function dropdownvendorKlas($klas1,$klas2) {
        if($klas1 !== "kosong" && $klas2 == "kosong"){
            $data = DB::table('tb_vendor_primary as p')
                    ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
                    ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
                    ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
                    ->select('p.id_vendor','p.nama_vendor','l.id_subklasifikasi1','m.id_subklasifikasi2')
                    ->where('p.status', 2)
                    ->where('k.klasifikasi1', $klas1)
                    ->groupBy('k.id_vendor')
                    ->get();
        } else if ($klas1 == "kosong" && $klas2 !== "kosong"){
            $data = DB::table('tb_vendor_primary as p')
                    ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
                    ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
                    ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
                    ->select('p.id_vendor','p.nama_vendor','l.id_subklasifikasi1','m.id_subklasifikasi2')
                    ->where('p.status', 2)
                    ->where('k.klasifikasi2', $klas2)
                    ->groupBy('k.id_vendor')
                    ->get();
        } else if ($klas1 !== "kosong" && $klas2 !== "kosong"){
            $data = DB::table('tb_vendor_primary as p')
                    ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
                    ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
                    ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
                    ->select('p.id_vendor','p.nama_vendor','l.id_subklasifikasi1','m.id_subklasifikasi2')
                    ->where('p.status', 2)
                    ->where('k.klasifikasi1', $klas1)
                    ->where('k.klasifikasi2', $klas2)
                    ->groupBy('k.id_vendor')
                    ->get();
        }

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_vendor,
                'text' => $key->nama_vendor,
            );
        }
        return $response;
    }

    // mengambil data by id
    public function show($id_vendor) {
        $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')
            ->leftjoin('tb_vendor_third as a', 'p.id_vendor', '=', 'a.id_vendor')
            ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
            ->where('p.id_vendor', $id_vendor)->first();

        $data->logo_vendorOld = $data->logo_vendor;

        $ihsi = null;
        if (filter_var($data->logo_vendor, FILTER_VALIDATE_URL)) {
            $data->logo_vendor = str_replace("open", "uc", $data->logo_vendor);
            $ihsi = str_replace("open", "uc", $data->logo_vendor);
        } else {
            $path = public_path() . '/uploads/logo_vendor/'.$data->logo_vendor;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->logo_vendor = $src;
            $ihsi = $src;
        }

        $dataPengurus = DB::table('tb_vendor_third as p')
            ->where('p.id_vendor', $id_vendor)->get();

        $dataPengurusFix = array();
        foreach ($dataPengurus as $key) {
            array_push($dataPengurusFix, array(
                "jenis_pengurus" => $key->jenis_pengurus,
                "nama_pengurus" => $key->nama_pengurus,
                "no_ktp" => $key->no_ktp,
                "foto_ktp" => $key->foto_ktp,
                "jabatan_pengurus" => $key->jabatan_pengurus,
            ));
        }  


        $getNoIujkSiup = DB::table('tb_vendor_iujksiup as p')
            ->select('p.*','p.no_iujksiup as no_iujksiup_old')
            ->where('p.id_vendor', $id_vendor)->get();

        $arrNoIujkSiup = array();
        foreach ($getNoIujkSiup as $key) {
            array_push($arrNoIujkSiup, $key->no_iujksiup);
        }
        $dataIujk = DB::table('tb_vendor_iujksiup as p')
            ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
            ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
            ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
            ->select('l.id_subklasifikasi1','m.id_subklasifikasi2','k.id_klasifikasi')
            ->where('p.id_vendor', $id_vendor)
            ->whereIn('k.id_klasifikasi', $arrNoIujkSiup)
            ->get();

        $dataIujkFix = array();
        foreach ($dataIujk as $key) {
            if( in_array( array(
                "id_klasifikasi" => $key->id_klasifikasi,
                "klasifikasi1" => $key->id_subklasifikasi1,
                "klasifikasi2" => $key->id_subklasifikasi2,
                ) , $dataIujkFix ) )
            {
            } else {
                array_push($dataIujkFix, array(
                    "id_klasifikasi" => $key->id_klasifikasi,
                    "klasifikasi1" => $key->id_subklasifikasi1,
                    "klasifikasi2" => $key->id_subklasifikasi2,
                ));
            } 
        } 

        $getNoSbu = DB::table('tb_vendor_sbu as p')
            ->select('p.*','p.no_sbu as no_sbu_old')
            ->where('p.id_vendor', $id_vendor)->get();

        $arrSbu = array();
        foreach ($getNoSbu as $key) {
            array_push($arrSbu, $key->no_sbu);
        }
        $dataSbu = DB::table('tb_vendor_sbu as p')
            ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
            ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
            ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
            ->select('l.id_subklasifikasi1','m.id_subklasifikasi2','k.id_klasifikasi')
            ->where('p.id_vendor', $id_vendor)
            ->whereIn('k.id_klasifikasi', $arrSbu)
            ->get();

        $dataSbuFix = array();
        foreach ($dataSbu as $key) {
            if( in_array( array(
                "id_klasifikasi" => $key->id_klasifikasi,
                "klasifikasi1" => $key->id_subklasifikasi1,
                "klasifikasi2" => $key->id_subklasifikasi2,
                ) , $dataSbuFix ) )
            {
            } else {
                array_push($dataSbuFix, array(
                    "id_klasifikasi" => $key->id_klasifikasi,
                    "klasifikasi1" => $key->id_subklasifikasi1,
                    "klasifikasi2" => $key->id_subklasifikasi2,
                ));
            } 
        }      

        $dataFix = array();
        array_push($dataFix, array(
            "rating" => $data->rating,
            "id_vendor" => $id_vendor,
            "nama_vendor" => $data->nama_vendor,
            "deskripsi_vendor" => $data->deskripsi_vendor,
            "nama_narahubung" => $data->nama_narahubung,
            "email_narahubung" => $data->email_narahubung,
            "tlp_narahubung" => $data->tlp_narahubung,
            "jenis_vendor" => $data->jenis_vendor,
            "email_vendor" => $data->email_vendor,
            'tlp_vendor' =>  $data->tlp_vendor,
            'fax_vendor' =>  $data->fax_vendor,
            "logo_vendor" => $ihsi,
            "logo_vendorOld" => $data->logo_vendorOld,
            'alamat_vendor' =>  $data->alamat_vendor,
            'provinsi_vendor' =>  $data->provinsi_vendor,
            'kota_vendor' =>  $data->kota_vendor,
            'kodepos_vendor' =>  $data->kodepos_vendor,
            "web_vendor" => $data->web_vendor,
            "facebook_vendor" => $data->facebook_vendor,
            "instagram_vendor" => $data->instagram_vendor,

            'cabang_vendor' =>  $data->cabang_vendor,
            'alamat_pusat' =>  $data->alamat_pusat,
            'email_pusat' =>  $data->email_pusat,
            'tlp_pusat' =>  $data->tlp_pusat,
            'fax_pusat' =>  $data->fax_pusat,

            'no_akta_diri' => $data->no_akta_diri,
            'notaris_diri' => $data->notaris_diri,
            'tgl_akta_diri' => $data->tgl_akta_diri,
            'file_pendirian' => $data->file_pendirian,
            'no_akta_ubah' => $data->no_akta_ubah,
            'notaris_ubah' => $data->notaris_ubah,
            'tgl_akta_ubah' => $data->tgl_akta_ubah,
            'file_perubahan' => $data->file_perubahan,
            'npwp_vendor' =>  $data->npwp_vendor,
            'file_npwp' => $data->file_npwp,
            'pkp_vendor' =>  $data->pkp_vendor,
            'file_pkp' => $data->file_pkp,
            'iujksiup' => $getNoIujkSiup,
            'sbu' => $getNoSbu,
            'klasifikasiIujk' => $dataIujkFix,
            'klasifikasiSbu' => $dataSbuFix,
            'pengurus' => $dataPengurus,
        ));

        return json_encode($dataFix[0]);
    }

    public function showMy($id_user) {
        $myVendor = DB::table('users_to_vendor')
            ->where('id_user', $id_user)->first();

        $id_vendor = $myVendor->id_vendor;

        $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')
            ->leftjoin('tb_vendor_third as a', 'p.id_vendor', '=', 'a.id_vendor')
            ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
            ->where('p.id_vendor', $id_vendor)->first();

        $data->logo_vendorOld = $data->logo_vendor;

        $ihsi = null;
        if (filter_var($data->logo_vendor, FILTER_VALIDATE_URL)) {
            $data->logo_vendor = str_replace("open", "uc", $data->logo_vendor);
            $ihsi = str_replace("open", "uc", $data->logo_vendor);
        } else {
            $path = public_path() . '/uploads/logo_vendor/'.$data->logo_vendor;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->logo_vendor = $src;
            $ihsi = $src;
        }

        $dataPengurus = DB::table('tb_vendor_third as p')
            ->where('p.id_vendor', $id_vendor)->get();

        $dataPengurusFix = array();
        foreach ($dataPengurus as $key) {
            array_push($dataPengurusFix, array(
                "jenis_pengurus" => $key->jenis_pengurus,
                "nama_pengurus" => $key->nama_pengurus,
                "no_ktp" => $key->no_ktp,
                "foto_ktp" => $key->foto_ktp,
                "jabatan_pengurus" => $key->jabatan_pengurus,
            ));
        }  


        $getNoIujkSiup = DB::table('tb_vendor_iujksiup as p')
            ->select('p.*','p.no_iujksiup as no_iujksiup_old')
            ->where('p.id_vendor', $id_vendor)->get();

        $arrNoIujkSiup = array();
        foreach ($getNoIujkSiup as $key) {
            array_push($arrNoIujkSiup, $key->no_iujksiup);
        }
        $dataIujk = DB::table('tb_vendor_iujksiup as p')
            ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
            ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
            ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
            ->select('l.id_subklasifikasi1','m.id_subklasifikasi2','k.id_klasifikasi')
            ->where('p.id_vendor', $id_vendor)
            ->whereIn('k.id_klasifikasi', $arrNoIujkSiup)
            ->get();

        $dataIujkFix = array();
        foreach ($dataIujk as $key) {
            if( in_array( array(
                "id_klasifikasi" => $key->id_klasifikasi,
                "klasifikasi1" => $key->id_subklasifikasi1,
                "klasifikasi2" => $key->id_subklasifikasi2,
                ) , $dataIujkFix ) )
            {
            } else {
                array_push($dataIujkFix, array(
                    "id_klasifikasi" => $key->id_klasifikasi,
                    "klasifikasi1" => $key->id_subklasifikasi1,
                    "klasifikasi2" => $key->id_subklasifikasi2,
                ));
            } 
        } 

        $getNoSbu = DB::table('tb_vendor_sbu as p')
            ->select('p.*','p.no_sbu as no_sbu_old')
            ->where('p.id_vendor', $id_vendor)->get();


        $arrSbu = array();
        foreach ($getNoSbu as $key) {
            array_push($arrSbu, $key->no_sbu);
        }
        $dataSbu = DB::table('tb_vendor_sbu as p')
            ->leftjoin('tb_vendor_klasifikasi as k', 'p.id_vendor', '=', 'k.id_vendor')
            ->leftjoin('tb_subklasifikasi1 as l', 'k.klasifikasi1', '=', 'l.id_subklasifikasi1')
            ->leftjoin('tb_subklasifikasi2 as m', 'k.klasifikasi2', '=', 'm.id_subklasifikasi2')
            ->select('l.id_subklasifikasi1','m.id_subklasifikasi2','k.id_klasifikasi')
            ->where('p.id_vendor', $id_vendor)
            ->whereIn('k.id_klasifikasi', $arrSbu)
            ->get();

        $dataSbuFix = array();
        foreach ($dataSbu as $key) {
            if( in_array( array(
                "id_klasifikasi" => $key->id_klasifikasi,
                "klasifikasi1" => $key->id_subklasifikasi1,
                "klasifikasi2" => $key->id_subklasifikasi2,
                ) , $dataSbuFix ) )
            {
            } else {
                array_push($dataSbuFix, array(
                    "id_klasifikasi" => $key->id_klasifikasi,
                    "klasifikasi1" => $key->id_subklasifikasi1,
                    "klasifikasi2" => $key->id_subklasifikasi2,
                ));
            } 
        }      

        $dataFix = array();
        array_push($dataFix, array(
            "rating" => $data->rating,
            "id_vendor" => $id_vendor,
            "nama_vendor" => $data->nama_vendor,
            "deskripsi_vendor" => $data->deskripsi_vendor,
            "nama_narahubung" => $data->nama_narahubung,
            "email_narahubung" => $data->email_narahubung,
            "tlp_narahubung" => $data->tlp_narahubung,
            "jenis_vendor" => $data->jenis_vendor,
            "email_vendor" => $data->email_vendor,
            'tlp_vendor' =>  $data->tlp_vendor,
            'fax_vendor' =>  $data->fax_vendor,
            "logo_vendor" => $ihsi,
            "logo_vendorOld" => $data->logo_vendorOld,
            'alamat_vendor' =>  $data->alamat_vendor,
            'provinsi_vendor' =>  $data->provinsi_vendor,
            'kota_vendor' =>  $data->kota_vendor,
            'kodepos_vendor' =>  $data->kodepos_vendor,
            "web_vendor" => $data->web_vendor,
            "facebook_vendor" => $data->facebook_vendor,
            "instagram_vendor" => $data->instagram_vendor,

            'cabang_vendor' =>  $data->cabang_vendor,
            'alamat_pusat' =>  $data->alamat_pusat,
            'email_pusat' =>  $data->email_pusat,
            'tlp_pusat' =>  $data->tlp_pusat,
            'fax_pusat' =>  $data->fax_pusat,

            'no_akta_diri' => $data->no_akta_diri,
            'notaris_diri' => $data->notaris_diri,
            'tgl_akta_diri' => $data->tgl_akta_diri,
            'file_pendirian' => $data->file_pendirian,
            'no_akta_ubah' => $data->no_akta_ubah,
            'notaris_ubah' => $data->notaris_ubah,
            'tgl_akta_ubah' => $data->tgl_akta_ubah,
            'file_perubahan' => $data->file_perubahan,
            'npwp_vendor' =>  $data->npwp_vendor,
            'file_npwp' => $data->file_npwp,
            'pkp_vendor' =>  $data->pkp_vendor,
            'file_pkp' => $data->file_pkp,
            'iujksiup' => $getNoIujkSiup,
            'sbu' => $getNoSbu,
            'klasifikasiIujk' => $dataIujkFix,
            'klasifikasiSbu' => $dataSbuFix,
            'pengurus' => $dataPengurus,
        ));

        return json_encode($dataFix[0]);
    }

    // menambah data vendor
    public function store(Request $request) {
        $image = $request->logo_vendor; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/logo_vendor';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        Vendor::insert(
            [
                'logo_vendor' =>  $imageName,
                'nama_vendor' =>  $request->nama_vendor,
                'deskripsi_vendor' =>  $request->deskripsi_vendor,
                'nama_narahubung' =>  $request->nama_narahubung,
                'email_narahubung' =>  $request->email_narahubung,
                'tlp_narahubung' =>  $request->tlp_narahubung,
                'jenis_vendor' =>  $request->jenis_vendor,
                'email_vendor' =>  $request->email_vendor,
                'tlp_vendor' =>  $request->tlp_vendor,
                'fax_vendor' =>  $request->fax_vendor,
                'alamat_vendor' =>  $request->alamat_vendor,
                'provinsi_vendor' =>  $request->provinsi_vendor,
                'kota_vendor' =>  $request->kota_vendor,
                'kodepos_vendor' =>  $request->kodepos_vendor,
                'status' => 2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $data = DB::table('tb_vendor_primary')
            ->where('nama_vendor', $request->nama_vendor)
            ->where('jenis_vendor', $request->jenis_vendor)
            ->where('nama_narahubung', $request->nama_narahubung)
            ->first();

        DB::table('tb_vendor_secondary')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'web_vendor' => $request->web_vendor,
                'facebook_vendor' => $request->facebook_vendor,
                'instagram_vendor' => $request->instagram_vendor,
                'cabang_vendor' =>  $request->cabang_vendor,
                'alamat_pusat' =>  $request->alamat_pusat,
                'email_pusat' =>  $request->email_pusat,
                'tlp_pusat' =>  $request->tlp_pusat,
                'fax_pusat' =>  $request->fax_pusat,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->pengurus) as $key) {   
            DB::table('tb_vendor_third')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'jenis_pengurus' => $key->pengurus ?? '',
                    'nama_pengurus' => $key->namaPengurus ?? '',
                    'no_ktp' => $key->noKTP ?? '',
                    'foto_ktp' => ($key->noKTP ?? '') .'0_'.(str_replace(' ', '_', $key->namaPengurus ?? '')) .'.'.'png', 
                    'jabatan_pengurus' => $key->jabatan ?? '' ,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }  

        $file1 = $request->file('file1');
        if(!empty($file1)){
          $imageNameFile = str_random(10).'-akta-pendirian.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
          $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
          // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
        } else {
            $imageNameFile = null;
        }

        $file2 = $request->file('file2');
        if(!empty($file2)){
          $imageNameFile2 = str_random(10).'-akta-perubahan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
          $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
          // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);
        } else {
            $imageNameFile2 = null;
        }

        $file3 = $request->file('file3');
        if(!empty($file3)){
          $imageNameFile3 = str_random(10).'-npwp.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile3, $file3);
          $upload = $file3->move(public_path()."/uploads/file_dokumen/",$imageNameFile3);
          // move_uploaded_file($_FILES['file3']['tmp_name'], "uploads/file_dokumen/".$imageNameFile3);
        } else {
            $imageNameFile3 = null;
        }

        $file4 = $request->file('file4');
        if(!empty($file4)){
          $imageNameFile4 = str_random(10).'-pkp.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile4, $file4);
          $upload = $file4->move(public_path()."/uploads/file_dokumen/",$imageNameFile4);
          // move_uploaded_file($_FILES['file4']['tmp_name'], "uploads/file_dokumen/".$imageNameFile4);
        } else {
            $imageNameFile4 = null;
        }

        $file5 = $request->file('file5');
        if(!empty($file5)){
          $imageNameFile5 = str_random(10).'-iujksiup.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile5, $file5);
          $upload = $file5->move(public_path()."/uploads/file_dokumen/",$imageNameFile5);
          // move_uploaded_file($_FILES['file5']['tmp_name'], "uploads/file_dokumen/".$imageNameFile5);
        } else {
            $imageNameFile5 = null;
        }

        $file6 = $request->file('file6');
        if(!empty($file6)){
          $imageNameFile6 = str_random(10).'-sbu.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile6, $file6);
          $upload = $file6->move(public_path()."/uploads/file_dokumen/",$imageNameFile6);
          // move_uploaded_file($_FILES['file6']['tmp_name'], "uploads/file_dokumen/".$imageNameFile6);
        } else {
            $imageNameFile6 = null;
        }

        DB::table('tb_vendor_dokumen')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'no_akta_diri' => $request->no_akta_diri,
                'notaris_diri' => $request->notaris_diri,
                'tgl_akta_diri' => $request->tgl_akta_diri,
                'file_pendirian' => $imageNameFile,
                'no_akta_ubah' => $request->no_akta_ubah,
                'notaris_ubah' => $request->notaris_ubah,
                'tgl_akta_ubah' => $request->tgl_akta_ubah,
                'file_perubahan' => $imageNameFile2,
                'npwp_vendor' =>  $request->npwp_vendor,
                'file_npwp' => $imageNameFile3,
                'pkp_vendor' =>  $request->pkp_vendor,
                'file_pkp' => $imageNameFile4,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_vendor_iujksiup')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'no_iujksiup' => $request->no_iujksiup,
                'instansi_iujksiup' => $request->instansi_iujksiup,
                'tgl_iujksiup' => $request->tgl_iujksiup,
                'file_iujksiup' => $imageNameFile5,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->klasifikasiIUJK) as $key) {   
            DB::table('tb_vendor_klasifikasi')->insert(
                [
                    'id_vendor' => $data->id_vendor,
                    'id_klasifikasi' => $request->no_iujksiup,
                    'klasifikasi1' => $key->klasifikasi1,
                    'klasifikasi2' => $key->klasifikasi2,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        DB::table('tb_vendor_sbu')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'no_sbu' => $request->no_sbu,
                'instansi_sbu' => $request->instansi_sbu,
                'tgl_sbu' => $request->tgl_sbu,
                'file_sbu' => $imageNameFile6,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->klasifikasiSBU) as $key) {   
            DB::table('tb_vendor_klasifikasi')->insert(
                [
                    'id_vendor' => $data->id_vendor,
                    'id_klasifikasi' => $request->no_sbu,
                    'klasifikasi1' => $key->klasifikasi1,
                    'klasifikasi2' => $key->klasifikasi2,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

                            $firstLast = explode(" ", $request->nama_narahubung);
                            $username = strtolower($firstLast[0]) . rand(10, 10000);

                            $firstLast = explode(" ", $request->nama_narahubung);
                            if (count($firstLast) < 2) {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => "",
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            } else {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => $firstLast[1],
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            }

                            $destinationPath = public_path() . '/uploads/photo_profile';
                            $img = file($destinationPath . "/avatarDefault.png");
                            \File::put($destinationPath . '/' . $username . ".png", $img);

                            $id = DB::getPdo()->lastInsertId();

                            DB::table('users')->insert(
                                [
                                    'id_person' => $id,
                                    'username' => $username,
                                    'password' => bcrypt($username),
                                    'email' => $request->email_narahubung,
                                    'role' => 'R002',
                                    'status' => 1,
                                ]
                            );
                            $dataUser = DB::table('users')
                                ->where('id_person', $id)
                                ->first();
                            $dataMenu = DB::table('tb_role_master')
                                ->where('kd_role', 'R002')
                                ->get();
                            foreach ($dataMenu as $key) {
                                DB::table('tb_role_user_access')->insert(
                                    [
                                        'kd_role' => $key->kd_role,
                                        'UserID' => $dataUser->id,
                                        'MenuID' => $key->MenuID,
                                        'DetailID' => $key->DetailID,
                                        'DetailID2' => $key->DetailID2,
                                        'Link' => $key->Link,
                                        'Action' => $key->Action,
                                    ]
                                );
                            }

                            DB::table('users_to_vendor')->insert(
                                [
                                    'id_user' => $dataUser->id,
                                    'id_vendor' => $data->id_vendor,
                                ]
                            );


        // $objDemo = DB::table('tb_content_email')->where('type', 'Register')->first();

        // $sendEmail = array();
        // array_push($sendEmail, $request->email_narahubung, $request->email_vendor);

        // foreach ($sendEmail as $key) {
        //     Mail::to($key)->send(new ThanksRegister($objDemo));
        // }
        // 
        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Vendor ".$request->nama_vendor,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $task = Vendor::count();
        $task2 = Person::count();
        try {
            // broadcast(new RefreshEvents());
            broadcast(new PersonEvents($task));
            broadcast(new PersonEvents($task2));
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return;
    }

    public function storeOnline(Request $request) {
        $image = $request->logo_vendor; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/logo_vendor';
        // $upload = base64_decode($image)->move(public_path()."/uploads/logo_vendor/",$imageName);
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        Vendor::insert(
            [
                'logo_vendor' =>  $imageName,
                'nama_vendor' =>  $request->nama_vendor,
                'deskripsi_vendor' =>  $request->deskripsi_vendor,
                'nama_narahubung' =>  $request->nama_narahubung,
                'email_narahubung' =>  $request->email_narahubung,
                'tlp_narahubung' =>  $request->tlp_narahubung,
                'jenis_vendor' =>  $request->jenis_vendor,
                'email_vendor' =>  $request->email_vendor,
                'tlp_vendor' =>  $request->tlp_vendor,
                'fax_vendor' =>  $request->fax_vendor,
                'alamat_vendor' =>  $request->alamat_vendor,
                'provinsi_vendor' =>  $request->provinsi_vendor,
                'kota_vendor' =>  $request->kota_vendor,
                'kodepos_vendor' =>  $request->kodepos_vendor,
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $data = DB::table('tb_vendor_primary')
            ->where('nama_vendor', $request->nama_vendor)
            ->where('jenis_vendor', $request->jenis_vendor)
            ->where('nama_narahubung', $request->nama_narahubung)
            ->first();

        DB::table('tb_vendor_secondary')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'web_vendor' => $request->web_vendor,
                'facebook_vendor' => $request->facebook_vendor,
                'instagram_vendor' => $request->instagram_vendor,
                'cabang_vendor' =>  $request->cabang_vendor,
                'alamat_pusat' =>  $request->alamat_pusat,
                'email_pusat' =>  $request->email_pusat,
                'tlp_pusat' =>  $request->tlp_pusat,
                'fax_pusat' =>  $request->fax_pusat,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->pengurus) as $key) {   
            DB::table('tb_vendor_third')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'jenis_pengurus' => $key->pengurus ?? '',
                    'nama_pengurus' => $key->namaPengurus ?? '',
                    'no_ktp' => $key->noKTP ?? '',
                    'foto_ktp' => ($key->noKTP ?? '') .'0_'.(str_replace(' ', '_', $key->namaPengurus ?? '')) .'.'.'png', 
                    'jabatan_pengurus' => $key->jabatan ?? '',
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );     
        }  

        $file1 = $request->file('file1');
        if(!empty($file1)){
          $imageNameFile = str_random(10).'-akta-pendirian.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
          $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
          // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
        } else {
            $imageNameFile = null;
        } 

        $file2 = $request->file('file2');
        if(!empty($file2)){
          $imageNameFile2 = str_random(10).'-akta-perubahan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
          $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
          // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);
        } else {
            $imageNameFile2 = null;
        }

        $file3 = $request->file('file3');
        if(!empty($file3)){
          $imageNameFile3 = str_random(10).'-npwp.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile3, $file3);
          $upload = $file3->move(public_path()."/uploads/file_dokumen/",$imageNameFile3);
          // move_uploaded_file($_FILES['file3']['tmp_name'], "uploads/file_dokumen/".$imageNameFile3);
        } else {
            $imageNameFile3 = null;
        }

        $file4 = $request->file('file4');
        if(!empty($file4)){
          $imageNameFile4 = str_random(10).'-pkp.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile4, $file4);
          $upload = $file4->move(public_path()."/uploads/file_dokumen/",$imageNameFile4);
          // move_uploaded_file($_FILES['file4']['tmp_name'], "uploads/file_dokumen/".$imageNameFile4);
        } else {
            $imageNameFile4 = null;
        }

        DB::table('tb_vendor_dokumen')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'no_akta_diri' => $request->no_akta_diri,
                'notaris_diri' => $request->notaris_diri,
                'tgl_akta_diri' => $request->tgl_akta_diri,
                'file_pendirian' => $imageNameFile,
                'no_akta_ubah' => $request->no_akta_ubah,
                'notaris_ubah' => $request->notaris_ubah,
                'tgl_akta_ubah' => $request->tgl_akta_ubah,
                'file_perubahan' => $imageNameFile2,
                'npwp_vendor' =>  $request->npwp_vendor,
                'file_npwp' => $imageNameFile3,
                'pkp_vendor' =>  $request->pkp_vendor,
                'file_pkp' => $imageNameFile4,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->isiIUJK) as $key) {   
            DB::table('tb_vendor_iujksiup')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'no_iujksiup' => $key->noIujkSiup ?? '',
                    'instansi_iujksiup' => $key->instansiIujkSiup ?? '',
                    'tgl_iujksiup' => $key->tglIujkSiup ?? '',
                    'file_iujksiup' => $key->noIujkSiup."_iujksiup.pdf",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        foreach (json_decode($request->klasifikasiIUJK) as $key) {   
            DB::table('tb_vendor_klasifikasi')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'id_klasifikasi' => $key->noIujkSiup ?? '',
                    'klasifikasi1' => $key->klasifikasi1 ?? '',
                    'klasifikasi2' => $key->klasifikasi2 ?? '',
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        foreach (json_decode($request->isiSBU) as $key) {   
            DB::table('tb_vendor_sbu')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'no_sbu' => $key->noSbu ?? '',
                    'instansi_sbu' => $key->instansiSbu ?? '',
                    'tgl_sbu' => $key->tglSbu ?? '',
                    'file_sbu' => $key->noSbu."_sbu.pdf",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        foreach (json_decode($request->klasifikasiSBU) as $key) {   
            DB::table('tb_vendor_klasifikasi')->insert(
                [
                    'id_vendor' => $data->id_vendor ?? '',
                    'id_klasifikasi' => $key->noSbu ?? '',
                    'klasifikasi1' => $key->klasifikasi1 ?? '',
                    'klasifikasi2' => $key->klasifikasi2 ?? '',
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }


        $objDemo = DB::table('tb_content_email')->where('type', 'Register')->first();

        $sendEmail = array();
        array_push($sendEmail, $request->email_narahubung, $request->email_vendor);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new ThanksRegister($objDemo));
        }

        //For Notif
        $create = date("Y-m-d H:i:s");
        DB::table('tb_notifikasi')->insert(
            [  
                'jenis' => 'Register',
                'id_sender' => Null,
                'id_receiver' => 14,  
                'isi' => "Request Registrasi Vendor ".$request->nama_vendor ?? '',
                'status' => 1,
                'created_at' => $create,
            ]
        );

        $data2 = DB::table('tb_notifikasi')
               ->where('jenis', 'Register')
               ->where('id_sender', Null)
               ->where('id_receiver', 14)
               ->where('isi', "Request Registrasi Vendor ".$request->nama_vendor ?? '')
               ->where('created_at', $create)
               ->first();

        $user = [
            "id_sender" => Null,
            "id_receiver" => 14,
        ];

        $message2 = [
            "id_notif" => $data2->id_notif,
            "id_sender" => $data2->id_sender,
            "id_receiver" => $data2->id_receiver,
            "jenis" => $data2->jenis,
            "isi" => $data2->isi,
            "status" => $data2->status,
            "created_at" => $data2->created_at,
            "updated_at" => $data2->updated_at,
        ];

        $task = Vendor::count();

        try {
            // broadcast(new RefreshEvents());
            broadcast(new VendorEvents($task));
        	broadcast(new NotifEvents($user, $message2));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
  //       echo json_encode([
		// 	'status' => 1
		// ]);

		return;
    }

    public function uploadIUJK(Request $request) {
        
        $file5 = $request->file('file');
        if(!empty($file5)){
          $imageNameFile5 = $request->no_iujksiup."_iujksiup.pdf";
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile5, $file5);
          $upload = $file5->move(public_path()."/uploads/file_dokumen/",$imageNameFile5);
          // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile5);
        } else {
            $imageNameFile5 = null;
        }

        return;
    }

    public function uploadSBU(Request $request) {
        
        $file6 = $request->file('file');
        if(!empty($file6)){
          $imageNameFile6 = $request->no_sbu."_sbu.pdf";
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile6, $file6);
          $upload = $file6->move(public_path()."/uploads/file_dokumen/",$imageNameFile6);
          // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile6);
        } else {
            $imageNameFile6 = null;
        }

        return;
    }

    public function uploadKTP(Request $request) {
        
        $fileKTP = $request->file('fileKTP');
        $imageNameFile = $request->no_ktp.'0_'.str_replace(' ', '_', $request->nama_pengurus).'.'.'png';
        if(!empty($fileKTP)){
          // \File::put(public_path()."/uploads/photo_owner". '/' . $imageNameFile, $fileKTP);
          $upload = $fileKTP->move(public_path()."/uploads/photo_owner/",$imageNameFile);
          // move_uploaded_file($_FILES['fileKTP']['tmp_name'], "uploads/photo_owner/".$imageNameFile);
        }  

        return;
    }

    public function storeFile(Request $request) {

        if($request->tipe == "IUJK / SIUP"){
            $file = $request->file('file');
            if(!empty($file)){
              $imageNameFile = str_random(10).'-iujksiup.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file);
              $upload = $file->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
            } else {
                $imageNameFile = null;
            }
            DB::table('tb_vendor_iujksiup')->insert(
                [
                    'id_vendor' => $request->id_vendor,
                    'no_iujksiup' => $request->no,
                    'instansi_iujksiup' => $request->instansi,
                    'tgl_iujksiup' => $request->tgl,
                    'file_iujksiup' => $imageNameFile,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            $file = $request->file('file');
            if(!empty($file)){
              $imageNameFile = str_random(10).'-sbu.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file);
              $upload = $file->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
            } else {
                $imageNameFile = null;
            }
            DB::table('tb_vendor_sbu')->insert(
                [
                    'id_vendor' => $request->id_vendor,
                    'no_sbu' => $request->no,
                    'instansi_sbu' => $request->instansi,
                    'tgl_sbu' => $request->tgl,
                    'file_sbu' => $imageNameFile,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        foreach (json_decode($request->klasifikasi) as $key) {   
            DB::table('tb_vendor_klasifikasi')->insert(
                [
                    'id_vendor' => $request->id_vendor,
                    'id_klasifikasi' => $request->no,
                    'klasifikasi1' => $key->klasifikasi1,
                    'klasifikasi2' => $key->klasifikasi2,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        return;
    }

    public function importXLXS(Request $request) {
        $isiXLSX = $request->isiXLSX;
        $log = array();
        $ret = "";
        foreach ($isiXLSX as $key => $row) {
            $data = new \App\Vendor();

            if ($row['nama_vendor'] == null || $row['nama_vendor'] == "") {
                array_push($log, "nama_vendor");
            } else {
                $data->nama_vendor = $row['nama_vendor'];
            }
            if ($row['deskripsi_vendor'] == null || $row['deskripsi_vendor'] == "") {
                array_push($log, "deskripsi_vendor");
            } else {
                $data->deskripsi_vendor = $row['deskripsi_vendor'];
            }
            if ($row['nama_narahubung'] == null || $row['nama_narahubung'] == "") {
                array_push($log, "nama_narahubung");
            } else {
                $data->nama_narahubung = $row['nama_narahubung'];
            }
            if ($row['email_narahubung'] == null || $row['email_narahubung'] == "") {
                array_push($log, "email_narahubung");
            } else {
                $data->email_narahubung = $row['email_narahubung'];
            }
            if ($row['tlp_narahubung'] == null || $row['tlp_narahubung'] == "") {
                array_push($log, "tlp_narahubung");
            } else {
                $data->tlp_narahubung = $row['tlp_narahubung'];
            }
            if ($row['email_vendor'] == null || $row['email_vendor'] == "") {
                array_push($log, "email_vendor");
            } else {
                $data->email_vendor = $row['email_vendor'];
            }
            if ($row['tlp_vendor'] == null || $row['tlp_vendor'] == "") {
                array_push($log, "tlp_vendor");
            } else {
                $data->tlp_vendor = $row['tlp_vendor'];
            }
            if ($row['fax_vendor'] == null || $row['fax_vendor'] == "") {
                array_push($log, "fax_vendor");
            } else {
                $data->fax_vendor = $row['fax_vendor'];
            }
            if ($row['alamat_vendor'] == null || $row['alamat_vendor'] == "") {
                array_push($log, "alamat_vendor");
            } else {
                $data->alamat_vendor = $row['alamat_vendor'];
            }
            if ($row['provinsi_vendor'] == null || $row['provinsi_vendor'] == "") {
                array_push($log, "provinsi_vendor");
            } else {
                $data->provinsi_vendor = $row['provinsi_vendor'];
            }
            if ($row['kota_vendor'] == null || $row['kota_vendor'] == "") {
                array_push($log, "kota_vendor");
            } else {
                $data->kota_vendor = $row['kota_vendor'];
            }
            if ($row['kodepos_vendor'] == null || $row['kodepos_vendor'] == "") {
                array_push($log, "kodepos_vendor");
            } else {
                $data->kodepos_vendor = $row['kodepos_vendor'];
            }

            if ($row['web_vendor'] == null || $row['web_vendor'] == "") {
                array_push($log, "web_vendor");
            } else {

            }
            if ($row['facebook_vendor'] == null || $row['facebook_vendor'] == "") {
                array_push($log, "facebook_vendor");
            } else {

            }
            if ($row['instagram_vendor'] == null || $row['instagram_vendor'] == "") {
                array_push($log, "instagram_vendor");
            } else {

            }
            if ($row['cabang_vendor'] == null || $row['cabang_vendor'] == "") {
                array_push($log, "cabang_vendor");
            } else {

            }
            if ($row['alamat_pusat'] == null || $row['alamat_pusat'] == "") {
                array_push($log, "alamat_pusat");
            } else {

            }
            if ($row['email_pusat'] == null || $row['email_pusat'] == "") {
                array_push($log, "email_pusat");
            } else {

            }
            if ($row['tlp_pusat'] == null || $row['tlp_pusat'] == "") {
                array_push($log, "tlp_pusat");
            } else {

            }
            if ($row['fax_pusat'] == null || $row['fax_pusat'] == "") {
                array_push($log, "fax_pusat");
            } else {

            }


            $data->status = "2";
            $data->created_at = date("Y-m-d H:i:s");

            $count = count($log);

            if (!empty($data)) {
                $cek = Vendor::where('nama_vendor', "{$row['email_vendor']}")
                    ->where('email_vendor', "{$row['email_vendor']}")
                    ->count();
                $cekID = Vendor::where('nama_vendor', "{$row['email_vendor']}")
                    ->where('email_vendor', "{$row['email_vendor']}")
                    ->first();

                if ($cek == 0) {
                    if ($count > 0) {
                        if (DB::table('log_import')->insert(
                            ['nama' => $row['nama_vendor'], 'keterangan' => json_encode($log)]
                        )) {
                            $log = array();
                        }
                    } else {

                        // foreach ($logo as $key) {
                        //     if ($row['logo_vendor'] == $key['name']) {
                        //         if (substr($key['path'], 0, 14) == "data:image/png") {
                        //             $image = str_replace('data:image/png;base64,', '', $key['path']);
                        //         } else if (substr($key['path'], 0, 15) == "data:image/jpeg") {
                        //             $image = str_replace('data:image/jpeg;base64,', '', $key['path']);
                        //         }
                        //         $image = str_replace(' ', '+', $image);
                        //         $imageName = $key['name'];
                        //         $destinationPath = public_path() . '/uploads/logo_vendor';
                        //         \File::put($destinationPath . '/' . $imageName, base64_decode($image));
                        //         $data->logo_vendor = $destinationPath . "/" . $imageName;
                        //         $data->save();
                        //         break;
                        //     } else if ($row['logo_vendor'] == null || $row['logo_vendor'] == "") {
                        //         $destinationPath = public_path() . '/uploads/logo_vendor';
                        //         $img = file($destinationPath."/avatarDefault.png");
                        //         \File::put($destinationPath. '/' . $row['nama_vendor']."_default", $img);
                        //         $data->logo_vendor = $destinationPath."/".$row['nama_vendor']."_default.png";
                        //         $data->save();
                        //     }
                        // }

                        if ($row['logo_vendor'] == null || $row['logo_vendor'] == "") {
                            $destinationPath = public_path() . '/uploads/logo_vendor';
                            $img = file($destinationPath."/avatarDefault.png");
                            \File::put($destinationPath. '/' . $row['nama_vendor']."_default.png", $img);
                            $data->logo_vendor = $row['nama_vendor']."_default.png";
                            $data->save();
                        }

                        $dataZ = DB::table('tb_vendor_primary')
                            ->where('nama_vendor', $row['nama_vendor'])
                            ->where('nama_narahubung', $row['nama_narahubung'])
                            ->first();

                        if (!empty($dataZ)) {
                            DB::table('tb_vendor_secondary')->insert(
                                [
                                    'id_vendor' => $dataZ->id_vendor,
                                    'web_vendor' => $row['web_vendor'],
                                    'facebook_vendor' => $row['facebook_vendor'],
                                    'instagram_vendor' => $row['instagram_vendor'],
                                    'cabang_vendor' =>  $row['cabang_vendor'],
                                    'alamat_pusat' =>  $row['alamat_pusat'],
                                    'email_pusat' =>  $row['email_pusat'],
                                    'tlp_pusat' =>  $row['tlp_pusat'],
                                    'fax_pusat' =>  $row['fax_pusat'],
                                    'created_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                            $destinationPathpdf = public_path() . '/uploads/file_dokumen';
                            $pdf = file($destinationPathpdf."/dummy.pdf");
                            $pdf1name = str_random(10).'-akta-pendirian.'.'pdf';
                            $pdf2name = str_random(10).'-akta-perubahan.'.'pdf';
                            $pdf3name = str_random(10).'-npwp.'.'pdf';
                            $pdf4name = str_random(10).'-pkp.'.'pdf';
                            $pdf5name = str_random(10).'-iujksiup.'.'pdf';
                            $pdf6name = str_random(10).'-sbu.'.'pdf';
                            \File::put($destinationPathpdf. '/' . $pdf1name, $pdf);
                            \File::put($destinationPathpdf. '/' . $pdf2name, $pdf);
                            \File::put($destinationPathpdf. '/' . $pdf3name, $pdf);
                            \File::put($destinationPathpdf. '/' . $pdf4name, $pdf);

                            DB::table('tb_vendor_dokumen')->insert(
                                [
                                    'id_vendor' => $dataZ->id_vendor,
                                    'no_akta_diri' => 000000,
                                    'notaris_diri' => "Dummy Default",
                                    'tgl_akta_diri' => date("Y-m-d"),
                                    'file_pendirian' => $pdf1name,
                                    'no_akta_ubah' => 000000,
                                    'notaris_ubah' => "Dummy Default",
                                    'tgl_akta_ubah' => date("Y-m-d"),
                                    'file_perubahan' => $pdf2name,
                                    'npwp_vendor' => 000000,
                                    'file_npwp' => $pdf3name,
                                    'pkp_vendor' => 000000,
                                    'file_pkp' => $pdf4name,
                                    'no_iujksiup' => 000000,
                                    'instansi_iujksiup' => "Dummy Default",
                                    'tgl_iujksiup' => date("Y-m-d"),
                                    'file_iujksiup' => $pdf5name,
                                    'no_sbu' => 000000,
                                    'instansi_sbu' => "Dummy Default",
                                    'tgl_sbu' => date("Y-m-d"),
                                    'file_sbu' => $pdf5name,

                                    'created_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                            $firstLast = explode(" ", $row['nama_narahubung']);
                            $username = strtolower($firstLast[0]) . rand(10, 10000);

                            $destinationPath = public_path() . '/uploads/photo_profile';
                            $img = file($destinationPath . "/avatarDefault.png");
                            \File::put($destinationPath . '/' . $username . ".png", $img);

                            if (count($firstLast) < 2) {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => "",
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            } else {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => $firstLast[1],
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            }

                            // $destinationPath = public_path('uploads\photo_profile');
                            // $img = file($destinationPath . "\avatarDefault.png");
                            // \File::put($destinationPath . '\\' . $username . ".png", $img);

                            $id = DB::getPdo()->lastInsertId();

                            DB::table('users')->insert(
                                [
                                    'id_person' => $id,
                                    'username' => $username,
                                    'password' => bcrypt($username),
                                    'email' => $row['email_narahubung'],
                                    'role' => 'R002',
                                    'status' => 1,
                                ]
                            );
                            $dataY = DB::table('users')
                                ->where('id_person', $id)
                                ->first();
                            $dataMenu = DB::table('tb_role_master')
                                ->where('kd_role', 'R002')
                                ->get();
                            foreach ($dataMenu as $key) {
                                DB::table('tb_role_user_access')->insert(
                                    [
                                        'kd_role' => $key->kd_role,
                                        'UserID' => $dataY->id,
                                        'MenuID' => $key->MenuID,
                                        'DetailID' => $key->DetailID,
                                        'DetailID2' => $key->DetailID2,
                                        'Link' => $key->Link,
                                        'Action' => $key->Action,
                                    ]
                                );
                            }

                            DB::table('users_to_vendor')->insert(
                                [
                                    'id_user' => $dataY->id,
                                    'id_vendor' => $dataZ->id_vendor,
                                ]
                            );

                            $task = Person::count();
                            try {
                                // broadcast(new RefreshEvents());
                                broadcast(new PersonEvents($task));
                            } catch (\Exception $e) {
                                return $e->getMessage();
                            }

                        }
                        $ret = "";
                        $ret .= "oke";
                    }
                } else {
                    if ($count > 0) {
                        if (DB::table('log_import')->insert(
                            ['nama' => $row['nama_vendor'], 'keterangan' => json_encode($log)]
                        )) {
                            $log = array();
                        }
                    } else {
                        DB::table('tb_vendor_primary')
                            ->where('id_vendor', $cekID->id_vendor)
                            ->update(
                                [
                                    'nama_vendor' =>  $row['nama_vendor'],
                                    'deskripsi_vendor' =>  $row['deskripsi_vendor'],
                                    'nama_narahubung' =>  $row['nama_narahubung'],
                                    'email_narahubung' =>  $row['email_narahubung'],
                                    'tlp_narahubung' =>  $row['tlp_narahubung'],
                                    'jenis_vendor' =>  $row['jenis_vendor'],
                                    'email_vendor' =>  $row['email_vendor'],
                                    'tlp_vendor' =>  $row['tlp_vendor'],
                                    'fax_vendor' =>  $row['fax_vendor'],
                                    'alamat_vendor' =>  $row['alamat_vendor'],
                                    'provinsi_vendor' =>  $row['provinsi_vendor'],
                                    'kota_vendor' =>  $row['kota_vendor'],
                                    'kodepos_vendor' =>  $row['kodepos_vendor'],
                                    'status' => 2,
                                    'updated_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                        DB::table('tb_vendor_secondary')
                            ->where('id_vendor', $cekID->id_vendor)
                            ->update(
                                [
                                    'web_vendor' => $row['web_vendor'],
                                    'facebook_vendor' => $row['facebook_vendor'],
                                    'instagram_vendor' => $row['instagram_vendor'],
                                    'cabang_vendor' =>  $row['cabang_vendor'],
                                    'alamat_pusat' =>  $row['alamat_pusat'],
                                    'email_pusat' =>  $row['email_pusat'],
                                    'tlp_pusat' =>  $row['tlp_pusat'],
                                    'fax_pusat' =>  $row['fax_pusat'],
                                    'updated_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                        $ret = "";
                        $ret .= "oke";
                    }
                }
            }

        }
        // echo $count;
        // return;

        $datazy = DB::table('log_import')
            ->get();

        if (count($datazy) > 0) {
            $ret = "";
        }

        $isi = null;
        foreach ($datazy as $key) {
            $isi .= "\n Nama : ";
            $isi .= $key->nama;
            $isi .= ". \n";
            $isi .= " Data Kosong Pada: ";
            $isi .= implode(", ", json_decode($key->keterangan));
            $isi .= ". \n";
            $isi .= " Silahkan melakukan pengecekan kembali pada data tersebut !";
            $isi .= "\n";
            $isi .= "\n";
        }

        DB::table('log_import')
            ->truncate();

        if (count($datazy) > 0) {
            $ret .= $isi;
        }

        echo $ret;
        return;
    }

    public function downloadExcelVendor() {
        return Excel::download(new VendorExport, 'ExportVendor ' . date("Y-m-d") . '.xlsx');
    }

    public function downloadRarVendor($id_vendor) {

        $data = DB::table('tb_vendor_dokumen as d')
                ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'd.id_vendor')
                ->where('d.id_vendor', '=', $id_vendor)->first();

        $destinationPathpdf = public_path() . '/uploads/file_dokumen';
        $pdf1 = file($destinationPathpdf."/".$data->file_pendirian);
        if($data->file_perubahan !== Null || $data->file_perubahan !== ""){
            $pdf2 = file($destinationPathpdf."/".$data->file_perubahan);
        }
        $pdf3 = file($destinationPathpdf."/".$data->file_npwp);
        $pdf4 = file($destinationPathpdf."/".$data->file_pkp);
        // $pdf5 = file($destinationPathpdf."/".$data->file_iujksiup);
        // $pdf6 = file($destinationPathpdf."/".$data->file_sbu);
        \File::put($destinationPathpdf. '/download/' . $data->file_pendirian, $pdf1);
        if($data->file_perubahan !== Null || $data->file_perubahan !== ""){
            \File::put($destinationPathpdf. '/download/' . $data->file_perubahan, $pdf2);
        }
        \File::put($destinationPathpdf. '/download/' . $data->file_npwp, $pdf3);
        \File::put($destinationPathpdf. '/download/' . $data->file_pkp, $pdf4);
        // \File::put($destinationPathpdf. '/download/' . $data->file_iujksiup, $pdf5);
        // \File::put($destinationPathpdf. '/download/' . $data->file_sbu, $pdf6);

        // Here we choose the folder which will be used.
        $dirName = public_path() . '/uploads/file_dokumen/download';
        // Choose a name for the archive.
        $zipFileName = $data->nama_vendor.'.zip';
        // Create "MyCoolName.zip" file in public directory of project.
        $zip = new ZipArchive;
        if ( $zip->open( $dirName . '/' . $zipFileName, ZipArchive::CREATE ) === true )
        {
            // Copy all the files from the folder and place them in the archive.
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();

            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );

            // Download .zip file.
            return Response::download( $dirName . '/' . $zipFileName, $zipFileName, $headers );
        }
    }

    public function downloadRarVendor2() {

        $id_vendor = $_GET['id_vendor'];

        $data = DB::table('tb_vendor_dokumen as d')
                ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'd.id_vendor')
                ->where('d.id_vendor', '=', $id_vendor)
                ->first();

        File::deleteDirectory(public_path() . '/uploads/file_dokumen/download');
        File::makeDirectory(public_path() . '/uploads/file_dokumen/download', $mode = 0777, true, true);

        $path = public_path() . '/uploads/file_dokumen/download/' . $data->nama_vendor;
        File::makeDirectory($path, $mode = 0777, true, true);

        $destinationPathpdf = public_path() . '/uploads/file_dokumen';
        $pdf1 = file($destinationPathpdf."/".$data->file_pendirian);
        if($data->file_perubahan == Null || $data->file_perubahan == ""){
            
        } else {
            $pdf2 = file($destinationPathpdf."/".$data->file_perubahan);
        }
        $pdf3 = file($destinationPathpdf."/".$data->file_npwp);
        $pdf4 = file($destinationPathpdf."/".$data->file_pkp);


        \File::put($destinationPathpdf. '/download/' . $data->nama_vendor . '' . $data->file_pendirian, $pdf1);
        if($data->file_perubahan == Null || $data->file_perubahan == ""){
            
        } else {
            \File::put($destinationPathpdf. '/download/' . $data->nama_vendor . '/' . $data->file_perubahan, $pdf2);
        }
        \File::put($destinationPathpdf. '/download/' . $data->nama_vendor . '/' . $data->file_npwp, $pdf3);
        \File::put($destinationPathpdf. '/download/' . $data->nama_vendor . '/' . $data->file_pkp, $pdf4);

        $dirName = public_path() . '/uploads/file_dokumen/download/' . $data->nama_vendor;
        $zipFileName = $data->nama_vendor.'.zip';
        $zip = new ZipArchive;
        if ( $zip->open( $dirName . '/' . $zipFileName, ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }
            $zip->close();

            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
            return Response::download( $dirName . '/' . $zipFileName, $zipFileName, $headers );
        }
    }

    // mengubah data
    public function updateVendor($id, Request $request) {

        if ($request->logo_vendor !== "kosong") {
            $image = $request->logo_vendor; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/logo_vendor';

            if ($destinationPath."/".$request->logo_vendor_old != $destinationPath . "/uploads/logo_vendor/imgDefault.png") {
                if (file_exists($destinationPath."/".$request->logo_vendor_old)) {
                    unlink($destinationPath."/".$request->logo_vendor_old);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));

            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'logo_vendor' =>  $imageName,
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-akta-pendirian.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
              $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file1_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file1_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-akta-perubahan.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
              $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
              // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file2_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file2_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file3 = $request->file('file3');
            if(!empty($file3)){
              $imageNameFile3 = str_random(10).'-npwp.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile3, $file3);
              $upload = $file3->move(public_path()."/uploads/file_dokumen/",$imageNameFile3);
              // move_uploaded_file($_FILES['file3']['tmp_name'], "uploads/file_dokumen/".$imageNameFile3);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file3_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file3_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'file_npwp' => $imageNameFile3,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file4 = $request->file('file4');
            if(!empty($file4)){
              $imageNameFile4 = str_random(10).'-pkp.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile4, $file4);
              $upload = $file4->move(public_path()."/uploads/file_dokumen/",$imageNameFile4);
              // move_uploaded_file($_FILES['file4']['tmp_name'], "uploads/file_dokumen/".$imageNameFile4);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file4_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file4_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'file_pkp' => $imageNameFile4,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        } else {
            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-akta-pendirian.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
              $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file1_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file1_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-akta-perubahan.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
              $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
              // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file2_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file2_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file3 = $request->file('file3');
            if(!empty($file3)){
              $imageNameFile3 = str_random(10).'-npwp.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile3, $file3);
              $upload = $file3->move(public_path()."/uploads/file_dokumen/",$imageNameFile3);
              // move_uploaded_file($_FILES['file3']['tmp_name'], "uploads/file_dokumen/".$imageNameFile3);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file3_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file3_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'file_npwp' => $imageNameFile3,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file4 = $request->file('file4');
            if(!empty($file4)){
              $imageNameFile4 = str_random(10).'-pkp.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile4, $file4);
              $upload = $file4->move(public_path()."/uploads/file_dokumen/",$imageNameFile4);
              // move_uploaded_file($_FILES['file4']['tmp_name'], "uploads/file_dokumen/".$imageNameFile4);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file4_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file4_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'npwp_vendor' =>  $request->npwp_vendor,
                      'pkp_vendor' =>  $request->pkp_vendor,
                      'file_pkp' => $imageNameFile4,
                      'no_iujksiup' => $request->no_iujksiup,
                      'instansi_iujksiup' => $request->instansi_iujksiup,
                      'tgl_iujksiup' => $request->tgl_iujksiup,
                      'no_sbu' => $request->no_sbu,
                      'instansi_sbu' => $request->instansi_sbu,
                      'tgl_sbu' => $request->tgl_sbu,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        }

        DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        foreach (json_decode($request->pengurus) as $key) {   
            DB::table('tb_vendor_third')->insert(
                [
                    'id_vendor' => $id,
                    'jenis_pengurus' => $key->jenis_pengurus,
                    'nama_pengurus' => $key->nama_pengurus,
                    'no_ktp' => $key->no_ktp,
                    'foto_ktp' => $key->no_ktp.'-'.$key->nama_pengurus.'.'.'png',
                    'jabatan_pengurus' => $key->jabatan_pengurus,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        } 

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function updateFileIujkSbu(Request $request) {
        if($request->tipe == "IUJKSIUP"){
            $file = $request->file('file');
            if(!empty($file)){
              $imageNameFile = str_random(10).'-iujksiup.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file);
              $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_iujksiup')->where('id_vendor', $request->id_vendor)->where('no_iujksiup', $request->no_old)
              ->update(
                  [
                      'no_iujksiup' => $request->no,
                      'instansi_iujksiup' => $request->instansi,
                      'tgl_iujksiup' => $request->tgl,
                      'file_iujk' => $imageNameFile,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            DB::table('tb_vendor_klasifikasi')->where('id_vendor', $request->id_vendor)->where('id_klasifikasi', $request->no_old)
            ->delete();
            foreach (json_decode($request->klasifikasiIUJK) as $key) {   
                DB::table('tb_vendor_klasifikasi')->insert(
                    [
                        'id_vendor' => $request->id_vendor,
                        'id_klasifikasi' => $request->no_iujksiup,
                        'klasifikasi1' => $key->klasifikasi1,
                        'klasifikasi2' => $key->klasifikasi2,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );    
            }
        } else {
            $file = $request->file('file');
            if(!empty($file)){
              $imageNameFile = str_random(10).'-sbu.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file);
              $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
              // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_dokumen/".$request->file_old)) {
                  if (!unlink(public_path()."/uploads/file_dokumen/".$request->file_old)) {   
                  }  
                  else { 
                  }
              }

              DB::table('tb_vendor_sbu')->where('id_vendor', $request->id_vendor)->where('no_sbu', $request->no_old)
              ->update(
                  [
                      'no_sbu' => $request->no,
                      'instansi_sbu' => $request->instansi,
                      'tgl_sbu' => $request->tgl,
                      'file_sbu' => $imageNameFile,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            DB::table('tb_vendor_klasifikasi')->where('id_vendor', $request->id_vendor)->where('id_klasifikasi', $request->no_old)
            ->delete();
            foreach (json_decode($request->klasifikasiSBU) as $key) {   
                DB::table('tb_vendor_klasifikasi')->insert(
                    [
                        'id_vendor' => $request->id_vendor,
                        'id_klasifikasi' => $request->no_sbu,
                        'klasifikasi1' => $key->klasifikasi1,
                        'klasifikasi2' => $key->klasifikasi2,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );    
            }
        }

        return;
    }

    public function updateuploadKTP(Request $request) {
        

        $fileKTP = $request->file('fileKTP');
        if(!empty($fileKTP)){

            if (file_exists(public_path()."/uploads/photo_owner/".$request->foto_ktp)) {
                if (!unlink(public_path()."/uploads/photo_owner/".$request->foto_ktp)) {   
                }  
                else { 
                }
            }

          $imageNameFile = $request->no_ktp.'0_'.str_replace(' ', '_', $request->nama_pengurus).'.'.'png';
          // \File::put(public_path()."/uploads/photo_owner". '/' . $imageNameFile, $fileKTP);
          $upload = $fileKTP->move(public_path()."/uploads/photo_owner/",$imageNameFile);
          // move_uploaded_file($_FILES['fileKTP']['tmp_name'], "uploads/photo_owner/".$imageNameFile);
        }  

        return;
    }

    // menghapus data
    public function delete($id) {
        $data = DB::table('tb_vendor_primary as p')
                ->join('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
                ->where('p.id_vendor', $id)
                ->first();
        $data2 = DB::table('tb_vendor_primary as p')
            ->join('users_to_vendor as u', 'p.id_vendor', '=', 'u.id_vendor')
            ->join('users as s', 'u.id_user', '=', 's.id')
            ->join('persons as r', 's.id_person', '=', 'r.id_person')
            ->select('p.id_vendor', 's.id', 'r.id_person', 'r.photo_profile')
            ->where('p.id_vendor', $id)->first();
        $data3 = DB::table('tb_vendor_third as t')
                ->select('t.foto_ktp')
                ->where('t.id_vendor', $id)
                ->get();
        $dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->first();
        $iujksiup = DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->get();
        $sbu = DB::table('tb_vendor_sbu')->where('id_vendor', $id)->get();
        $cek_sekunder = DB::table('tb_vendor_secondary')->where('id_vendor', $id)->count();
        $cek_tersier = DB::table('tb_vendor_third')->where('id_vendor', $id)->count();
        $cek_dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->count();
        $cek_iujksiup = DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->count();
        $cek_sbu = DB::table('tb_vendor_sbu')->where('id_vendor', $id)->count();

        if (!empty($data)) {
            $destinationPath2 = public_path() . '/uploads/logo_vendor';
            $img2 = file($destinationPath2."/".$data->logo_vendor);
            if (file_exists($destinationPath2."/".$data->logo_vendor)) {
                $destinationPathOld2 = public_path() . '/uploads/logo_vendor/';
                unlink($destinationPathOld2.$data->logo_vendor);
            }
        }
        if (!empty($data2)) {
            $destinationPath1 = public_path() . '/uploads/photo_profile';
            $img1 = file($destinationPath1 . "/" . $data2->photo_profile);
            if (!empty($img1)) {
                $destinationPathOld1 = public_path() . '/uploads/photo_profile/';
                unlink($destinationPathOld1 . $data2->photo_profile);
            }
        }
        if (!empty($data3)) {
            foreach ($data3 as $key) {
                if($key->foto_ktp !== null || $key->foto_ktp !== "" || $key->foto_ktp !== 'AvatarDefault.png'){
                    if (file_exists(public_path()."/uploads/photo_owner/".$key->foto_ktp)) {
                        if (!unlink(public_path()."/uploads/photo_owner/".$key->foto_ktp)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }
        if (!empty($dokumen)) {
            if ($dokumen->file_pendirian !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_pendirian)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_pendirian)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_perubahan !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_perubahan)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_perubahan)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_npwp !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_npwp)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_npwp)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_pkp !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_pkp)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_pkp)) {   
                    }  
                    else { 
                    }
                }
            }
        }
        if (!empty($iujksiup)) {
            foreach ($iujksiup as $key) {
                if ($key->file_iujksiup !== null) {
                    if (file_exists(public_path()."/uploads/file_dokumen/".$key->file_iujksiup)) {
                        if (!unlink(public_path()."/uploads/file_dokumen/".$key->file_iujksiup)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }
        if (!empty($sbu)) {
            foreach ($sbu as $key) {
                if ($key->file_sbu !== null) {
                    if (file_exists(public_path()."/uploads/file_dokumen/".$key->file_sbu)) {
                        if (!unlink(public_path()."/uploads/file_dokumen/".$key->file_sbu)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }

        if ($data->status == 2) {
            DB::table('persons')->where('id_person', $data2->id_person)->delete();
            DB::table('tb_role_user_access')->where('UserID', $data2->id)->delete();
            DB::table('users')->where('id_person', $data2->id_person)->delete();
            DB::table('users_to_vendor')->where('id_user', $data2->id)->where('id_vendor', $data2->id_vendor)->delete();
        }

        if ($cek_sekunder == 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0 && $cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
        } else if ($cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        }

        if ($cek_dokumen == 1) {
            DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->delete();
        }

        if ($cek_iujksiup > 0) {
            DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->delete();
            foreach ($iujksiup as $key) {
                DB::table('tb_vendor_klasifikasi')->where('id_vendor', $id)->where('id_klasifikasi', $key->no_iujksiup)->delete();
            }
        }

        if ($cek_sbu > 0) {
            DB::table('tb_vendor_sbu')->where('id_vendor', $id)->delete();
            foreach ($sbu as $key) {
                DB::table('tb_vendor_klasifikasi')->where('id_vendor', $id)->where('id_klasifikasi', $key->no_sbu)->delete();
            }
        }

        $task = Person::count();
        try {
            // broadcast(new RefreshEvents());
            broadcast(new PersonEvents($task));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function deleteVendor($id) {
        $data = DB::table('tb_vendor_primary as p')
                ->join('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
                ->where('p.id_vendor', $id)
                ->first();
        $data2 = DB::table('tb_vendor_primary as p')
            ->join('users_to_vendor as u', 'p.id_vendor', '=', 'u.id_vendor')
            ->join('users as s', 'u.id_user', '=', 's.id')
            ->join('persons as r', 's.id_person', '=', 'r.id_person')
            ->select('p.id_vendor', 's.id', 'r.id_person', 'r.photo_profile')
            ->where('p.id_vendor', $id)->first();
        $data3 = DB::table('tb_vendor_third as t')
                ->select('t.foto_ktp')
                ->where('t.id_vendor', $id)
                ->get();
        $dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->first();
        $iujksiup = DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->get();
        $sbu = DB::table('tb_vendor_sbu')->where('id_vendor', $id)->get();
        $cek_sekunder = DB::table('tb_vendor_secondary')->where('id_vendor', $id)->count();
        $cek_tersier = DB::table('tb_vendor_third')->where('id_vendor', $id)->count();
        $cek_dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->count();
        $cek_iujksiup = DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->count();
        $cek_sbu = DB::table('tb_vendor_sbu')->where('id_vendor', $id)->count();

        if (!empty($data)) {
            $destinationPath2 = public_path() . '/uploads/logo_vendor';
            $img2 = file($destinationPath2."/".$data->logo_vendor);
            if (file_exists($destinationPath2."/".$data->logo_vendor)) {
                $destinationPathOld2 = public_path() . '/uploads/logo_vendor/';
                unlink($destinationPathOld2.$data->logo_vendor);
            }
        }
        if (!empty($data2)) {
            $destinationPath1 = public_path() . '/uploads/photo_profile';
            $img1 = file($destinationPath1 . "/" . $data2->photo_profile);
            if (!empty($img1)) {
                $destinationPathOld1 = public_path() . '/uploads/photo_profile/';
                unlink($destinationPathOld1 . $data2->photo_profile);
            }
        }
        if (!empty($data3)) {
            foreach ($data3 as $key) {
                if($key->foto_ktp !== null || $key->foto_ktp !== "" || $key->foto_ktp !== 'AvatarDefault.png'){
                    if (file_exists(public_path()."/uploads/photo_owner/".$key->foto_ktp)) {
                        if (!unlink(public_path()."/uploads/photo_owner/".$key->foto_ktp)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }
        if (!empty($dokumen)) {
            if ($dokumen->file_pendirian !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_pendirian)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_pendirian)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_perubahan !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_perubahan)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_perubahan)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_npwp !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_npwp)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_npwp)) {   
                    }  
                    else { 
                    }
                }
            }
            if ($dokumen->file_pkp !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_pkp)) {
                    if (!unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_pkp)) {   
                    }  
                    else { 
                    }
                }
            }
        }
        if (!empty($iujksiup)) {
            foreach ($iujksiup as $key) {
                if ($key->file_iujksiup !== null) {
                    if (file_exists(public_path()."/uploads/file_dokumen/".$key->file_iujksiup)) {
                        if (!unlink(public_path()."/uploads/file_dokumen/".$key->file_iujksiup)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }
        if (!empty($sbu)) {
            foreach ($sbu as $key) {
                if ($key->file_sbu !== null) {
                    if (file_exists(public_path()."/uploads/file_dokumen/".$key->file_sbu)) {
                        if (!unlink(public_path()."/uploads/file_dokumen/".$key->file_sbu)) {   
                        }  
                        else { 
                        }
                    }
                }
            }
        }

        if ($data->status == 2) {
            DB::table('persons')->where('id_person', $data2->id_person)->delete();
            DB::table('tb_role_user_access')->where('UserID', $data2->id)->delete();
            DB::table('users')->where('id_person', $data2->id_person)->delete();
            DB::table('users_to_vendor')->where('id_user', $data2->id)->where('id_vendor', $data2->id_vendor)->delete();
        }

        if ($cek_sekunder == 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0 && $cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
        } else if ($cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        }

        if ($cek_dokumen == 1) {
            DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->delete();
        }

        if ($cek_iujksiup > 0) {
            DB::table('tb_vendor_iujksiup')->where('id_vendor', $id)->delete();
            foreach ($iujksiup as $key) {
                DB::table('tb_vendor_klasifikasi')->where('id_vendor', $id)->where('id_klasifikasi', $key->no_iujksiup)->delete();
            }
        }

        if ($cek_sbu > 0) {
            DB::table('tb_vendor_sbu')->where('id_vendor', $id)->delete();
            foreach ($sbu as $key) {
                DB::table('tb_vendor_klasifikasi')->where('id_vendor', $id)->where('id_klasifikasi', $key->no_sbu)->delete();
            }
        }

        $task = Person::count();
        try {
            // broadcast(new RefreshEvents());
            broadcast(new PersonEvents($task));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        

        return 204;
    }

    public function deleteFileIujkSbu(Request $request) {
        if($request->tipe == "IUJKSIUP"){
            if (file_exists(public_path()."/uploads/file_dokumen/".$request->file_old)) {
                if (!unlink(public_path()."/uploads/file_dokumen/".$request->file_old)) {   
                }  
                else { 
                }
            }
            DB::table('tb_vendor_iujksiup')->where('id_vendor', $request->id_vendor)->where('no_iujksiup', $request->no_old)
            ->delete();
            DB::table('tb_vendor_klasifikasi')->where('id_vendor', $request->id_vendor)->where('id_klasifikasi', $request->no_old)
            ->delete();
        } else {
            if (file_exists(public_path()."/uploads/file_dokumen/".$request->file_old)) {
                if (!unlink(public_path()."/uploads/file_dokumen/".$request->file_old)) {   
                }  
                else { 
                }
            }

            DB::table('tb_vendor_sbu')->where('id_vendor', $request->id_vendor)->where('no_sbu', $request->no_old)
            ->delete();
            DB::table('tb_vendor_klasifikasi')->where('id_vendor', $request->id_vendor)->where('id_klasifikasi', $request->no_old)
            ->delete();
        }

        return;
    }

    public function addRating(Request $request) {
        
        $rating = $request->rating;
        $id_vendor = $request->id_vendor; 

        $getRat =  DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->select('rating','nama_vendor')
            ->first();

        if($getRat->rating == $rating){
            if($getRat->rating !== 0){
                $rating = $getRat->rating - 1;
            }
        } 

        DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->update(
                [
                    'rating' =>  $rating,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memberikan rating ".$rating." pada ".$getRat->nama_vendor,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function getPenilaianVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_vendor');
        $type = $request->input('rating', 'desc');
        $vendors = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_jenis_vendor as j', 'p.jenis_vendor', '=', 'j.id_jenis_vendor')
            ->select('p.*','j.jenis_vendor as jenis_vendor_new')
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            if (filter_var($vendors['data'][$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $vendors['data'][$key]->logo_vendor = str_replace("open", "uc", $vendors['data'][$key]->logo_vendor);
            } else {
                $destinationPath = '/uploads/logo_vendor';
                $path = $destinationPath."/".$vendors['data'][$key]->logo_vendor;
                // $src = str_replace(public_path(), url('/'), $path);
                $vendors['data'][$key]->logo_vendor = $path;
            }

            $dataPekerjaan = DB::table('tb_pekerjaan as p')
                        ->where('p.id_vendor', 'LIKE', '%' . $vendors['data'][$key]->id_vendor . '%')
                        ->count();
            $vendors['data'][$key]->pekerjaan = $dataPekerjaan;
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function getPenilaianIkutSerta($id_vendor, Request $request) {

        $dataPekerjaan = DB::table('tb_pekerjaan as p')
                    ->leftjoin('tb_pekerjaan_penawaran as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_vendor', 'LIKE', '%'.$id_vendor.'%')
                    ->select('p.id_pekerjaan','p.nama_pekerjaan','v.penawaran','v.jaminan_penawaran','v.status_penawaran','v.catatan_penawaran','v.validasi_penawaran')
                    ->groupBy('p.id_pekerjaan')
                    ->get();

        $dataFix = array();
        foreach ($dataPekerjaan as $key) {

            array_push($dataFix, array(
                'id_pekerjaan' => $key->id_pekerjaan,
                'nama_pekerjaan' => $key->nama_pekerjaan,
                'penawaran' => $key->penawaran,
                'jaminan_penawaran' => $key->jaminan_penawaran,
                'status_penawaran' => $key->status_penawaran,
                'catatan_penawaran' => $key->catatan_penawaran,
                'validasi_penawaran' => $key->validasi_penawaran,
            ));
        }

        return $dataFix;
    }

    public function getVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_jenis_vendor as j', 'p.jenis_vendor', '=', 'j.id_jenis_vendor')
            ->select('p.*','j.jenis_vendor as jenis_vendor_new')
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            if (filter_var($vendors['data'][$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $vendors['data'][$key]->logo_vendor = str_replace("open", "uc", $vendors['data'][$key]->logo_vendor);
            } else {
                $destinationPath = '/uploads/logo_vendor';
                $path = $destinationPath."/".$vendors['data'][$key]->logo_vendor;
                // $src = str_replace(public_path(), url('/'), $path);

                $vendors['data'][$key]->logo_vendor = $path;
            }
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function getReqVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_jenis_vendor as j', 'p.jenis_vendor', '=', 'j.id_jenis_vendor')
            ->select('p.*','j.jenis_vendor as jenis_vendor_new')
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->where('status', 1)
            ->orwhere('status', 3)
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            if (filter_var($vendors['data'][$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $vendors['data'][$key]->logo_vendor = str_replace("open", "uc", $vendors['data'][$key]->logo_vendor);
            } else {
                $destinationPath = '/uploads/logo_vendor';
                $path = $destinationPath."/".$vendors['data'][$key]->logo_vendor;
                // $src = str_replace(public_path(), url('/'), $path);

                $vendors['data'][$key]->logo_vendor = $path;
            }
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function pdfVendor($id_vendor) {
        $pdf = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')
                ->leftjoin('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
                ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
                ->where('p.id_vendor', '=', $id_vendor)
                ->first();
        view()->share('pdf', $pdf);

        $nama_pdf = $pdf->id_vendor . "-" . $pdf->nama_vendor . ".pdf";
        if ($id_vendor) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfVendor'); 
            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }

    public function approveVendor(Request $request) {

        $id_vendor = $request->id_vendor;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $username = $request->username;
        $password = $request->password;
        $catatan = $request->catatan;

        DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->update(
                [
                    'status' => 2,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $firstLast = explode(" ", $request->nama_narahubung);
        if (count($firstLast) < 2) {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => "",
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        } else {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => $firstLast[1],
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        }

        $destinationPath = public_path() . '/uploads/photo_profile';
        $img = file($destinationPath . "/avatarDefault.png");
        \File::put($destinationPath . '/' . $username . ".png", $img);

        $id = DB::getPdo()->lastInsertId();

        DB::table('users')->insert(
            [
                'id_person' => $id,
                'username' => $username,
                'password' => bcrypt($password),
                'email' => $email_narahubung,
                'role' => 'R002',
                'status' => 1,
            ]
        );
        $data = DB::table('users')
            ->where('id_person', $id)
            ->first();
        $dataMenu = DB::table('tb_role_master')
            ->where('kd_role', 'R002')
            ->get();
        foreach ($dataMenu as $key) {
            DB::table('tb_role_user_access')->insert(
                [
                    'kd_role' => $key->kd_role,
                    'UserID' => $data->id,
                    'MenuID' => $key->MenuID,
                    'DetailID' => $key->DetailID,
                    'DetailID2' => $key->DetailID2,
                    'Link' => $key->Link,
                    'Action' => $key->Action,
                ]
            );
        }

        DB::table('users_to_vendor')->insert(
            [
                'id_user' => $data->id,
                'id_vendor' => $id_vendor,
            ]
        );

        $task = Person::count();
        try {
            // broadcast(new RefreshEvents());
            broadcast(new PersonEvents($task));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        

        $get = DB::table('tb_vendor_primary')->where('id_vendor', $id_vendor)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'Approve')->first();
        array_push($content, $objDemo->content, $username, $password, $email_narahubung, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_vendor);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new ApproveVendor($content));
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username2,
                'fullname' => $request->fullname2,
                'ip' => request()->ip(),
                'log' => "Approve Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    public function notapproveVendor(Request $request) {

        $id_vendor = $request->id_vendor;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $catatan = $request->catatan;

        DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->update(
                [
                    'status' => 3,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $get = DB::table('tb_vendor_primary')->where('id_vendor', $id_vendor)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'NotApprove')->first();
        array_push($content, $objDemo->content, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_vendor);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new NotApproveVendor($content));
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Not Approve Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    public function getListVendor($limit) {
        $test = array();
        if ($limit == 0) {
            array_push($test, array(
                "id_vendor" => "",
                "nama_vendor" => "",
                "deskripsi_vendor" => "",
                "nama_narahubung" => "",
                "email_narahubung" => "",
                "tlp_narahubung" => "",
                "jenis_vendor" => "",
                "email_vendor" => "",
                'tlp_vendor' =>  "",
                'fax_vendor' =>  "",
                "logo_vendor" => "",
                "logo_vendorOld" => "",
                'alamat_vendor' =>  "",
                'provinsi_vendor' =>  "",
                'kota_vendor' =>  "",
                'kodepos_vendor' =>  "",
                "web_vendor" => "",
                "facebook_vendor" => "",
                "instagram_vendor" => "",
                "created_at" => "",
                "updated_at" => ""
            ));
        }
        $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')->where('p.status', 2)->offset($limit)->limit(6)->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_vendor = str_replace("open", "uc", $data[$key]->logo_vendor);
            } else {
                $path = $data[$key]->logo_vendor;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->logo_vendor = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }
    // public function getListVendorByJenis($id, $category, $cluster, $scope) {
    //     if ($category !== "kosong" && $cluster == "kosong" && $scope == "kosong") {
    //         $data = Vendor::where('status', 2)->where('jenis_vendor', $category)->get();
    //     } else if ($category == "kosong" && $cluster !== "kosong" && $scope == "kosong") {
    //         $data = Vendor::where('status', 2)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
    //     } else if ($category == "kosong" && $cluster == "kosong" && $scope !== "kosong") {
    //         $data = Vendor::where('status', 2)->where('lingkup_vendor', 'like', '%' . $scope . '%')->get();
    //     } else if ($category !== "kosong" && $cluster !== "kosong" && $scope == "kosong") {
    //         $data = Vendor::where('status', 2)->where('jenis_vendor', $category)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
    //     } else if ($category !== "kosong" && $cluster == "kosong" && $scope !== "kosong") {
    //         $data = Vendor::where('status', 2)->where('jenis_vendor', $category)->where('lingkup_vendor', 'like', '%' . $scope . '%')->get();
    //     } else if ($category == "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
    //         $data = Vendor::where('status', 2)->where('klaster_kerja', 'like', '%' . $cluster . '%')->where('lingkup_vendor', 'like', '%' . $scope . '%')->get();
    //     } else if ($category !== "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
    //         $data = Vendor::where('status', 2)->where('jenis_vendor', $category)->where('klaster_kerja', 'like', '%' . $cluster . '%')->where('lingkup_vendor', 'like', '%' . $scope . '%')->get();
    //     } else if ($category == "kosong" || $cluster == "kosong" || $scope == "kosong") {
    //         $data = Vendor::where('status', 2)->get();
    //     }

    //     $test = array();
    //     array_push($test, array("id_vendor" => "",
    //         "nama_vendor" => "",
    //         "nama_narahubung" => "",
    //         "email_narahubung" => "",
    //         "jenis_vendor" => "",
    //         "email_vendor" => "",
    //         "logo_vendor" => "",
    //         "klaster_kerja" => "",
    //         "lingkup_vendor" => "",
    //         "provinsi_kerja" => "",
    //         "kota_kerja" => "",
    //         "status" => "",
    //         "catatan" => "",
    //         "created_at" => "",
    //         "updated_at" => ""));

    //     foreach ($data as $key => $value) {
    //         if (filter_var($data[$key]->logo_vendor, FILTER_VALIDATE_URL)) {
    //             $data[$key]->logo_vendor = str_replace("open", "uc", $data[$key]->logo_vendor);
    //         } else {
    //             $path = $data[$key]->logo_vendor;
    //             $src = str_replace(public_path(), url('/'), $path);

    //             // $dataImg = file_get_contents($path);
    //             // $type = pathinfo($path, PATHINFO_EXTENSION);
    //             // $base64 = base64_encode($dataImg);
    //             // $src = 'data:image/' . $type . ';base64,' . $base64;

    //             $data[$key]->logo_vendor = $src;
    //         }
    //     }

    //     foreach ($data as $key) {
    //         array_push($test, $key);
    //     }
    //     return $test;
    // }

    public function getListVendorBySearch($search) {
        if ($search !== "kosong" || $search !== "") {
            $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')->where('p.status', 2)->where('p.nama_vendor', 'like', '%' . $search . '%')->get();
        } else {
            $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')->where('p.status', 2)->get();
        }

        $test = array();
        array_push($test, array(
            "id_vendor" => "",
            "nama_vendor" => "",
            "deskripsi_vendor" => "",
            "nama_narahubung" => "",
            "email_narahubung" => "",
            "tlp_narahubung" => "",
            "jenis_vendor" => "",
            "email_vendor" => "",
            'tlp_vendor' =>  "",
            'fax_vendor' =>  "",
            "logo_vendor" => "",
            "logo_vendorOld" => "",
            'alamat_vendor' =>  "",
            'provinsi_vendor' =>  "",
            'kota_vendor' =>  "",
            'kodepos_vendor' =>  "",
            "web_vendor" => "",
            "facebook_vendor" => "",
            "instagram_vendor" => "",
            "created_at" => "",
            "updated_at" => ""
        ));
        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_vendor = str_replace("open", "uc", $data[$key]->logo_vendor);
            } else {
                $path = $data[$key]->logo_vendor;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->logo_vendor = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    // public function sendMaillAllVendor(Request $request) {
    //     $content = $request->content;

    //     $data = DB::table('users as u')
    //         ->leftjoin('users_to_vendor as s', 'u.id', '=', 's.id_user')
    //         ->select('u.*', 's.id_vendor')
    //         ->where('status', 1)
    //         ->get();

    //     $sendEmail = array();
    //     foreach ($data as $key) {
    //         array_push($sendEmail, array($key->email, $key->username, $content));
    //     }

    //     foreach ($sendEmail as $key) {
    //         Mail::to($key[0])->send(new BulkMail($key));
    //     }
    //     return;
    // }

    // public function sendMaillAllVendor2(Request $request) {
    //     $content = $request->content;

    //     $data = DB::table('users_to_vendor as s')
    //         ->leftjoin('users as u', 'u.id', '=', 's.id_user')
    //         ->select('u.*', 's.id_vendor')
    //         ->where('u.status', 1)
    //         ->get();

    //     $sendEmail = array();
    //     foreach ($data as $key) {
    //         $str = $key->email;
    //         if (!isset($str) || trim($str) === '' || trim($str) === '-') {
    //             continue;
    //         }
    //         array_push($sendEmail, array($key->email, $key->username, $content, $key->id_vendor));
    //     }

    //     $failEmail = array();
    //     $successEmail = array();
    //     $send = date("Y-m-d H:i:s");
    //     foreach ($sendEmail as $key) {
    //         try {
    //             Mail::to($key[0])->send(new BulkMail($key));
    //             array_push($successEmail, array($key[0], "success"));
    //             DB::table('tb_mail_history')->insert(
    //                 [
    //                     'id_vendor' => $key[3],
    //                     'body_mail' => $content,
    //                     'send' => $send,
    //                     'status' => "1",
    //                     'created_at' => date("Y-m-d H:i:s"),
    //                 ]
    //             );
    //         } catch (Exception $e) {
    //             if (count(Mail::failures()) > 0) {
    //                 array_push($failEmail, array($key[0], "fail"));
    //                 DB::table('tb_mail_history')->insert(
    //                     [
    //                         'id_vendor' => $key[3],
    //                         'body_mail' => $content,
    //                         'send' => $send,
    //                         'status' => "2",
    //                         'created_at' => date("Y-m-d H:i:s"),
    //                     ]
    //                 );
    //             }
    //         }
    //     }

    //     $failEmail2 = array();
    //     foreach ($failEmail as $key) {
    //         array_push($failEmail2, $key[0]);
    //     }

    //     return response()->json(['fail' => $failEmail, 'success' => $successEmail, 'emailfail' => implode(", ", $failEmail2)]);
    // }

    public function sendMaillAllVendorFix($email, $id, Request $request) {
        $content = $request->content;
        $subject = $request->subject;

        $data = DB::table('users_to_vendor as s')
            ->leftjoin('users as u', 'u.id', '=', 's.id_user')
            ->leftjoin('tb_vendor_primary as p', 's.id_vendor', '=', 'p.id_vendor')
            ->select('u.*', 's.id_vendor', 'p.email_vendor')
            ->where('s.id_vendor', $id)
            ->get();

        $sendEmail = array();
        foreach ($data as $key) {
            $str = $key->email;
            if (!isset($str) || trim($str) === '' || trim($str) === '-') {
                continue;
            }
            array_push($sendEmail, array($key->email_vendor, $key->username, $content, $key->id_vendor, $subject));
        }

        $failEmail = array();
        $successEmail = array();
        $send = date("Y-m-d H:i:s");
        foreach ($sendEmail as $key) {
            try {
                Mail::to($key[0])->send(new BulkMail($key));
                array_push($successEmail, array($key[0], "success"));
                DB::table('tb_vendor_mail_history')->insert(
                    [
                        'id_vendor' => $key[3],
                        'subject' => $subject,
                        'body_mail' => $content,
                        'send' => $send,
                        'status' => "1",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            } catch (Exception $e) {
                if (count(Mail::failures()) > 0) {
                    array_push($failEmail, array($key[0], "fail"));
                    DB::table('tb_vendor_mail_history')->insert(
                        [
                            'id_vendor' => $key[3],
                            'subject' => $subject,
                            'body_mail' => $content,
                            'send' => $send,
                            'status' => "2",
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Mengirim Email pada Semua Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return $email;
    }

    public function sendMailltoVendor(Request $request) {
        $subject = $request->subject;
        $content = $request->content;
        $email = $request->email;
        $namavendor = $request->namavendor;

        $data1 = DB::table('tb_vendor_primary')->where('nama_vendor', $namavendor)->where('email_vendor', $email)->first();
        $data2 = DB::table('users_to_vendor')->where('id_vendor', $data1->id_vendor)->first();
        $data3 = $data = DB::table('users')->where('id', $data2->id_user)->where('status', 1)->first();

        $sendEmail = array();
        array_push($sendEmail, array($content, $data3->username, $subject));

        Mail::to($email)->send(new VendorMail($sendEmail));

        $send = date("Y-m-d H:i:s");
        if (count(Mail::failures()) > 0) {
            DB::table('tb_vendor_mail_history')->insert(
                [
                    'id_vendor' => $data1->id_vendor,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "2",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_vendor_mail_history')->insert(
                [
                    'id_vendor' => $data1->id_vendor,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "1",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Mengirim Email pada Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function getHistoryVendorMail(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_vendor_mail_history as m')
            ->leftjoin('tb_vendor_primary as p', 'm.id_vendor', '=', 'p.id_vendor')
            ->select('m.*', 'p.nama_vendor')
            ->where('m.body_mail', 'LIKE', '%' . $search_query . '%')
            ->groupBy('m.body_mail', 'm.subject')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }

    public function delHistoryVendorMail($id, Request $request) {

        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        DB::table('tb_vendor_mail_history')->where('subject', $data->subject)->where('body_mail', $data->body_mail)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus History Email",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return 204;
    }

    public function getHistoryVendorMailID($id) {
        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        return json_encode($data);
    }

    public function getHistoryVendorMailView($id, Request $request) {
        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_vendor_mail_history as m')
            ->leftjoin('tb_vendor_primary as p', 'm.id_vendor', '=', 'p.id_vendor')
            ->select('m.*', 'p.nama_vendor')
            ->where('m.subject', $data->subject)
            ->where('m.body_mail', $data->body_mail)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }


    public function getLogActivity(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'username');
        $type = $request->input('created_at', 'desc');
        $logs = DB::table('tb_log_activity as p')
            ->select('p.*')
            ->where('p.fullname', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $logs['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $logs['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'logs' => $logs,
        ]);
    }

}
