<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {
    public function get_profil($id) {

        $data = DB::table('users as u')
            ->LeftJoin('persons as ps', 'u.id_person', '=', 'ps.id_person')
            ->LeftJoin('tb_role as r', 'u.role', '=', 'r.kd_role')
            ->select('u.id', 'u.email', 'u.username',
                'u.role',
                'r.role as nama_role',
                'u.password',
                'ps.photo_profile',
                'ps.first_name',
                'ps.last_name',
                'ps.gender',
                'ps.birth_place',
                'ps.birth_date',
                'ps.address'
            )
            ->where('u.id', $id)
            ->get();
        $profil = [];
        foreach ($data as $key) {
            array_push($profil, [
                'id' => $key->id,
                'first_name' => $key->first_name,
                'last_name' => $key->last_name,
                'email' => $key->email,
                'username' => $key->username,
                'photo_profile' => $key->photo_profile,
                'gender' => $key->gender,
                'birth_place' => $key->birth_place,
                'birth_date' => $key->birth_date,
                'address' => $key->address,
                'role' => $key->role,
                'nama_role' => $key->nama_role,
            ]);
        }
        return json_encode($profil[0]);
    }

    public function updateProfile($id, Request $request) {
        $id = $request->id;
        $user_name = $request->user_name;
        $email = $request->email;
        $role_code = $request->role_code;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $birth_date = $request->birth_date;
        $birth_place = $request->birth_place;

        $cekVendor = DB::table('users_to_vendor')->where('id_user', $id)->first();

        $cekUser = DB::table('users')->where('id', $id)->first();

        $cekPerson = DB::table('persons')->where('id_person', $cekUser->id_person)->first();

        if (!empty($cekUser)) {
            DB::table('users')->where('id', $id)->update(
                [
                    'email' => $email,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        if (!empty($cekPerson)) {
            if ($request->photo_profile !== "kosong") {
                $image = $request->photo_profile; // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = str_random(10) . '.' . 'png';
                $destinationPath = public_path() . '/uploads/photo_profile';
                $destinationPathOld = public_path() . '/uploads/photo_profile/';
                if ($request->photo_profile_old != 'avatarDefault.png') {
                    if (file_exists($destinationPathOld . $request->photo_profile_old)) {
                        unlink($destinationPathOld . $request->photo_profile_old);
                    }
                }

                \File::put($destinationPath . '/' . $imageName, base64_decode($image));

                DB::table('persons')->where('id_person', $cekUser->id_person)->update(
                    [
                        'photo_profile' => $imageName,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'birth_date' => $birth_date,
                        'birth_place' => $birth_place,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            } else {
                DB::table('persons')->where('id_person', $cekUser->id_person)->update(
                    [
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'birth_date' => $birth_date,
                        'birth_place' => $birth_place,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        if (!empty($cekVendor)) {
            DB::table('tb_vendor_primary')->where('id_vendor', $cekVendor->id_vendor)->update(
                [
                    'nama_narahubung' => $first_name . " " . $last_name,
                    'email_narahubung' => $email,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Profile", 
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function updatePass(Request $request) {
        $oldPass = $request->oldPass;
        $newPass = bcrypt($request->newPass);
        $id = $request->id;
        $cek = DB::table('users')->where('id', $id)->first();

        // foreach ($cek as $key) {
        //     if (!(Hash::check($oldPass, $key->password))) {
        //         print_r('gagal');
        //         return response()->json(['status' => 'gagal']);
        //     } else {
        //         print_r('oke');
        //         $updates = ['password' => $newPass];
        //         $update = DB::table('users')
        //             ->where('id', $id)
        //             ->update($updates);
        //         return response()->json(['status' => 'sukses']);
        //     }
        // }

        if (!(Hash::check($oldPass, $cek->password))) {
            return response()->json(['status' => 'gagal']);
        } else {
            $updates = ['password' => $newPass];
            $update = DB::table('users')
                ->where('id', $id)
                ->update($updates);
            return response()->json(['status' => 'sukses']);
        }
    }

    public function updateVendor(Request $request) {
        $id_vendor = $request->id_vendor;
        $klaster_kerja = implode(";", $request->klaster_kerja);
        $lingkup_komunitas = implode(";", $request->lingkup_komunitas);

        if ($request->logo_vendor !== "kosong") {
            $image = $request->logo_vendor; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/logo_vendor';
            $destinationPathOld = public_path() . '/uploads/logo_vendor';

            if (filter_var($request->logo_vendorOld, FILTER_VALIDATE_URL)) {

            } else {
                unlink($request->logo_vendorOld);
            }

            $newPath = $destinationPath . '/' . $imageName;
            \File::put($newPath, base64_decode($image));

            $update = DB::table('tb_vendor_primary')
                ->where('id_vendor', $id_vendor)
                ->update(
                    [
                        'nama_komunitas' => $request->nama_komunitas,
                        'deskripsi_komunitas' => $request->deskripsi_komunitas,
                        'nama_narahubung' => $request->nama_narahubung,
                        'email_narahubung' => $request->email_narahubung,
                        'tlp_narahubung' => $request->tlp_narahubung,
                        'jenis_komunitas' => $request->jenis_komunitas,
                        'email_komunitas' => $request->email_komunitas,
                        'logo_vendor' => $destinationPath . "/" . $imageName,
                        'klaster_kerja' => $klaster_kerja,
                        'lingkup_komunitas' => $lingkup_komunitas,
                        'provinsi_kerja' => $request->provinsi_kerja,
                        'kota_kerja' => $request->kota_kerja,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

        } else {
            $update = DB::table('tb_vendor_primary')
                ->where('id_vendor', $id_vendor)
                ->update(
                    [
                        'nama_komunitas' => $request->nama_komunitas,
                        'deskripsi_komunitas' => $request->deskripsi_komunitas,
                        'nama_narahubung' => $request->nama_narahubung,
                        'email_narahubung' => $request->email_narahubung,
                        'tlp_narahubung' => $request->tlp_narahubung,
                        'jenis_komunitas' => $request->jenis_komunitas,
                        'email_komunitas' => $request->email_komunitas,
                        'klaster_kerja' => $klaster_kerja,
                        'lingkup_komunitas' => $lingkup_komunitas,
                        'provinsi_kerja' => $request->provinsi_kerja,
                        'kota_kerja' => $request->kota_kerja,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

        }

        if ($update) {
            $update_2 = DB::table('tb_vendor_secondary')->where('id_vendor', $id_vendor)
                ->update(
                    [
                        'tgl_berdiri_kop' => $request->tgl_komunitas,
                        'web_komunitas' => $request->web_komunitas,
                        'twitter_komunitas' => $request->twitter_komunitas,
                        'facebook_komunitas' => $request->facebook_komunitas,
                        'instagram_komunitas' => $request->instagram_komunitas,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        }
        if ($update_2) {
            return \response()->json(['status' => true, 'message' => 'success']);
        } else {
            return \response()->json(['status' => false, 'message' => 'gagal']);
        }

    }


}
