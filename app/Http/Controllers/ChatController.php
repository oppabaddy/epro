<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Auth;
use DB;
use App\Events\ChatMessage;
use App\Events\NotifEvents;
use App\Events\ClearNotifEvents;
use Illuminate\Support\Facades\Session;


class ChatController extends Controller

{

   /**

    * Create a new controller instance.

    *

    * @return void

    */

   public function __construct()

   {

       // $this->middleware('auth');

   }

   public function getMessageAddPl($id_pekerjaan, Request $request){
      $data = DB::table('tb_message as m')
          ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
          ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
          ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
          ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
          ->where('p.id_pekerjaan', $id_pekerjaan)
          ->first();

      $data2 = DB::table('users as u')
          ->where('u.role', 'R000')
          ->first();

      // $id_sender_receiver = explode(',',$data->id_user_committe);
      $id_sender_receiver = array_map('intval', explode(',',$data->id_user_committe));
      array_push($id_sender_receiver, $data->id_user_vendor, $data2->id);

      $dataFix = array();
      array_push($dataFix, array(
          'id_pekerjaan' => $id_pekerjaan,
          'id_message' => $data->id_message,
          'id_sender_receiver' => $id_sender_receiver,
          'nama_pekerjaan' => $data->nama_pekerjaan,
      ));

      return json_encode($dataFix[0]);
   }


   public function sendMessage(Request $request) {
       $user = [
           "id_sender" => $request->id_sender,
           "id_receiver" => $request->id_receiver,
       ];

       $message = [
           "id_sender" => $request->id_sender,
           "id_receiver" => $request->id_receiver,
           "message" => $request->message
       ];

       
       $data = DB::table('tb_message')
              ->where('id_pekerjaan', $request->id_pekerjaan)
              ->first();

       $create = date("Y-m-d H:i:s");
       DB::table('tb_message_detail')->insert(
           [  
               'id_message' => $data->id_message,
               'id_sender' => $request->id_sender,
               'id_receiver' => $request->id_receiver,
               'message' => $request->message,
               'created_at' => $create,
           ]
       );

       
       //For Notif
       $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();

       $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();

       $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
       array_push($id_sender_receiver, $dataZ->id_user_vendor, $dataZ2->id);
       foreach ($id_sender_receiver as $key) {
        if($request->id_sender == $key){
          
        } else {
          DB::table('tb_notifikasi')->insert(
              [  
                  'jenis' => $request->jenis,
                  'id_pekerjaan' => $request->id_pekerjaan,
                  'id_sender' => $request->id_sender,
                  'id_receiver' => $key,  
                  'isi' => $request->message,
                  'status' => 1,
                  'created_at' => $create,
              ]
          );

          $data2 = DB::table('tb_notifikasi')
                 ->where('jenis', $request->jenis)
                 ->where('id_pekerjaan', $request->id_pekerjaan)
                 ->where('id_sender', $request->id_sender)
                 ->where('id_receiver', $key)
                 ->where('isi', $request->message)
                 ->first();

          $message2 = [
              "id_notif" => $data2->id_notif,
              "id_pekerjaan" => $data2->id_pekerjaan,
              "id_sender" => $data2->id_sender,
              "id_receiver" => $data2->id_receiver,
              "jenis" => $data2->jenis,
              "isi" => $data2->isi,
              "status" => $data2->status,
              "created_at" => $data2->created_at,
              "updated_at" => $data2->updated_at,
          ];
        }
       }
       
       broadcast(new ChatMessage($user, $message));
       broadcast(new NotifEvents($user, $message2));
       return ['status' => 'Message Sent!'];

   }

   public function fetchMessages($id_message)
   {
      $data2 = DB::table('tb_message as m')
            ->leftjoin('tb_message_detail as d', 'm.id_message', '=', 'd.id_message')
            ->leftjoin('users as u', 'u.id', '=', 'd.id_sender')
            ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
            ->select('d.*','p.photo_profile as photo_profile', 'u.username as username')
            ->where('m.id_message', $id_message)
            ->get();

      $dataFix = array();
      foreach ($data2 as $key) {
        array_push($dataFix, array(
            "id_message" => $key->id_message,
            "id_sender" => $key->id_sender,
            "username" => $key->username,
            "photo_profile" => $key->photo_profile,
            "message" => $key->message,
            "created_at" => $key->created_at
          ));
      }

      return $dataFix;
   }

   public function fetchNotif($id_receiver)
   {
     return DB::table('tb_notifikasi as n')
            ->leftjoin('users as u', 'u.id', '=', 'n.id_sender')
            ->select('n.*','u.username')
            ->where('n.id_receiver', 'LIKE' ,'%'.$id_receiver.'%')
            ->where('n.status', 1)
            // ->offset(0)
            // ->limit(5)
            ->get();
   }
   public function openNotif($id_notif)
   {
       DB::table('tb_notifikasi as n')
      ->where('n.id_notif', $id_notif)
      // ->where('n.id_receiver', $id_receiver)
      ->update(
            [
                'status' => 2,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

      return;
   }

   public function openNotifChat($id_receiver,$id_pekerjaan)
   {
       DB::table('tb_notifikasi as n')
      ->where('n.id_receiver', $id_receiver)
      ->where('n.id_pekerjaan', $id_pekerjaan)
      ->update(
            [
                'status' => 2,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
      broadcast(new ClearNotifEvents());
      return;
   }

   public function openNotifReqVendor()
   {
       DB::table('tb_notifikasi as n')
      ->where('n.jenis', 'Register')
      ->update(
            [
                'status' => 2,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
      broadcast(new ClearNotifEvents());
      return;
   }

}