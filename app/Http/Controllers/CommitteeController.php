<?php
namespace App\Http\Controllers;

use App\Events\PersonEvents;
use App\Exports\IuranExport;
use App\Exports\VendorExport;
// import file model Vendor
use App\Vendor;
use App\Mail\ApproveVendor;
use App\Mail\BulkMail;
use App\Mail\VendorMail;
use App\Mail\NotApproveVendor;
use App\Mail\ThanksRegister;
use App\Person;
use DateTime;
use DB;
use Excel;
use PDF;
use ZipArchive;
use File;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;

use App\Events\VendorEvents;

class CommitteeController extends Controller {
    
    public function dropdownuserCommittee() {
        $data = DB::table('users as u')
                ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
                ->select('u.id','u.id_person','u.username','p.first_name','p.last_name')
                ->where('u.role', 'R003')
                ->where('u.status', 1)->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id,
                'text' => $key->username . " - " . $key->first_name . " " . $key->last_name,
            );
        }
        return $response;
    }
    public function store(Request $request) {

        $data = DB::table('tb_committee')->where('status',2)->count();
        $create = date("Y-m-d H:i:s");
        if($data > 0) {
            return ['status' => 'Fail'];
        } else {
            DB::table('tb_committee')->insert(
                [
                    'id_user' => $request->id_user,
                    'title_committee' => $request->title_committee,
                    'valid_from' => $request->valid_from,
                    'valid_to' => $request->valid_to,
                    'status' => 2,
                    'created_at' => $create,
                ]
            );
        }

        $id_sender_receiver = array_map('intval', explode(',',$request->id_user));
        foreach ($id_sender_receiver as $key) {
            if($request->id_sender == $key){
               
            } else {
               DB::table('tb_notifikasi')->insert(
                   [  
                       'jenis' => 'Umum',
                       'id_sender' => $request->id_sender,
                       'id_receiver' => $key,  
                       'isi' => 'Anda terpilih sebagai '.$request->title_committee,
                       'status' => 1,
                       'created_at' => $create,
                   ]
               );

               $data2 = DB::table('tb_notifikasi')
                      ->where('jenis', 'Umum')
                      ->where('id_sender', $request->id_sender)
                      ->where('id_receiver', $key)
                      ->where('isi', 'Anda terpilih sebagai '.$request->title_committee)
                      ->first();

                $user = [
                   "id_sender" => $request->id_sender,
                   "id_receiver" => $key,
                ];
                $message2 = [
                   "id_notif" => $data2->id_notif,
                   "id_pekerjaan" => $data2->id_pekerjaan,
                   "id_sender" => $data2->id_sender,
                   "id_receiver" => $data2->id_receiver,
                   "jenis" => $data2->jenis,
                   "isi" => $data2->isi,
                   "status" => $data2->status,
                   "created_at" => $data2->created_at,
                   "updated_at" => $data2->updated_at,
               ];

               broadcast(new NotifEvents($user, $message2));
            }
        }   

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Committee baru",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return ['status' => 'Success'];
    }

    public function getCommittee(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'title_committee');
        $type = $request->input('type', 'asc');
        $committees = DB::table('tb_committee')
            // ->where('status', 2)
            ->where('title_committee', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $committees['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $committees['searchTerm'] = $search_query ? null : '';
        }

        $currentDate = date("Y-m-d");
        foreach ($committees['data'] as $key => $value) {
            if (($currentDate >= $committees['data'][$key]->valid_from) && ($currentDate <= $committees['data'][$key]->valid_to)){
                $committees['data'][$key]->valid = "active";
            }else{
                $committees['data'][$key]->valid = "deactive";
                DB::table('tb_committee')->where('id_committee', $committees['data'][$key]->id_committee)->update(
                    [
                        'status' => 1,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );  
            }
        }

        return response()->json([
            'committees' => $committees,
        ]);
    }

    // menghapus data
    public function delete($id, Request $request) {

        DB::table('tb_committee')->where('id_committee', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Committee",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    // mengambil data by id
    public function show($id_committee) {
        $data = DB::table('tb_committee')->where('id_committee', $id_committee)->first();

        $dataFix = array();
        array_push($dataFix, array(
            'id_committee' => $id_committee,
            'id_user' => explode(",",$data->id_user),
            'title_committee' => $data->title_committee,
            'valid_from' => $data->valid_from,
            'valid_to' => $data->valid_to,
        ));

        return json_encode($dataFix[0]);
    }

    // mengubah data
    public function updateCommittee($id, Request $request) {

        DB::table('tb_committee')->where('id_committee', $id)->update(
            [
                'id_user' => $request->id_user,
                'title_committee' => $request->title_committee,
                'valid_from' => $request->valid_from,
                'valid_to' => $request->valid_to,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Committee",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }
}
