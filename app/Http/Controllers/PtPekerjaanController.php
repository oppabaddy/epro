<?php
namespace App\Http\Controllers;

use App\Events\PersonEvents;
use App\Exports\IuranExport;
use App\Exports\VendorExport;
// import file model Vendor
use App\Vendor;
use App\Mail\ApproveVendor;
use App\Mail\BulkMail;
use App\Mail\VendorMail;
use App\Mail\NotApproveVendor;
use App\Mail\ThanksRegister;
use App\Person;
use DateTime;
use DB;
use Excel;
use PDF;
use ZipArchive;
use File;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Storage;

use App\Events\VendorEvents;
use App\Events\NotifEvents;
use App\Events\RefreshEvents;

class PtPekerjaanController extends Controller {

    public function getIp(Request $request ){
        
            $url = request()->getHttpHost();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            $health = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($health) {
                $json = json_encode(['health' => $health, 'status' => '1']);
                return $json;
            } else {
                $json = json_encode(['health' => $health, 'status' => '0']);
                return $json;
            }
    }
    
    public function dropdownKategoriPekerjaanPT() {
        $data = DB::table('tb_kategori_pekerjaan')->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_kategori_pekerjaan,
                'text' => $key->kategori_pekerjaan,
            );
        }
        return $response;
    }

    public function getPT(Request $request)
    {   
        $arrNamaBulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $response  = array();
        $cnt = array();

        if($request->filter == null || $request->filter == ''){
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_pekerjaan')
                        ->where('metode', '=', 'Pelelangan Terbatas')
                        ->whereYear('created_at', '=', date('Y'))
                        ->whereMonth('created_at', '=', $key)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }

            $total = DB::table('tb_pekerjaan')
                        ->where('metode', '=', 'Pelelangan Terbatas')
                        ->whereYear('created_at', '=', date('Y'))
                        ->count();
        } else {
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_pekerjaan')
                        ->where('metode', '=', 'Pelelangan Terbatas')
                        ->whereMonth('created_at', '=', $key)
                        ->whereYear('created_at', '=', $request->filter)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }

            $total = DB::table('tb_pekerjaan')
                        ->where('metode', '=', 'Pelelangan Terbatas')
                        ->whereYear('created_at', '=', $request->filter)
                        ->count();
        }

        $response[] = array(
                        'total' => $total,
                        'tahun' => $request->filter,
                        'data' => array( 
                                'name' => 'Pelelangan Terbatas',
                                'data' => $cnt,
                            )
                    );
        return $response;
    }

    public function getCountPT()
    {   
        $all = DB::table('tb_pekerjaan as p')
                ->where('p.metode', '=', 'Pelelangan Terbatas')
                ->count();

        $arrNamaJadwal = array('Pemasukan Dokumen Kualifikasi', 'Pelaksanaan Aanwijzing', 'Pemasukan Penawaran', 'Evaluasi Penawaran', 'Klarifikasi & Negosiasi', 'Penetapan Pemenang');
        $cnt = array();
        foreach ($arrNamaJadwal as $key) {
            $data = DB::table('tb_pekerjaan_jadwal as j')
                    ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
                    ->where('p.metode', '=', 'Pelelangan Terbatas')
                    ->where('j.nama_jadwal', '=', $key)
                    ->count();
            array_push($cnt, $data);
        }
        
        $response[] = array(
                        'total' => $all,
                        'count' => $cnt,
                    );
        return $response;
    } 

    public function pelelanganStorePT(Request $request) { 

        $data = DB::table('tb_committee')->where('status',2)->first();

        $file1 = $request->file('file1');
        if(!empty($file1)){
          $imageNameFile = str_random(10).'-informasi-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
          $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
          // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
        }

        $file2 = $request->file('file2');
        if(!empty($file2)){
          $imageNameFile2 = str_random(10).'-dokumen-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
          $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
          // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);
        }

        DB::table('tb_pekerjaan')->insert(
            [   
                'kode_pekerjaan' => strtoupper(str_random(10)),
                'nama_pekerjaan' => $request->nama_pekerjaan,
                'file_pekerjaan' => $imageNameFile,
                'dokumen_pekerjaan' => $imageNameFile2,
                'id_committee' => $data->id_committee,
                'id_vendor' => $request->id_vendor,
                'keterangan' => $request->keterangan, 
                'id_kategori_pekerjaan' => $request->id_kategori_pekerjaan, 
                'anggaran' => $request->anggaran, 
                'lokasi' => $request->lokasi, 
                'nilai_pagu' => $request->nilai_pagu, 
                'kualifikasi' => $request->kualifikasi, 
                'id_divisi' => $request->divisi, 
                'metode' => "Pelelangan Terbatas",
                'valid_from' => $request->valid_from,
                'valid_to' => $request->valid_to,
                'status' => 0,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $cntKat = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_kategori_pekerjaan as k', 'p.id_kategori_pekerjaan', '=', 'k.id_kategori_pekerjaan')
            ->select('p.*', 'k.kategori_pekerjaan')
            ->where('k.id_kategori_pekerjaan', $request->id_kategori_pekerjaan)
            ->count();

        if($request->id_kategori_pekerjaan == 1){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'barang' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->id_kategori_pekerjaan == 2){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'kontruksi' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->id_kategori_pekerjaan == 3){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'badanusaha' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->id_kategori_pekerjaan == 4){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'perorangan' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        $data = DB::table('tb_pekerjaan')
                ->where('nama_pekerjaan', $request->nama_pekerjaan)
                ->where('file_pekerjaan', $imageNameFile)
                ->where('metode', "Pelelangan Terbatas")
                ->first();

        $id_vendor_array = array_map('intval', explode(',',$request->id_vendor)); 
        foreach ($id_vendor_array as $key) {
            DB::table('tb_pekerjaan_vendor')->insert(
                [
                    'id_pekerjaan' => $data->id_pekerjaan,
                    'id_vendor' => $key,
                    'approve_pekerjaan' => 0,
                    'validasi' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        $id_ketahui_array = array_map('intval', explode(',',$request->ketahui)); 
        foreach ($id_ketahui_array as $key) {
            DB::table('tb_pekerjaan_ketahui')->insert(
                [
                    'id_pekerjaan' => $data->id_pekerjaan,
                    'ketahui' => $key,
                    'approve' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        $id_teruskan_array = array_map('intval', explode(',',$request->teruskan)); 
        foreach ($id_teruskan_array as $key) {
            DB::table('tb_pekerjaan_teruskan')->insert(
                [
                    'id_pekerjaan' => $data->id_pekerjaan,
                    'teruskan' => $key,
                    'approve' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        DB::table('tb_message')->insert(
            [
                'id_pekerjaan' => $data->id_pekerjaan,
                'jenis' => "Pelelangan Terbatas",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_pekerjaan_aanwijzing_ba')->insert(
            [
                'id_pekerjaan' => $data->id_pekerjaan,
                'sts_signature' => 0,
                'status' => 0,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        //For Notif
        $dataZ = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', $id_vendor_array)
           ->get();
        $create = date("Y-m-d H:i:s");
        foreach ($dataZ as $key) {
            DB::table('tb_notifikasi')->insert(
                [  
                    'jenis' => 'Pekerjaan',
                    'id_pekerjaan' => $data->id_pekerjaan,
                    'id_sender' => $request->id_user,
                    'id_receiver' => $key->id_user,  
                    'isi' => "Pelelangan Terbatas kepada ".$key->nama_vendor." untuk pekerjaan ".$request->nama_pekerjaan,
                    'status' => 1,
                    'created_at' => $create,
                ]
            );

            $data3 = DB::table('tb_notifikasi')
                   ->where('jenis', 'Pekerjaan')
                   ->where('id_pekerjaan', $data->id_pekerjaan)
                   ->where('id_sender', $request->id_user)
                   ->where('id_receiver', $key->id_user)
                   ->where('isi', "Pelelangan Terbatas kepada ".$key->nama_vendor." untuk pekerjaan ".$request->nama_pekerjaan)
                   ->where('created_at', $create)
                   ->first();

            $user = [
                "id_sender" => $request->id_user,
                "id_receiver" => $key->id_user,
            ];

            $message2 = [
                "id_notif" => $data3->id_notif,
                "id_pekerjaan" => $data3->id_pekerjaan,
                "id_sender" => $data3->id_sender,
                "id_receiver" => $data3->id_receiver,
                "jenis" => $data3->jenis,
                "isi" => $data3->isi,
                "status" => $data3->status,
                "created_at" => $data3->created_at,
                "updated_at" => $data3->updated_at,
            ];

            try {
                broadcast(new NotifEvents($user, $message2));
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Pekerjaan ".$request->nama_pekerjaan,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Pekerjaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        
        return;
    }

    public function getPelelanganPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $pelelangans = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_kategori_pekerjaan as k', 'p.id_kategori_pekerjaan', '=', 'k.id_kategori_pekerjaan')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'k.kategori_pekerjaan')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $pelelangans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $pelelangans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'pelelangans' => $pelelangans,
        ]);
    }

    public function getPelelanganVendorPT($id_user, Request $request) {

        $getVendor = DB::table('users_to_vendor as u')
                ->where('u.id_user', $id_user)
                ->select('u.id_vendor')
                ->first();

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $pelelangans = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_kategori_pekerjaan as k', 'p.id_kategori_pekerjaan', '=', 'k.id_kategori_pekerjaan')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'p.id_vendor')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_vendor as z', 'p.id_pekerjaan', '=', 'z.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'k.kategori_pekerjaan','z.approve_pekerjaan')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->whereIn('z.approve_pekerjaan', [0,1,2])
            ->where('z.id_vendor', $getVendor->id_vendor)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $pelelangans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $pelelangans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'pelelangans' => $pelelangans,
        ]);
    }

    // menghapus data
    public function deletePelelanganPT(Request $request, $id_pekerjaan) {
        $dokumen = DB::table('tb_pekerjaan')->where('id_pekerjaan', $id_pekerjaan)->first();
        if (!empty($dokumen)) {
            if ($dokumen->file_pekerjaan !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->file_pekerjaan)) {
                    unlink(public_path()."/uploads/file_dokumen/".$dokumen->file_pekerjaan);
                }
            }
            if ($dokumen->dokumen_pekerjaan !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen/".$dokumen->dokumen_pekerjaan)) {
                    unlink(public_path()."/uploads/file_dokumen/".$dokumen->dokumen_pekerjaan);
                }
            }
        }

        $kualifikasi = DB::table('tb_pekerjaan_kualifikasi')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($kualifikasi as $key) {
            if ($key->file_dokumen_kualifikasi !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen_kualifikasi/".$key->file_dokumen_kualifikasi)) {
                    unlink(public_path()."/uploads/file_dokumen_kualifikasi/".$key->file_dokumen_kualifikasi);
                }
            }
        }

        $signature = DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signature as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePimpinanAanwijzing = DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePimpinanAanwijzing as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePenyediaAanwijzing = DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePenyediaAanwijzing as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $penawaran = DB::table('tb_pekerjaan_penawaran_file')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($penawaran as $key) {
            if ($key->file_dokumen_penawaran !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen_penawaran/".$key->file_dokumen_penawaran)) {
                    unlink(public_path()."/uploads/file_dokumen_penawaran/".$key->file_dokumen_penawaran);
                }
            }
        }

        $evaluasi = DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($evaluasi as $key) {
            if ($key->file_notulen !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen_evaluasi/".$key->file_notulen)) {
                    unlink(public_path()."/uploads/file_dokumen_evaluasi/".$key->file_notulen);
                }
            }
        }

        $signaturePimpinanPenawaran = DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePimpinanPenawaran as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePesertaPenawaran = DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePesertaPenawaran as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePenyediaPenawaran = DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePenyediaPenawaran as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePimpinanNegosiasi = DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePimpinanNegosiasi as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePenyediaNegosiasi = DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePenyediaNegosiasi as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $signaturePimpinanPemenang = DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($signaturePimpinanPemenang as $key) {
            if ($key->signature !== null) {
                if (file_exists(public_path()."/uploads/signature/".$key->signature)) {
                    unlink(public_path()."/uploads/signature/".$key->signature);
                }
            }
        }

        $kontrak = DB::table('tb_pekerjaan_dokumen_kontrak')->where('id_pekerjaan', $id_pekerjaan)->get();
        foreach ($kontrak as $key) {
            if ($key->file_dokumen_kontrak !== null) {
                if (file_exists(public_path()."/uploads/file_dokumen_kontrak/".$key->file_dokumen_kontrak)) {
                    unlink(public_path()."/uploads/file_dokumen_kontrak/".$key->file_dokumen_kontrak);
                }
            }
        }

        $BAaanwijzing = DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $id_pekerjaan)->first();
        $BAaanwijzingNew = explode(',', $BAaanwijzing->attachment);
        if(count($BAaanwijzingNew) > 0){
            foreach ($BAaanwijzingNew as $key) {
                if ($key !== null) {
                    if (file_exists(public_path()."/uploads/attachmentBA/".$key)) {
                        unlink(public_path()."/uploads/attachmentBA/".$key);
                    }
                }
            }
        }

        $BAnegosiasi = DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $id_pekerjaan)->first();
        $BAnegosiasiNew = explode(',', $BAnegosiasi->attachment);
        if(count($BAnegosiasiNew) > 0){
            foreach ($BAnegosiasiNew as $key) {
                if ($key !== null) {
                    if (file_exists(public_path()."/uploads/attachmentBA/".$key)) {
                        unlink(public_path()."/uploads/attachmentBA/".$key);
                    }
                }
            }
        }

        $BApenawaran = DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $id_pekerjaan)->first();
        $BApenawaranNew = explode(',', $BApenawaran->attachment);
        if(count($BApenawaranNew) > 0){
            foreach ($BApenawaranNew as $key) {
                if ($key !== null) {
                    if (file_exists(public_path()."/uploads/attachmentBA/".$key)) {
                        unlink(public_path()."/uploads/attachmentBA/".$key);
                    }
                }
            }
        }

        $BApemenang = DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $id_pekerjaan)->first();
        $BApemenangNew = explode(',', $BApemenang->attachment);
        if(count($BApemenangNew) > 0){
            foreach ($BApemenangNew as $key) {
                if ($key !== null) {
                    if (file_exists(public_path()."/uploads/attachmentBA/".$key)) {
                        unlink(public_path()."/uploads/attachmentBA/".$key);
                    }
                }
            }
        }

        DB::table('tb_pekerjaan')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_aanwijzing')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_aanwijzing_ba_peserta')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_ketahui')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_teruskan')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_kualifikasi')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_jadwal')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_penawaran_file')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_negosiasi_ba_peserta')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_dokumen_kontrak')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_pekerjaan_jadwal_addendum')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_message as m')
            ->leftjoin('tb_message_detail as d', 'm.id_message', '=', 'd.id_message')
            ->where('m.id_pekerjaan', $id_pekerjaan)
            ->delete();
        DB::table('tb_message')->where('id_pekerjaan', $id_pekerjaan)->delete();
        DB::table('tb_notifikasi')->where('id_pekerjaan', $id_pekerjaan)->delete();

        $cntKat = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_kategori_pekerjaan as k', 'p.id_kategori_pekerjaan', '=', 'k.id_kategori_pekerjaan')
            ->select('p.*', 'k.kategori_pekerjaan')
            ->where('k.id_kategori_pekerjaan', $dokumen->id_kategori_pekerjaan)
            ->count();
        if($dokumen->id_kategori_pekerjaan == 1){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'barang' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($dokumen->id_kategori_pekerjaan == 2){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'kontruksi' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($dokumen->id_kategori_pekerjaan == 3){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'badanusaha' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($dokumen->id_kategori_pekerjaan == 4){
            DB::table('tb_content_counter')->where('id', 1)->update(
                [
                    'perorangan' => $cntKat,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }
        
        try {
            broadcast(new RefreshEvents());
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 204;
    }

    // mengambil data by id
    public function showPelelanganPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as p')
                ->leftjoin('tb_kategori_pekerjaan as k', 'p.id_kategori_pekerjaan', '=', 'k.id_kategori_pekerjaan')
                ->select('p.*', 'k.kategori_pekerjaan')
                ->where('p.id_pekerjaan', $id_pekerjaan)
                ->first();

        $dataKetahuiFix = array();
        $dataKetahui = DB::table('tb_pekerjaan_ketahui as p')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.ketahui')
                    ->get();
        foreach ($dataKetahui as $key) {
            array_push($dataKetahuiFix, $key->ketahui);
        }

        $dataTeruskanFix = array();
        $dataTeruskan = DB::table('tb_pekerjaan_teruskan as p')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.teruskan')
                    ->get();
        foreach ($dataTeruskan as $key) {
            array_push($dataTeruskanFix, $key->teruskan);
        }

        $dataDivKetahui = DB::table('tb_pekerjaan_ketahui as p')
                    ->leftjoin('users as u', 'p.ketahui', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('s.id_divisi')
                    ->first();

        $dataDivTeruskan = DB::table('tb_pekerjaan_teruskan as p')
                    ->leftjoin('users as u', 'p.teruskan', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('s.id_divisi')
                    ->first();


        $dataFix = array();
        array_push($dataFix, array(
            'id_pekerjaan' => $data->id_pekerjaan,
            'kode_pekerjaan' => $data->kode_pekerjaan,
            'nama_pekerjaan' => $data->nama_pekerjaan,
            'file_pekerjaan' => $data->file_pekerjaan,
            'dokumen_pekerjaan' => $data->dokumen_pekerjaan,
            'id_committee' => $data->id_committee,
            'id_vendor' => explode(",",$data->id_vendor),
            'keterangan' => $data->keterangan, 
            'id_kategori_pekerjaan' => $data->id_kategori_pekerjaan, 
            'anggaran' => $data->anggaran, 
            'lokasi' => $data->lokasi, 
            'nilai_pagu' => $data->nilai_pagu, 
            'kualifikasi' => $data->kualifikasi, 
            'id_divisi' => $data->id_divisi,
            'metode' => $data->metode,
            'ketahuiDiv' => $dataDivKetahui->id_divisi,
            'ketahui' => $dataKetahuiFix,
            'teruskanDiv' => $dataDivTeruskan->id_divisi,
            'teruskan' => $dataTeruskanFix,
            'valid_from' => $data->valid_from,
            'valid_to' => $data->valid_to,
        ));

        return json_encode($dataFix[0]);
    }

    public function showVendorPekerjaanPT($id_vendor) {
        $data = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')
            ->leftjoin('tb_vendor_third as a', 'p.id_vendor', '=', 'a.id_vendor')
            ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
            ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))->first();
        // $data->logo_vendorOld = $data->logo_vendor;
        // $data->photo_pemilikOld = $data->photo_pemilik;

        // $ihsi = null;
        // if (filter_var($data->logo_vendor, FILTER_VALIDATE_URL)) {
        //     $data->logo_vendor = str_replace("open", "uc", $data->logo_vendor);
        //     $ihsi = str_replace("open", "uc", $data->logo_vendor);
        // } else {
        //     $path = public_path() . '/uploads/logo_vendor/'.$data->logo_vendor;
        //     $dataImg = file_get_contents($path);
        //     $type = pathinfo($path, PATHINFO_EXTENSION);
        //     $base64 = base64_encode($dataImg);
        //     $src = 'data:image/' . $type . ';base64,' . $base64;

        //     $data->logo_vendor = $src;
        //     $ihsi = $src;
        // }

        // if (filter_var($data->photo_pemilik, FILTER_VALIDATE_URL)) {
        //     $data->photo_pemilik = str_replace("open", "uc", $data->photo_pemilik);
        //     $ihsi2 = str_replace("open", "uc", $data->photo_pemilik);
        // } else {
        //     $path2 = public_path() . '/uploads/photo_owner/'.$data->photo_pemilik;
        //     $dataImg2 = file_get_contents($path2);
        //     $type2 = pathinfo($path2, PATHINFO_EXTENSION);
        //     $base642 = base64_encode($dataImg2);
        //     $src2 = 'data:image/' . $type2 . ';base64,' . $base642;

        //     $data->photo_pemilik = $src2;
        //     $ihsi2 = $src2;
        // }

        // $dataFix = array();
        // array_push($dataFix, array(
        //     "id_vendor" => $id_vendor,
        //     "nama_vendor" => $data->nama_vendor,
        //     "deskripsi_vendor" => $data->deskripsi_vendor,
        //     "nama_narahubung" => $data->nama_narahubung,
        //     "email_narahubung" => $data->email_narahubung,
        //     "tlp_narahubung" => $data->tlp_narahubung,
        //     "jenis_vendor" => $data->jenis_vendor,
        //     "email_vendor" => $data->email_vendor,
        //     'tlp_vendor' =>  $data->tlp_vendor,
        //     'fax_vendor' =>  $data->fax_vendor,
        //     "logo_vendor" => $ihsi,
        //     "logo_vendorOld" => $data->logo_vendorOld,
        //     'alamat_vendor' =>  $data->alamat_vendor,
        //     'provinsi_vendor' =>  $data->provinsi_vendor,
        //     'kota_vendor' =>  $data->kota_vendor,
        //     'kodepos_vendor' =>  $data->kodepos_vendor,
        //     "tgl_berdiri_vendor" => $data->tgl_berdiri_vendor,
        //     "web_vendor" => $data->web_vendor,
        //     "facebook_vendor" => $data->facebook_vendor,
        //     "instagram_vendor" => $data->instagram_vendor,

        //     'tgl_berdiri_vendor' => $data->tgl_berdiri_vendor,
        //     'cabang_vendor' =>  $data->cabang_vendor,
        //     'alamat_pusat' =>  $data->alamat_pusat,
        //     'email_pusat' =>  $data->email_pusat,
        //     'tlp_pusat' =>  $data->tlp_pusat,
        //     'fax_pusat' =>  $data->fax_pusat,

        //     'no_akta_diri' => $data->no_akta_diri,
        //     'notaris_diri' => $data->notaris_diri,
        //     'tgl_akta_diri' => $data->tgl_akta_diri,
        //     'file_pendirian' => $data->file_pendirian,
        //     'no_akta_ubah' => $data->no_akta_ubah,
        //     'notaris_ubah' => $data->notaris_ubah,
        //     'tgl_akta_ubah' => $data->tgl_akta_ubah,
        //     'file_perubahan' => $data->file_perubahan,
        //     'npwp_vendor' =>  $data->npwp_vendor,
        //     'file_npwp' => $data->file_npwp,
        //     'pkp_vendor' =>  $data->pkp_vendor,
        //     'file_pkp' => $data->file_pkp,
        //     'no_iujksiup' => $data->no_iujksiup,
        //     'instansi_iujksiup' => $data->instansi_iujksiup,
        //     'tgl_iujksiup' => $data->tgl_iujksiup,
        //     'file_iujksiup' => $data->file_iujksiup,
        //     'no_sbu' => $data->no_sbu,
        //     'instansi_sbu' => $data->instansi_sbu,
        //     'tgl_sbu' => $data->tgl_sbu,
        //     'file_sbu' => $data->file_sbu,

        //     "photo_pemilik" => $ihsi2,
        //     "photo_pemilikOld" => $data->photo_pemilikOld,
        //     'nama_pemilik' => $data->nama_pemilik,
        //     'alamat_pemilik' => $data->alamat_pemilik,
        //     'ktp_pemilik' => $data->ktp_pemilik,
        // ));

        // return json_encode($dataFix[0]);
    }

    public function getVendorPekerjaanbyIDPT($id_pekerjaan,$id_vendor,$id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary as p')
            ->leftjoin('users_to_vendor as u', 'p.id_vendor', '=', 'u.id_vendor')
            // ->leftjoin('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
            // ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
            ->leftjoin('tb_pekerjaan_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
            ->select('p.*','v.approve_pekerjaan','v.signature','v.id_pekerjaan','v.validasi')
            ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
            ->where('v.id_pekerjaan', $id_pekerjaan)
            ->where('u.id_user', $id_user)
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function getVendorPekerjaanPT($id_pekerjaan,$id_vendor, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_pekerjaan_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
            ->select('p.*','v.approve_pekerjaan','v.signature','v.id_pekerjaan','v.validasi')
            ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
            ->where('v.id_pekerjaan', $id_pekerjaan)
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function showViewJadwalPT($id_pekerjaan, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_pekerjaan_jadwal')
            ->where('id_pekerjaan', $id_pekerjaan)
            ->where('nama_jadwal', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            $cnt = DB::table('tb_pekerjaan_jadwal_addendum')
                ->where('id_pekerjaan', $id_pekerjaan)->count();

            $vendors['data'][$key]->addendum = $cnt;
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function showPelelanganMailPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as p')
                ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->select('p.*', 'v.email_narahubung', 'v.email_vendor', 'v.nama_narahubung', 'v.nama_vendor')
                ->where('p.id_pekerjaan', $id_pekerjaan)->first();

        $dataFix = array();
        array_push($dataFix, array(
            'id_pekerjaan' => $id_pekerjaan,
            'nama_pekerjaan' => $data->nama_pekerjaan,
            'file_pekerjaan' => $data->file_pekerjaan,
            'id_vendor' => $data->id_vendor,
            'id_committee' => $data->id_committee,
            'valid_from' => $data->valid_from,
            'valid_to' => $data->valid_to,
            'metode' => $data->metode,
            'email_narahubung' => $data->email_narahubung,
            'email_vendor' => $data->email_vendor,
            'nama_narahubung' => $data->nama_narahubung,
            'nama_vendor' => $data->nama_vendor,
        ));

        return json_encode($dataFix[0]);
    }

    // mengubah data
    public function updatePelelanganPT($id, Request $request) {

        $file1 = $request->file('file1');
        $file2 = $request->file('file2');
        if(!empty($file1) && !empty($file2)){
          $imageNameFile = str_random(10).'-informasi-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
          $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
          // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
          if (file_exists(public_path()."/uploads/file_dokumen/".$request->file1_old)) {
              unlink(public_path()."/uploads/file_dokumen/".$request->file1_old);
          }

          $imageNameFile2 = str_random(10).'-dokumen-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
          $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
          // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);
          if (file_exists(public_path()."/uploads/file_dokumen/".$request->file2_old)) {
              unlink(public_path()."/uploads/file_dokumen/".$request->file2_old);
          }

          DB::table('tb_pekerjaan')->where('id_pekerjaan', $id)->update(
              [
                  'nama_pekerjaan' => $request->nama_pekerjaan,
                  'file_pekerjaan' => $imageNameFile,
                  'dokumen_pekerjaan' => $imageNameFile2,
                  'id_committee' => $request->id_committee,
                  'id_vendor' => $request->id_vendor,
                  'keterangan' => $request->keterangan, 
                  'id_kategori_pekerjaan' => $request->id_kategori_pekerjaan, 
                  'anggaran' => $request->anggaran, 
                  'lokasi' => $request->lokasi, 
                  'nilai_pagu' => $request->nilai_pagu, 
                  'kualifikasi' => $request->kualifikasi, 
                  'id_divisi' => $request->divisi, 
                  'metode' => "Pelelangan Terbatas",
                  'valid_from' => $request->valid_from,
                  'valid_to' => $request->valid_to,
                  'status' => 0,
                  'updated_at' => date("Y-m-d H:i:s"),
              ]
          );

           DB::table('tb_pekerjaan_ketahui')->where('id_pekerjaan', $id)->delete();
           DB::table('tb_pekerjaan_teruskan')->where('id_pekerjaan', $id)->delete();

           $id_ketahui_array = array_map('intval', explode(',',$request->ketahui)); 
           foreach ($id_ketahui_array as $key) {
               DB::table('tb_pekerjaan_ketahui')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'ketahui' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }

           $id_teruskan_array = array_map('intval', explode(',',$request->teruskan)); 
           foreach ($id_teruskan_array as $key) {
               DB::table('tb_pekerjaan_teruskan')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'teruskan' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }
        } else if(!empty($file1)){
          $imageNameFile = str_random(10).'-informasi-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile, $file1);
          $upload = $file1->move(public_path()."/uploads/file_dokumen/",$imageNameFile);
          // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen/".$imageNameFile);
          if (file_exists(public_path()."/uploads/file_dokumen/".$request->file1_old)) {
              unlink(public_path()."/uploads/file_dokumen/".$request->file1_old);
          }

          DB::table('tb_pekerjaan')->where('id_pekerjaan', $id)->update(
              [
                  'nama_pekerjaan' => $request->nama_pekerjaan,
                  'file_pekerjaan' => $imageNameFile,
                  'id_committee' => $request->id_committee,
                  'id_vendor' => $request->id_vendor,
                  'keterangan' => $request->keterangan, 
                  'id_kategori_pekerjaan' => $request->id_kategori_pekerjaan, 
                  'anggaran' => $request->anggaran, 
                  'lokasi' => $request->lokasi, 
                  'nilai_pagu' => $request->nilai_pagu, 
                  'kualifikasi' => $request->kualifikasi, 
                  'id_divisi' => $request->divisi, 
                  'metode' => "Pelelangan Terbatas",
                  'valid_from' => $request->valid_from,
                  'valid_to' => $request->valid_to,
                  'status' => 0,
                  'updated_at' => date("Y-m-d H:i:s"),
              ]
          );

           DB::table('tb_pekerjaan_ketahui')->where('id_pekerjaan', $id)->delete();
           DB::table('tb_pekerjaan_teruskan')->where('id_pekerjaan', $id)->delete();

           $id_ketahui_array = array_map('intval', explode(',',$request->ketahui)); 
           foreach ($id_ketahui_array as $key) {
               DB::table('tb_pekerjaan_ketahui')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'ketahui' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }

           $id_teruskan_array = array_map('intval', explode(',',$request->teruskan)); 
           foreach ($id_teruskan_array as $key) {
               DB::table('tb_pekerjaan_teruskan')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'teruskan' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }
        } else if(!empty($file2)){
          $imageNameFile2 = str_random(10).'-dokumen-pekerjaan.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen". '/' . $imageNameFile2, $file2);
          $upload = $file2->move(public_path()."/uploads/file_dokumen/",$imageNameFile2);
          // move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_dokumen/".$imageNameFile2);
          if (file_exists(public_path()."/uploads/file_dokumen/".$request->file2_old)) {
              unlink(public_path()."/uploads/file_dokumen/".$request->file2_old);
          }

          DB::table('tb_pekerjaan')->where('id_pekerjaan', $id)->update(
              [
                  'nama_pekerjaan' => $request->nama_pekerjaan,
                  'dokumen_pekerjaan' => $imageNameFile2,
                  'id_committee' => $request->id_committee,
                  'id_vendor' => $request->id_vendor,
                  'keterangan' => $request->keterangan, 
                  'id_kategori_pekerjaan' => $request->id_kategori_pekerjaan, 
                  'anggaran' => $request->anggaran, 
                  'lokasi' => $request->lokasi, 
                  'nilai_pagu' => $request->nilai_pagu, 
                  'kualifikasi' => $request->kualifikasi, 
                  'id_divisi' => $request->divisi, 
                  'metode' => "Pelelangan Terbatas",
                  'valid_from' => $request->valid_from,
                  'valid_to' => $request->valid_to,
                  'status' => 0,
                  'updated_at' => date("Y-m-d H:i:s"),
              ]
          );

           DB::table('tb_pekerjaan_ketahui')->where('id_pekerjaan', $id)->delete();
           DB::table('tb_pekerjaan_teruskan')->where('id_pekerjaan', $id)->delete();

           $id_ketahui_array = array_map('intval', explode(',',$request->ketahui)); 
           foreach ($id_ketahui_array as $key) {
               DB::table('tb_pekerjaan_ketahui')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'ketahui' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }

           $id_teruskan_array = array_map('intval', explode(',',$request->teruskan)); 
           foreach ($id_teruskan_array as $key) {
               DB::table('tb_pekerjaan_teruskan')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'teruskan' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }
        } else {
           DB::table('tb_pekerjaan')->where('id_pekerjaan', $id)->update(
               [
                   'nama_pekerjaan' => $request->nama_pekerjaan,
                   'id_committee' => $request->id_committee,
                   'id_vendor' => $request->id_vendor,
                   'keterangan' => $request->keterangan, 
                   'id_kategori_pekerjaan' => $request->id_kategori_pekerjaan, 
                   'anggaran' => $request->anggaran, 
                   'lokasi' => $request->lokasi, 
                   'nilai_pagu' => $request->nilai_pagu, 
                   'kualifikasi' => $request->kualifikasi, 
                   'id_divisi' => $request->divisi, 
                   'metode' => "Pelelangan Terbatas",
                   'valid_from' => $request->valid_from,
                   'valid_to' => $request->valid_to,
                   'status' => 0,
                   'updated_at' => date("Y-m-d H:i:s"),
               ]
           ); 

           DB::table('tb_pekerjaan_ketahui')->where('id_pekerjaan', $id)->delete();
           DB::table('tb_pekerjaan_teruskan')->where('id_pekerjaan', $id)->delete();

           $id_ketahui_array = array_map('intval', explode(',',$request->ketahui)); 
           foreach ($id_ketahui_array as $key) {
               DB::table('tb_pekerjaan_ketahui')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'ketahui' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }

           $id_teruskan_array = array_map('intval', explode(',',$request->teruskan)); 
           foreach ($id_teruskan_array as $key) {
               DB::table('tb_pekerjaan_teruskan')->insert(
                   [
                       'id_pekerjaan' => $id,
                       'teruskan' => $key,
                       'approve' => 0,
                       'created_at' => date("Y-m-d H:i:s"),
                   ]
               );
           }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Pekerjaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function approvePekerjaanPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        $getVendor = DB::table('users_to_vendor as u')
                ->where('u.id_user', $request->id_user)
                ->select('u.id_vendor')
                ->first();

        DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $getVendor->id_vendor)->update(
            [
                'approve_pekerjaan' => 2,
                'signature' => $imageName,
                'approve_catatan' => $request->catatan,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        $cntApprove = DB::table('tb_pekerjaan_vendor')
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('approve_pekerjaan', 0)
                    ->count();
        if($cntApprove < 1){
            DB::table('tb_pekerjaan')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            DB::table('tb_pekerjaan_jadwal')->insert(
                [
                    'id_pekerjaan' =>$request->id_pekerjaan,
                    'nama_jadwal' => "Pemasukan Dokumen Kualifikasi",
                    'tgl_mulai' => date("Y-m-d"),
                    'jam_mulai' => date("H:i:s"),
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        $getFileVendor1 = DB::table('tb_vendor_dokumen as d')
                ->where('d.id_vendor', $getVendor->id_vendor)
                ->select('d.file_pendirian','d.file_perubahan','d.file_npwp','d.file_pkp')
                ->first();

        if($getFileVendor1->file_pendirian == Null){
        } else {
            $filePendirian = file(public_path()."/uploads/file_dokumen". '/' .$getFileVendor1->file_pendirian);
            \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $getFileVendor1->file_pendirian, $filePendirian);

            DB::table('tb_pekerjaan_kualifikasi')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $getVendor->id_vendor,
                    'dokumen_kualifikasi' => "Akta Pendirian",
                    'file_dokumen_kualifikasi' => $getFileVendor1->file_pendirian,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        if($getFileVendor1->file_perubahan == Null){
        } else {
            $filePerubahan = file(public_path()."/uploads/file_dokumen". '/' .$getFileVendor1->file_perubahan);
            \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $getFileVendor1->file_perubahan, $filePerubahan);

            DB::table('tb_pekerjaan_kualifikasi')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $getVendor->id_vendor,
                    'dokumen_kualifikasi' => "Akta Perubahan",
                    'file_dokumen_kualifikasi' => $getFileVendor1->file_perubahan,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        if($getFileVendor1->file_npwp == Null){
        } else {
            $fileNPWP = file(public_path()."/uploads/file_dokumen". '/' .$getFileVendor1->file_npwp);
            \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $getFileVendor1->file_npwp, $fileNPWP);

            DB::table('tb_pekerjaan_kualifikasi')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $getVendor->id_vendor,
                    'dokumen_kualifikasi' => "NPWP",
                    'file_dokumen_kualifikasi' => $getFileVendor1->file_npwp,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        if($getFileVendor1->file_pkp == Null){
        } else {
            $filePKP = file(public_path()."/uploads/file_dokumen". '/' .$getFileVendor1->file_pkp);
            \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $getFileVendor1->file_pkp, $filePKP);

            DB::table('tb_pekerjaan_kualifikasi')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $getVendor->id_vendor,
                    'dokumen_kualifikasi' => "PKP",
                    'file_dokumen_kualifikasi' => $getFileVendor1->file_pkp,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        $getFileVendor2 = DB::table('tb_vendor_iujksiup as i')
                ->where('i.id_vendor', $getVendor->id_vendor)
                ->select('i.file_iujksiup')
                ->get();
        foreach ($getFileVendor2 as $key) {
            if($key->file_iujksiup == Null){
            } else {
                $fileIUJK = file(public_path()."/uploads/file_dokumen". '/' .$key->file_iujksiup);
                \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $key->file_iujksiup, $fileIUJK);

                DB::table('tb_pekerjaan_kualifikasi')->insert(
                    [
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_vendor' => $getVendor->id_vendor,
                        'dokumen_kualifikasi' => "IUJK / SIUP",
                        'file_dokumen_kualifikasi' => $key->file_iujksiup,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        $getFileVendor3 = DB::table('tb_vendor_sbu as s')
                ->where('s.id_vendor', $getVendor->id_vendor)
                ->select('s.file_sbu')
                ->get();
        foreach ($getFileVendor3 as $key) {
            if($key->file_sbu == Null){
            } else {
                $fileSBU = file(public_path()."/uploads/file_dokumen". '/' .$key->file_sbu);
                \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $key->file_sbu, $fileSBU);

                DB::table('tb_pekerjaan_kualifikasi')->insert(
                    [
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_vendor' => $getVendor->id_vendor,
                        'dokumen_kualifikasi' => "SBU",
                        'file_dokumen_kualifikasi' => $key->file_sbu,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->select('p.nama_pekerjaan','m.id_message', 'c.id_user as id_user_committe')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ2->id);
        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();
              DB::table('tb_notifikasi')->insert(
                  [  
                      'jenis' => "Pekerjaan",
                      'id_pekerjaan' => $request->id_pekerjaan,
                      'id_sender' => $request->id_user,
                      'id_receiver' => $key,  
                      'isi' => $nama->nama_vendor." Menerima Pekerjaan",
                      'status' => 1,
                      'created_at' => date("Y-m-d H:i:s"),
                  ]
              );

              $data2 = DB::table('tb_notifikasi')
                     ->where('jenis', "Pekerjaan")
                     ->where('id_pekerjaan', $request->id_pekerjaan)
                     ->where('id_sender', $request->id_user)
                     ->where('id_receiver', $key)
                     ->where('isi', $nama->nama_vendor." Menerima Pekerjaan")
                     ->first();

              $user = [
                  "id_sender" => $request->id_user,
                  "id_receiver" => $key,
              ];

              $message2 = [
                  "id_notif" => $data2->id_notif,
                  "id_pekerjaan" => $data2->id_pekerjaan,
                  "id_sender" => $data2->id_sender,
                  "id_receiver" => $data2->id_receiver,
                  "jenis" => $data2->jenis,
                  "isi" => $data2->isi,
                  "status" => $data2->status,
                  "created_at" => $data2->created_at,
                  "updated_at" => $data2->updated_at,
              ];

              try {
                    broadcast(new NotifEvents($user, $message2));
              } catch (\Exception $e) {
                  return $e->getMessage();
              }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Approve Pekerjaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return;
    }

    public function notapprovePekerjaanPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        $getVendor = DB::table('users_to_vendor as u')
                ->where('u.id_user', $request->id_user)
                ->select('u.id_vendor')
                ->first();

        DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $getVendor->id_vendor)->update(
            [
                'approve_pekerjaan' => 1,
                'signature' => $imageName,
                'approve_catatan' => $request->catatan,
                'validasi' => 1,
                'validasi_catatan' => "Vendor menolak",
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        $cntApprove = DB::table('tb_pekerjaan_vendor')
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('approve_pekerjaan', 0)
                    ->count();
        if($cntApprove < 1){
            DB::table('tb_pekerjaan')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            DB::table('tb_pekerjaan_jadwal')->insert(
                [
                    'id_pekerjaan' =>$request->id_pekerjaan,
                    'nama_jadwal' => "Pemasukan Dokumen Kualifikasi",
                    'tgl_mulai' => date("Y-m-d"),
                    'jam_mulai' => date("H:i:s"),
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ->id_user_vendor, $dataZ2->id);
        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();
              DB::table('tb_notifikasi')->insert(
                  [  
                      'jenis' => "Pekerjaan",
                      'id_pekerjaan' => $request->id_pekerjaan,
                      'id_sender' => $request->id_user,
                      'id_receiver' => $key,  
                      'isi' => $nama->nama_vendor." Menolak Pekerjaan",
                      'status' => 1,
                      'created_at' => date("Y-m-d H:i:s"),
                  ]
              );

              $data2 = DB::table('tb_notifikasi')
                     ->where('jenis', "Pekerjaan")
                     ->where('id_pekerjaan', $request->id_pekerjaan)
                     ->where('id_sender', $request->id_user)
                     ->where('id_receiver', $key)
                     ->where('isi', $nama->nama_vendor." Menolak Pekerjaan")
                     ->first();

              $user = [
                  "id_sender" => $request->id_user,
                  "id_receiver" => $key,
              ];

              $message2 = [
                  "id_notif" => $data2->id_notif,
                  "id_pekerjaan" => $data2->id_pekerjaan,
                  "id_sender" => $data2->id_sender,
                  "id_receiver" => $data2->id_receiver,
                  "jenis" => $data2->jenis,
                  "isi" => $data2->isi,
                  "status" => $data2->status,
                  "created_at" => $data2->created_at,
                  "updated_at" => $data2->updated_at,
              ];

              try {
                broadcast(new NotifEvents($user, $message2));
              } catch (\Exception $e) {
                  return $e->getMessage();
              }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Not Approve Pekerjaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function setJadwalPekerjaanPT(Request $request) {
        $cek = DB::table('tb_pekerjaan_jadwal')
            ->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('nama_jadwal', $request->nama_jadwal)
            ->count();

        if($cek < 1){
            DB::table('tb_pekerjaan_jadwal')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'nama_jadwal' => $request->nama_jadwal,
                    'tgl_mulai' => $request->tgl_mulai,
                    'jam_mulai' => $request->jam_mulai,
                    'tgl_selesai' => $request->tgl_selesai,
                    'jam_selesai' => $request->jam_selesai,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_jadwal')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('nama_jadwal', $request->nama_jadwal)->update(
                [
                    'id_pekerjaan' =>$request->id_pekerjaan,
                    'nama_jadwal' => $request->nama_jadwal,
                    'tgl_selesai' => $request->tgl_selesai,
                    'jam_selesai' => $request->jam_selesai,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        if($request->nama_jadwal == "Evaluasi Penawaran"){
            $cek2 = DB::table('tb_pekerjaan_evaluasi')
            ->where('id_pekerjaan', $request->id_pekerjaan)
            ->count();
            if($cek2 < 1){
                DB::table('tb_pekerjaan_evaluasi')->insert(
                    [
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'status_evaluasi' => 0,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        // Notif
        if($request->nama_jadwal == 'Pemasukan Dokumen Kualifikasi'){
            $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_vendor as q', 'p.id_pekerjaan', '=', 'q.id_pekerjaan')
           ->leftjoin('users_to_vendor as v', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'q.approve_pekerjaan')
           ->where('q.approve_pekerjaan',2)
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->get();
        } else if($request->nama_jadwal == 'Pelaksanaan Aanwijzing'){
            $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_vendor as q', 'p.id_pekerjaan', '=', 'q.id_pekerjaan')
           ->leftjoin('users_to_vendor as v', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'q.approve_pekerjaan')
           ->where('q.validasi',2)
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->get();
        } else {
            $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_vendor as q', 'p.id_pekerjaan', '=', 'q.id_pekerjaan')
           ->leftjoin('users_to_vendor as v', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'q.approve_pekerjaan')
           ->where('q.validasi',2)
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->get();
        }
        

        $id_sender_receiver = array();
        foreach ($dataZ as $key) {
            array_push($id_sender_receiver, $key->id_user_vendor);
         } 
        // $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_vendor));
        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              if($request->nama_jadwal == "Pemasukan Dokumen Kualifikasi"){
                $isi = $request->nama_jadwal." berakhir pada ".$request->tgl_selesai." jam ".$request->jam_selesai;
              } else {
                $isi = $request->nama_jadwal." dimulai pada ".$request->tgl_mulai." jam ".$request->jam_mulai." dan berakhir pada ".$request->tgl_selesai." jam ".$request->jam_selesai;
              }
              DB::table('tb_notifikasi')->insert(
                  [  
                      'jenis' => "Pekerjaan",
                      'id_pekerjaan' => $request->id_pekerjaan,
                      'id_sender' => $request->id_user,
                      'id_receiver' => $key,  
                      'isi' => $isi,
                      'status' => 1,
                      'created_at' => date("Y-m-d H:i:s"),
                  ]
              );

              $data2 = DB::table('tb_notifikasi')
                     ->where('jenis', "Pekerjaan")
                     ->where('id_pekerjaan', $request->id_pekerjaan)
                     ->where('id_sender', $request->id_user)
                     ->where('id_receiver', $key)
                     ->where('isi', $isi)
                     ->first();

              $user = [
                  "id_sender" => $request->id_user,
                  "id_receiver" => $key,
              ];

              $message2 = [
                  "id_notif" => $data2->id_notif,
                  "id_pekerjaan" => $data2->id_pekerjaan,
                  "id_sender" => $data2->id_sender,
                  "id_receiver" => $data2->id_receiver,
                  "jenis" => $data2->jenis,
                  "isi" => $data2->isi,
                  "status" => $data2->status,
                  "created_at" => $data2->created_at,
                  "updated_at" => $data2->updated_at,
              ];
              try {
                broadcast(new NotifEvents($user, $message2));
              } catch (\Exception $e) {
                  return $e->getMessage();
              }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Jadwal Pekerjaan ".$isi,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
    }



    public function getDokumenKualifikasiPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $dokumens = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.validasi as validasi')
            ->whereIn('a.validasi', [0,2])
            ->where('j.nama_jadwal', 'Pemasukan Dokumen Kualifikasi')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $dokumens['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $dokumens['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'dokumens' => $dokumens,
        ]);
    }

    public function getDokumenKualifikasiPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $dokumens = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'a.id_vendor')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.validasi as validasi', 'a.approve_pekerjaan as approve_pekerjaan')
            ->whereIn('a.approve_pekerjaan', [2])
            ->where('j.nama_jadwal', 'Pemasukan Dokumen Kualifikasi')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $dokumens['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $dokumens['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'dokumens' => $dokumens,
        ]);
    }

    public function dokumenPekerjaanPT(Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $request->id_user)
                ->select('id_vendor')
                ->first();
        
        $file = $request->file('file');
        if(!empty($file)){
          $imageNameFile = str_random(10).'-'.$request->dokumen.'.'.'pdf';
          // \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $imageNameFile, $file);
          $upload = $file->move(public_path()."/uploads/file_dokumen_kualifikasi/",$imageNameFile);
          // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen_kualifikasi/".$imageNameFile);
        }

        DB::table('tb_pekerjaan_kualifikasi')->insert(
            [
                'id_pekerjaan' => $request->id_pekerjaan,
                'id_vendor' => $data->id_vendor,
                'dokumen_kualifikasi' => $request->dokumen,
                'file_dokumen_kualifikasi' => $imageNameFile,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Dokumen Kualifikasi Pekerjaan ".$request->dokumen,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function updatedokumenPekerjaanPT(Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $request->id_user)
                ->select('id_vendor')
                ->first();

        $file = $request->file('file');
        if(!empty($file)){
            
            if (file_exists(public_path()."/uploads/file_dokumen_kualifikasi/".$request->file_dokumen)) {
                if (!unlink(public_path()."/uploads/file_dokumen_kualifikasi/".$request->file_dokumen)) {   
                }  
                else { 
                    $imageNameFile = str_random(10).'-'.$request->dokumen.'.'.'pdf';
                    // \File::put(public_path()."/uploads/file_dokumen_kualifikasi". '/' . $imageNameFile, $file);
                    $upload = $file->move(public_path()."/uploads/file_dokumen_kualfikasi/",$imageNameFile);
                    // move_uploaded_file($_FILES['file']['tmp_name'], "uploads/file_dokumen_kualifikasi/".$imageNameFile);
                    DB::table('tb_pekerjaan_kualifikasi')
                    ->where('id_pekerjaan',$request->id_pekerjaan)
                    ->where('id_vendor',$data->id_vendor)
                    ->where('file_dokumen_kualifikasi',$request->file_dokumen)
                    ->update(
                        [
                            'id_pekerjaan' => $request->id_pekerjaan,
                            'id_vendor' => $data->id_vendor,
                            'dokumen_kualifikasi' => $request->dokumen,
                            'file_dokumen_kualifikasi' => $imageNameFile,
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    ); 

                    DB::table('tb_log_activity')->insert(
                        [
                            'username' => $request->username,
                            'fullname' => $request->fullname,
                            'ip' => request()->ip(),
                            'log' => "Memperbarui Dokumen Kualifikasi Pekerjaan ".$request->dokumen,
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }  
            } else {
            }
        }
        
        return;
    }

    public function getDokumenPT($id_pekerjaan, $id_user, $id_vendor, Request $request) {

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.validasi','v.validasi_catatan')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataDoc = DB::table('tb_pekerjaan_kualifikasi as k')
                ->where('k.id_vendor', $key->id_vendor)
                ->where('k.id_pekerjaan', $id_pekerjaan)
                ->select('k.*')
                ->get();
            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'validasi' => $key->validasi,
                'catatan' => $key->validasi_catatan,
                'isi' => $dataDoc
            ));
        }

        return $dataFix;
    }

    public function getDokumenPTbyID($id_pekerjaan, $id_user, Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $id_user)
                ->select('id_vendor')
                ->first();

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->where('p.id_vendor', $data->id_vendor)
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.validasi','v.validasi_catatan')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataDoc = DB::table('tb_pekerjaan_kualifikasi as k')
                ->where('k.id_vendor', $key->id_vendor)
                ->where('k.id_pekerjaan', $id_pekerjaan)
                ->select('k.*')
                ->get();
            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'validasi' => $key->validasi,
                'catatan' => $key->validasi_catatan,
                'isi' => $dataDoc
            ));
        }

        return $dataFix;
    }
    public function getDokumenPTbyID2($id_pekerjaan, $id_user, Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $id_user)
                ->select('id_vendor')
                ->first();

        $dataNama = DB::table('tb_vendor_primary as p')
                ->where('p.id_vendor', $data->id_vendor)
                ->select('p.nama_vendor','p.id_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataDoc = DB::table('tb_pekerjaan_kualifikasi as k')
                ->where('k.id_vendor', $key->id_vendor)
                ->where('k.id_pekerjaan', $id_pekerjaan)
                ->select('k.*')
                ->get();
                foreach ($dataDoc as $key2) {
                    array_push($dataFix, array('dokumen' => $key2->dokumen_kualifikasi, 'file_doc' => $key2->file_dokumen_kualifikasi));
                }
        }

        return $dataFix;
    }

    public function validasiPekerjaanPT(Request $request) {

        if($request->jenis == "Terima"){
            DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $request->id_vendor)->update(
                [
                    'validasi' => 2,
                    'validasi_catatan' => $request->catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            $cntApprove = DB::table('tb_pekerjaan_vendor')
                        ->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('validasi', 0)
                        ->count();
            if($cntApprove < 1){
                DB::table('tb_pekerjaan')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                    [
                        'status' => 3,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menerima Dokumen Kualifikasi Pekerjaan",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_vendor')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $request->id_vendor)->update(
                [
                    'validasi' => 1,
                    'validasi_catatan' => $request->catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            $cntApprove = DB::table('tb_pekerjaan_vendor')
                        ->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('validasi', 0)
                        ->count();
            if($cntApprove < 1){
                DB::table('tb_pekerjaan')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                    [
                        'status' => 3,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menolak Dokumen Kualifikasi Pekerjaan",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        // Notif
          $nama = DB::table('users_to_vendor as v')
             ->select('v.id_user')
             ->where('v.id_vendor', $request->id_vendor)
             ->first();
          DB::table('tb_notifikasi')->insert(
              [  
                  'jenis' => "Pekerjaan",
                  'id_pekerjaan' => $request->id_pekerjaan,
                  'id_sender' => $request->id_user,
                  'id_receiver' => $nama->id_user,  
                  'isi' => "Dokumen Kualifikasi Di ".$request->jenis,
                  'status' => 1,
                  'created_at' => date("Y-m-d H:i:s"),
              ]
          );

          $data2 = DB::table('tb_notifikasi')
                 ->where('jenis', "Pekerjaan")
                 ->where('id_pekerjaan', $request->id_pekerjaan)
                 ->where('id_sender', $request->id_user)
                 ->where('id_receiver', $nama->id_user)
                 ->where('isi', "Dokumen Kualifikasi Di ".$request->jenis)
                 ->first();

          $user = [
              "id_sender" => $request->id_user,
              "id_receiver" => $nama->id_user,
          ];

          $message2 = [
              "id_notif" => $data2->id_notif,
              "id_pekerjaan" => $data2->id_pekerjaan,
              "id_sender" => $data2->id_sender,
              "id_receiver" => $data2->id_receiver,
              "jenis" => $data2->jenis,
              "isi" => $data2->isi,
              "status" => $data2->status,
              "created_at" => $data2->created_at,
              "updated_at" => $data2->updated_at,
          ];
          try {
            broadcast(new NotifEvents($user, $message2));
          } catch (\Exception $e) {
              return $e->getMessage();
          }
        return;
    }



    public function getAanwijzingPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $aanwijzings = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba')
            ->whereIn('a.status', [0,1,2])
            ->where('j.nama_jadwal', 'Pelaksanaan Aanwijzing')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $aanwijzings['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $aanwijzings['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'aanwijzings' => $aanwijzings,
        ]);
    }

    public function getAanwijzingPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $aanwijzings = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'b.id_vendor')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba', 'b.approve_pekerjaan as approve_pekerjaan', 'b.validasi')
            ->whereIn('b.validasi', [2])
            ->where('j.nama_jadwal', 'Pelaksanaan Aanwijzing')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $aanwijzings['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $aanwijzings['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'aanwijzings' => $aanwijzings,
        ]);
    }

    public function insertQNAPT(Request $request) {
        $data2 = DB::table('tb_message')
                ->where('id_pekerjaan', $request->id_pekerjaan)
                ->first();

        $getIdVendor = DB::table('users_to_vendor')
            ->where('id_user', $request->id_user)
            ->first();

        DB::table('tb_pekerjaan_aanwijzing')->insert(
            [
                'id_pekerjaan' => $request->id_pekerjaan,
                'id_vendor' => $getIdVendor->id_vendor,
                'id_message' => $data2->id_message,
                'pertanyaan' => $request->content,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memberi Pertanyaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ2->id);
        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();
              DB::table('tb_notifikasi')->insert(
                  [  
                      'jenis' => "Pekerjaan",
                      'id_pekerjaan' => $request->id_pekerjaan,
                      'id_sender' => $request->id_user,
                      'id_receiver' => $key,  
                      'isi' => $nama->nama_vendor." Memberi Pertanyaan pada pekerjaan ".$dataZ->nama_pekerjaan,
                      'status' => 1,
                      'created_at' => date("Y-m-d H:i:s"),
                  ]
              );

              $data2 = DB::table('tb_notifikasi')
                     ->where('jenis', "Pekerjaan")
                     ->where('id_pekerjaan', $request->id_pekerjaan)
                     ->where('id_sender', $request->id_user)
                     ->where('id_receiver', $key)
                     ->where('isi', $nama->nama_vendor." Memberi Pertanyaan pada pekerjaan ".$dataZ->nama_pekerjaan)
                     ->first();

              $user = [
                  "id_sender" => $request->id_user,
                  "id_receiver" => $key,
              ];

              $message2 = [
                  "id_notif" => $data2->id_notif,
                  "id_pekerjaan" => $data2->id_pekerjaan,
                  "id_sender" => $data2->id_sender,
                  "id_receiver" => $data2->id_receiver,
                  "jenis" => $data2->jenis,
                  "isi" => $data2->isi,
                  "status" => $data2->status,
                  "created_at" => $data2->created_at,
                  "updated_at" => $data2->updated_at,
              ];
              try {
                broadcast(new NotifEvents($user, $message2));
              } catch (\Exception $e) {
                  return $e->getMessage();
              }
            }
        }
    }

    public function updateQNAPT(Request $request) {
        DB::table('tb_pekerjaan_aanwijzing')
        ->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('id_vendor', $request->id_vendor)
        ->where('created_at', $request->created_at)
        ->update(
            [
                'jawaban' => $request->content,
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memberi Jawaban",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // Notif
          $nama = DB::table('users_to_vendor as v')
             ->select('v.id_user')
             ->where('v.id_vendor', $request->id_vendor)
             ->first();
          DB::table('tb_notifikasi')->insert(
              [  
                  'jenis' => "Pekerjaan",
                  'id_pekerjaan' => $request->id_pekerjaan,
                  'id_sender' => $request->id_user,
                  'id_receiver' => $nama->id_user,  
                  'isi' => "Panitia memberi jawaban",
                  'status' => 1,
                  'created_at' => date("Y-m-d H:i:s"),
              ]
          );

          $data2 = DB::table('tb_notifikasi')
                 ->where('jenis', "Pekerjaan")
                 ->where('id_pekerjaan', $request->id_pekerjaan)
                 ->where('id_sender', $request->id_user)
                 ->where('id_receiver', $nama->id_user)
                 ->where('isi', "Panitia memberi jawaban")
                 ->first();

          $user = [
              "id_sender" => $request->id_user,
              "id_receiver" => $nama->id_user,
          ];

          $message2 = [
              "id_notif" => $data2->id_notif,
              "id_pekerjaan" => $data2->id_pekerjaan,
              "id_sender" => $data2->id_sender,
              "id_receiver" => $data2->id_receiver,
              "jenis" => $data2->jenis,
              "isi" => $data2->isi,
              "status" => $data2->status,
              "created_at" => $data2->created_at,
              "updated_at" => $data2->updated_at,
          ];
          try {
            broadcast(new NotifEvents($user, $message2));
          } catch (\Exception $e) {
              return $e->getMessage();
          }
    }

    public function deleteQNAPT(Request $request) {
        DB::table('tb_pekerjaan_aanwijzing')
        ->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('id_vendor', $request->id_vendor)
        ->where('created_at', $request->created_at)
        ->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Pertanyaan",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    public function getQNAPT($id_pekerjaan, $id_user, $id_vendor, Request $request) {

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_aanwijzing as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'v.id_pekerjaan', '=', 'b.id_pekerjaan')
                ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.created_at','b.status')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataQNA = DB::table('tb_pekerjaan_aanwijzing as k')
                ->where('k.id_vendor', $key->id_vendor)
                ->where('k.id_pekerjaan', $id_pekerjaan)
                ->select('k.*')
                ->get();
            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'isi' => $dataQNA,
                'status' => $key->status
            ));
        }

        return $dataFix;
    }

    public function getQNAPTbyID($id_pekerjaan, $id_user, Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $id_user)
                ->select('id_vendor')
                ->first();

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'v.id_pekerjaan', '=', 'b.id_pekerjaan')
                ->where('p.id_vendor', $data->id_vendor)
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.created_at','b.status')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataQNA = DB::table('tb_pekerjaan_aanwijzing as k')
                ->where('k.id_vendor', $key->id_vendor)
                ->where('k.id_pekerjaan', $id_pekerjaan)
                ->select('k.*')
                ->get();
            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'isi' => $dataQNA,
                'status' => $key->status
            ));
        }

        return $dataFix;
    }

    public function baAanwijzingPT(Request $request) {

        $attachment = $request->file('attachment');
        if($attachment == null || $attachment == ''){
            $nameAtch = array();
        } else {
            $nameAtch = array();
            if (count($attachment) > 0) {
                foreach ($attachment as $atc) {
                    $extention = $atc->getClientOriginalExtension();
                    $attachName = str_random() . '.' . $extention;
                    array_push($nameAtch, $attachName);
                    $upload = $atc->move(public_path() . '/uploads/attachmentBA/',$attachName);
                }
            } else {
                
            }
        }

        $id_array = array();
        array_push($id_array, $request->pimpinan_rapat);
        DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('status', 0)->update(
            [
                'id_pekerjaan' => $request->id_pekerjaan,
                'no_ba' => $request->no_ba,
                'lokasi' => $request->lokasi,
                'pimpinan_rapat' => $request->pimpinan_rapat,
                'sts_signature' => 0,
                'jabatan_pimpinan' => $request->jabatan_pimpinan,
                'dimulai_jam' => $request->dimulai_jam,
                'hasil' => $request->hasil,
                'attachment' => implode(",",$nameAtch),
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        foreach (json_decode($request->peserta) as $key) {   
            DB::table('tb_pekerjaan_aanwijzing_ba_peserta')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'nama' => $key->namaPeserta,
                    'jabatan' => $key->jabatanPeserta,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        $notArrUser = array();
        $notArrVendor = array();
        $not = DB::table('tb_pekerjaan_vendor as v')
           ->leftjoin('users_to_vendor as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('q.id_user','q.id_vendor')
           ->where('v.id_pekerjaan', $request->id_pekerjaan)
           ->where('v.approve_pekerjaan', 1)
           ->get();
        foreach ($not as $key) {
            array_push($notArrUser, $key->id_user);
            array_push($notArrVendor, $key->id_vendor);
        }

        foreach (explode(",",$request->penyedia) as $key) {   
            if (in_array($key, $notArrVendor)){

            } else {
                $id = DB::table('users_to_vendor')->where('id_vendor',$key)
                    ->select('id_user')->first();
                array_push($id_array, $id->id_user);
                DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->insert(
                    [
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_vendor' => $key,
                        'id_user' => $id->id_user,
                        'sts_signature' => 0,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }    
        } 

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat Berita Acara Aanwijzing",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );  

        //For Notif
        $id_vendor_array = array_map('intval', explode(',',$request->id_vendor));
        $datas = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', $id_vendor_array)
           ->get();
        $dataZ= array();
        foreach ($datas as $key) {
            if (in_array($key->id_user, $notArrUser)){

            } else {
                array_push($dataZ, $key->id_user);
            }
        }
        array_push($dataZ, $request->pimpinan_rapat);

        $create = date("Y-m-d H:i:s");
        foreach ($dataZ as $key) {
            DB::table('tb_notifikasi')->insert(
                [  
                    'jenis' => 'Pekerjaan',
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_sender' => $request->id_user,
                    'id_receiver' => $key,  
                    'isi' => "Berita Acara Aanwijzing mohon ditandatangani",
                    'status' => 1,
                    'created_at' => $create,
                ]
            );

            $data3 = DB::table('tb_notifikasi')
                   ->where('jenis', 'Pekerjaan')
                   ->where('id_pekerjaan', $request->id_pekerjaan)
                   ->where('id_sender', $request->id_user)
                   ->where('id_receiver', $key)
                   ->where('isi', "Berita Acara Aanwijzing mohon ditandatangani")
                   ->where('created_at', $create)
                   ->first();

            $user = [
                "id_sender" => $request->id_user,
                "id_receiver" => $key,
            ];

            $message2 = [
                "id_notif" => $data3->id_notif,
                "id_pekerjaan" => $data3->id_pekerjaan,
                "id_sender" => $data3->id_sender,
                "id_receiver" => $data3->id_receiver,
                "jenis" => $data3->jenis,
                "isi" => $data3->isi,
                "status" => $data3->status,
                "created_at" => $data3->created_at,
                "updated_at" => $data3->updated_at,
            ];

            try {
                broadcast(new NotifEvents($user, $message2));
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return;
    }

    public function getBAAanwijzing($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan_aanwijzing_ba as a')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_aanwijzing_ba_peserta as p')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.nama','p.jabatan')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("nama" => $key->nama,"jabatan" => $key->jabatan));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_aanwijzing_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->leftjoin('tb_pekerjaan_vendor as z', 'p.id_vendor', '=', 'z.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->where('z.validasi', 2)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature','z.approve_pekerjaan','z.validasi')
                    ->groupBy('p.id_pekerjaan')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'no_ba' => $data->no_ba,
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'attachment' => explode(',', $data->attachment),
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'no_ba' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'attachment' => array(),
                'created_at' => "", 
            ));
        }

        return json_encode($dataFix[0]);
    }

    public function approveBAAanwijzingPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        if($request->tipe == "Pimpinan"){
            DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('pimpinan_rapat', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        } else if($request->tipe == "Penyedia") {
            DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('id_user', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->tipe == "Admin") {
            $cekPimpinan = DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPimpinan > 0){
                DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)
                ->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            $cekPenyedia = DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPenyedia > 0){
                DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        $cekPimpinanFix = DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)->where('sts_signature', 0)
                        ->count();
        $cekPenyediaFix = DB::table('tb_pekerjaan_aanwijzing_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('sts_signature', 0)->count();
        $idVendor = DB::table('tb_pekerjaan_vendor')->select('id_vendor')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('validasi', 2)->get();
        if($cekPimpinanFix == 0 && $cekPenyediaFix == 0){
            DB::table('tb_pekerjaan_aanwijzing_ba')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            ); 

            foreach ($idVendor as $key) {
                DB::table('tb_pekerjaan_penawaran')->insert(
                    [
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_vendor' => $key->id_vendor,
                        'penawaran' => 0,
                        'status_penawaran' => 0,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'b.id_pekerjaan', '=', 'p.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe', 'b.pimpinan_rapat')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ->pimpinan_rapat, $dataZ2->id);

        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();

                $judul;
                if(!empty($nama)){
                    $judul = $nama->nama_vendor;
                } else {
                    if($request->tipe == "Admin"){
                        $judul = "Admin";
                    } else if($request->tipe == "Pimpinan"){
                        $judul = "Ketua Panitia";
                    }
                }

                DB::table('tb_notifikasi')->insert(
                    [  
                        'jenis' => "Pekerjaan",
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_sender' => $request->id_user,
                        'id_receiver' => $key,  
                        'isi' => $judul." Menandatangani Berita Acara Aanwizjing",
                        'status' => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $data2 = DB::table('tb_notifikasi')
                    ->where('jenis', "Pekerjaan")
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('id_sender', $request->id_user)
                    ->where('id_receiver', $key)
                    ->where('isi', $judul." Menandatangani Berita Acara Aanwizjing")
                    ->first();

                $user = [
                    "id_sender" => $request->id_user,
                    "id_receiver" => $key,
                ];

                $message2 = [
                    "id_notif" => $data2->id_notif,
                    "id_pekerjaan" => $data2->id_pekerjaan,
                    "id_sender" => $data2->id_sender,
                    "id_receiver" => $data2->id_receiver,
                    "jenis" => $data2->jenis,
                    "isi" => $data2->isi,
                    "status" => $data2->status,
                    "created_at" => $data2->created_at,
                    "updated_at" => $data2->updated_at,
                ];
                try {
                    broadcast(new NotifEvents($user, $message2));
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menandatangani Berita Acara Aanwijzing",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        ); 

        return;
    }

    public function getPenyediaPT($id_pekerjaan) {

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_vendor as z')
                    ->where('z.id_pekerjaan', $id_pekerjaan)
                    ->where('z.validasi', 2)
                    ->select('z.id_vendor')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, $key->id_vendor);
        }

        $penyediaFix = implode(',',$penyediaFix);
        return $penyediaFix;
    }

    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }           
        return $hasil;
    }

    function hari_ini($hari){
     
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
     
            case 'Mon':         
                $hari_ini = "Senin";
            break;
     
            case 'Tue':
                $hari_ini = "Selasa";
            break;
     
            case 'Wed':
                $hari_ini = "Rabu";
            break;
     
            case 'Thu':
                $hari_ini = "Kamis";
            break;
     
            case 'Fri':
                $hari_ini = "Jumat";
            break;
     
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }
     
        return $hari_ini;
     
    }

    public function pdfAanwijzingPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as z')
                ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'z.id_pekerjaan', '=', 'a.id_pekerjaan')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('z.nama_pekerjaan','a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_aanwijzing_ba_peserta as p')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.nama','p.jabatan')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("nama" => $key->nama,"jabatan" => $key->jabatan));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_aanwijzing_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->leftjoin('tb_pekerjaan_vendor as z', 'p.id_vendor', '=', 'z.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->where('z.validasi', 2)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature','z.approve_pekerjaan','z.validasi')
                    ->groupBy('p.id_pekerjaan')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'nama_pekerjaan1' => strtoupper($data->nama_pekerjaan),
                'nama_pekerjaan2' => $data->nama_pekerjaan,
                'no_ba' => $data->no_ba,
                'hari' => $this->hari_ini(date("D", strtotime($data->created_at))),
                'tanggal' => $this->terbilang(date("d", strtotime($data->created_at))),
                'bulan' => $this->terbilang(date("m", strtotime($data->created_at))),
                'tahun' => $this->terbilang(date("Y", strtotime($data->created_at))),
                'tahun2' => date("Y", strtotime($data->created_at)),
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'nama_pekerjaan1' => "",
                'nama_pekerjaan2' => "",
                'no_ba' => "",
                'hari' => "",
                'tanggal' => "",
                'bulan' => "",
                'tahun' => "",
                'tahun2' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'created_at' => "", 
            ));
        }

        $dataFix = json_decode(json_encode($dataFix[0]), FALSE);
        view()->share('dataFix', $dataFix);

        $nama_pdf = $dataFix->id_pekerjaan . "-" . $dataFix->no_ba .".pdf";
        if ($id_pekerjaan) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfAanwijzing');

            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }



    public function getPenawaranPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $penawarans = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status_penawaran as status_penawaran')
            ->whereIn('a.status_penawaran', [0,1,2,3])
            ->where('j.nama_jadwal', 'Pemasukan Penawaran')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $penawarans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $penawarans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'penawarans' => $penawarans,
        ]);
    }

    public function getPenawaranPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $penawarans = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'b.id_vendor')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as d', 'p.id_pekerjaan', '=', 'd.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba', 'b.approve_pekerjaan as approve_pekerjaan', 'd.status_penawaran as status_penawaran', 'b.validasi as validasi')
            ->whereIn('b.validasi', [2])
            ->where('j.nama_jadwal', 'Pemasukan Penawaran')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $penawarans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $penawarans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'penawarans' => $penawarans,
        ]);
    }

    public function getPenawaranbyID($id_pekerjaan,$id_vendor,$id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $penawarans = DB::table('tb_vendor_primary as p')
            ->leftjoin('users_to_vendor as u', 'p.id_vendor', '=', 'u.id_vendor')
            // ->leftjoin('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
            // ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
            ->leftjoin('tb_pekerjaan_penawaran as q', 'p.id_vendor', '=', 'q.id_vendor')
            ->select('p.*','q.status_penawaran','q.penawaran','q.updated_at as created_ats','q.created_at as updated_ats')
            ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
            ->where('q.id_pekerjaan', $id_pekerjaan)
            ->where('u.id_user', $id_user)
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $penawarans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $penawarans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'penawarans' => $penawarans,
        ]);
    }

    public function getPenawaran($id_pekerjaan,$id_vendor, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $penawarans = DB::table('tb_vendor_primary as p')
            ->leftjoin('tb_pekerjaan_penawaran as q', 'p.id_vendor', '=', 'q.id_vendor')
            ->select('p.*','q.status_penawaran','q.penawaran','q.updated_at as created_ats','q.created_at as updated_ats')
            ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
            ->where('q.id_pekerjaan', $id_pekerjaan)
            ->where('p.status', 2)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $penawarans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $penawarans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'penawarans' => $penawarans,
        ]);
    }

    public function getFilePenawaranPT($id_pekerjaan, $id_user, $id_vendor, Request $request) {

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_penawaran as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->whereIn('p.id_vendor', array_map('intval', explode(',',$id_vendor)))
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.penawaran','v.jaminan_penawaran','v.status_penawaran','v.catatan_penawaran','v.validasi_penawaran')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            $dataFile = DB::table('tb_pekerjaan_penawaran as v')
                    ->leftjoin('tb_pekerjaan_penawaran_file as q', 'v.id_pekerjaan', '=', 'q.id_pekerjaan')
                    ->where('v.id_vendor', $key->id_vendor)
                    ->where('v.id_pekerjaan', $id_pekerjaan)
                    ->select('q.dokumen_penawaran','q.file_dokumen_penawaran')
                    ->get();
            $filedoc = array();
            foreach ($dataFile as $keyz) {
                array_push($filedoc, array(
                    'dokumen_penawaran' => $keyz->dokumen_penawaran,
                    'file_dokumen_penawaran' => $keyz->file_dokumen_penawaran,
                ));
            }

            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'penawaran' => $key->penawaran,
                'jaminan_penawaran' => $key->jaminan_penawaran,
                'status_penawaran' => $key->status_penawaran,
                'catatan_penawaran' => $key->catatan_penawaran,
                'validasi_penawaran' => $key->validasi_penawaran,
                'isi' => $filedoc
            ));
        }

        return $dataFix;
    }

    public function getFilePenawaranPTbyID($id_pekerjaan, $id_user, Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $id_user)
                ->select('id_vendor')
                ->first();

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_penawaran as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->where('p.id_vendor', $data->id_vendor)
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.penawaran','v.jaminan_penawaran','v.status_penawaran','v.catatan_penawaran','v.validasi_penawaran')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFile = DB::table('tb_pekerjaan_penawaran as v')
                ->leftjoin('tb_pekerjaan_penawaran_file as q', 'v.id_pekerjaan', '=', 'q.id_pekerjaan')
                ->where('v.id_vendor', $data->id_vendor)
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('q.dokumen_penawaran','q.file_dokumen_penawaran')
                ->get();
        $filedoc = array();
        foreach ($dataFile as $key) {
            array_push($filedoc, array(
                'dokumen_penawaran' => $key->dokumen_penawaran,
                'file_dokumen_penawaran' => $key->file_dokumen_penawaran,
            ));
        }

        $dataFix = array();
        foreach ($dataNama as $key) {
            array_push($dataFix, array(
                'nama_vendor' => $key->nama_vendor,
                'id_vendor' => $key->id_vendor,
                'penawaran' => $key->penawaran,
                'jaminan_penawaran' => $key->jaminan_penawaran,
                'status_penawaran' => $key->status_penawaran,
                'catatan_penawaran' => $key->catatan_penawaran,
                'validasi_penawaran' => $key->validasi_penawaran,
                'isi' => $filedoc
            ));
        }

        return $dataFix;
    }
    public function getFilePenawaranPTbyID2($id_pekerjaan, $id_user, Request $request) {

        $dataNama = DB::table('tb_pekerjaan_penawaran_file as p')
                ->where('p.id_pekerjaan', $id_pekerjaan)
                ->select('p.dokumen_penawaran','p.file_dokumen_penawaran')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            array_push($dataFix, array('dokumen' => $key->dokumen_penawaran, 'file_dokumen' => $key->file_dokumen_penawaran));
        }

        return $dataFix;
    }

    public function penawaranStorePT(Request $request) {
        $getIdVendor = DB::table('users_to_vendor')->where('id_user',$request->id_user)
                        ->first();
        $data = DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan',$request->id_pekerjaan)
                ->where('id_vendor',$getIdVendor->id_vendor)
                ->count();

        if($data == 1){
                DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan',$request->id_pekerjaan)
                ->where('id_vendor',$getIdVendor->id_vendor)->update(
                    [   
                        'penawaran' => $request->nilai_penawaran,
                        'jaminan_penawaran' => $request->nilai_jaminan_penawaran,
                        'status_penawaran' => 1,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        }
        
        // For Notif
        $dataCommite = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();

        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_vendor as s', 'p.id_pekerjaan', '=', 's.id_pekerjaan')
           ->leftjoin('users_to_vendor as v', 's.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor','s.validasi')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->where('s.validasi', 2)
           ->get();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataCommite->id_user_committe));
        array_push($id_sender_receiver, $dataZ2->id);
        foreach ($dataZ as $key) {
            array_push($id_sender_receiver, $key->id_user_vendor);
        }

        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();
              DB::table('tb_notifikasi')->insert(
                  [  
                      'jenis' => "Pekerjaan",
                      'id_pekerjaan' => $request->id_pekerjaan,
                      'id_sender' => $request->id_user,
                      'id_receiver' => $key,  
                      'isi' => $nama->nama_vendor." Memasukan Penawaran baru senilai Rp. ".$request->nilai_penawaran,
                      'status' => 1,
                      'created_at' => date("Y-m-d H:i:s"),
                  ]
              );

              $data2 = DB::table('tb_notifikasi')
                     ->where('jenis', "Pekerjaan")
                     ->where('id_pekerjaan', $request->id_pekerjaan)
                     ->where('id_sender', $request->id_user)
                     ->where('id_receiver', $key)
                     ->where('isi', $nama->nama_vendor." Memasukan Penawaran baru senilai Rp. ".$request->nilai_penawaran)
                     ->first();

              $user = [
                  "id_sender" => $request->id_user,
                  "id_receiver" => $key,
              ];

              $message2 = [
                  "id_notif" => $data2->id_notif,
                  "id_pekerjaan" => $data2->id_pekerjaan,
                  "id_sender" => $data2->id_sender,
                  "id_receiver" => $data2->id_receiver,
                  "jenis" => $data2->jenis,
                  "isi" => $data2->isi,
                  "status" => $data2->status,
                  "created_at" => $data2->created_at,
                  "updated_at" => $data2->updated_at,
              ];
              try {
                broadcast(new NotifEvents($user, $message2));
              } catch (\Exception $e) {
                  return $e->getMessage();
              }
            }
        }
        
        return;
    }

    public function getBAPenawaranPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as z')
                ->leftjoin('tb_pekerjaan_penawaran_ba as a', 'z.id_pekerjaan', '=', 'a.id_pekerjaan')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('z.nama_pekerjaan','a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_penawaran_ba_peserta as p')
                    ->leftjoin('users as u', 'p.id_user', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_user','s.first_name','s.last_name','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("id_user" => $key->id_user, "nama" => $key->first_name." ".$key->last_name,"jabatan" => "Anggota","signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_penawaran_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        $penawaranFix = array();
        $dataPenawaran = DB::table('tb_pekerjaan_penawaran as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.penawaran','p.jaminan_penawaran','p.validasi_penawaran')
                    ->get();
        foreach ($dataPenawaran as $key) {
            $dataFile = DB::table('tb_pekerjaan_penawaran_file as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.dokumen_penawaran')
                    ->get();

            $dataFileFix = array();
            foreach ($dataFile as $keyr) {
                array_push($dataFileFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"dokumen_penawaran" => $keyr->dokumen_penawaran));
            }
            if(count($dataFile) < 14){
                for ($i=0; $i < (14-count($dataFile)); $i++) { 
                    array_push($dataFileFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"dokumen_penawaran" => ""));
                }
            }

            array_push($penawaranFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"penawaran" => $key->penawaran,"jaminan_penawaran" => $key->jaminan_penawaran,"validasi_penawaran" => $key->validasi_penawaran,"validasi_penawaran" => $key->validasi_penawaran,"dokumen_penawaran" => $dataFileFix));
        } 

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'no_ba' => $data->no_ba,
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'penawaran' => $penawaranFix,
                'attachment' => explode(',', $data->attachment),
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'no_ba' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'penawaran' => array(),
                'attachment' => array(),
                'created_at' => "", 
            ));
        }

        return json_encode($dataFix[0]);
    }

    public function baPenawaranPT(Request $request) {

        $attachment = $request->file('attachment');
        if($attachment == null || $attachment == ''){
            $nameAtch = array();
        } else {
            $nameAtch = array();
            if (count($attachment) > 0) {
                foreach ($attachment as $atc) {
                    $extention = $atc->getClientOriginalExtension();
                    $attachName = str_random() . '.' . $extention;
                    array_push($nameAtch, $attachName);
                    $upload = $atc->move(public_path() . '/uploads/attachmentBA/',$attachName);
                }
            } else {
                
            }
        }

        $id_array = array();
        array_push($id_array, $request->pimpinan_rapat);
        $cek = DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('status', 0)->count();
        if($cek < 1){
            DB::table('tb_pekerjaan_penawaran_ba')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_ba' => $request->no_ba,
                    'lokasi' => $request->lokasi,
                    'pimpinan_rapat' => $request->pimpinan_rapat,
                    'sts_signature' => 0,
                    'jabatan_pimpinan' => $request->jabatan_pimpinan,
                    'dimulai_jam' => $request->dimulai_jam,
                    'hasil' => $request->hasil,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('status', 0)->update(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_ba' => $request->no_ba,
                    'lokasi' => $request->lokasi,
                    'pimpinan_rapat' => $request->pimpinan_rapat,
                    'sts_signature' => 0,
                    'jabatan_pimpinan' => $request->jabatan_pimpinan,
                    'dimulai_jam' => $request->dimulai_jam,
                    'hasil' => $request->hasil,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        foreach (explode(",",$request->peserta) as $key) {   
            DB::table('tb_pekerjaan_penawaran_ba_peserta')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_user' => $key,
                    'sts_signature' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        foreach (explode(",",$request->penyedia) as $key) {   
            $id = DB::table('users_to_vendor')->where('id_vendor',$key)
                ->select('id_user')->first();
            array_push($id_array, $id->id_user);
            DB::table('tb_pekerjaan_penawaran_ba_penyedia')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $key,
                    'id_user' => $id->id_user,
                    'sts_signature' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan',$request->id_pekerjaan)
                ->update(
                    [   
                        'status_penawaran' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

        DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan',$request->id_pekerjaan)
                ->update(
                    [   
                        'status_evaluasi' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        

        //For Notif
        $id_vendor_array = array_map('intval', explode(',',$request->id_vendor2));
        $datas = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', $id_vendor_array)
           ->get();
        $dataZ= array();
        foreach ($datas as $key) {
            array_push($dataZ, $key->id_user);
        }
        foreach (explode(",",$request->peserta) as $key) {
            array_push($dataZ, $key);
        }
        array_push($dataZ, $request->pimpinan_rapat);

        $create = date("Y-m-d H:i:s");
        foreach ($dataZ as $key) {
            DB::table('tb_notifikasi')->insert(
                [  
                    'jenis' => 'Pekerjaan',
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_sender' => $request->id_user,
                    'id_receiver' => $key,  
                    'isi' => "Berita Acara Penawaran mohon ditandatangani",
                    'status' => 1,
                    'created_at' => $create,
                ]
            );

            $data3 = DB::table('tb_notifikasi')
                   ->where('jenis', 'Pekerjaan')
                   ->where('id_pekerjaan', $request->id_pekerjaan)
                   ->where('id_sender', $request->id_user)
                   ->where('id_receiver', $key)
                   ->where('isi', "Berita Acara Penawaran mohon ditandatangani")
                   ->where('created_at', $create)
                   ->first();

            $user = [
                "id_sender" => $request->id_user,
                "id_receiver" => $key,
            ];

            $message2 = [
                "id_notif" => $data3->id_notif,
                "id_pekerjaan" => $data3->id_pekerjaan,
                "id_sender" => $data3->id_sender,
                "id_receiver" => $data3->id_receiver,
                "jenis" => $data3->jenis,
                "isi" => $data3->isi,
                "status" => $data3->status,
                "created_at" => $data3->created_at,
                "updated_at" => $data3->updated_at,
            ];

            try {
                broadcast(new NotifEvents($user, $message2));
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Membuat Berita Acara Penawaran",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function approveBAPenawaranPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        if($request->tipe == "Pimpinan"){
            DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('pimpinan_rapat', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        } else if($request->tipe == "Penyedia") {
            DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('id_user', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->tipe == "Peserta") {
            DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('id_user', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->tipe == "Admin") {

            $cekPimpinan = DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPimpinan > 0){
                DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            $cekPenyedia = DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPenyedia > 0){
                DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            $cekPeserta = DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPenyedia > 0){
                DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        $cekPimpinanFix = DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)->where('sts_signature', 0)
                        ->count();
        $cekPenyediaFix = DB::table('tb_pekerjaan_penawaran_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('sts_signature', 0)->count();
        $cekPesertaFix = DB::table('tb_pekerjaan_penawaran_ba_peserta')->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('sts_signature', 0)->count();
        $idVendor = DB::table('tb_pekerjaan_vendor')->select('id_vendor')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('validasi', 2)->get();
        if($cekPimpinanFix == 0 && $cekPenyediaFix == 0 && $cekPesertaFix == 0){
            DB::table('tb_pekerjaan_penawaran_ba')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            ); 
        }

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'b.id_pekerjaan', '=', 'p.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe', 'b.pimpinan_rapat')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ->pimpinan_rapat, $dataZ2->id);

        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();

                $judul;
                if(!empty($nama)){
                    $judul = $nama->nama_vendor;
                } else {
                    if($request->tipe == "Admin"){
                        $judul = "Admin";
                    } else if($request->tipe == "Pimpinan"){
                        $judul = "Ketua Panitia";
                    } else if($request->tipe == "Peserta"){
                        $judul = "Anggota Panitia";
                    }
                }

                DB::table('tb_notifikasi')->insert(
                    [  
                        'jenis' => "Pekerjaan",
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_sender' => $request->id_user,
                        'id_receiver' => $key,  
                        'isi' => $judul." Menandatangani Berita Acara Penawaran",
                        'status' => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $data2 = DB::table('tb_notifikasi')
                    ->where('jenis', "Pekerjaan")
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('id_sender', $request->id_user)
                    ->where('id_receiver', $key)
                    ->where('isi', $judul." Menandatangani Berita Acara Penawaran")
                    ->first();

                $user = [
                    "id_sender" => $request->id_user,
                    "id_receiver" => $key,
                ];

                $message2 = [
                    "id_notif" => $data2->id_notif,
                    "id_pekerjaan" => $data2->id_pekerjaan,
                    "id_sender" => $data2->id_sender,
                    "id_receiver" => $data2->id_receiver,
                    "jenis" => $data2->jenis,
                    "isi" => $data2->isi,
                    "status" => $data2->status,
                    "created_at" => $data2->created_at,
                    "updated_at" => $data2->updated_at,
                ];
                try {
                    broadcast(new NotifEvents($user, $message2));
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menandatangani Berita Acara Penawaran",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function dropdownvendorValidasiPT($id_pekerjaan) {
        $data = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_vendor as q', 'p.id_vendor', '=', 'q.id_vendor')
                ->where('q.id_pekerjaan', $id_pekerjaan)
                ->where('q.validasi', 2)
                ->select('p.nama_vendor','q.id_vendor')
                ->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_vendor,
                'text' => $key->nama_vendor,
            );
        }
        return $response;
    }

    public function uploadFilePenawaranPT(Request $request) {
        $getIdVendor = DB::table('users_to_vendor')->where('id_user',$request->id_user)
                        ->first();

        $filePenawaran = $request->file('filePenawaran');
        if(!empty($filePenawaran)){
            $imageNameFile = $request->id_pekerjaan."-".$request->dokumen.'-penawaran-pekerjaan.'.'pdf';

            $getFile = DB::table('tb_pekerjaan_penawaran_file')->where('id_pekerjaan',$request->id_pekerjaan)
                ->where('dokumen_penawaran',$request->dokumen)
                ->first();
            if(!empty($getFile)){
                if($getFile->file_dokumen_penawaran == Null || $getFile->file_dokumen_penawaran == ""){
                    
                } else {
                    if (file_exists(public_path()."/uploads/file_dokumen_penawaran/".$getFile->file_dokumen_penawaran)) {
                      unlink(public_path()."/uploads/file_dokumen_penawaran/".$getFile->file_dokumen_penawaran);
                    }
                }
            }

            // \File::put(public_path()."/uploads/file_dokumen_penawaran". '/' . $imageNameFile, $filePenawaran);
            $upload = $filePenawaran->move(public_path()."/uploads/file_dokumen_penawaran/",$imageNameFile);
            // move_uploaded_file($_FILES['filePenawaran']['tmp_name'], "uploads/file_dokumen_penawaran/".$imageNameFile);

            if(!empty($getFile)){
                if($getFile->file_dokumen_penawaran == Null || $getFile->file_dokumen_penawaran == ""){
                    DB::table('tb_pekerjaan_penawaran_file')
                    ->insert([   
                                'id_pekerjaan' => $request->id_pekerjaan,
                                'id_vendor' => $getIdVendor->id_vendor,
                                'dokumen_penawaran' => $request->dokumen,
                                'file_dokumen_penawaran' => $imageNameFile,
                                'created_at' => date("Y-m-d H:i:s"),
                            ]);

                    DB::table('tb_log_activity')->insert(
                        [
                            'username' => $request->username,
                            'fullname' => $request->fullname,
                            'ip' => request()->ip(),
                            'log' => "Menambah Penawaran ".$request->dokumen,
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                } else {
                    DB::table('tb_pekerjaan_penawaran_file')->where('id_pekerjaan',$request->id_pekerjaan)
                    ->where('dokumen_penawaran',$request->dokumen)
                    ->update([   
                                'id_pekerjaan' => $request->id_pekerjaan,
                                'id_vendor' => $getIdVendor->id_vendor,
                                'dokumen_penawaran' => $request->dokumen,
                                'file_dokumen_penawaran' => $imageNameFile,
                                'updated_at' => date("Y-m-d H:i:s"),
                            ]);
                }
            } else {
               DB::table('tb_pekerjaan_penawaran_file')
               ->insert([   
                           'id_pekerjaan' => $request->id_pekerjaan,
                           'id_vendor' => $getIdVendor->id_vendor,
                           'dokumen_penawaran' => $request->dokumen,
                           'file_dokumen_penawaran' => $imageNameFile,
                           'created_at' => date("Y-m-d H:i:s"),
                       ]); 

               DB::table('tb_log_activity')->insert(
                    [
                        'username' => $request->username,
                        'fullname' => $request->fullname,
                        'ip' => request()->ip(),
                        'log' => "Menambah Penawaran ".$request->dokumen,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }      
        }  

        return;
    }

    public function validasiPenawaranPT(Request $request) {

        if($request->jenis == "Terima"){
            DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $request->id_vendor)->update(
                [
                    'validasi_penawaran' => 2,
                    'catatan_penawaran' => $request->catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan', $request->id_pekerjaan)->where('id_vendor', $request->id_vendor)->update(
                [
                    'validasi_penawaran' => 1,
                    'catatan_penawaran' => $request->catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        // Notif
          // $nama = DB::table('users_to_vendor as v')
          //    ->select('v.id_user')
          //    ->where('v.id_vendor', $request->id_vendor)
          //    ->first();
          // DB::table('tb_notifikasi')->insert(
          //     [  
          //         'jenis' => "Pekerjaan",
          //         'id_pekerjaan' => $request->id_pekerjaan,
          //         'id_sender' => $request->id_user,
          //         'id_receiver' => $nama->id_user,  
          //         'isi' => "Penawaran Di ".$request->jenis,
          //         'status' => 1,
          //         'created_at' => date("Y-m-d H:i:s"),
          //     ]
          // );

          // $data2 = DB::table('tb_notifikasi')
          //        ->where('jenis', "Pekerjaan")
          //        ->where('id_pekerjaan', $request->id_pekerjaan)
          //        ->where('id_sender', $request->id_user)
          //        ->where('id_receiver', $nama->id_user)
          //        ->where('isi', "Penawaran Di ".$request->jenis)
          //        ->first();

          // $user = [
          //     "id_sender" => $request->id_user,
          //     "id_receiver" => $nama->id_user,
          // ];

          // $message2 = [
          //     "id_notif" => $data2->id_notif,
          //     "id_pekerjaan" => $data2->id_pekerjaan,
          //     "id_sender" => $data2->id_sender,
          //     "id_receiver" => $data2->id_receiver,
          //     "jenis" => $data2->jenis,
          //     "isi" => $data2->isi,
          //     "status" => $data2->status,
          //     "created_at" => $data2->created_at,
          //     "updated_at" => $data2->updated_at,
          // ];
          //   try {
          //        broadcast(new NotifEvents($user, $message2));
          //   } catch (\Exception $e) {
          //       return $e->getMessage();
          //   }
        return;
    }

    public function pdfPenawaranPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as z')
                ->leftjoin('tb_pekerjaan_penawaran_ba as a', 'z.id_pekerjaan', '=', 'a.id_pekerjaan')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('z.nama_pekerjaan','a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_penawaran_ba_peserta as p')
                    ->leftjoin('users as u', 'p.id_user', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_user','s.first_name','s.last_name','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("id_user" => $key->id_user, "nama" => $key->first_name." ".$key->last_name,"jabatan" => "Anggota","signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_penawaran_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        $penawaranFix = array();
        $dataPenawaran = DB::table('tb_pekerjaan_penawaran as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.penawaran','p.jaminan_penawaran','p.validasi_penawaran')
                    ->get();
        foreach ($dataPenawaran as $key) {
            $dataFile = DB::table('tb_pekerjaan_penawaran_file as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.dokumen_penawaran')
                    ->get();

            $dataFileFix = array();
            foreach ($dataFile as $keyr) {
                array_push($dataFileFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"dokumen_penawaran" => $keyr->dokumen_penawaran));
            }
            if(count($dataFile) < 14){
                for ($i=0; $i < (14-count($dataFile)); $i++) { 
                    array_push($dataFileFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"dokumen_penawaran" => ""));
                }
            }

            array_push($penawaranFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"penawaran" => $key->penawaran,"jaminan_penawaran" => $key->jaminan_penawaran,"validasi_penawaran" => $key->validasi_penawaran,"validasi_penawaran" => $key->validasi_penawaran,"dokumen_penawaran" => $dataFileFix));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'nama_pekerjaan1' => strtoupper($data->nama_pekerjaan),
                'nama_pekerjaan2' => $data->nama_pekerjaan,
                'no_ba' => $data->no_ba,
                'hari' => $this->hari_ini(date("D", strtotime($data->created_at))),
                'tanggal' => $this->terbilang(date("d", strtotime($data->created_at))),
                'bulan' => $this->terbilang(date("m", strtotime($data->created_at))),
                'tahun' => $this->terbilang(date("Y", strtotime($data->created_at))),
                'tahun2' => date("Y", strtotime($data->created_at)),
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'penawaran' => $penawaranFix,
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'nama_pekerjaan1' => "",
                'nama_pekerjaan2' => "",
                'no_ba' => "",
                'hari' => "",
                'tanggal' => "",
                'bulan' => "",
                'tahun' => "",
                'tahun2' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'penawaran' => array(),
                'created_at' => "", 
            ));
        }

        $dataFix = json_decode(json_encode($dataFix[0]), FALSE);
        view()->share('dataFix', $dataFix);

        $nama_pdf = $dataFix->id_pekerjaan . "-" . $dataFix->no_ba .".pdf";
        if ($id_pekerjaan) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfPenawaran');

            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }



    public function getEvaluasiPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $evaluasis = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_evaluasi as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status_penawaran as status_penawaran', 'b.status_evaluasi as status_evaluasi')
            ->whereIn('a.status_penawaran', [0,1,2,3])
            ->where('j.nama_jadwal', 'Evaluasi Penawaran')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $evaluasis['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $evaluasis['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'evaluasis' => $evaluasis,
        ]);
    }

    public function getEvaluasiPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $penawarans = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'b.id_vendor')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as d', 'p.id_pekerjaan', '=', 'd.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba', 'b.approve_pekerjaan as approve_pekerjaan', 'd.status_penawaran as status_penawaran')
            ->whereIn('b.approve_pekerjaan', [2])
            ->where('j.nama_jadwal', 'Evaluasi Penawaran')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $penawarans['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $penawarans['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'penawarans' => $penawarans,
        ]);
    }

    public function getFileNotulenPT($id_pekerjaan, $id_user, $id_vendor, Request $request) {

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_evaluasi as v', 'p.id_vendor', '=', 'v.pemenang')
                ->leftjoin('tb_pekerjaan_penawaran as w', 'v.id_pekerjaan', '=', 'w.id_pekerjaan')
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.notulen','v.file_notulen','v.status_evaluasi','w.penawaran')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            array_push($dataFix, array(
                'pemenang' => $key->nama_vendor,
                'penawaran' => $key->penawaran,
                'id_vendor' => $key->id_vendor,
                'notulen' => $key->notulen,
                'file_notulen' => $key->file_notulen,
                'status_evaluasi' => $key->status_evaluasi,
            ));
        } 

        return $dataFix;
    }

    public function getFileNotulenPTbyID($id_pekerjaan, $id_user, Request $request) {

        $data = DB::table('users_to_vendor')
                ->where('id_user', $id_user)
                ->select('id_vendor')
                ->first();

        $dataNama = DB::table('tb_vendor_primary as p')
                ->leftjoin('tb_pekerjaan_evaluasi as v', 'p.id_vendor', '=', 'v.pemenang')
                ->leftjoin('tb_pekerjaan_penawaran as w', 'v.id_pekerjaan', '=', 'w.id_pekerjaan')
                ->where('p.id_vendor', $data->id_vendor)
                ->where('v.id_pekerjaan', $id_pekerjaan)
                ->select('p.nama_vendor','p.id_vendor','v.notulen','v.file_notulen','v.status_evaluasi','w.penawaran')
                ->groupBy('p.nama_vendor')
                ->get();

        $dataFix = array();
        foreach ($dataNama as $key) {
            array_push($dataFix, array(
                'pemenang' => $key->nama_vendor,
                'penawaran' => $key->penawaran,
                'id_vendor' => $key->id_vendor,
                'notulen' => $key->notulen,
                'file_notulen' => $key->file_notulen,
                'status_evaluasi' => $key->status_evaluasi,
            ));
        }

        return $dataFix;
    }

    public function evaluasiStorePT(Request $request) {
        $data = DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan',$request->id_pekerjaan)
                ->count();

        print_r($data);

        if($data == 1){
            $file1 = $request->file('file1');
            if(!empty($file1)){

              $getFile = DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan',$request->id_pekerjaan)
                ->first();
              if($getFile->file_notulen == Null || $getFile->file_notulen == ""){
                
              } else {
                if (file_exists(public_path()."/uploads/file_dokumen_evaluasi/".$getFile->file_notulen)) {
                  unlink(public_path()."/uploads/file_dokumen_evaluasi/".$getFile->file_notulen);
                }
              }

              $imageNameFile = str_random(10).'-notulen-evaluasi-penawaran.'.'pdf';
              // \File::put(public_path()."/uploads/file_dokumen_evaluasi". '/' . $imageNameFile, $file1);
              $upload = $file1->move(public_path()."/uploads/file_dokumen_evaluasi/",$imageNameFile);
              // move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_dokumen_evaluasi/".$imageNameFile);

                DB::table('tb_pekerjaan_evaluasi')->where('id_pekerjaan',$request->id_pekerjaan)
                ->update(
                    [   
                        'notulen' => $request->content,
                        'pemenang' => $request->id_vendor,
                        'file_notulen' => $imageNameFile,
                        'status_evaluasi' => 1,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

                DB::table('tb_pekerjaan_penawaran')->where('id_pekerjaan',$request->id_pekerjaan)
                ->update(
                    [   
                        'status_penawaran' => 1,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname, 
                'ip' => request()->ip(),
                'log' => "Menambahkan Evaluasi Penawaran",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        
        // For Notif
        // $dataZ = DB::table('tb_message as m')
        //    ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
        //    ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
        //    ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
        //    ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe')
        //    ->where('p.id_pekerjaan', $request->id_pekerjaan)
        //    ->first();
        // print_r($dataZ);
        // $dataZ2 = DB::table('users as u')
        //    ->where('u.role', 'R000')
        //    ->first();
        // $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        // array_push($id_sender_receiver, $dataZ->id_user_vendor, $dataZ2->id);
        // foreach ($id_sender_receiver as $key) {
        //     if($request->id_user == $key){
              
        //     } else {
        //       $nama = DB::table('users_to_vendor as v')
        //          ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
        //          ->select('p.nama_vendor')
        //          ->where('v.id_user', $request->id_user)
        //          ->first();
        //       DB::table('tb_notifikasi')->insert(
        //           [  
        //               'jenis' => "Pekerjaan",
        //               'id_pekerjaan' => $request->id_pekerjaan,
        //               'id_sender' => $request->id_user,
        //               'id_receiver' => $key,  
        //               'isi' => $nama->nama_vendor." Memasukan Penawaran baru senilai Rp. ".$request->nilai_penawaran,
        //               'status' => 1,
        //               'created_at' => date("Y-m-d H:i:s"),
        //           ]
        //       );

        //       $data2 = DB::table('tb_notifikasi')
        //              ->where('jenis', "Pekerjaan")
        //              ->where('id_pekerjaan', $request->id_pekerjaan)
        //              ->where('id_sender', $request->id_user)
        //              ->where('id_receiver', $key)
        //              ->where('isi', $nama->nama_vendor." Memasukan Penawaran baru senilai Rp. ".$request->nilai_penawaran)
        //              ->first();

        //       $user = [
        //           "id_sender" => $request->id_user,
        //           "id_receiver" => $key,
        //       ];

        //       $message2 = [
        //           "id_notif" => $data2->id_notif,
        //           "id_pekerjaan" => $data2->id_pekerjaan,
        //           "id_sender" => $data2->id_sender,
        //           "id_receiver" => $data2->id_receiver,
        //           "jenis" => $data2->jenis,
        //           "isi" => $data2->isi,
        //           "status" => $data2->status,
        //           "created_at" => $data2->created_at,
        //           "updated_at" => $data2->updated_at,
        //       ];
        //       try {
                //  broadcast(new NotifEvents($user, $message2));
                // } catch (\Exception $e) {
                //     return $e->getMessage();
                // }
        //     }
        // }
        
        return;
    }



    public function getNegosiasiPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $negosiasis = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_evaluasi as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_negosiasi_ba as e', 'p.id_pekerjaan', '=', 'e.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status_penawaran as status_penawaran', 'b.status_evaluasi as status_evaluasi','e.status as status_ba_nego')
            ->whereIn('b.status_evaluasi', [0,1,2,3])
            ->where('j.nama_jadwal', 'Klarifikasi & Negosiasi')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $negosiasis['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $negosiasis['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'negosiasis' => $negosiasis,
        ]);
    }

    public function getNegosiasiPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $negosiasis = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_evaluasi as x', 'p.id_pekerjaan', '=', 'x.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'x.pemenang')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as d', 'p.id_pekerjaan', '=', 'd.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_negosiasi_ba as e', 'p.id_pekerjaan', '=', 'e.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba', 'b.approve_pekerjaan as approve_pekerjaan', 'd.status_penawaran as status_penawaran','x.status_evaluasi','e.status as status_ba_nego')
            ->whereIn('x.status_evaluasi', [2])
            ->where('j.nama_jadwal', 'Klarifikasi & Negosiasi')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $negosiasis['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $negosiasis['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'negosiasis' => $negosiasis,
        ]);
    }

    public function getBANegosiasiPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan_negosiasi_ba as a')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_negosiasi_ba_peserta as p')
                    ->leftjoin('users as u', 'p.id_user', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_user','s.first_name','s.last_name')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("id_user" => $key->id_user, "nama" => $key->first_name." ".$key->last_name,"jabatan" => "Anggota"));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_negosiasi_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'no_ba' => $data->no_ba,
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'attachment' => explode(',', $data->attachment),
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'no_ba' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'attachment' => array(),
                'created_at' => "", 
            ));
        }

        return json_encode($dataFix[0]);
    }

    public function baNegosiasiPT(Request $request) {

        $attachment = $request->file('attachment');
        
        if($attachment == null || $attachment == ''){
            $nameAtch = array();
        } else {
            $nameAtch = array();
            if (count($attachment) > 0) {
                foreach ($attachment as $atc) {
                    $extention = $atc->getClientOriginalExtension();
                    $attachName = str_random() . '.' . $extention;
                    array_push($nameAtch, $attachName);
                    $upload = $atc->move(public_path() . '/uploads/attachmentBA/',$attachName);
                }
            } else {
                
            }
        }

        $id_array = array();
        array_push($id_array, $request->pimpinan_rapat);
        $cek = DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('status', 0)->count();
        if($cek < 1){
            DB::table('tb_pekerjaan_negosiasi_ba')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_ba' => $request->no_ba,
                    'lokasi' => $request->lokasi,
                    'pimpinan_rapat' => $request->pimpinan_rapat,
                    'sts_signature' => 0,
                    'jabatan_pimpinan' => $request->jabatan_pimpinan,
                    'dimulai_jam' => $request->dimulai_jam,
                    'hasil' => $request->hasil,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('status', 0)->update(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_ba' => $request->no_ba,
                    'lokasi' => $request->lokasi,
                    'pimpinan_rapat' => $request->pimpinan_rapat,
                    'sts_signature' => 0,
                    'jabatan_pimpinan' => $request->jabatan_pimpinan,
                    'dimulai_jam' => $request->dimulai_jam,
                    'hasil' => $request->hasil,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }

        foreach (explode(",",$request->peserta) as $key) {   
            DB::table('tb_pekerjaan_negosiasi_ba_peserta')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_user' => $key,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }

        foreach (explode(",",$request->penyedia) as $key) {   
            $id = DB::table('users_to_vendor')->where('id_vendor',$key)
                ->select('id_user')->first();
            array_push($id_array, $id->id_user);
            DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_vendor' => $key,
                    'id_user' => $id->id_user,
                    'sts_signature' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );    
        }
        
        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname, 
                'ip' => request()->ip(),
                'log' => "Membuat Berita Acara Negosiasi",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        //For Notif
        $id_vendor_array = array_map('intval', explode(',',$request->id_vendor2));
        $datas = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', $id_vendor_array)
           ->get();
        $dataZ= array();
        foreach ($datas as $key) {
            array_push($dataZ, $key->id_user);
        }
        array_push($dataZ, $request->pimpinan_rapat);

        $create = date("Y-m-d H:i:s");
        foreach ($dataZ as $key) {
            DB::table('tb_notifikasi')->insert(
                [  
                    'jenis' => 'Pekerjaan',
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_sender' => $request->id_user,
                    'id_receiver' => $key,  
                    'isi' => "Berita Acara Klarifikasi & Negosiasi mohon ditandatangani",
                    'status' => 1,
                    'created_at' => $create,
                ]
            );

            $data3 = DB::table('tb_notifikasi')
                   ->where('jenis', 'Pekerjaan')
                   ->where('id_pekerjaan', $request->id_pekerjaan)
                   ->where('id_sender', $request->id_user)
                   ->where('id_receiver', $key)
                   ->where('isi', "Berita Acara Klarifikasi & Negosiasi mohon ditandatangani")
                   ->where('created_at', $create)
                   ->first();

            $user = [
                "id_sender" => $request->id_user,
                "id_receiver" => $key,
            ];

            $message2 = [
                "id_notif" => $data3->id_notif,
                "id_pekerjaan" => $data3->id_pekerjaan,
                "id_sender" => $data3->id_sender,
                "id_receiver" => $data3->id_receiver,
                "jenis" => $data3->jenis,
                "isi" => $data3->isi,
                "status" => $data3->status,
                "created_at" => $data3->created_at,
                "updated_at" => $data3->updated_at,
            ];

            try {
                broadcast(new NotifEvents($user, $message2));
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return;
    }

    public function approveBANegosiasiPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        if($request->tipe == "Pimpinan"){
            DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('pimpinan_rapat', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        } else if($request->tipe == "Penyedia") {
            DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('id_user', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->tipe == "Admin") {
            $cekPimpinan = DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPimpinan > 0){
                DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }

            $cekPenyedia = DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPenyedia > 0){
                DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                ->where('sts_signature', 0)->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        $cekPimpinanFix = DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)->where('sts_signature', 0)
                        ->count();
        $cekPenyediaFix = DB::table('tb_pekerjaan_negosiasi_ba_penyedia')->where('id_pekerjaan', $request->id_pekerjaan)
                        ->where('sts_signature', 0)->count();
        $idVendor = DB::table('tb_pekerjaan_vendor')->select('id_vendor')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('validasi', 2)->get();
        if($cekPimpinanFix == 0 && $cekPenyediaFix == 0){
            DB::table('tb_pekerjaan_negosiasi_ba')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            ); 
        }

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'b.id_pekerjaan', '=', 'p.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe', 'b.pimpinan_rapat')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ->pimpinan_rapat, $dataZ2->id);

        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();

                $judul;
                if(!empty($nama)){
                    $judul = $nama->nama_vendor;
                } else {
                    if($request->tipe == "Admin"){
                        $judul = "Admin";
                    } else if($request->tipe == "Pimpinan"){
                        $judul = "Ketua Panitia";
                    } else if($request->tipe == "Peserta"){
                        $judul = "Anggota Panitia";
                    }
                }

                DB::table('tb_notifikasi')->insert(
                    [  
                        'jenis' => "Pekerjaan",
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_sender' => $request->id_user,
                        'id_receiver' => $key,  
                        'isi' => $judul." Menandatangani Berita Acara Klarifikasi & Negosiasi",
                        'status' => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $data2 = DB::table('tb_notifikasi')
                    ->where('jenis', "Pekerjaan")
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('id_sender', $request->id_user)
                    ->where('id_receiver', $key)
                    ->where('isi', $judul." Menandatangani Berita Acara Klarifikasi & Negosiasi")
                    ->first();

                $user = [
                    "id_sender" => $request->id_user,
                    "id_receiver" => $key,
                ];

                $message2 = [
                    "id_notif" => $data2->id_notif,
                    "id_pekerjaan" => $data2->id_pekerjaan,
                    "id_sender" => $data2->id_sender,
                    "id_receiver" => $data2->id_receiver,
                    "jenis" => $data2->jenis,
                    "isi" => $data2->isi,
                    "status" => $data2->status,
                    "created_at" => $data2->created_at,
                    "updated_at" => $data2->updated_at,
                ];
                try {
                    broadcast(new NotifEvents($user, $message2));
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname, 
                'ip' => request()->ip(),
                'log' => "Menandatangani Berita Acara Negosiasi",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;
    }

    public function pdfNegosiasiPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as z')
                ->leftjoin('tb_pekerjaan_negosiasi_ba as a', 'z.id_pekerjaan', '=', 'a.id_pekerjaan')
                ->leftjoin('users as u', 'a.pimpinan_rapat', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('z.nama_pekerjaan','a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $dataV = DB::table('tb_pekerjaan_negosiasi_ba_penyedia as z')
                ->leftjoin('tb_vendor_primary as a', 'z.id_vendor', '=', 'a.id_vendor')
                ->select('a.nama_vendor',)
                ->where('z.id_pekerjaan', $id_pekerjaan)
                ->first();

        $pesertaFix = array();
        $dataPeserta = DB::table('tb_pekerjaan_negosiasi_ba_peserta as p')
                    ->leftjoin('users as u', 'p.id_user', '=', 'u.id')
                    ->leftjoin('persons as s', 's.id_person', '=', 'u.id_person')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_user','s.first_name','s.last_name')
                    ->get();
        foreach ($dataPeserta as $key) {
            array_push($pesertaFix, array("id_user" => $key->id_user, "nama" => $key->first_name." ".$key->last_name,"jabatan" => "Anggota"));
        }

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_negosiasi_ba_penyedia as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','p.id_user','p.signature','p.sts_signature')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"signature" => $key->signature,"sts_signature" => $key->sts_signature));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'nama_vendor' => $dataV->nama_vendor,
                'nama_pekerjaan1' => strtoupper($data->nama_pekerjaan),
                'nama_pekerjaan2' => $data->nama_pekerjaan,
                'no_ba' => $data->no_ba,
                'hari' => $this->hari_ini(date("D", strtotime($data->created_at))),
                'tanggal' => $this->terbilang(date("d", strtotime($data->created_at))),
                'bulan' => $this->terbilang(date("m", strtotime($data->created_at))),
                'tahun' => $this->terbilang(date("Y", strtotime($data->created_at))),
                'tahun2' => date("Y", strtotime($data->created_at)),
                'lokasi' => $data->lokasi,
                'id_pimpinan_rapat' => $data->pimpinan_rapat,
                'pimpinan_rapat' => $data->first_name." ".$data->last_name,
                'jabatan_pimpinan' => $data->jabatan_pimpinan,
                'signature' => $data->signature,
                'sts_signature_pimpinan' => $data->sts_signature,
                'dimulai_jam' => $data->dimulai_jam,
                'hasil' => $data->hasil, 
                'peserta' => $pesertaFix,
                'penyedia' => $penyediaFix,
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'nama_vendor' => "",
                'nama_pekerjaan1' => "",
                'nama_pekerjaan2' => "",
                'no_ba' => "",
                'hari' => "",
                'tanggal' => "",
                'bulan' => "",
                'tahun' => "",
                'tahun2' => "",
                'lokasi' => "",
                'id_pimpinan_rapat' => "",
                'pimpinan_rapat' => "",
                'jabatan_pimpinan' => "",
                'signature' => "",
                'sts_signature_pimpinan' => "",
                'dimulai_jam' => "",
                'hasil' => "", 
                'peserta' => array(),
                'penyedia' => array(),
                'created_at' => "", 
            ));
        }

        $dataFix = json_decode(json_encode($dataFix[0]), FALSE);
        view()->share('dataFix', $dataFix);

        $nama_pdf = $dataFix->id_pekerjaan . "-" . $dataFix->no_ba .".pdf";
        if ($id_pekerjaan) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isJavascriptEnabled'=> true]);
            // pass view file
            $pdf = PDF::loadView('pdfNegosiasi');

            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }



    public function getPemenangPT(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $pemenangs = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_evaluasi as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_pemenang as e', 'p.id_pekerjaan', '=', 'e.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss','j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status_penawaran as status_penawaran', 'b.status_evaluasi as status_evaluasi' , 'e.status as status_pemenang')
            ->whereIn('b.status_evaluasi', [0,1,2,3])
            ->where('j.nama_jadwal', 'Penetapan Pemenang')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $pemenangs['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $pemenangs['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'pemenangs' => $pemenangs,
        ]);
    }

    public function getPemenangPTVendor($id_user, Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'nama_pekerjaan');
        $type = $request->input('p.created_at', 'desc');
        $pemenangs = DB::table('tb_pekerjaan as p')
            ->leftjoin('tb_committee as c', 'p.id_committee', '=', 'c.id_committee')
            ->leftjoin('tb_pekerjaan_jadwal as j', 'p.id_pekerjaan', '=', 'j.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_vendor as b', 'p.id_pekerjaan', '=', 'b.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_evaluasi as x', 'p.id_pekerjaan', '=', 'x.id_pekerjaan')
            ->leftjoin('users_to_vendor as v', 'v.id_vendor', '=', 'x.pemenang')
            ->leftjoin('users as u', 'u.id', '=', 'v.id_user')
            ->leftjoin('tb_pekerjaan_aanwijzing_ba as a', 'p.id_pekerjaan', '=', 'a.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_penawaran as d', 'p.id_pekerjaan', '=', 'd.id_pekerjaan')
            ->leftjoin('tb_pekerjaan_pemenang as e', 'p.id_pekerjaan', '=', 'e.id_pekerjaan')
            ->select('p.*', 'c.*', 'p.created_at as created_ats', 'p.valid_from as valid_froms', 'p.valid_to as valid_tos', 'p.status as statuss', 'j.tgl_mulai','j.jam_mulai','j.tgl_selesai','j.jam_selesai', 'a.status as status_ba', 'b.approve_pekerjaan as approve_pekerjaan', 'd.status_penawaran as status_penawaran','x.status_evaluasi', 'e.status as status_pemenang')
            ->whereIn('x.status_evaluasi', [2])
            ->where('j.nama_jadwal', 'Penetapan Pemenang')
            ->where('p.metode', 'Pelelangan Terbatas')
            ->where('v.id_user', $id_user)
            ->where('p.nama_pekerjaan', 'LIKE', '%' . $search_query . '%')
            ->groupBy('p.id_pekerjaan')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $pemenangs['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $pemenangs['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'pemenangs' => $pemenangs,
        ]);
    }

    public function pemenangPT(Request $request) {

        $attachment = $request->file('attachment');
        if($attachment == null || $attachment == ''){
            $nameAtch = array();
        } else {
            $nameAtch = array();
            if (count($attachment) > 0) {
                foreach ($attachment as $atc) {
                    $extention = $atc->getClientOriginalExtension();
                    $attachName = str_random() . '.' . $extention;
                    array_push($nameAtch, $attachName);
                    $upload = $atc->move(public_path() . '/uploads/attachmentBA/',$attachName);
                }
            }
        }

        $id_array = array();
        array_push($id_array, $request->pimpinan_rapat);
        $cek = DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
        ->where('status', 0)->count();
        if($cek < 1){
            DB::table('tb_pekerjaan_pemenang')->insert(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_surat_dir' => $request->no_surat_dir,
                    'tgl_surat_dir' => $request->tgl_surat_dir,
                    'no_surat' => $request->no_surat,
                    'id_vendor' => $request->id_vendor2,
                    'harga_sepakat' => $request->kesepakatan,
                    'pelaksanaan' => $request->pelaksanaan,
                    'pemeliharaan' => $request->pemeliharaan,
                    'ketua_panitia' => $request->ketua_panitia,
                    'sts_signature' => 0,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('status', 0)->update(
                [
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'no_surat_dir' => $request->no_surat_dir,
                    'tgl_surat_dir' => $request->tgl_surat_dir,
                    'no_surat' => $request->no_surat,
                    'id_vendor' => $request->id_vendor2,
                    'harga_sepakat' => $request->kesepakatan,
                    'pelaksanaan' => $request->pelaksanaan,
                    'pemeliharaan' => $request->pemeliharaan,
                    'ketua_panitia' => $request->ketua_panitia,
                    'sts_signature' => 0,
                    'attachment' => implode(",",$nameAtch),
                    'status' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }
        
        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menetapkan Pemenang",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        ); 

        //For Notif
        $id_vendor_array = array_map('intval', explode(',',$request->id_vendor));
        $datas = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', $id_vendor_array)
           ->get();
        $namaV = DB::table('users_to_vendor as v')
           ->leftjoin('tb_vendor_primary as q', 'q.id_vendor', '=', 'v.id_vendor')
           ->select('v.id_user','q.nama_vendor')
           ->wherein('v.id_vendor', explode(',',$request->id_vendor2))
           ->first();
        $dataZ= array();
        foreach ($datas as $key) {
            array_push($dataZ, $key->id_user);
        }
        array_push($dataZ, $request->pimpinan_rapat);

        $create = date("Y-m-d H:i:s");
        foreach ($dataZ as $key) {
            DB::table('tb_notifikasi')->insert(
                [  
                    'jenis' => 'Pekerjaan',
                    'id_pekerjaan' => $request->id_pekerjaan,
                    'id_sender' => $request->id_user,
                    'id_receiver' => $key,  
                    'isi' => "Pemenang Lelang ".$namaV->nama_vendor,
                    'status' => 1,
                    'created_at' => $create,
                ]
            );

            $data3 = DB::table('tb_notifikasi')
                   ->where('jenis', 'Pekerjaan')
                   ->where('id_pekerjaan', $request->id_pekerjaan)
                   ->where('id_sender', $request->id_user)
                   ->where('id_receiver', $key)
                   ->where('isi', "Pemenang Lelang ".$namaV->nama_vendor)
                   ->where('created_at', $create)
                   ->first();

            $user = [
                "id_sender" => $request->id_user,
                "id_receiver" => $key,
            ];

            $message2 = [
                "id_notif" => $data3->id_notif,
                "id_pekerjaan" => $data3->id_pekerjaan,
                "id_sender" => $data3->id_sender,
                "id_receiver" => $data3->id_receiver,
                "jenis" => $data3->jenis,
                "isi" => $data3->isi,
                "status" => $data3->status,
                "created_at" => $data3->created_at,
                "updated_at" => $data3->updated_at,
            ];

            try {
                broadcast(new NotifEvents($user, $message2));
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return;
    }

    public function getPemenang($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan_pemenang as a')
                ->leftjoin('users as u', 'a.ketua_panitia', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $data2 = DB::table('tb_pekerjaan_pemenang as p')
                ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->leftjoin('tb_vendor_dokumen as w', 'p.id_vendor', '=', 'w.id_vendor')
                ->select('v.nama_vendor','v.alamat_vendor','w.npwp_vendor')
                ->where('p.id_pekerjaan', $id_pekerjaan)
                ->first();

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_vendor as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->leftjoin('users_to_vendor as q', 'p.id_vendor', '=', 'q.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','q.id_user','v.alamat_vendor')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"alamat_vendor" => $key->alamat_vendor));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'no_surat_dir' => $data->no_surat_dir,
                'tgl_surat_dir' => $data->tgl_surat_dir,
                'no_surat' => $data->no_surat,
                'ketua_panitia' => $data->ketua_panitia,
                'nama_ketua_panitia' => $data->first_name." ".$data->last_name,
                'signature' => $data->signature,
                'sts_signature' => $data->sts_signature,
                'nama_vendor' => $data2->nama_vendor,
                'alamat_vendor' => $data2->alamat_vendor,
                'npwp_vendor' => $data2->npwp_vendor,
                'kesepakatan' => $data->harga_sepakat,
                'pelaksanaan' => $data->pelaksanaan,
                'pemeliharaan' => $data->pemeliharaan,
                'penyedia' => $penyediaFix,
                'attachment' => explode(',', $data->attachment),
                'created_at' => $data->created_at, 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'no_surat_dir' => "",
                'tgl_surat_dir' => "",
                'no_surat' => "",
                'ketua_panitia' => "",
                'nama_ketua_panitia' => "",
                'signature' => "",
                'sts_signature' => "",
                'nama_vendor' => "",
                'alamat_vendor' => "",
                'npwp_vendor' => "",
                'kesepakatan' => "",
                'pelaksanaan' => "",
                'pemeliharaan' => "",
                'penyedia' => array(),
                'attachment' => array(),
                'created_at' => "", 
            ));
        }

        return json_encode($dataFix[0]);
    }

    public function approvePemenangPT(Request $request) {
        $image = $request->signature; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/signature';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        if($request->tipe == "Pimpinan"){
            DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('ketua_panitia', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        } else if($request->tipe == "Penyedia") {
            DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('id_user', $request->id_user)->update(
                [
                    'signature' => $imageName,
                    'sts_signature' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else if($request->tipe == "Admin") {
            $cekPimpinan = DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('sts_signature', 0)->count();
            if($cekPimpinan > 0){
                DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)
                ->update(
                    [
                        'signature' => $imageName,
                        'sts_signature' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }
        }

        $cekPimpinanFix = DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)->where('sts_signature', 0)
                        ->count();
        $idVendor = DB::table('tb_pekerjaan_vendor')->select('id_vendor')->where('id_pekerjaan', $request->id_pekerjaan)
            ->where('validasi', 2)->get();
        if($cekPimpinanFix == 0){
            DB::table('tb_pekerjaan_pemenang')->where('id_pekerjaan', $request->id_pekerjaan)->update(
                [
                    'status' => 2,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            ); 
        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menandatangani Penetapan Pemenang", 
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // Notif
        $dataZ = DB::table('tb_message as m')
           ->leftjoin('tb_pekerjaan as p', 'p.id_pekerjaan', '=', 'm.id_pekerjaan')
           ->leftjoin('tb_pekerjaan_aanwijzing_ba as b', 'b.id_pekerjaan', '=', 'p.id_pekerjaan')
           ->leftjoin('tb_committee as c', 'c.id_committee', '=', 'p.id_committee')
           ->leftjoin('users_to_vendor as v', 'p.id_vendor', '=', 'v.id_vendor')
           ->select('p.nama_pekerjaan','m.id_message', 'v.id_user as id_user_vendor', 'c.id_user as id_user_committe', 'b.pimpinan_rapat')
           ->where('p.id_pekerjaan', $request->id_pekerjaan)
           ->first();
        $dataZ2 = DB::table('users as u')
           ->where('u.role', 'R000')
           ->first();
        $id_sender_receiver = array_map('intval', explode(',',$dataZ->id_user_committe));
        array_push($id_sender_receiver, $dataZ->pimpinan_rapat, $dataZ2->id);

        foreach ($id_sender_receiver as $key) {
            if($request->id_user == $key){
              
            } else {
              $nama = DB::table('users_to_vendor as v')
                 ->leftjoin('tb_vendor_primary as p', 'p.id_vendor', '=', 'v.id_vendor')
                 ->select('p.nama_vendor')
                 ->where('v.id_user', $request->id_user)
                 ->first();

                $judul;
                if(!empty($nama)){
                    $judul = $nama->nama_vendor;
                } else {
                    if($request->tipe == "Admin"){
                        $judul = "Admin";
                    } else if($request->tipe == "Pimpinan"){
                        $judul = "Ketua Panitia";
                    } else if($request->tipe == "Peserta"){
                        $judul = "Anggota Panitia";
                    }
                }

                DB::table('tb_notifikasi')->insert(
                    [  
                        'jenis' => "Pekerjaan",
                        'id_pekerjaan' => $request->id_pekerjaan,
                        'id_sender' => $request->id_user,
                        'id_receiver' => $key,  
                        'isi' => $judul." Menandatangani Berita Acara Klarifikasi & Negosiasi",
                        'status' => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                $data2 = DB::table('tb_notifikasi')
                    ->where('jenis', "Pekerjaan")
                    ->where('id_pekerjaan', $request->id_pekerjaan)
                    ->where('id_sender', $request->id_user)
                    ->where('id_receiver', $key)
                    ->where('isi', $judul." Menandatangani Berita Acara Klarifikasi & Negosiasi")
                    ->first();

                $user = [
                    "id_sender" => $request->id_user,
                    "id_receiver" => $key,
                ];

                $message2 = [
                    "id_notif" => $data2->id_notif,
                    "id_pekerjaan" => $data2->id_pekerjaan,
                    "id_sender" => $data2->id_sender,
                    "id_receiver" => $data2->id_receiver,
                    "jenis" => $data2->jenis,
                    "isi" => $data2->isi,
                    "status" => $data2->status,
                    "created_at" => $data2->created_at,
                    "updated_at" => $data2->updated_at,
                ];
                try {
                    broadcast(new NotifEvents($user, $message2));
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        return;
    }

    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public function pdfPemenangPT($id_pekerjaan) {
        $data = DB::table('tb_pekerjaan as z')
                ->leftjoin('tb_pekerjaan_pemenang as a', 'z.id_pekerjaan', '=', 'a.id_pekerjaan')
                ->leftjoin('users as u', 'a.ketua_panitia', '=', 'u.id')
                ->leftjoin('persons as p', 'p.id_person', '=', 'u.id_person')
                ->select('z.nama_pekerjaan','a.*','p.first_name','p.last_name')
                ->where('a.id_pekerjaan', $id_pekerjaan)
                ->first();

        $data2 = DB::table('tb_pekerjaan_pemenang as p')
                ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                ->leftjoin('tb_vendor_dokumen as w', 'p.id_vendor', '=', 'w.id_vendor')
                ->select('v.nama_vendor','v.alamat_vendor','w.npwp_vendor')
                ->where('p.id_pekerjaan', $id_pekerjaan)
                ->first();

        $dataV = DB::table('tb_pekerjaan_negosiasi_ba_penyedia as z')
                ->leftjoin('tb_vendor_primary as a', 'z.id_vendor', '=', 'a.id_vendor')
                ->select('a.nama_vendor',)
                ->where('z.id_pekerjaan', $id_pekerjaan)
                ->first();

        $penyediaFix = array();
        $dataPenyedia = DB::table('tb_pekerjaan_vendor as p')
                    ->leftjoin('tb_vendor_primary as v', 'p.id_vendor', '=', 'v.id_vendor')
                    ->leftjoin('users_to_vendor as q', 'p.id_vendor', '=', 'q.id_vendor')
                    ->where('p.id_pekerjaan', $id_pekerjaan)
                    ->select('p.id_vendor','v.nama_vendor','q.id_user','v.alamat_vendor')
                    ->get();
        foreach ($dataPenyedia as $key) {
            array_push($penyediaFix, array("id_vendor" => $key->id_vendor,"nama_vendor" => $key->nama_vendor,"id_user" => $key->id_user,"alamat_vendor" => $key->alamat_vendor));
        }

        if(!empty($data)){
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => $data->id_pekerjaan,
                'nama_vendor' => $dataV->nama_vendor,
                'nama_pekerjaan1' => strtoupper($data->nama_pekerjaan),
                'nama_pekerjaan2' => $data->nama_pekerjaan,
                'no_surat_dir' => $data->no_surat_dir,
                'tgl_surat_dir' => $data->tgl_surat_dir,
                'no_surat' => $data->no_surat,
                'ketua_panitia' => $data->ketua_panitia,
                'nama_ketua_panitia' => $data->first_name." ".$data->last_name,
                'signature' => $data->signature,
                'sts_signature' => $data->sts_signature,
                'nama_vendor' => $data2->nama_vendor,
                'alamat_vendor' => $data2->alamat_vendor,
                'npwp_vendor' => $data2->npwp_vendor,
                'kesepakatan' => $data->harga_sepakat,
                'kesepakatan2' => $this->terbilang($data->harga_sepakat),
                'pelaksanaan' => $data->pelaksanaan,
                'pelaksanaan2' => $this->terbilang($data->pelaksanaan),
                'pemeliharaan' => $data->pemeliharaan,
                'pemeliharaan2' => $this->terbilang($data->pemeliharaan),
                'penyedia' => $penyediaFix,
                'created_at' => $this->tgl_indo(date("Y-m-d", strtotime($data->created_at))), 
                'created_at2' => date("Y", strtotime($data->created_at)), 
            ));
        } else {
            $dataFix = array();
            array_push($dataFix, array(
                'id_pekerjaan' => "",
                'nama_vendor' => "",
                'nama_pekerjaan1' => "",
                'nama_pekerjaan2' => "",
                'no_surat_dir' => "",
                'tgl_surat_dir' => "",
                'no_surat' => "",
                'ketua_panitia' => "",
                'nama_ketua_panitia' => "",
                'signature' => "",
                'sts_signature' => "",
                'nama_vendor' => "",
                'alamat_vendor' => "",
                'npwp_vendor' => "",
                'kesepakatan' => "",
                'kesepakatan2' => "",
                'pelaksanaan' => "",
                'pelaksanaan2' => "",
                'pemeliharaan' => "",
                'pemeliharaan2' => "",
                'penyedia' => array(),
                'created_at' => "", 
                'created_at2' => "", 
            ));
        }

        $dataFix = json_decode(json_encode($dataFix[0]), FALSE);
        view()->share('dataFix', $dataFix);

        $nama_pdf = $dataFix->id_pekerjaan . "-" . $dataFix->no_surat .".pdf";
        if ($id_pekerjaan) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isJavascriptEnabled'=> true]);
            // pass view file
            $pdf = PDF::loadView('pdfPemenang');

            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }
}
