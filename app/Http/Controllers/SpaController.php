<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class SpaController extends Controller
{
    public function admin($any=null)
    {
    	$data = DB::table('tb_role_user_access as r')
    	        ->select('a.Name as nameA', 'b.Name as nameB', 'c.Name as nameC')
    	        ->leftjoin('tb_menu_detail_a as a', 'r.Link', '=', 'a.Link')
    	        ->leftjoin('tb_menu_detail_b as b', 'r.Link', '=', 'b.Link')
    	        ->leftjoin('tb_menu_detail_c as c', 'r.Link', '=', 'c.Link')
    	        ->where('r.Link', "/".$any)->count();
    	if($any == "login" || $any == "forgotPass" ) {
            if(Session::get('login')){
                return redirect('/dashboard');
            }
            else{
                if($any == "login") {
                    return view('login'); 
                } else if($any == "forgotPass") {
                    return view('forgot');
                } 
            }
        } else if($data < 1 || $any == null){
            return view('welcomeLanding');
        } else {
    		return view('welcome');
    	}
    }
}
