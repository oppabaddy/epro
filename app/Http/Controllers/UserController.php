<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
// import file model Person
use App\Person;
use App\Exports\UserExport;
use DB;
use Excel;
use App\Events\PersonEvents;

class UserController extends Controller
{

    public function dropdownuser()
    {
        $data = Person::where('status',1)->where('status_user',0)->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id_person,
                      'text' => $key->first_name." ".$key->last_name,
                    );
        }
        return $response;
    } 
    public function dropdownuserPekerjaan()
    {
        $data = DB::table('users as u')
                ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
                ->select('u.id','p.first_name','p.last_name','p.id_divisi','u.status')
                ->where('u.status',1)
                ->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id,
                      'text' => $key->first_name." ".$key->last_name,
                    );
        }
        return $response;
    } 
    public function dropdownuserbyID($id_divisi)
    {
        $data = DB::table('users as u')
                ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
                ->select('u.id','p.first_name','p.last_name','p.id_divisi','u.status')
                ->where('p.id_divisi',$id_divisi)
                ->where('u.status',1)
                ->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id,
                      'text' => $key->first_name." ".$key->last_name,
                    );
        }
        return $response;
    } 
    public function dropdownuserto()
    {
        $data = DB::table('users')->where('status',1)->where('role', 'R002')->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id,
                      'text' => $key->username,
                    );
        }
        return $response;
    }
    public function dropdownuserAll()
    {
        $data = DB::table('users as u')
        ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
        ->select('u.id','u.role','p.first_name','p.last_name','p.id_divisi','u.status')
        ->where('u.status',1)->get();

        $response  = array();
        foreach ($data as $key) {
          if($key->role !== "R002"){
            $response[] = array(
                        'id' => $key->id,
                        'text' => $key->first_name." ".$key->last_name,
                      );
          }
        }
        return $response;
    }

    public function dropdownrole()
    {
        $data = DB::table('tb_role')->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->kd_role,
                      'text' => $key->role,
                    );
        }
        return $response;
    }

    public function user($id)
    {
      $data = DB::table('users')->select('users.*','persons.first_name','persons.last_name')
      ->join('persons', 'users.id_person', '=', 'persons.id_person')
      ->where('users.id', $id)
      ->first();
      return json_encode($data);  
    }

    public function generateuserpass($id_person)
    {
        $data = DB::table('persons')
                ->where('id_person', $id_person)
                ->first();

          $date = explode('-', $data->birth_date);
          $last = explode(' ', $data->last_name);
          
          $response  = array();
          $response[] = array(
                      'username' => strtolower($data->first_name).strtolower(substr($last[0],0,1)).$date[1].$date[2],
                      'password' => strtolower($data->first_name).strtolower(substr($last[0],0,1)).$date[1].$date[2],
                    );
        return $response;
    }

    public function roleByIDuser($id){
      $data = DB::table('tb_role_user_access')->where('UserID', $id)->get();
      
      $response = array();
      foreach ($data as $key) {
        $response[] = $key->MenuID."-".$key->DetailID."-".$key->DetailID2."-".$key->Link;
      }
      return $response; 
    }

    public function menuByIDuser($id){
      $data = DB::table('tb_role_user_access')->where('UserID', $id)->get();
      
      
      return $data; 
    }

    public function actionByID($id){
      $data = DB::table('tb_role_user_access')->where('UserID', $id)->get();
      
      $response = array();
      foreach ($data as $key) {
        $data2 = explode(",",$key->Action);
        if($key->Action == null || $key->Action == ""){
    
        } else {
          foreach ($data2 as $key2) {
            $response[] = $key->MenuID."-".$key->DetailID."-".$key->DetailID2."-".$key->Link."-".$key2;
          }
        }
      }
      return $response; 
    }

    //menambah data user
    public function store(Request $request)
    {   
      DB::table('users')->insert(
          [ 
            'id_person' => $request->id_person, 
            'username' => $request->username, 
            'password' => bcrypt($request->password), 
            'email' => $request->email, 
            'role' => $request->role, 
            'status' => 1, 
          ]
      );

      DB::table('persons')->where('id_person', $request->id_person)->update(
        [ 
          'status_user' => 1
        ]
      );

      $data = DB::table('users')
                ->where('id_person', $request->id_person)
                ->first();
      $dataMenu = DB::table('tb_role_master')
                ->where('kd_role', $request->role)
                ->get();

      foreach ($dataMenu as $key) {
        DB::table('tb_role_user_access')->insert(
            [ 
              'kd_role' => $key->kd_role, 
              'UserID' => $data->id, 
              'MenuID' => $key->MenuID, 
              'DetailID' => $key->DetailID,
              'DetailID2' => $key->DetailID2, 
              'Link' =>  $key->Link, 
              'Action' =>  $key->Action
            ]
        );
      }

      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username2,
              'fullname' => $request->fullname2,
              'ip' => request()->ip(),
              'log' => "Menambahkan User baru ".$request->username,
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );

      return;
    }
    
    // public function store(Request $request)
    // {   
    //   $destinationPath = public_path() . '/uploads/photo_profile';
    //   $img = file($destinationPath."/avatarDefault.png");
    //   $imageName =$request->username.".png";
      
    //   \File::put($destinationPath. '/' . $imageName, $img);

    //   Person::insert(
    //       [
    //         'photo_profile' => $imageName,
    //         'first_name' => $request->first_name,
    //         'last_name' => $request->last_name,
    //         'status' => 1,
    //         'status_user' => 1,
    //       ]
    //   );

    //   $id = DB::getPdo()->lastInsertId();

    //   DB::table('users')->insert(
    //       [ 
    //         'id_person' => $id, 
    //         'username' => $request->username, 
    //         'password' => bcrypt($request->password), 
    //         'email' => $request->email, 
    //         'role' => $request->role, 
    //         'status' => 1, 
    //       ]
    //   );
      
    //   $data = DB::table('users')
    //             ->where('id_person', $id)
    //             ->first();
    //   $dataMenu = DB::table('tb_role_master')
    //             ->where('kd_role', $request->role)
    //             ->get();

    //   foreach ($dataMenu as $key) {
    //     DB::table('tb_role_user_access')->insert(
    //         [ 
    //           'kd_role' => $key->kd_role, 
    //           'UserID' => $data->id, 
    //           'MenuID' => $key->MenuID, 
    //           'DetailID' => $key->DetailID,
    //           'DetailID2' => $key->DetailID2, 
    //           'Link' =>  $key->Link, 
    //           'Action' =>  $key->Action
    //         ]
    //     );
    //   }
      
    //   $task = Person::count();
    //   broadcast(new PersonEvents($task));

    //   return;
    // }

    public function storetovendor(Request $request)
    { 

        DB::table('users_to_vendor')->insert(
            [
              'id_user' => $request->id_user,
              'id_vendor' => $request->id_vendor,
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username2,
                'fullname' => $request->fullname2,
                'ip' => request()->ip(),
                'log' => "Menambahkan User to Vendor",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );
        return;  
    }

    public function delete($id, Request $request)
    {
      $data = DB::table('users')->where('id', $id)->first();
      
      // $dataP = DB::table('persons')->where('id_person', $data->id_person)->first();
      // DB::table('persons')->where('id_person', $data->id_person)->delete();
      // $destinationPathOld = public_path() . '/uploads/photo_profile/';
      // unlink($destinationPathOld.$dataP->photo_profile);
      
      DB::table('persons')->where('id_person', $data->id_person)->update(
        [ 
          'status_user' => 0
        ]
      );

      DB::table('users')->where('id', $id)->delete();
      DB::table('users_to_vendor')->where('id_user', $id)->delete();
      DB::table('tb_role_user_access')->where('UserID', $id)->delete();

      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username2,
              'fullname' => $request->fullname2,
              'ip' => request()->ip(),
              'log' => "Menghapus User",
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );

      return 204;
    }
    public function deletetovendor($id)
    {
      DB::table('users_to_vendor')->where('id', $id)->delete();
      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username2,
              'fullname' => $request->fullname2,
              'ip' => request()->ip(),
              'log' => "Menghapus User to Vendor",
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );
      return 204;
    }

    // mengubah data user
    public function updateUser($id, Request $request)
    {
      $id = $request->id;
      $user_name = $request->user_name;
      $email = $request->email;
      $role_code = $request->role_code;
      $role_master = $request->role_master;
      $role_action = $request->role_action;

      DB::table('users')->where('id', $id)->update(
        [ 
          'username' => $user_name,
          'email' => $email,
          'updated_at' => date("Y-m-d H:i:s"),
        ]
      );

      DB::table('tb_role_user_access')
        ->where('kd_role', $role_code)
        ->where('UserID', $id)
        ->delete();  

      $action2 = array();
      foreach ($role_master as $key) {
        $split = explode("-",$key);
        $action = array();
        foreach ($role_action as $keys) {
          $split2 = explode("-",$keys);
          if($split[3] == $split2[3]){
            array_push($action, $split2[4]);
          }
        }
        array_push($action2, $split[0]."-".$split[1]."-".$split[2]."-".$split[3]."-".implode(",",$action));
      }

      // echo json_encode($action2);
      // return; 
      foreach ($action2 as $keyz){
        $split3 = explode("-",$keyz);
        DB::table('tb_role_user_access')->insert(
            [
              'kd_role' => $role_code,
              'UserID' => $id,
              'MenuID' => $split3[0],
              'DetailID' => $split3[1],
              'DetailID2' => $split3[2],
              'Link' => $split3[3],
              'Action' => $split3[4]
            ]
        );
      }

      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username2,
              'fullname' => $request->fullname2,
              'ip' => request()->ip(),
              'log' => "Memperbarui User ".$user_name,
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );

      return;
    }

    public function updateProfile($id, Request $request)
    {
      $id = $request->id;
      $user_name = $request->user_name;
      $email = $request->email;
      $role_code = $request->role_code;
      $first_name = $request->first_name;
      $last_name = $request->last_name;
      $birth_date = $request->birth_date;
      $birth_place = $request->birth_place;

      $cekKOP = DB::table('users_to_vendor')->where('id_user', $id)->get();
      print_r($id);
      return;
      DB::table('users')->where('id', $id)->update(
        [ 
          'username' => $user_name,
          'email' => $email,
          'updated_at' => date("Y-m-d H:i:s"),
        ]
      );

      DB::table('tb_role_user_access')
        ->where('kd_role', $role_code)
        ->where('UserID', $id)
        ->delete();  

      $action2 = array(); 
      foreach ($role_master as $key) {
        $split = explode("-",$key);
        $action = array();
        foreach ($role_action as $keys) {
          $split2 = explode("-",$keys);
          if($split[3] == $split2[3]){
            array_push($action, $split2[4]);
          }
        }
        array_push($action2, $split[0]."-".$split[1]."-".$split[2]."-".$split[3]."-".implode(",",$action));
      }

      // echo json_encode($action2);
      // return; 
      foreach ($action2 as $keyz){
        $split3 = explode("-",$keyz);
        DB::table('tb_role_user_access')->insert(
            [
              'kd_role' => $role_code,
              'UserID' => $id,
              'MenuID' => $split3[0],
              'DetailID' => $split3[1],
              'DetailID2' => $split3[2],
              'Link' => $split3[3],
              'Action' => $split3[4]
            ]
        );
      }

      return;
    }

    public function updateUserActived($id, Request $request)
    {

      DB::table('users')
          ->where('id', $id)
          ->update(
            [ 
              'status' => 1,
            ]
          ); 

      return;
    }
    public function updateUserDeactive($id, Request $request)
    {

      DB::table('users')
          ->where('id', $id)
          ->update(
            [ 
              'status' => 2,
            ]
          ); 

      return;
    }

    // menampilkan datatable
    public function getUser( Request $request ) {
       $search_query = $request->searchTerm;
       $perPage      = $request->per_page;
       $field      = $request->input('field','username');
       $type      = $request->input('type','asc');
       $users        = DB::table('users')
                           ->leftjoin('persons', 'persons.id_person', '=', 'users.id_person')
                           ->leftjoin('tb_role', 'users.role', '=', 'tb_role.kd_role')
                           ->leftjoin('users_to_vendor', 'users.id', '=', 'users_to_vendor.id_user')
                           ->leftjoin('tb_vendor_primary', 'users_to_vendor.id_vendor', '=', 'tb_vendor_primary.id_vendor')
                           ->select('users.*', 'persons.first_name', 'persons.last_name', 'tb_role.role as nama_role', 'tb_vendor_primary.nama_vendor')
                           // ->where( 'users.status', '=', 1 )
                           ->where( 'users.username', 'LIKE', '%' . $search_query . '%' )
                           ->orwhere( 'tb_vendor_primary.nama_vendor', 'LIKE', '%' . $search_query . '%' )
                           ->orderBy($field, $type)
                           ->paginate( $perPage )
                           ->toArray();
                       
       if ( $search_query !== null) {
          $users['searchTerm'] = $search_query ?: '';
       } else if ( $search_query == null) {
          $users['searchTerm'] = $search_query ? null : '';
       }

       foreach($users['data'] as $key => $value)
       { 
          $getVendor = DB::table('users_to_vendor')
                        ->where('id_user', $users['data'][$key]->id)
                        ->first();

          if($getVendor !== null){
            $getVendorname = DB::table('tb_vendor_primary')
                        ->where('id_vendor', $getVendor->id_vendor)
                        ->first();
            $users['data'][$key]->nama_vendor = $getVendorname->nama_vendor;           
          } else {
            $users['data'][$key]->nama_vendor = "belum ada";
          }

       }

       return response()->json( [
          'users' => $users
       ] );
    }

    // menampilkan datatable
    public function getUsertoVendor( Request $request ) {
       $search_query = $request->searchTerm;
       $perPage      = $request->per_page;
       $field      = $request->input('field','username');
       $type      = $request->input('type','asc');
       $usersvendor        = DB::table('users_to_vendor')
                           ->leftjoin('users', 'users.id', '=', 'users_to_vendor.id_user')
                           ->leftjoin('tb_vendor_primary', 'tb_vendor_primary.id_vendor', '=', 'users_to_vendor.id_vendor')
                           ->leftjoin('tb_role', 'users.role', '=', 'tb_role.kd_role')
                           ->select('users_to_vendor.id', 'users.username', 'tb_vendor_primary.nama_vendor', 'tb_role.role as nama_role')
                           // ->where( 'users.status', '=', 1 )
                           ->where( 'users.username', 'LIKE', '%' . $search_query . '%' )
                           ->orderBy($field, $type)
                           ->paginate( $perPage )
                           ->toArray();
                       
       if ( $search_query !== null) {
          $usersvendor['searchTerm'] = $search_query ?: '';
       } else if ( $search_query == null) {
          $usersvendor['searchTerm'] = $search_query ? null : '';
       }

       return response()->json( [
          'usersvendor' => $usersvendor
       ] );
    }

    public function downloadExcelUser()
    {
        return Excel::download(new UserExport, 'Export User '.date("Y-m-d").'.xlsx');
    }

    
}