<?php
namespace App\Http\Controllers;

use App\Events\NotifEvents;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class BeritaPengumumanController extends Controller {

    public function getCountBerita(Request $request)
    {   
        $arrNamaBulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $response  = array();
        $cnt = array();

        if($request->filter == null || $request->filter == ''){
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_content_berita')
                        ->whereYear('created_at', '=', date('Y'))
                        ->whereMonth('created_at', '=', $key)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }
            $total = DB::table('tb_content_berita')
                    ->whereYear('created_at', '=', date('Y'))
                        ->count();
        } else {
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_content_berita')
                        ->whereMonth('created_at', '=', $key)
                        ->whereYear('created_at', '=', $request->filter)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }
            $total = DB::table('tb_content_berita')
                    ->whereYear('created_at', '=', $request->filter)
                        ->count();
        }   

        $response[] = array(
                        'total' => $total,
                        'data' => array( 
                                'name' => 'Berita',
                                'data' => $cnt,
                            )
                    );
        return $response;
    } 

    public function getCountPengumuman(Request $request)
    {   
        $arrNamaBulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $response  = array();
        $cnt = array();

        if($request->filter == null || $request->filter == ''){
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_content_pengumuman')
                        ->whereYear('created_at', '=', date('Y'))
                        ->whereMonth('created_at', '=', $key)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }
            $total = DB::table('tb_content_pengumuman')
                    ->whereYear('created_at', '=', date('Y'))
                        ->count();
        } else {
            foreach ($arrNamaBulan as $key) {
                $data = DB::table('tb_content_pengumuman')
                        ->whereMonth('created_at', '=', $key)
                        ->whereYear('created_at', '=', $request->filter)
                        ->count();
                array_push($cnt, array('x' => "Bulan - ".$key, 'y' => $data));
            }
            $total = DB::table('tb_content_pengumuman')
                    ->whereYear('created_at', '=', $request->filter)
                        ->count();
        }


        $response[] = array(
                        'total' => $total,
                        'data' => array( 
                                'name' => 'Pengumuman',
                                'data' => $cnt,
                            )
                    );
        return $response;
    } 

    public function getMaps($param) {
        // `https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=${lngLat}&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ`
        $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $param . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
        $response = Curl::to($url)
            ->withData()
            ->get();

        $data = json_decode($response, true);

        return response()->json([$data]);
    }

    public function insert_Berita(Request $request) {
        $images = $request->gambar;
        if (count($images) > 0) {
            foreach ($images as $img) {
                $exploded = explode(',', $img);
                $dec = base64_decode($exploded[1]);
                if (str_contains($exploded[0], 'jpeg')) {
                    $extention = 'jpg';
                } elseif (str_contains($exploded[0], 'gif')) {
                    $extention = 'gif';
                } else {
                    $extention = 'png';
                }
                $imageName = str_random() . '.' . $extention;
                $destinationPath = public_path() . '/uploads/berita/' . $imageName;
                file_put_contents($destinationPath, $dec);
            }
        } else {
            $destinationPath = public_path() . '/uploads/berita';
            $img = file($destinationPath . "/imgDefault.png");
            $imageName = str_random() . '.png';
            \File::put($destinationPath . '/' . $imageName, $img);
        }
        if ($request->flag == 2) {
            $publish = Carbon::now();
        } else {
            $publish = null;
        }

        $data = [
            'judul' => $request->judul,
            "content" => $request->content,
            "gambar" => $imageName,
            'id_kop' => $request->id_kop,
            "link_fb" => $request->link_fb,
            "link_instagram" => $request->link_instagram,
            "link_youtube" => $request->link_youtube,
            'flag' => $request->flag,
            'created_at' => Carbon::now(),
            'tanggal_publish' => $publish,
        ];

        $insert = DB::table('tb_content_berita')->insert($data);
        if ($insert) {
            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menambahkan Berita ".$request->judul,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }

    }

    public function update_berita(Request $request) {
        $imagesOld = $request->gambar;
        $images = $request->images;
        foreach ($images as $img) {
            $exploded = explode(',', $img);
            $dec = base64_decode($exploded[1]);
            if (str_contains($exploded[0], 'jpeg')) {
                $extention = 'jpg';
            } elseif (str_contains($exploded[0], 'gif')) {
                $extention = 'gif';
            } else {
                $extention = 'png';
            }
            $imageName = str_random() . '.' . $extention;
            $destinationPath = public_path() . '/uploads/berita/' . $imageName;
            unlink(public_path() . '/uploads/berita/' . $imagesOld);
            file_put_contents($destinationPath, $dec);
        }

        if (!empty($images)) {
            $data = [
                'judul' => $request->judul,
                "content" => $request->content,
                "gambar" => $imageName,
                'id_kop' => $request->id_kop,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'updated_at' => Carbon::now(),
            ];
        } else {
            $data = [
                'judul' => $request->judul,
                "content" => $request->content,
                'id_kop' => $request->id_kop,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'updated_at' => Carbon::now(),
            ];
        }

        $update = DB::table('tb_content_berita')->where('id', $request->id)->update($data);
        if ($update) {
            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Memperbarui Berita ".$request->judul,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getBerita(Request $request, $role) {
        $id = $request->id_kop;
        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'created_at');
        $type = $request->input('type', 'desc');

        if ($role == "R000") {
            $berita = DB::table('tb_content_berita')
                ->where('created_at', 'LIKE', '%' . $search_query . '%')
            // ->where('flag', 2)
            // ->orwhere('flag', 3)
            // ->orwhere('flag', 1)
                ->orderBy($field, $type)
                ->paginate($perPage)
                ->toArray();
        } else {
            $berita = DB::table('tb_content_berita')
                ->where('created_at', 'LIKE', '%' . $search_query . '%')
                ->where('id_kop', $id)
                ->orderBy($field, $type)
                ->paginate($perPage)
                ->toArray();
        }

        if ($search_query !== null) {
            $berita['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $berita['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'berita' => $berita,
        ]);
    }

    public function getBeritabyID($id) {
        $data = DB::table('tb_content_berita')->where('id', $id)->first();

        $data->gambarOld = $data->gambar;

        if (filter_var($data->gambar, FILTER_VALIDATE_URL)) {
            $data->gambar = str_replace("open", "uc", $data->gambar);
        } else {
            $path = public_path() . '/uploads/berita/' . $data->gambar;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->gambar = $src;
        }

        $dataID = DB::table('tb_content_berita')->get();
        $dataIDget = array();
        foreach ($dataID as $key) {
            array_push($dataIDget, $key->id);
        }

        $key = array_search($data->id, $dataIDget);
        $next = array_slice($dataIDget, $key + 1, 1);
        $prev = array_slice($dataIDget, $key - 1, 1);
        if (!empty($next)) {
            $data->next = $next[0];
        } else {
            $data->next = 0;
        }
        if (!empty($prev)) {
            $data->prev = $prev[0];
        } else {
            $data->prev = 0;
        }

        return json_encode($data);
    }

    public function detailBeritaShare() {
        $id = $_GET['id'];

        $data = DB::table('tb_content_berita')->where('id', $id)->first();

        $data->gambarOld = $data->gambar;

        if (filter_var($data->gambar, FILTER_VALIDATE_URL)) {
            $data->gambar = str_replace("open", "uc", $data->gambar);
        } else {
            $path = public_path() . '/uploads/berita/' . $data->gambar;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            // $data->gambar = $src;
            $data->gambar = $data->gambar;
        }

        $tipe = "Berita";
        $dataCom = DB::table('tb_comment as c')
            ->select('c.*', 'p.photo_profile')
            ->leftjoin('users as u', 'c.id_user', '=', 'u.id')
            ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
            ->where('c.id_post', $id)
            ->where('c.tipe', $tipe)
            ->get();

        $test = array();

        foreach ($dataCom as $key) {
            if ($key->photo_profile !== null) {
                if (filter_var($key->photo_profile, FILTER_VALIDATE_URL)) {
                    $key->photo_profile = str_replace("open", "uc", $key->photo_profile);
                } else {
                    $path = public_path() . '/uploads/photo_profile/' . $key->photo_profile;
                    $dataImg = file_get_contents($path);
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $base64 = base64_encode($dataImg);
                    $src = 'data:image/' . $type . ';base64,' . $base64;

                    $key->photo_profile = $src;
                }
            } else {
                $path = public_path() . '/uploads/photo_profile/avatarDefault.png';
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                $key->photo_profile = $src;
            }
        }

        foreach ($dataCom as $key) {
            array_push($test, $key);
        }
        return view('detailBeritaShare', ['dataFix' => $data, 'dataCom' => $test]);
    }

    public function deleteBerita($id, Request $request) {
        $data = DB::table('tb_content_berita')->where('id', $id)->first();
        if (!empty($data)) {
            $destinationPathOld = public_path() . '/uploads/berita/';
            unlink($destinationPathOld . $data->gambar);
            DB::table('tb_content_berita')->where('id', $id)->delete();
            DB::table('tb_comment')->where('id_post', $id)->delete();
        } else {

        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Berita",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function prosesBerita(Request $request) {
        $id = $request->id;
        $flag = $request->flag;
        if ($flag == 2) {
            $update = DB::table('tb_content_berita')->where('id', $id)->update(['flag' => $flag, 'tanggal_publish' => Carbon::now()]);

        } else {
            $update = DB::table('tb_content_berita')->where('id', $id)->update(['flag' => $flag, 'tanggal_publish' => null]);

        }

        if ($update) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }

    }

    public function getListBerita() {
        $test = array();
        array_push($test, array(
            "id" => "",
            "id_kop" => "",
            "gambar" => "",
            "judul" => "",
            "tanggal_publish" => "",
            "updated_at" => ""));
        $data = DB::table('tb_content_berita')
            ->select("id", "id_kop", "gambar", "judul", "tanggal_publish", "updated_at")
            ->where('flag', 2)->limit(3)->orderBy('tanggal_publish', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/berita/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/berita/' . $data[$key]->gambar;
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Berita')->count();
            $data[$key]->valComment = $dataComment;

            if (date("Y-m-d H:i:s") >= $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListBeritaAll($limit) {
        $test = array();
        if ($limit == 0) {
            array_push($test, array(
                "id" => "",
                "id_kop" => "",
                "gambar" => "",
                "judul" => "",
                "tanggal_publish" => "",
                "updated_at" => ""));
        }
        $data = DB::table('tb_content_berita')
            ->select("id", "id_kop", "gambar", "judul", "tanggal_publish", "updated_at")
            ->where('flag', 2)->offset($limit)->limit(6)->orderBy('tanggal_publish', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/berita/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/berita/' . $data[$key]->gambar;
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Berita')->count();
            $data[$key]->valComment = $dataComment;

            if (date("Y-m-d H:i:s") >= $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListBeritaAllCari($cari) {
        $test = array();
        array_push($test, array(
            "id" => "",
            "id_kop" => "",
            "gambar" => "",
            "judul" => "",
            "tanggal_publish" => "",
            "updated_at" => ""));

        if ($cari !== "kosong") {
            $data = DB::table('tb_content_berita')
                ->select("id", "id_kop", "gambar", "judul", "tanggal_publish", "updated_at")
                ->where('flag', 2)->where('judul', 'like', '%' . $cari . '%')->orderBy('tanggal_publish', 'desc')->get();
        }

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/berita/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/berita/' . $data[$key]->gambar;
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Berita')->count();
            $data[$key]->valComment = $dataComment;

            if (date("Y-m-d H:i:s") >= $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function insert_Pengumuman(Request $request) {
        $images = $request->gambar;
        if (count($images) > 0) {
            foreach ($images as $img) {
                $exploded = explode(',', $img);
                $dec = base64_decode($exploded[1]);
                if (str_contains($exploded[0], 'jpeg')) {
                    $extention = 'jpg';
                } elseif (str_contains($exploded[0], 'gif')) {
                    $extention = 'gif';
                } else {
                    $extention = 'png';
                }
                $imageName = str_random() . '.' . $extention;
                $destinationPath = public_path() . '/uploads/pengumuman/' . $imageName;
                file_put_contents($destinationPath, $dec);
            }
        } else {
            $destinationPath = public_path() . '/uploads/pengumuman';
            $img = file($destinationPath . "/imgDefault.png");
            $imageName = str_random() . '.png';
            \File::put($destinationPath . '/' . $imageName, $img);
        }

        if ($request->flag == 2) {
            $status = 'Publish';
            // $publish = Carbon::now();
            $publish = $request->tanggal;
        } else {

            $publish = null;
            $status = 'Draft';
        }

        $tgl = $request->tanggal;
        $tglPublish = $request->tanggal_publish;

        if (strpos($tgl, '/') !== false) {
            $tgl = substr($tgl, 6, 4) . '-' . substr($tgl, 3, 2) . '-' . substr($tgl, 0, 2);
        }

        if (strpos($tglPublish, '/') !== false) {
            $tglPublish = substr($tglPublish, 6, 4) . '-' . substr($tglPublish, 3, 2) . '-' . substr($tglPublish, 0, 2);
        }

        $start = $request->startTime;
        $end = $request->endTime;
        $jamPublish = $request->jamPublish;

        $data = [
            'judul' => $request->judul,
            "content" => $request->content,
            "gambar" => $imageName,
            'id_kop' => $request->id_kop,
            'tanggal' => $tgl,
            'jam_mulai' => $start,
            'jam_selesai' => $end,
            'alamat' => $request->alamat,
            "link_fb" => $request->link_fb,
            "link_instagram" => $request->link_instagram,
            "link_youtube" => $request->link_youtube,
            'flag' => $request->flag,
            'status' => $status,
            'created_at' => Carbon::now(),
            'tanggal_publish' => $tglPublish . ' ' . $jamPublish . ':00',
            'jam_publish' => $jamPublish . ':00',
        ];

        $insert = DB::table('tb_content_pengumuman')->insert($data);
        if ($insert) {

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Menambah Pengumuman baru ".$request->judul,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }

    }

    public function update_Pengumuman(Request $request) {
        $imagesOld = $request->gambar;
        $images = $request->images;
        foreach ($images as $img) {
            $exploded = explode(',', $img);
            $dec = base64_decode($exploded[1]);
            if (str_contains($exploded[0], 'jpeg')) {
                $extention = 'jpg';
            } elseif (str_contains($exploded[0], 'gif')) {
                $extention = 'gif';
            } else {
                $extention = 'png';
            }
            $imageName = str_random() . '.' . $extention;
            $destinationPath = public_path() . '/uploads/pengumuman/' . $imageName;
            unlink(public_path() . '/uploads/pengumuman/' . $imagesOld);
            file_put_contents($destinationPath, $dec);
        }

        $tgl = $request->tanggal;
        $tglPublish = $request->tanggal_publish;

        if (strpos($tgl, '/') !== false) {
            $tgl = substr($tgl, 6, 4) . '-' . substr($tgl, 3, 2) . '-' . substr($tgl, 0, 2);
        }

        if (strpos($tglPublish, '/') !== false) {
            $tglPublish = substr($tglPublish, 6, 4) . '-' . substr($tglPublish, 3, 2) . '-' . substr($tglPublish, 0, 2);
        }

        $start = $request->startTime;
        $end = $request->endTime;
        $jamPublish = $request->jamPublish;

        if (!empty($images)) {
            $data = [
                'judul' => $request->judul,
                "content" => $request->content,
                "gambar" => $imageName,
                'id_kop' => $request->id_kop,
                'tanggal' => $tgl,
                'waktu_event' => $request->waktu_event,
                'jam_mulai' => $start,
                'jam_selesai' => $end,
                'alamat' => $request->alamat,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'updated_at' => Carbon::now(),
                'tanggal_publish' => $tglPublish . ' ' . $jamPublish . ':00',
                'jam_publish' => $jamPublish . ':00',
            ];

        } else {
            $data = [
                'judul' => $request->judul,
                "content" => $request->content,
                'id_kop' => $request->id_kop,
                'tanggal' => $tgl,
                'waktu_event' => $request->waktu_event,
                'jam_mulai' => $start,
                'jam_selesai' => $end,
                'alamat' => $request->alamat,
                "link_fb" => $request->link_fb,
                "link_instagram" => $request->link_instagram,
                "link_youtube" => $request->link_youtube,
                'updated_at' => Carbon::now(),
                'tanggal_publish' => $tglPublish . ' ' . $jamPublish . ':00',
                'jam_publish' => $jamPublish . ':00',
            ];
        }

        $update = DB::table('tb_content_pengumuman')->where('id', $request->id)->update($data);
        if ($update) {

            DB::table('tb_log_activity')->insert(
                [
                    'username' => $request->username,
                    'fullname' => $request->fullname,
                    'ip' => request()->ip(),
                    'log' => "Memperbarui Pengumuman ".$request->judul,
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );

            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getPengumuman(Request $request, $role) {
        $id = $request->id_kop;
        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'id');
        $type = $request->input('type', 'asc');

        if ($role == "R000") {
            $pengumuman = DB::table('tb_content_pengumuman')
                ->where('created_at', 'LIKE', '%' . $search_query . '%')
            // ->where('flag', 2)
            // ->orwhere('flag', 3)
            // ->orwhere('flag', 1)
                ->orderBy($field, $type)
                ->paginate($perPage)
                ->toArray();
        } else {
            $pengumuman = DB::table('tb_content_pengumuman')
                ->where('created_at', 'LIKE', '%' . $search_query . '%')
                ->where('id_kop', $id)
                ->orderBy($field, $type)
                ->paginate($perPage)
                ->toArray();
        }

        if ($search_query !== null) {
            $pengumuman['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $pengumuman['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'pengumuman' => $pengumuman,
        ]);
    }

    public function getPengumumanbyID($id) {

        $data = DB::table('tb_content_pengumuman')->where('id', $id)->first();
        $data->gambarOld = $data->gambar;

        if (filter_var($data->gambar, FILTER_VALIDATE_URL)) {
            $data->gambar = str_replace("open", "uc", $data->gambar);
        } else {
            $path = public_path() . '/uploads/pengumuman/' . $data->gambar;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->gambar = $src;
        }

        if ($data->alamat !== null) {
            $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
            $response = Curl::to($url)
                ->withData()
                ->get();

            $alamat = json_decode($response, true);
            $data->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
        } else {
            $data->alamat2 = "No Location";
        }

        $dataID = DB::table('tb_content_pengumuman')->get();
        $dataIDget = array();
        foreach ($dataID as $key) {
            array_push($dataIDget, $key->id);
        }

        $key = array_search($data->id, $dataIDget);
        $next = array_slice($dataIDget, $key + 1, 1);
        $prev = array_slice($dataIDget, $key - 1, 1);
        if (!empty($next)) {
            $data->next = $next[0];
        } else {
            $data->next = 0;
        }
        if (!empty($prev)) {
            $data->prev = $prev[0];
        } else {
            $data->prev = 0;
        }

        return json_encode($data);
    }

    public function detailPengumumanShare() {
        $id = $_GET['id'];

        $data = DB::table('tb_content_pengumuman')->where('id', $id)->first();

        $data->gambarOld = $data->gambar;

        if (filter_var($data->gambar, FILTER_VALIDATE_URL)) {
            $data->gambar = str_replace("open", "uc", $data->gambar);
        } else {
            $path = public_path() . '/uploads/pengumuman/' . $data->gambar;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            // $data->gambar = $src;
            $data->gambar = $data->gambar;
        }

        if ($data->alamat !== null) {
            $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
            $response = Curl::to($url)
                ->withData()
                ->get();

            $alamat = json_decode($response, true);
            $data->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
        } else {
            $data->alamat2 = "No Location";
        }

        $tipe = "Pengumuman";
        $dataCom = DB::table('tb_comment as c')
            ->select('c.*', 'p.photo_profile')
            ->leftjoin('users as u', 'c.id_user', '=', 'u.id')
            ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
            ->where('c.id_post', $id)
            ->where('c.tipe', $tipe)
            ->get();

        $test = array();

        foreach ($dataCom as $key) {
            if ($key->photo_profile !== null) {
                if (filter_var($key->photo_profile, FILTER_VALIDATE_URL)) {
                    $key->photo_profile = str_replace("open", "uc", $key->photo_profile);
                } else {
                    $path = public_path() . '/uploads/photo_profile/' . $key->photo_profile;
                    $dataImg = file_get_contents($path);
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $base64 = base64_encode($dataImg);
                    $src = 'data:image/' . $type . ';base64,' . $base64;

                    $key->photo_profile = $src;
                }
            } else {
                $path = public_path() . '/uploads/photo_profile/avatarDefault.png';
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                $key->photo_profile = $src;
            }
        }

        foreach ($dataCom as $key) {
            array_push($test, $key);
        }
        return view('detailPengumumanShare', ['dataFix' => $data, 'dataCom' => $test]);
    }

    public function deletePengumuman($id, Request $request) {
        $data = DB::table('tb_content_pengumuman')->where('id', $id)->first();
        if (!empty($data)) {
            $destinationPathOld = public_path() . '/uploads/pengumuman/';
            unlink($destinationPathOld . $data->gambar);
            DB::table('tb_content_pengumuman')->where('id', $id)->delete();
            DB::table('tb_comment')->where('id_post', $id)->delete();
        } else {

        }

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Pengumuman",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return 204;
    }

    public function prosesPengumuman(Request $request) {
        $id = $request->id;
        $flag = $request->flag;
        $getTgl = DB::table('tb_content_pengumuman')->where('id', $id)->first();
        if ($flag == 2) {
            $update = DB::table('tb_content_pengumuman')->where('id', $id)->update(['keterangan' => null, 'flag' => $flag, 'tanggal_publish' => $getTgl->tanggal, 'status' => 'Publish']);

        } else if ($flag == 1) {
            $update = DB::table('tb_content_pengumuman')->where('id', $id)->update(['keterangan' => null, 'flag' => $flag, 'tanggal_publish' => null, 'status' => 'Draft']);
        } else {
            $alasan = $request->alasan;
            $update = DB::table('tb_content_pengumuman')->where('id', $id)->update(['keterangan' => $alasan, 'flag' => $flag, 'tanggal_publish' => null, 'status' => 'Unpublish']);

        }

        if ($update) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }

    }

    public function getListPengumuman() {
        $test = array();
        array_push($test, array(
            "id" => "",
            "id_kop" => "",
            "gambar" => "",
            "judul" => "",
            "tanggal" => "",
            "jam_mulai" => "",
            "jam_selesai" => "",
            "alamat" => "",
            "alamat2" => "",
            "updated_at" => date("Y-m-d H:i:s")));
        $data = DB::table('tb_content_pengumuman')
            ->select("id", "id_kop", "gambar", "judul", "tanggal", "jam_mulai", "jam_selesai", "alamat", "updated_at", "tanggal_publish")
            ->where('flag', 2)->limit(3)->orderBy('tanggal', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/pengumuman/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/pengumuman/' . $data[$key]->gambar;
            }

            if (date("Y-m-d H:i:s") >= $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }

            if ($data[$key]->alamat !== null) {
                $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data[$key]->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
                $response = Curl::to($url)
                    ->withData()
                    ->get();

                $alamat = json_decode($response, true);
                $data[$key]->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
            } else {
                $data[$key]->alamat2 = "No Location";
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Pengumuman')->count();
            $data[$key]->valComment = $dataComment;
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListPengumumanAll($limit) {
        $test = array();
        if ($limit == 0) {
            array_push($test, array(
                "id" => "",
                "id_kop" => "",
                "gambar" => "",
                "judul" => "",
                "tanggal" => "",
                "jam_mulai" => "",
                "jam_selesai" => "",
                "alamat" => "",
                "alamat2" => "",
                "updated_at" => ""));
        }
        $data = DB::table('tb_content_pengumuman')
            ->select("id", "id_kop", "gambar", "judul", "tanggal", "jam_mulai", "jam_selesai", "alamat", "updated_at", "tanggal_publish")
            ->where('flag', 2)->offset($limit)->limit(6)->orderBy('tanggal', 'desc')->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/pengumuman/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/pengumuman/' . $data[$key]->gambar;
            }

            if (date("Y-m-d H:i:s") > $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }

            if ($data[$key]->alamat !== null) {
                $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data[$key]->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
                $response = Curl::to($url)
                    ->withData()
                    ->get();

                $alamat = json_decode($response, true);
                $data[$key]->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
            } else {
                $data[$key]->alamat2 = "No Location";
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Pengumuman')->count();
            $data[$key]->valComment = $dataComment;
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListPengumumanAllCari($cari) {
        $test = array();
        array_push($test, array(
            "id" => "",
            "id_kop" => "",
            "gambar" => "",
            "judul" => "",
            "tanggal" => "",
            "jam_mulai" => "",
            "jam_selesai" => "",
            "alamat" => "",
            "alamat2" => "",
            "updated_at" => ""));

        if ($cari !== "kosong") {
            $data = DB::table('tb_content_pengumuman')
                ->select("id", "id_kop", "gambar", "judul", "tanggal", "jam_mulai", "jam_selesai", "alamat", "updated_at", "tanggal_publish")
                ->where('flag', 2)->where('judul', 'like', '%' . $cari . '%')->orderBy('tanggal', 'desc')->get();
        }

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->gambar, FILTER_VALIDATE_URL)) {
                $data[$key]->gambar = str_replace("open", "uc", $data[$key]->gambar);
            } else {
                $path = public_path() . '/uploads/pengumuman/' . $data[$key]->gambar;
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                // $data[$key]->gambar = $src;
                $data[$key]->gambar = '/uploads/pengumuman/' . $data[$key]->gambar;
            }

            if (date("Y-m-d H:i:s") > $data[$key]->tanggal_publish) {
                $data[$key]->publishDate = "lewat";
            } else {
                // $dateold = new DateTime($data[$key]->tanggal_publish);
                // $datenow = new DateTime(date("Y-m-d"));
                // $data[$key]->publishDate = $dateold->diff($datenow)->days;
                $data[$key]->publishDate = "belum";
            }

            if ($data[$key]->alamat !== null) {
                $url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?prox=' . $data[$key]->alamat . '&mode=retrieveAddresses&maxresults=1&gen=9&app_id=BdrutESIfQQBUCsq70ES&app_code=NREmJXPu7Cqhwa59QgWqrQ';
                $response = Curl::to($url)
                    ->withData()
                    ->get();

                $alamat = json_decode($response, true);
                $data[$key]->alamat2 = $alamat['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
            } else {
                $data[$key]->alamat2 = "No Location";
            }

            $dataComment = DB::table('tb_comment')
                ->where('id_post', $data[$key]->id)->where('tipe', 'Pengumuman')->count();
            $data[$key]->valComment = $dataComment;
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function addComment(Request $request) {
        $email = $request->email;
        $id_post = $request->id_post;
        $id_user = $request->id_user;
        $role = $request->role;
        $username = $request->username;
        $nama = $request->nama;
        $comment = $request->comment;
        $tipe = $request->tipe;

        if ($id_user == null) {
            $id_kop = null;
            $id_user = null;
        }
        if ($role !== "R00") {
            $getId = DB::table('users_to_vendor')->where('id_user', $id_user)->first();
            if (!empty($getId)) {
                $id_kop = $getId->id_kop;
            } else {
                $id_kop = null;
            }
        } else {
            $id_kop = null;
        }

        $data = [
            'id_post' => $id_post,
            'id_kop' => $id_kop,
            "id_user" => $id_user,
            "nama" => $nama,
            'email' => $email,
            "tipe" => $tipe,
            "comment" => $comment,
            "flag" => 1,
            'created_at' => Carbon::now(),
        ];

        $insert = DB::table('tb_comment')->insert($data);
        if ($insert) {
            $task = DB::table('tb_comment')->get();
            broadcast(new NotifEvents($task));
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function getCommentbyID($id, $tipe) {
        $data = DB::table('tb_comment as c')
            ->select('c.*', 'p.photo_profile')
            ->leftjoin('users as u', 'c.id_user', '=', 'u.id')
            ->leftjoin('persons as p', 'u.id_person', '=', 'p.id_person')
            ->where('c.id_post', $id)
            ->where('c.tipe', $tipe)
            ->orderBy('c.created_at', 'desc')
            ->get();

        $test = array();
        array_push($test, array(
            "id" => "",
            "id_kop" => "",
            "id_post_" => "",
            "id_user" => "",
            "comment" => "",
            "created_at" => "",
            "email" => "",
            "flag" => "",
            "nama" => "",
            "photo_profile" => "",
            "tipe" => "",
            "updated_at" => ""));

        foreach ($data as $key) {
            if ($key->photo_profile !== null) {
                if (filter_var($key->photo_profile, FILTER_VALIDATE_URL)) {
                    $key->photo_profile = str_replace("open", "uc", $key->photo_profile);
                } else {
                    $path = public_path() . '/uploads/photo_profile/' . $key->photo_profile;
                    $dataImg = file_get_contents($path);
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $base64 = base64_encode($dataImg);
                    $src = 'data:image/' . $type . ';base64,' . $base64;

                    $key->photo_profile = $src;
                }
            } else {
                $path = public_path() . '/uploads/photo_profile/avatarDefault.png';
                $dataImg = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $base64 = base64_encode($dataImg);
                $src = 'data:image/' . $type . ';base64,' . $base64;

                $key->photo_profile = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return json_encode($test);

        // return json_encode($data);
    }

    public function getCommentbyIDAdm($id) {
        $data = DB::table('tb_comment as c')
            ->select('c.*')
            ->where('c.id', $id)
            ->orderBy('c.created_at', 'desc')
            ->first();

        if ($data->tipe == 'Pengumuman') {
            $getJudul = DB::table('tb_content_pengumuman')
                ->where('id', $data->id_post)
                ->first();
        } else if ($data->tipe == 'Berita') {
            $getJudul = DB::table('tb_content_berita')
                ->where('id', $data->id_post)
                ->first();
        } else if ($data->tipe == 'Pesan') {
            $getJudul = DB::table('pesan_baik')
                ->where('id', $data->id_post)
                ->first();
        } else if ($data->tipe == 'Praktik') {
            $getJudul = DB::table('praktik_baik')
                ->where('id', $data->id_post)
                ->first();
        }

        if ($getJudul !== null) {
            $data->judul_post = $getJudul->judul;
        } else {
            $data->judul_post = "belum ada";
        }

        return json_encode($data);
    }

    public function updateComment(Request $request) {
        $cekUser = DB::table('tb_comment')->where('id', $request->id)->first();
        $cekAdm = DB::table('users')->where('id', $cekUser->id_user)->first();

        if ($cekAdm && $cekAdm->role == 'R000') {
            $data = [
                'comment' => $request->comment,
                'edited_by' => 'Admin',
                'updated_at' => Carbon::now(),
            ];
        } else {
            $data = [
                'comment' => $request->comment,
                'updated_at' => Carbon::now(),
            ];
        }

        $update = DB::table('tb_comment')->where('id', $request->id)->update($data);

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Memperbarui Comment",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return $update;
    }

    public function deleteComment($id, Request $request) {
        $delete = DB::table('tb_comment')->where('id', $id)->delete();

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menghapus Comment",
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        return $delete;
    }

    public function getAllComment(Request $request) {
        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'username');
        $type = $request->input('type', 'asc');
        $comments = DB::table('tb_comment as c')
            ->leftjoin('tb_content_pengumuman as p', 'c.id_post', '=', 'p.id')
            ->leftjoin('tb_content_berita as b', 'c.id_post', '=', 'b.id')
            ->select('c.*')
            ->where('c.comment', 'LIKE', '%' . $search_query . '%')
            ->orwhere('c.tipe', 'LIKE', '%' . $search_query . '%')
            ->orwhere('c.nama', 'LIKE', '%' . $search_query . '%')
            ->orwhere('p.judul', 'LIKE', '%' . $search_query . '%')
            ->orwhere('b.judul', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $comments['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $comments['searchTerm'] = $search_query ? null : '';
        }

        foreach ($comments['data'] as $key => $value) {
            if ($comments['data'][$key]->tipe == 'Pengumuman') {
                $getJudul = DB::table('tb_content_pengumuman')
                    ->where('id', $comments['data'][$key]->id_post)
                    ->first();
            } else if ($comments['data'][$key]->tipe == 'Berita') {
                $getJudul = DB::table('tb_content_berita')
                    ->where('id', $comments['data'][$key]->id_post)
                    ->first();
            }

            if ($getJudul !== null) {
                $comments['data'][$key]->judul_post = $getJudul->judul;
            } else {
                $comments['data'][$key]->judul_post = "belum ada";
            }
        }

        return response()->json([
            'comments' => $comments,
        ]);
    }

}
