<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPass;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Location;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginPost(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $token = bcrypt($request->username . Carbon::now());

        $data = DB::table('users')
            ->join('persons', 'persons.id_person', '=', 'users.id_person')
            ->join('tb_role', 'users.role', '=', 'tb_role.kd_role')
            ->select('users.*', 'persons.id_divisi', 'persons.id_jabatan', 'persons.first_name', 'persons.last_name', 'tb_role.role as nama_role')
            ->where('username', $username)
            ->where('users.status', 1)
            ->first();

        $clientIP = request()->ip();

        if ($data) {
            if (Hash::check($password, $data->password)) {

                $IPloc = Location::get($clientIP);

                DB::table('users')->where('id', '=', $data->id)->update(['active_login' => 1, 'ip' => $clientIP, 'ip_location' => 0]);
                Session::put('id', $data->id);
                Session::put('id_person', $data->id_person);
                Session::put('username', $data->username);
                Session::put('email', $data->email);
                Session::put('role', $data->role);
                Session::put('nama_role', $data->nama_role);
                Session::put('id_divisi', $data->id_divisi);
                Session::put('id_jabatan', $data->id_jabatan);
                Session::put('first_name', $data->first_name);
                Session::put('last_name', $data->last_name);
                // Session::put('photo',$data->photo_profile);
                Session::put('login', true);
                // return Session::all();
                // return redirect('dashboard');
                
                DB::table('tb_log_activity')->insert(
                    [
                        'username' => $data->username,
                        'fullname' => $data->first_name." ".$data->last_name,
                        'ip' => $clientIP,
                        'log' => "Berhasil Login",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );

                if ($data->role == "R000") {
                    return redirect('/dashboard');
                } else {
                    return redirect('/');
                }
            } else {
                DB::table('tb_log_activity')->insert(
                    [
                        'username' => $data->username,
                        'fullname' => $data->first_name." ".$data->last_name,
                        'ip' => $clientIP,
                        'log' => "Gagal Login. Password atau Username, Salah !",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
                return redirect('login')->with('alert', 'Password atau Username, Salah !');
            }
        } else {
            DB::table('tb_log_activity')->insert(
                [
                    'username' => $data->username,
                    'fullname' => $data->first_name." ".$data->last_name,
                    'ip' => $clientIP,
                    'log' => "Gagal Login. Password atau Username Tidak Ada, Salah!",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
            return redirect('login')->with('alert', 'Password atau Username Tidak Ada, Salah!');
        }
    }

    public function loginMobile(Request $request, User $user)
    {
        $username = $request->username;
        $password = $request->password;
        $token = bcrypt($request->username . Carbon::now());
        $data = DB::table('users')
            ->join('persons', 'persons.id_person', '=', 'users.id_person')
            ->select('users.*', 'persons.position', 'persons.first_name', 'persons.last_name')
            ->where('username', $username)
            ->where('users.status', 1)
            ->first();
        if ($data != null) {

            if (Hash::check($password, $data->password)) {
                DB::table('users')->where('id', '=', $data->id)->update(['api_token' => $token]);
                $dataApi = DB::table('users')
                    ->join('persons', 'persons.id_person', '=', 'users.id_person')
                    ->join('users_to_kop', 'users_to_kop.id_user', '=', 'users.id')
                    ->select('users.*', 'persons.position')
                    ->where('username', $username)->first();

                if ($dataApi != null) {
                    return response()->json(
                        [
                            'token' => $dataApi->api_token,
                            'user' => $dataApi,
                            'login' => true,
                            'meta' => [
                                'success' => 1,
                                'message' => 'Success',
                            ],
                        ],
                        200
                    );
                }
            } else {
                return response()->json(
                    ['meta' =>
                        [
                            'success' => 0,
                            'message' => 'Username atau Password salah',
                        ],
                    ],
                    401
                );

            }
        } else {
            return response()->json(
                ['meta' =>
                    [
                        'success' => 0,
                        'message' => 'Username Anda Tidak Ada',
                    ],
                ], 401);
        }
    }

    public function session(Request $request)
    {
        return json_encode(Session::all());
    }

    public function forgotPost(Request $request)
    {
        $username = $request->username;
        $email = $request->email;

        $data = DB::table('users')
            ->where('username', $username)
            ->where('email', $email)
            ->where('status', 1)
            ->first();

        if (empty($data)) {
            return redirect('forgotPass')->with('alert', 'Gagal! Silahkan cek username dan email anda');
        }

        DB::table('users')->where('username', $username)->where('email', $email)->update(
            [
                'password' => bcrypt($data->username),
            ]
        );

        Mail::to($email)->send(new ForgotPass($data));
        return redirect('forgotPass')->with('alert-success', 'Berhasil! Silahkan cek email anda');
    }

    public function logout()
    {
        if (DB::table('users')
            ->where('username', Session::get('username'))
            ->update(['active_login' => "0"])) {
            Session::flush();
            return redirect('/')->with('alert', 'Kamu sudah logout');
        } else {
            Session::flush();
            return redirect('/')->with('alert', 'Kamu sudah logout');
        }
    }
}
