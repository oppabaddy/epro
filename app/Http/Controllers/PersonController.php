<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
// import file model Person
use App\Person;
use App\Imports\PersonImport;
use App\Exports\PersonExport;
use DB;
use Excel;
use PDF;

use App\Events\PersonEvents;

class PersonController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return Person::all();
    }

    public function getCountPerson()
    {
        return Person::count();
    }

    public function dropdownperson()
    {
        $data = where('status',1)->get();

        $response  = array();
        foreach ($data as $key) {
          $response[] = array(
                      'id' => $key->id_person,
                      'text' => $key->first_name." ".$key->last_name,
                    );
        }
        return $response;
    }

    // mengambil data by id
    public function show($id_person)
    {
        return Person::where('id_person', $id_person)->first();
    }

    // menambah data person
    public function store(Request $request)
    {
        return Person::create($request->all());
    }
    public function store2(Request $request)
    { 
        $image = $request->photo_profile;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        $destinationPath = public_path() . '/uploads/photo_profile';
        \File::put($destinationPath. '/' . $imageName, base64_decode($image));

        Person::insert(
            [
              'photo_profile' => $imageName,
              'first_name' => $request->first_name,
              'last_name' => $request->last_name,
              'gender' => $request->gender,
              'birth_place' => $request->birth_place,
              'birth_date' => $request->birth_date,
              'id_divisi' => $request->divisi,
              'id_jabatan' => $request->jabatan,
              'address' => $request->address,
              'status' => 1,
              'status_user' => 0,
            ]
        );

        DB::table('tb_log_activity')->insert(
            [
                'username' => $request->username,
                'fullname' => $request->fullname,
                'ip' => request()->ip(),
                'log' => "Menambahkan Person ".$request->first_name." ".$request->last_name,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $task = Person::count();
        broadcast(new PersonEvents($task));
        return; 
    }

    public function import(Request $request)
    { 
        if($request->hasFile('file')){
            $person = Excel::toArray(new PersonImport, $request->file('file'));
                $log = array();
                $logNama = array();
                $data = new \App\Person();
                $count = count($person[0]);
                $count2 = count($person[0][0]);
                for ($x = 0; $x < $count; $x++) {
                  for ($y = 0; $y < $count2; $y++) {
                    if($person[0][$x][$y] == null || $person[0][$x][$y] == ""){
                      if ($x == 0){
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("photo_profile", $log, true)){
                           array_push($log, "photo_profile");
                        }
                      } else if ($x == 1) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("first_name", $log, true)){
                           array_push($log, "first_name");
                        }
                      } else if ($x == 2) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("last_name", $log, true)){
                           array_push($log, "last_name");
                        }
                      } else if ($x == 3) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("gender", $log, true)){
                           array_push($log, "gender");
                        }
                      } else if ($x == 4) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("birth_place", $log, true)){
                           array_push($log, "birth_place");
                        }
                      } else if ($x == 5) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("birth_date", $log, true)){
                           array_push($log, "birth_date");
                        }
                      } else if ($x == 6) {
                        if(!in_array($person[0][1][$y]." ".$person[0][2][$y], $logNama, true)){
                            array_push($logNama, $person[0][1][$y]." ".$person[0][2][$y]);
                        }
                        if(!in_array("address", $log, true)){
                           array_push($log, "address");
                        }
                      }
                    } else {
                      if ($x == 0){
                        $data->photo_profile = $person[0][$x][1];
                      } else if ($x == 1) {
                        $data->first_name = $person[0][$x][1];
                      } else if ($x == 2) {
                        $data->last_name = $person[0][$x][1];
                      } else if ($x == 3) {
                        $data->gender = $person[0][$x][1];
                      } else if ($x == 4) {
                        $data->birth_place = $person[0][$x][1];
                      } else if ($x == 5) {
                        $birth_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($person[0][$x][1]);
                        $data->birth_date = $birth_date;
                      } else if ($x == 6) {
                        $data->address = $person[0][$x][1];
                      }
                        $data->status = 1;
                        $data->status_user = 0;
                    }
                  } 
                }

                $count = count($log);
                 
                if(!empty($data)) {
                    $cek = Person::where('first_name', "{$data->first_name}")
                    ->where('last_name', "{$data->last_name}")
                    ->where('gender', "{$data->gender}")
                    ->where('birth_place', "{$data->birth_place}")
                    ->count();

                    if ($cek < 1){
                        if($count > 0){
                            if(DB::table('log_import')->insert(
                                ['nama' => json_encode($logNama), 'keterangan' => json_encode($log)]
                            )){
                                $log = array();
                            }
                        } else {
                            $data->save();
                            $ret = "oke";
                            return;
                        }
                    } else {
                      $ret = "fail";
                      return;
                    }
                }

                $dataz = DB::table('log_import')
                ->get();

                $isi = null;
                foreach ($dataz as $key) {
                    // $isi .= "\n";
                    $isi .= "\n Nama : ";
                    $isi .= implode(", ", json_decode($key->nama));
                    $isi .= ". \n";
                    $isi .= " Data Kosong Pada: ";
                    $isi .= implode(", ", json_decode($key->keterangan));
                    $isi .= ". \n";
                    $isi .= " Silahkan melakukan pengecekan kembali pada data tersebut !";
                    $isi .= "\n";
                }

                DB::table('log_import')
                ->truncate();

                $ret = $isi;
        }   
        return $ret;
    }

    public function importXLXS(Request $request)
    {   
      $isiXLSX = $request->isiXLSX;
      $log = array();
      $ret = "";
      foreach ($isiXLSX as $key => $row) {
        $data = new \App\Person();
        if($row['photo_profile'] == null || $row['photo_profile'] == ""){
            array_push($log, "photo_profile");
        } else {
            $data->photo_profile = $row['photo_profile'];
        }
        if($row['first_name'] == null || $row['first_name'] == ""){
            array_push($log, "first_name");
        } else {
            $data->first_name = $row['first_name'];
        }
        if($row['last_name'] == null || $row['last_name'] == ""){
            array_push($log, "last_name");
        } else {
            $data->last_name = $row['last_name'];
        }
        if($row['gender'] == null || $row['gender'] == ""){
            array_push($log, "gender");
        } else {
            $data->gender = $row['gender'];
        }
        if($row['birth_place'] == null || $row['birth_place'] == ""){
            array_push($log, "birth_place");
        } else {
            $data->birth_place = $row['birth_place'];
        }
        if($row['birth_date'] == null || $row['birth_date'] == ""){
            array_push($log, "birth_date");
        } else {
            $data->birth_date = $row['birth_date'];
        }
        if($row['address'] == null || $row['address'] == ""){
            array_push($log, "address");
        } else {
            $data->address = $row['address'];
        }
        $data->status = "1";
        $data->status_user = "0";

        $count = count($log);
        if(!empty($data)) {
            $cek = Person::where('first_name', "{$row['first_name']}")
            ->where('last_name', "{$row['last_name']}")
            ->where('gender', "{$row['gender']}")
            ->where('birth_place', "{$row['birth_place']}")
            ->count();

            if ($cek < 1){
                if($count > 0){
                    if(DB::table('log_import')->insert(
                        ['nama' =>  $row['first_name']." ".$row['last_name'], 'keterangan' => json_encode($log)]
                    )){
                        $log = array();
                    }
                } else {
                  $destinationPath = public_path() . '/uploads/photo_profile';
                  $img = file($destinationPath."/avatarDefault.png");
                  \File::put($destinationPath. '/' . $row['first_name']."_".$row['photo_profile'], $img);
                    $data->save();
                    $ret .= "oke";
                    return;
                }
            } else {
              $ret .= "fail";
              return;
            }
        }
      }

      $dataz = DB::table('log_import')
        ->get();

        $isi = null;
        foreach ($dataz as $key) { 
            // $isi .= "\n";
            $isi .= "\n Nama : ";
            $isi .= $key->nama;
            $isi .= ". \n";
            $isi .= " Data Kosong Pada: ";
            $isi .= implode(", ", json_decode($key->keterangan));
            $isi .= ". \n";
            $isi .= " Silahkan melakukan pengecekan kembali pada data tersebut !";
            $isi .= "\n";
        }

        DB::table('log_import')
        ->truncate();

        $ret .= $isi;
        echo $ret;

        $task = Person::count();
        broadcast(new PersonEvents($task));
      return;
    }

    public function downloadExcelPerson()
    {
        return Excel::download(new PersonExport, 'Export Person '.date("Y-m-d").'.xlsx');
    }

    // mengubah data
    public function update($id, Request $request)
    {
      if($request->photo_profile !== "kosong"){
        $image = $request->photo_profile;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        $destinationPath = public_path() . '/uploads/photo_profile';
        $destinationPathOld = public_path() . '/uploads/photo_profile/';
        unlink($destinationPathOld.$request->photo_profile_old);
        \File::put($destinationPath. '/' . $imageName, base64_decode($image));

          DB::table('persons')
              ->where('id_person', $id)
              ->update(
                [ 
                  'photo_profile' => $imageName,
                  'first_name' => $request->first_name,
                  'last_name' => $request->last_name,
                  'gender' => $request->gender,
                  'birth_place' => $request->birth_place,
                  'birth_date' => $request->birth_date,
                  'id_divisi' => $request->divisi,
                  'id_jabatan' => $request->jabatan,
                  'address' => $request->address
                ]
              );
      } else {
        DB::table('persons')
            ->where('id_person', $id)
            ->update(
              [ 
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'gender' => $request->gender,
                'birth_place' => $request->birth_place,
                'birth_date' => $request->birth_date,
                'id_divisi' => $request->divisi,
                'id_jabatan' => $request->jabatan,
                'address' => $request->address
              ]
            );
      }
      
      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username,
              'fullname' => $request->fullname,
              'ip' => request()->ip(),
              'log' => "Memperbarui Person ".$request->first_name." ".$request->last_name,
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );

        return;
    }
    
    // menghapus data
    public function delete($id, Request $request)
    {
      $data = DB::table('persons')->where('id_person', $id)->first();
      $dataz = DB::table('users')
                ->where('id_person', $id)
                ->first();
      $data2 = DB::table('users')->where('id_person', $id)->first();
      if(empty($data2)){
        DB::table('persons')->where('id_person', $id)->delete();
        $destinationPathOld = public_path() . '/uploads/photo_profile/';
        unlink($destinationPathOld.$data->photo_profile);
      } else {
        DB::table('persons')->where('id_person', $id)->delete(); 
        DB::table('tb_role_user_access')->where('UserID', $data2->id)->delete();
        DB::table('users')->where('id_person', $id)->delete();
        DB::table('users_to_vendor')->where('id_user', $dataz->id)->delete();
        $destinationPathOld = public_path() . '/uploads/photo_profile/';
        unlink($destinationPathOld.$data->photo_profile);
      }

      DB::table('tb_log_activity')->insert(
          [
              'username' => $request->username,
              'fullname' => $request->fullname,
              'ip' => request()->ip(),
              'log' => "Menghapus Person",
              'created_at' => date("Y-m-d H:i:s"),
          ]
      );

      $task = Person::count();
      broadcast(new PersonEvents($task));
      
      return 204;
    }

    public function getRecords2( Request $request ) {

       $search_query = $request->searchTerm;
       $perPage      = $request->per_page;
       $field      = $request->input('field','first_name');
       $type      = $request->input('type','asc');
       $persons        = DB::table('persons as p') 
                        ->leftjoin('users as u', 'u.id_person', '=', 'p.id_person')
                        ->leftjoin('tb_divisi as d', 'p.id_divisi', '=', 'd.id_divisi')
                        ->leftjoin('tb_jabatan as j', 'p.id_jabatan', '=', 'j.id_jabatan')
                        ->select('p.*', 'u.role', 'u.id', 'd.divisi', 'j.jabatan')
                        ->where( 'p.first_name', 'LIKE', '%' . $search_query . '%' )
                           ->orderBy($field, $type)
                           ->paginate( $perPage )
                           ->toArray();

       if ( $search_query !== null) {
          $persons['searchTerm'] = $search_query ?: '';
       } else if ( $search_query == null) {
          $persons['searchTerm'] = $search_query ? null : '';
       }

       foreach($persons['data'] as $key => $value)
       { 
          $getVendor = DB::table('users_to_vendor')
                        ->where('id_user', $persons['data'][$key]->id)
                        ->first();

          if($getVendor !== null){
            $getVendorname = DB::table('tb_vendor_primary')
                        ->where('id_vendor', $getVendor->id_vendor)
                        ->first();
            $persons['data'][$key]->nama_vendor = $getVendorname->nama_vendor;           
          } else {
            $persons['data'][$key]->nama_vendor = "belum ada";
          }

       }

       return response()->json( [
          'persons' => $persons
       ] );
    }

    public function pdfPerson($id_person)
    {   
        $pdf = Person::where('id_person','=',$id_person)->first();
        view()->share('pdf',$pdf);
        
        $nama_pdf = $pdf->id_person."-".$pdf->first_name.".pdf";
        if($id_person){
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfPerson');
            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }
}