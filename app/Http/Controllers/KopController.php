<?php
namespace App\Http\Controllers;

use App\Events\PersonEvents;
use App\Exports\IuranExport;
use App\Exports\KopExport;
// import file model Kop
use App\Kop;
use App\Mail\ApproveKop;
use App\Mail\BulkMail;
use App\Mail\KopMail;
use App\Mail\NotApproveKop;
use App\Mail\ThanksRegister;
use App\Person;
use DateTime;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;

class KopController extends Controller {
    // mengambil semua data
    public function all() {
        return Kop::all();
    }

    public function all2() {
        return Kop::where('status', 2)->get();
    }

    public function dropdownkop() {
        $data = Kop::where('status', 2)->get();

        $response = array();
        foreach ($data as $key) {
            $response[] = array(
                'id' => $key->id_kop,
                'text' => $key->nama_komunitas,
            );
        }
        return $response;
    }

    public function dropdownprovince() {
        $link = 'https://exiglosi.com/api/public/master/provinsi/';
        $data = Curl::to($link)
            ->get();
        $data = json_decode($data);

        if (is_array($data) || is_object($data)) {
            $response = array();
            foreach ($data as $key) {
                $response[] = array(
                    'id' => $key->kode,
                    'text' => $key->nama,
                );
            }
        }
        return $response;
    }

    public function dropdowncity($id) {
        $link = 'https://exiglosi.com/api/public/master/kabupaten/' . $id;
        $data = Curl::to($link)
            ->get();
        $data = json_decode($data);

        if (is_array($data) || is_object($data)) {
            $response = array();
            foreach ($data as $key) {
                $response[] = array(
                    'id' => $key->kode,
                    'text' => $key->nama,
                );
            }
        }
        return $response;
    }

    // mengambil data by id
    public function show($id_kop) {
        $data = DB::table('tb_member_premier as p')
            ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
            ->leftjoin('tb_assessment as a', 'p.id_kop', '=', 'a.id_kop')
            ->where('p.id_kop', $id_kop)->first();

        $data->logo_komunitasOld = $data->logo_komunitas;

        $ihsi = null;
        if (filter_var($data->logo_komunitas, FILTER_VALIDATE_URL)) {
            $data->logo_komunitas = str_replace("open", "uc", $data->logo_komunitas);
            $ihsi = str_replace("open", "uc", $data->logo_komunitas);
        } else {
            $path = $data->logo_komunitas;
            $dataImg = file_get_contents($path);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = base64_encode($dataImg);
            $src = 'data:image/' . $type . ';base64,' . $base64;

            $data->logo_komunitas = $src;
            $ihsi = $src;
        }

        // $linkprovince = 'https://exiglosi.com/api/public/master/provinsi/' . $data->provinsi_kerja_id;
        // $province = Curl::to($linkprovince)
        //     ->get();
        // $province = json_decode($province);
        // foreach ($province as $key) {
        //     $province = $key->nama;
        // }
        $province = $data->provinsi_kerja;

        // $linkcity = 'https://exiglosi.com/api/public/master/kabupaten/' . $data->kota_kerja_id;
        // $city = Curl::to($linkcity)
        //     ->get();
        // $city = json_decode($city);
        // foreach ($city as $key) {
        //     $city = $key->nama;
        // }
        $city = $data->kota_kerja;

        $dataFix = array();
        array_push($dataFix, array(
            "id_kop" => $id_kop,
            "nama_komunitas" => $data->nama_komunitas,
            "deskripsi_komunitas" => $data->deskripsi_komunitas,
            "nama_narahubung" => $data->nama_narahubung,
            "email_narahubung" => $data->email_narahubung,
            "tlp_narahubung" => $data->tlp_narahubung,
            "jenis_komunitas" => $data->jenis_komunitas,
            "email_komunitas" => $data->email_komunitas,
            "logo_komunitas" => $ihsi,
            "logo_komunitasOld" => $data->logo_komunitasOld,
            "klaster_kerja" => explode(";", $data->klaster_kerja),
            "lingkup_komunitas" => explode(";", $data->lingkup_komunitas),
            "provinsi_kerja" => $province,
            "kota_kerja" => $city,
            "tgl_berdiri_kop" => $data->tgl_berdiri_kop,
            "web_komunitas" => $data->web_komunitas,
            "twitter_komunitas" => $data->twitter_komunitas,
            "facebook_komunitas" => $data->facebook_komunitas,
            "instagram_komunitas" => $data->instagram_komunitas,
            "youtube_komunitas" => $data->youtube_komunitas,
            'pelaporan_keuangan' => $data->pelaporan_keuangan,
            'membiayai_kegiatan' => $data->membiayai_kegiatan,
            'pengelolaan_dana' => $data->pengelolaan_dana,
            'dapat_berjalan' => $data->dapat_berjalan,
            'pengelolaan_pengurus' => $data->pengelolaan_pengurus,
            'peningkatan_kapasitas' => $data->peningkatan_kapasitas,
            'pengelolaan_relawan' => $data->pengelolaan_relawan,
            'mendukung_peningkatan' => $data->mendukung_peningkatan,
            'memahami_nilai' => $data->memahami_nilai,
            'efektivitas_program' => $data->efektivitas_program,
            'efektivitas_program_setahun' => $data->efektivitas_program_setahun,
            'cakupan_program' => $data->cakupan_program,
            'pengukuran_intervensi' => $data->pengukuran_intervensi,
            'mencapai_visi' => $data->mencapai_visi,
            'perencanaan_program' => $data->perencanaan_program,
            'komunikasikan_program' => $data->komunikasikan_program,
            'kemitraan_komunitas' => $data->kemitraan_komunitas,
            'kemitraan_pemerintah' => $data->kemitraan_pemerintah,
            'memecahkan_masalah' => $data->memecahkan_masalah,
            'mendukung_inovasi' => $data->mendukung_inovasi,
            'memberikan_ruang' => $data->memberikan_ruang,
            'pendapatan_utama' => explode(";", $data->pendapatan_utama),
            'relawan_kop' => $data->relawan_kop,
            'jumlah_kemitraan' => $data->jumlah_kemitraan,
            'melakukan_komunitas' => explode(";", $data->melakukan_komunitas),
            'teknologi_komunitas' => explode(";", $data->teknologi_komunitas),
            'berapa_kegiatan' => $data->berapa_kegiatan,
            'nama_media_kegiatan' => $data->nama_media_kegiatan,
            'masalah_utama' => explode(";", $data->masalah_utama),
            'pengembangan_komunitas' => explode(";", $data->pengembangan_komunitas),
        ));

        return json_encode($dataFix[0]);
    }

    // menambah data kop
    public function store(Request $request) {
        $image = $request->logo_komunitas; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/logo_komunitas';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        $klaster_kerja = implode(";", $request->klaster_kerja);
        $lingkup_komunitas = implode(";", $request->lingkup_komunitas);

        $linkprov = 'https://exiglosi.com/api/public/master/provinsi/' . $request->provinsi_kerja;
        $provinsi = Curl::to($linkprov)
            ->get();
        $provinsi = json_decode($provinsi);
        $provinsi = $provinsi[0]->nama;

        $linkcity = 'https://exiglosi.com/api/public/master/kabupaten/' . $request->kota_kerja;
        $city = Curl::to($linkcity)
            ->get();
        $city = json_decode($city);
        $city = $city[0]->nama;

        Kop::insert(
            [
                'nama_komunitas' => $request->nama_komunitas,
                'deskripsi_komunitas' => $request->deskripsi_komunitas,
                'nama_narahubung' => $request->nama_narahubung,
                'email_narahubung' => $request->email_narahubung,
                'tlp_narahubung' => $request->tlp_narahubung,
                'jenis_komunitas' => $request->jenis_komunitas,
                'email_komunitas' => $request->email_komunitas,
                'logo_komunitas' => $destinationPath . "/" . $imageName,
                'klaster_kerja' => $klaster_kerja,
                'lingkup_komunitas' => $lingkup_komunitas,
                'provinsi_kerja' => $provinsi,
                'provinsi_kerja_id' => $request->provinsi_kerja,
                'kota_kerja' => $city,
                'kota_kerja_id' => $request->kota_kerja,
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $data = DB::table('tb_member_premier')
            ->where('nama_komunitas', $request->nama_komunitas)
            ->where('jenis_komunitas', $request->jenis_komunitas)
            ->where('nama_narahubung', $request->nama_narahubung)
            ->first();

        DB::table('tb_member_sekunder')->insert(
            [
                'id_kop' => $data->id_kop,
                'tgl_berdiri_kop' => $request->tgl_komunitas,
                'web_komunitas' => $request->web_komunitas,
                'twitter_komunitas' => $request->twitter_komunitas,
                'facebook_komunitas' => $request->facebook_komunitas,
                'instagram_komunitas' => $request->instagram_komunitas,
                'youtube_komunitas' => $request->youtube_komunitas,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // $task = Kop::count();
        // broadcast(new KopEvents($task));
        return;
    }

    public function storeOnline(Request $request) {
        $image = $request->logo_komunitas; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/logo_komunitas';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        $klaster_kerja = implode(";", $request->klaster_kerja);
        $lingkup_komunitas = implode(";", $request->lingkup_komunitas);

        $linkprov = 'https://exiglosi.com/api/public/master/provinsi/' . $request->provinsi_kerja;
        $provinsi = Curl::to($linkprov)
            ->get();
        $provinsi = json_decode($provinsi);
        $provinsi = $provinsi[0]->nama;

        $linkcity = 'https://exiglosi.com/api/public/master/kabupaten/' . $request->kota_kerja;
        $city = Curl::to($linkcity)
            ->get();
        $city = json_decode($city);
        $city = $city[0]->nama;

        Kop::insert(
            [
                'nama_komunitas' => $request->nama_komunitas,
                'deskripsi_komunitas' => $request->deskripsi_komunitas,
                'nama_narahubung' => $request->nama_narahubung,
                'email_narahubung' => $request->email_narahubung,
                'tlp_narahubung' => $request->tlp_narahubung,
                'jenis_komunitas' => $request->jenis_komunitas,
                'email_komunitas' => $request->email_komunitas,
                'logo_komunitas' => $destinationPath . "/" . $imageName,
                'klaster_kerja' => $klaster_kerja,
                'lingkup_komunitas' => $lingkup_komunitas,
                'provinsi_kerja' => $provinsi,
                'provinsi_kerja_id' => $request->provinsi_kerja,
                'kota_kerja' => $city,
                'kota_kerja_id' => $request->kota_kerja,
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        // $data = DB::table('tb_member_premier')
        //     ->where('nama_komunitas', $request->nama_komunitas)
        //     ->where('jenis_komunitas', $request->jenis_komunitas)
        //     ->where('nama_narahubung', $request->nama_narahubung)
        //     ->first();

        // DB::table('tb_member_sekunder')->insert(
        //     [
        //         'id_kop' => $data->id_kop,
        //         'tgl_berdiri_kop' => $request->tgl_komunitas,
        //         'web_komunitas' => $request->web_komunitas,
        //         'twitter_komunitas' => $request->twitter_komunitas,
        //         'facebook_komunitas' => $request->facebook_komunitas,
        //         'instagram_komunitas' => $request->instagram_komunitas,
        //         'youtube_komunitas' => $request->youtube_komunitas,
        //         'created_at' => date("Y-m-d H:i:s"),
        //     ]
        // );

        // $pendapatan_utama = implode(";", $request->pendapatan_utama);
        // $melakukan_komunitas = implode(";", $request->melakukan_komunitas);
        // $teknologi_komunitas = implode(";", $request->teknologi_komunitas);
        // $masalah_utama = implode(";", $request->masalah_utama);
        // $pengembangan_komunitas = implode(";", $request->pengembangan_komunitas);

        // DB::table('tb_assessment')->insert(
        //     [
        //         'id_kop' => $data->id_kop,
        //         'pelaporan_keuangan' => $request->pelaporan_keuangan,
        //         'membiayai_kegiatan' => $request->membiayai_kegiatan,
        //         'pengelolaan_dana' => $request->pengelolaan_dana,
        //         'dapat_berjalan' => $request->dapat_berjalan,
        //         'pengelolaan_pengurus' => $request->pengelolaan_pengurus,
        //         'peningkatan_kapasitas' => $request->peningkatan_kapasitas,
        //         'pengelolaan_relawan' => $request->pengelolaan_relawan,
        //         'mendukung_peningkatan' => $request->mendukung_peningkatan,
        //         'memahami_nilai' => $request->memahami_nilai,
        //         'efektivitas_program' => $request->efektivitas_program,
        //         'efektivitas_program_setahun' => $request->efektivitas_program_setahun,
        //         'cakupan_program' => $request->cakupan_program,
        //         'pengukuran_intervensi' => $request->pengukuran_intervensi,
        //         'mencapai_visi' => $request->mencapai_visi,
        //         'perencanaan_program' => $request->perencanaan_program,
        //         'komunikasikan_program' => $request->komunikasikan_program,
        //         'kemitraan_komunitas' => $request->kemitraan_komunitas,
        //         'kemitraan_pemerintah' => $request->kemitraan_pemerintah,
        //         'memecahkan_masalah' => $request->memecahkan_masalah,
        //         'mendukung_inovasi' => $request->mendukung_inovasi,
        //         'memberikan_ruang' => $request->memberikan_ruang,
        //         'pendapatan_utama' => $pendapatan_utama,
        //         'total_dana' => $request->total_dana,
        //         'pengurus_kop' => $request->pengurus_kop,
        //         'relawan_kop' => $request->relawan_kop,
        //         'jumlah_kemitraan' => $request->jumlah_kemitraan,
        //         'melakukan_komunitas' => $melakukan_komunitas,
        //         'teknologi_komunitas' => $teknologi_komunitas,
        //         'berapa_kegiatan' => $request->berapa_kegiatan,
        //         'nama_media_kegiatan' => $request->nama_media_kegiatan,
        //         'masalah_utama' => $masalah_utama,
        //         'pengembangan_komunitas' => $pengembangan_komunitas,
        //         'created_at' => date("Y-m-d H:i:s"),
        //     ]
        // );

        // if ($request->donasi_tahunan == "") {

        // } else if ($request->donasi_tahunan == "Belum bersedia") {
        //     $donasi_tahunan = $request->donasi_tahunan;
        //     $ket_tdk_bersedia = $request->ket_tdk_bersedia;
        //     $konfirmasi_donasi = $request->konfirmasi_donasi;

        //     $data = DB::table('tb_member_premier')
        //         ->where('nama_komunitas', $request->nama_komunitas)
        //         ->where('jenis_komunitas', $request->jenis_komunitas)
        //         ->where('nama_narahubung', $request->nama_narahubung)
        //         ->first();

        //     DB::table('tb_donasi')->insert(
        //         [
        //             'id_kop' => $data->id_kop,
        //             'donasi_tahunan' => $request->donasi_tahunan,
        //             'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
        //             'konfirmasi_donasi' => $request->konfirmasi_donasi,
        //             'created_at' => date("Y-m-d H:i:s"),
        //         ]
        //     );
        // } else {
        //     $donasi_tahunan = $request->donasi_tahunan;
        //     $ket_tdk_bersedia = $request->ket_tdk_bersedia;
        //     $konfirmasi_donasi = $request->konfirmasi_donasi;
        //     if ($request->bukti_donasi == "kosong") {
        //         $imageName2 = null;
        //     } else {
        //         $image2 = $request->bukti_donasi;
        //         $image2 = str_replace('data:image/png;base64,', '', $image2);
        //         $image2 = str_replace(' ', '+', $image2);
        //         $imageName2 = str_random(10) . '.' . 'png';
        //         $destinationPath2 = public_path() . '/uploads/bukti_transfer';
        //         \File::put($destinationPath2 . '/' . $imageName2, base64_decode($image2));
        //     }

        //     DB::table('tb_donasi')->insert(
        //         [
        //             'id_kop' => $data->id_kop,
        //             'donasi_tahunan' => $request->donasi_tahunan,
        //             'ket_tdk_bersedia' => $request->ket_tdk_bersedia,
        //             'konfirmasi_donasi' => $request->konfirmasi_donasi,
        //             'bukti_donasi' => $imageName2,
        //             'created_at' => date("Y-m-d H:i:s"),
        //         ]
        //     );
        // }

        $objDemo = DB::table('tb_content_email')->where('type', 'Register')->first();

        $sendEmail = array();
        array_push($sendEmail, $request->email_narahubung, $request->email_komunitas);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new ThanksRegister($objDemo));
        }

        // $task = Kop::count();
        // broadcast(new KopEvents($task));
        return;
    }

    public function importXLXS(Request $request) {
        $isiXLSX = $request->isiXLSX;
        $logo = $request->logo;
        $log = array();
        $ret = "";
        foreach ($isiXLSX as $key => $row) {
            $data = new \App\Kop();

            $fotoUrl = false;
            if ($row['logo_komunitas'] == null || $row['logo_komunitas'] == "") {
                array_push($log, "logo_komunitas");
            } else {
                // $data->logo_komunitas = $row['nama_komunitas']."_".$row['logo_komunitas'];
                if (filter_var($row['logo_komunitas'], FILTER_VALIDATE_URL)) {
                    $data->logo_komunitas = $row['logo_komunitas'];
                    $fotoUrl = true;
                }
            }
            if ($row['nama_komunitas'] == null || $row['nama_komunitas'] == "") {
                array_push($log, "nama_komunitas");
            } else {
                $data->nama_komunitas = $row['nama_komunitas'];
            }
            if ($row['deskripsi_komunitas'] == null || $row['deskripsi_komunitas'] == "") {
                array_push($log, "deskripsi_komunitas");
            } else {
                $data->deskripsi_komunitas = $row['deskripsi_komunitas'];
            }
            if ($row['nama_narahubung'] == null || $row['nama_narahubung'] == "") {
                array_push($log, "nama_narahubung");
            } else {
                $data->nama_narahubung = $row['nama_narahubung'];
            }
            if ($row['email_narahubung'] == null || $row['email_narahubung'] == "") {
                array_push($log, "email_narahubung");
            } else {
                $data->email_narahubung = $row['email_narahubung'];
            }
            if ($row['tlp_narahubung'] == null || $row['tlp_narahubung'] == "") {
                array_push($log, "tlp_narahubung");
            } else {
                $data->tlp_narahubung = $row['tlp_narahubung'];
            }
            if ($row['jenis_komunitas'] == null || $row['jenis_komunitas'] == "") {
                array_push($log, "jenis_komunitas");
            } else {
                $data->jenis_komunitas = $row['jenis_komunitas'];
            }
            if ($row['email_komunitas'] == null || $row['email_komunitas'] == "") {
                array_push($log, "email_komunitas");
            } else {
                $data->email_komunitas = $row['email_komunitas'];
            }
            if ($row['klaster_kerja'] == null || $row['klaster_kerja'] == "") {
                array_push($log, "klaster_kerja");
            } else {
                $data->klaster_kerja = $row['klaster_kerja'];
            }
            if ($row['lingkup_komunitas'] == null || $row['lingkup_komunitas'] == "") {
                array_push($log, "lingkup_komunitas");
            } else {
                $data->lingkup_komunitas = $row['lingkup_komunitas'];
            }
            if ($row['provinsi_kerja'] == null || $row['provinsi_kerja'] == "") {
                array_push($log, "provinsi_kerja");
            } else {
                $data->provinsi_kerja = $row['provinsi_kerja'];
            }
            if ($row['kota_kerja'] == null || $row['kota_kerja'] == "") {
                array_push($log, "kota_kerja");
            } else {
                $data->kota_kerja = $row['kota_kerja'];
            }
            if ($row['tgl_berdiri_kop'] == null || $row['tgl_berdiri_kop'] == "") {
                array_push($log, "tgl_berdiri_kop");
            } else {

            }
            if ($row['web_komunitas'] == null || $row['web_komunitas'] == "") {
                array_push($log, "web_komunitas");
            } else {

            }
            if ($row['twitter_komunitas'] == null || $row['twitter_komunitas'] == "") {
                array_push($log, "twitter_komunitas");
            } else {

            }
            if ($row['facebook_komunitas'] == null || $row['facebook_komunitas'] == "") {
                array_push($log, "facebook_komunitas");
            } else {

            }
            if ($row['instagram_komunitas'] == null || $row['instagram_komunitas'] == "") {
                array_push($log, "instagram_komunitas");
            } else {

            }
            if ($row['youtube_komunitas'] == null || $row['youtube_komunitas'] == "") {
                array_push($log, "youtube_komunitas");
            } else {

            }
            $data->status = "2";
            $data->created_at = date("Y-m-d H:i:s");

            $count = count($log);

            if (!empty($data)) {
                $cek = Kop::where('nama_komunitas', "{$row['nama_komunitas']}")
                    ->where('jenis_komunitas', "{$row['jenis_komunitas']}")
                    ->count();
                $cekID = Kop::where('nama_komunitas', "{$row['nama_komunitas']}")
                    ->where('jenis_komunitas', "{$row['jenis_komunitas']}")
                    ->first();

                if ($cek == 0) {
                    if ($count > 0) {
                        if (DB::table('log_import')->insert(
                            ['nama' => $row['nama_komunitas'], 'keterangan' => json_encode($log)]
                        )) {
                            $log = array();
                        }
                    } else {

                        foreach ($logo as $key) {
                            // if (filter_var($row['logo_komunitas'], FILTER_VALIDATE_URL)) {
                            //     $data->logo_komunitas = $row['logo_komunitas'];
                            //     $data->save();
                            // }
                            if ($row['logo_komunitas'] == $key['name']) {
                                if (substr($key['path'], 0, 14) == "data:image/png") {
                                    $image = str_replace('data:image/png;base64,', '', $key['path']);
                                } else if (substr($key['path'], 0, 15) == "data:image/jpeg") {
                                    $image = str_replace('data:image/jpeg;base64,', '', $key['path']);
                                }
                                $image = str_replace(' ', '+', $image);
                                $imageName = $key['name'];
                                $destinationPath = public_path() . '/uploads/logo_komunitas';
                                \File::put($destinationPath . '/' . $imageName, base64_decode($image));
                                $data->logo_komunitas = $destinationPath . "/" . $imageName;
                                $data->save();
                                break;
                            } else if ($row['logo_komunitas'] == null || $row['logo_komunitas'] == "") {
                                // $destinationPath = public_path('uploads\logo_komunitas');
                                // $img = file($destinationPath."\avatarDefault.png");
                                // \File::put($destinationPath. '\\' . $row['nama_komunitas']."_default", $img);
                                // $data->logo_komunitas = $destinationPath."\\".$row['nama_komunitas']."_default.png";
                                // $data->save();
                            }
                            // else if ($row['logo_komunitas'] !== $key['name']) {
                            //   // $data->logo_komunitas = $row['logo_komunitas'];
                            //   // $data->save();
                            //   array_push($log, "logo_komunitas dengan file berbeda");
                            // }

                        }

                        if ($fotoUrl) {
                            $data->save();
                        }

                        // $destinationPath = public_path('uploads\logo_komunitas');
                        // $img = file($destinationPath."\avatarDefault.png");
                        // \File::put($destinationPath. '\\' . $row['nama_komunitas']."_".$row['logo_komunitas'], $img);
                        // $data->save();

                        $dataZ = DB::table('tb_member_premier')
                            ->where('nama_komunitas', $row['nama_komunitas'])
                            ->where('jenis_komunitas', $row['jenis_komunitas'])
                            ->where('nama_narahubung', $row['nama_narahubung'])
                            ->first();

                        if (!empty($dataZ)) {
                            DB::table('tb_member_sekunder')->insert(
                                [
                                    'id_kop' => $dataZ->id_kop,
                                    'tgl_berdiri_kop' => $row['tgl_berdiri_kop'],
                                    'web_komunitas' => $row['web_komunitas'],
                                    'twitter_komunitas' => $row['twitter_komunitas'],
                                    'facebook_komunitas' => $row['facebook_komunitas'],
                                    'instagram_komunitas' => $row['instagram_komunitas'],
                                    'youtube_komunitas' => $row['youtube_komunitas'],
                                    'created_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                            $firstLast = explode(" ", $row['nama_narahubung']);
                            $username = strtolower($firstLast[0]) . rand(10, 10000);

                            $destinationPath = public_path() . '/uploads/photo_profile';
                            $img = file($destinationPath . "/avatarDefault.png");
                            \File::put($destinationPath . '/' . $username . ".png", $img);

                            if (count($firstLast) < 2) {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => "",
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            } else {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => $firstLast[1],
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            }

                            // $destinationPath = public_path('uploads\photo_profile');
                            // $img = file($destinationPath . "\avatarDefault.png");
                            // \File::put($destinationPath . '\\' . $username . ".png", $img);

                            $id = DB::getPdo()->lastInsertId();

                            DB::table('users')->insert(
                                [
                                    'id_person' => $id,
                                    'username' => $username,
                                    'password' => bcrypt($username),
                                    'email' => $row['email_narahubung'],
                                    'role' => 'R002',
                                    'status' => 1,
                                ]
                            );
                            $dataY = DB::table('users')
                                ->where('id_person', $id)
                                ->first();
                            $dataMenu = DB::table('tb_role_master')
                                ->where('kd_role', 'R002')
                                ->get();
                            foreach ($dataMenu as $key) {
                                DB::table('tb_role_user_access')->insert(
                                    [
                                        'kd_role' => $key->kd_role,
                                        'UserID' => $dataY->id,
                                        'MenuID' => $key->MenuID,
                                        'DetailID' => $key->DetailID,
                                        'DetailID2' => $key->DetailID2,
                                        'Link' => $key->Link,
                                        'Action' => $key->Action,
                                    ]
                                );
                            }

                            DB::table('users_to_vendor')->insert(
                                [
                                    'id_user' => $dataY->id,
                                    'id_vendor' => $dataZ->id_kop,
                                ]
                            );

                            $task = Person::count();
                            broadcast(new PersonEvents($task));
                        }
                        $ret = "";
                        $ret .= "oke";
                    }
                } else {
                    if ($count > 0) {
                        if (DB::table('log_import')->insert(
                            ['nama' => $row['nama_komunitas'], 'keterangan' => json_encode($log)]
                        )) {
                            $log = array();
                        }
                    } else {
                        DB::table('tb_member_premier')
                            ->where('id_kop', $cekID->id_kop)
                            ->update(
                                [
                                    'nama_komunitas' => $row['nama_komunitas'],
                                    'nama_narahubung' => $row['nama_narahubung'],
                                    'email_narahubung' => $row['email_narahubung'],
                                    'jenis_komunitas' => $row['jenis_komunitas'],
                                    'email_komunitas' => $row['email_komunitas'],
                                    'klaster_kerja' => $row['klaster_kerja'],
                                    'lingkup_komunitas' => $row['lingkup_komunitas'],
                                    'provinsi_kerja' => $row['provinsi_kerja'],
                                    'kota_kerja' => $row['kota_kerja'],
                                    'status' => 2,
                                    'updated_at' => date("Y-m-d H:i:s"),
                                ]
                            );

                        DB::table('tb_member_sekunder')
                            ->where('id_kop', $cekID->id_kop)
                            ->update(
                                [
                                    'tgl_berdiri_kop' => $row['tgl_berdiri_kop'],
                                    'web_komunitas' => $row['web_komunitas'],
                                    'twitter_komunitas' => $row['twitter_komunitas'],
                                    'facebook_komunitas' => $row['facebook_komunitas'],
                                    'instagram_komunitas' => $row['instagram_komunitas'],
                                    'youtube_komunitas' => $row['youtube_komunitas'],
                                    'updated_at' => date("Y-m-d H:i:s"),
                                ]
                            );
                        $ret = "";
                        $ret .= "oke";
                    }
                }
            }

        }
        // echo $count;
        // return;

        $datazy = DB::table('log_import')
            ->get();

        if (count($datazy) > 0) {
            $ret = "";
        }

        $isi = null;
        foreach ($datazy as $key) {
            $isi .= "\n Nama : ";
            $isi .= $key->nama;
            $isi .= ". \n";
            $isi .= " Data Kosong Pada: ";
            $isi .= implode(", ", json_decode($key->keterangan));
            $isi .= ". \n";
            $isi .= " Silahkan melakukan pengecekan kembali pada data tersebut !";
            $isi .= "\n";
            $isi .= "\n";
        }

        DB::table('log_import')
            ->truncate();

        if (count($datazy) > 0) {
            $ret .= $isi;
        }

        echo $ret;
        return;
    }

    public function downloadExcelKop() {
        return Excel::download(new KopExport, 'ExportKop ' . date("Y-m-d") . '.xlsx');
    }

    // mengubah data
    public function update($id, Request $request) {
        if (is_numeric($request->provinsi_kerja)) {
            $linkprov = 'https://exiglosi.com/api/public/master/provinsi/' . $request->provinsi_kerja;
            $provinsi = Curl::to($linkprov)
                ->get();
            $provinsi = json_decode($provinsi);
            $provinsi = $provinsi[0]->nama;

            $linkcity = 'https://exiglosi.com/api/public/master/kabupaten/' . $request->kota_kerja;
            $city = Curl::to($linkcity)
                ->get();
            $city = json_decode($city);
            $city = $city[0]->nama;
        }

        if ($request->logo_komunitas !== "kosong") {
            $image = $request->logo_komunitas; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/logo_komunitas';
            // $destinationPathOld = public_path('uploads\logo_komunitas\\');
            // unlink($destinationPathOld.$request->logo_komunitas_old);

            if ($request->logo_komunitas_old != $destinationPath . "/uploads/logo_komunitas/imgDefault.png") {
                if (file_exists($request->logo_komunitas_old)) {
                    unlink($request->logo_komunitas_old);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));
            $klaster_kerja = implode(";", $request->klaster_kerja);
            $lingkup_komunitas = implode(";", $request->lingkup_komunitas);

            if (is_numeric($request->provinsi_kerja)) {
                DB::table('tb_member_premier')
                    ->where('id_kop', $id)
                    ->update(
                        [
                            'nama_komunitas' => $request->nama_komunitas,
                            'deskripsi_komunitas' => $request->deskripsi_komunitas,
                            'nama_narahubung' => $request->nama_narahubung,
                            'email_narahubung' => $request->email_narahubung,
                            'tlp_narahubung' => $request->tlp_narahubung,
                            'jenis_komunitas' => $request->jenis_komunitas,
                            'email_komunitas' => $request->email_komunitas,
                            'logo_komunitas' => $destinationPath . "/" . $imageName,
                            // 'logo_komunitas' => $imageName,
                            'klaster_kerja' => $klaster_kerja,
                            'lingkup_komunitas' => $lingkup_komunitas,
                            'provinsi_kerja' => $provinsi,
                            'provinsi_kerja_id' => $request->provinsi_kerja,
                            'kota_kerja' => $city,
                            'kota_kerja_id' => $request->kota_kerja,
                            'status' => 2,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
            } else {
                DB::table('tb_member_premier')
                    ->where('id_kop', $id)
                    ->update(
                        [
                            'nama_komunitas' => $request->nama_komunitas,
                            'deskripsi_komunitas' => $request->deskripsi_komunitas,
                            'nama_narahubung' => $request->nama_narahubung,
                            'email_narahubung' => $request->email_narahubung,
                            'tlp_narahubung' => $request->tlp_narahubung,
                            'jenis_komunitas' => $request->jenis_komunitas,
                            'email_komunitas' => $request->email_komunitas,
                            'logo_komunitas' => $destinationPath . "/" . $imageName,
                            // 'logo_komunitas' => $imageName,
                            'klaster_kerja' => $klaster_kerja,
                            'lingkup_komunitas' => $lingkup_komunitas,
                            'status' => 2,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
            }

            DB::table('tb_member_sekunder')->where('id_kop', $id)
                ->update(
                    [
                        'tgl_berdiri_kop' => $request->tgl_komunitas,
                        'web_komunitas' => $request->web_komunitas,
                        'twitter_komunitas' => $request->twitter_komunitas,
                        'facebook_komunitas' => $request->facebook_komunitas,
                        'instagram_komunitas' => $request->instagram_komunitas,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        } else {
            $klaster_kerja = implode(";", $request->klaster_kerja);
            $lingkup_komunitas = implode(";", $request->lingkup_komunitas);

            if (is_numeric($request->provinsi_kerja)) {
                DB::table('tb_member_premier')
                    ->where('id_kop', $id)
                    ->update(
                        [
                            'nama_komunitas' => $request->nama_komunitas,
                            'deskripsi_komunitas' => $request->deskripsi_komunitas,
                            'nama_narahubung' => $request->nama_narahubung,
                            'email_narahubung' => $request->email_narahubung,
                            'tlp_narahubung' => $request->tlp_narahubung,
                            'jenis_komunitas' => $request->jenis_komunitas,
                            'email_komunitas' => $request->email_komunitas,
                            'klaster_kerja' => $klaster_kerja,
                            'lingkup_komunitas' => $lingkup_komunitas,
                            'provinsi_kerja' => $provinsi,
                            'provinsi_kerja_id' => $request->provinsi_kerja,
                            'kota_kerja' => $city,
                            'kota_kerja_id' => $request->kota_kerja,
                            'status' => 2,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
            } else {
                DB::table('tb_member_premier')
                    ->where('id_kop', $id)
                    ->update(
                        [
                            'nama_komunitas' => $request->nama_komunitas,
                            'deskripsi_komunitas' => $request->deskripsi_komunitas,
                            'nama_narahubung' => $request->nama_narahubung,
                            'email_narahubung' => $request->email_narahubung,
                            'tlp_narahubung' => $request->tlp_narahubung,
                            'jenis_komunitas' => $request->jenis_komunitas,
                            'email_komunitas' => $request->email_komunitas,
                            'klaster_kerja' => $klaster_kerja,
                            'lingkup_komunitas' => $lingkup_komunitas,
                            'status' => 2,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
            }

            DB::table('tb_member_sekunder')->where('id_kop', $id)
                ->update(
                    [
                        'tgl_berdiri_kop' => $request->tgl_komunitas,
                        'web_komunitas' => $request->web_komunitas,
                        'twitter_komunitas' => $request->twitter_komunitas,
                        'facebook_komunitas' => $request->facebook_komunitas,
                        'instagram_komunitas' => $request->instagram_komunitas,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
        }

        return;
    }

    // menghapus data
    public function delete($id) {
        $data = DB::table('tb_member_premier')->where('id_kop', $id)->first();
        $data2 = DB::table('tb_member_premier as p')
            ->join('users_to_vendor as u', 'p.id_kop', '=', 'u.id_vendor')
            ->join('users as s', 'u.id_user', '=', 's.id')
            ->join('persons as r', 's.id_person', '=', 'r.id_person')
            ->select('p.id_kop', 's.id', 'r.id_person', 'r.photo_profile')
            ->where('p.id_kop', $id)->first();
        $donasi = DB::table('tb_donasi')->where('id_kop', $id)->first();
        $cek_sekunder = DB::table('tb_member_sekunder')->where('id_kop', $id)->count();
        $cek_tersier = DB::table('tb_member_tersier')->where('id_kop', $id)->count();
        $cek_donation = DB::table('tb_donasi')->where('id_kop', $id)->count();
        $cek_assessment = DB::table('tb_assessment')->where('id_kop', $id)->count();

        // $img2 = file($data->logo_komunitas);
        // if(file_exists($data->logo_komunitas)){
        //   echo "yes";
        // } else {
        //   echo "no";
        // }
        // print_r($img2);
        // return;

        if (!empty($data2)) {
            $destinationPath1 = public_path() . '/uploads/photo_profile';
            $img1 = file($destinationPath1 . "/" . $data2->photo_profile);
            if (!empty($img1)) {
                $destinationPathOld1 = public_path() . '/uploads/photo_profile/';
                unlink($destinationPathOld1 . $data2->photo_profile);
            }
        }
        if (!empty($data)) {
            // $destinationPath2 = public_path('uploads\logo_komunitas\\');
            // $img2 = file($destinationPath2."\\".$data->logo_komunitas);
            $img2 = file($data->logo_komunitas);
            if (file_exists($data->logo_komunitas)) {
                // $destinationPathOld2 = public_path('uploads\logo_komunitas\\');
                // unlink($destinationPathOld2.$data->logo_komunitas);
                unlink($data->logo_komunitas);
            }
        }
        if (!empty($donasi)) {
            if ($donasi->bukti_donasi !== null) {
                $destinationPath3 = public_path() . '/uploads/bukti_transfer';
                $img3 = file($destinationPath3 . "/" . $donasi->bukti_donasi);
                if (!empty($img3)) {
                    $destinationPathOld3 = public_path() . '/uploads/bukti_transfer/';
                    unlink($destinationPathOld3 . $donasi->bukti_donasi);
                }
            }
        }

        if ($data->status == 2) {
            DB::table('persons')->where('id_person', $data2->id_person)->delete();
            DB::table('tb_role_user_access')->where('UserID', $data2->id)->delete();
            DB::table('users')->where('id_person', $data2->id_person)->delete();
            DB::table('users_to_vendor')->where('id_user', $data2->id)->where('id_vendor', $data2->id_kop)->delete();
        }

        if ($cek_sekunder == 0) {
            DB::table('tb_member_premier')->where('id_kop', $id)->delete();
        } else if ($cek_sekunder > 0 && $cek_tersier > 0) {
            DB::table('tb_member_premier')->where('id_kop', $id)->delete();
            DB::table('tb_member_sekunder')->where('id_kop', $id)->delete();
            DB::table('tb_member_tersier')->where('id_kop', $id)->delete();
        } else if ($cek_sekunder > 0) {
            DB::table('tb_member_premier')->where('id_kop', $id)->delete();
            DB::table('tb_member_sekunder')->where('id_kop', $id)->delete();
        } else if ($cek_tersier > 0) {
            DB::table('tb_member_premier')->where('id_kop', $id)->delete();
            DB::table('tb_member_tersier')->where('id_kop', $id)->delete();
        }

        if ($cek_donation == 1) {
            DB::table('tb_donasi')->where('id_kop', $id)->delete();
        }

        if ($cek_assessment == 1) {
            DB::table('tb_assessment')->where('id_kop', $id)->delete();
        }

        $task = Person::count();
        broadcast(new PersonEvents($task));

        return 204;
    }

    public function getKop(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $kops = DB::table('tb_member_premier')
            ->where('status', 2)
            ->where('nama_komunitas', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $kops['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $kops['searchTerm'] = $search_query ? null : '';
        }

        foreach ($kops['data'] as $key => $value) {
            if (filter_var($kops['data'][$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $kops['data'][$key]->logo_komunitas = str_replace("open", "uc", $kops['data'][$key]->logo_komunitas);
            } else {
                $path = $kops['data'][$key]->logo_komunitas;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $kops['data'][$key]->logo_komunitas = $src;
            }
        }

        return response()->json([
            'kops' => $kops,
        ]);
    }
    public function getReqKop(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $kops = DB::table('tb_member_premier')
            ->where('nama_komunitas', 'LIKE', '%' . $search_query . '%')
            ->where('status', 1)
            ->orwhere('status', 3)
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $kops['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $kops['searchTerm'] = $search_query ? null : '';
        }

        foreach ($kops['data'] as $key => $value) {
            if (filter_var($kops['data'][$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $kops['data'][$key]->logo_komunitas = str_replace("open", "uc", $kops['data'][$key]->logo_komunitas);
            } else {
                $path = $kops['data'][$key]->logo_komunitas;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $kops['data'][$key]->logo_komunitas = $src;
            }
        }

        return response()->json([
            'kops' => $kops,
        ]);
    }

    public function pdfKop($id_kop) {
        $pdf = Kop::where('id_kop', '=', $id_kop)->first();
        view()->share('pdf', $pdf);

        $nama_pdf = $pdf->id_kop . "-" . $pdf->first_name . ".pdf";
        if ($id_kop) {
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfKop');
            // download pdf
            return $pdf->download($nama_pdf);
        }
        return;
    }

    public function approveKop(Request $request) {

        $id_kop = $request->id_kop;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $username = $request->username;
        $password = $request->password;
        $catatan = $request->catatan;

        DB::table('tb_member_premier')
            ->where('id_kop', $id_kop)
            ->update(
                [
                    'status' => 2,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $firstLast = explode(" ", $request->nama_narahubung);
        if (count($firstLast) < 2) {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => "",
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        } else {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => $firstLast[1],
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        }

        $destinationPath = public_path() . '/uploads/photo_profile';
        $img = file($destinationPath . "/avatarDefault.png");
        \File::put($destinationPath . '/' . $username . ".png", $img);

        $id = DB::getPdo()->lastInsertId();

        DB::table('users')->insert(
            [
                'id_person' => $id,
                'username' => $username,
                'password' => bcrypt($password),
                'email' => $email_narahubung,
                'role' => 'R002',
                'status' => 1,
            ]
        );
        $data = DB::table('users')
            ->where('id_person', $id)
            ->first();
        $dataMenu = DB::table('tb_role_master')
            ->where('kd_role', 'R002')
            ->get();
        foreach ($dataMenu as $key) {
            DB::table('tb_role_user_access')->insert(
                [
                    'kd_role' => $key->kd_role,
                    'UserID' => $data->id,
                    'MenuID' => $key->MenuID,
                    'DetailID' => $key->DetailID,
                    'DetailID2' => $key->DetailID2,
                    'Link' => $key->Link,
                    'Action' => $key->Action,
                ]
            );
        }

        DB::table('users_to_vendor')->insert(
            [
                'id_user' => $data->id,
                'id_vendor' => $id_kop,
            ]
        );

        $task = Person::count();
        broadcast(new PersonEvents($task));

        $get = DB::table('tb_member_premier')->where('id_kop', $id_kop)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'Approve')->first();
        array_push($content, $objDemo->content, $username, $password, $email_narahubung, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_komunitas);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new ApproveKop($content));
        }
    }

    public function notapproveKop(Request $request) {

        $id_kop = $request->id_kop;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $catatan = $request->catatan;

        DB::table('tb_member_premier')
            ->where('id_kop', $id_kop)
            ->update(
                [
                    'status' => 3,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $get = DB::table('tb_member_premier')->where('id_kop', $id_kop)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'NotApprove')->first();
        array_push($content, $objDemo->content, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_komunitas);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new NotApproveKop($content));
        }
    }

    public function getListKop($limit) {
        $test = array();
        if ($limit == 0) {
            array_push($test, array("id_kop" => "",
                "nama_komunitas" => "",
                "nama_narahubung" => "",
                "email_narahubung" => "",
                "jenis_komunitas" => "",
                "email_komunitas" => "",
                "logo_komunitas" => "",
                "klaster_kerja" => "",
                "lingkup_komunitas" => "",
                "provinsi_kerja" => "",
                "kota_kerja" => "",
                "status" => "",
                "catatan" => "",
                "created_at" => "",
                "updated_at" => ""));
        }
        $data = DB::table('tb_member_premier as p')
            ->leftjoin('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')->where('p.status', 2)->offset($limit)->limit(6)->get();

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_komunitas = str_replace("open", "uc", $data[$key]->logo_komunitas);
            } else {
                $path = $data[$key]->logo_komunitas;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->logo_komunitas = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }
    public function getListKopByJenis($id, $category, $cluster, $scope) {
        if ($category !== "kosong" && $cluster == "kosong" && $scope == "kosong") {
            $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->get();
        } else if ($category == "kosong" && $cluster !== "kosong" && $scope == "kosong") {
            $data = Kop::where('status', 2)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
        } else if ($category == "kosong" && $cluster == "kosong" && $scope !== "kosong") {
            $data = Kop::where('status', 2)->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category !== "kosong" && $cluster !== "kosong" && $scope == "kosong") {
            $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->where('klaster_kerja', 'like', '%' . $cluster . '%')->get();
        } else if ($category !== "kosong" && $cluster == "kosong" && $scope !== "kosong") {
            $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category == "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
            $data = Kop::where('status', 2)->where('klaster_kerja', 'like', '%' . $cluster . '%')->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category !== "kosong" && $cluster !== "kosong" && $scope !== "kosong") {
            $data = Kop::where('status', 2)->where('jenis_komunitas', $category)->where('klaster_kerja', 'like', '%' . $cluster . '%')->where('lingkup_komunitas', 'like', '%' . $scope . '%')->get();
        } else if ($category == "kosong" || $cluster == "kosong" || $scope == "kosong") {
            $data = Kop::where('status', 2)->get();
        }

        $test = array();
        array_push($test, array("id_kop" => "",
            "nama_komunitas" => "",
            "nama_narahubung" => "",
            "email_narahubung" => "",
            "jenis_komunitas" => "",
            "email_komunitas" => "",
            "logo_komunitas" => "",
            "klaster_kerja" => "",
            "lingkup_komunitas" => "",
            "provinsi_kerja" => "",
            "kota_kerja" => "",
            "status" => "",
            "catatan" => "",
            "created_at" => "",
            "updated_at" => ""));

        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_komunitas = str_replace("open", "uc", $data[$key]->logo_komunitas);
            } else {
                $path = $data[$key]->logo_komunitas;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->logo_komunitas = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function getListKopBySearch($search) {
        if ($search !== "kosong" || $search !== "") {
            $data = Kop::where('status', 2)->where('nama_komunitas', 'like', '%' . $search . '%')->get();
        } else {
            $data = Kop::where('status', 2)->get();
        }

        $test = array();
        array_push($test, array("id_kop" => "",
            "nama_komunitas" => "",
            "nama_narahubung" => "",
            "email_narahubung" => "",
            "jenis_komunitas" => "",
            "email_komunitas" => "",
            "logo_komunitas" => "",
            "klaster_kerja" => "",
            "lingkup_komunitas" => "",
            "provinsi_kerja" => "",
            "kota_kerja" => "",
            "status" => "",
            "catatan" => "",
            "created_at" => "",
            "updated_at" => ""));
        foreach ($data as $key => $value) {
            if (filter_var($data[$key]->logo_komunitas, FILTER_VALIDATE_URL)) {
                $data[$key]->logo_komunitas = str_replace("open", "uc", $data[$key]->logo_komunitas);
            } else {
                $path = $data[$key]->logo_komunitas;
                $src = str_replace(public_path(), url('/'), $path);

                // $dataImg = file_get_contents($path);
                // $type = pathinfo($path, PATHINFO_EXTENSION);
                // $base64 = base64_encode($dataImg);
                // $src = 'data:image/' . $type . ';base64,' . $base64;

                $data[$key]->logo_komunitas = $src;
            }
        }

        foreach ($data as $key) {
            array_push($test, $key);
        }
        return $test;
    }

    public function sendMaillAllKop(Request $request) {
        $content = $request->content;

        $data = DB::table('users as u')
            ->leftjoin('users_to_vendor as s', 'u.id', '=', 's.id_user')
            ->select('u.*', 's.id_vendor')
            ->where('status', 1)
            ->get();

        $sendEmail = array();
        foreach ($data as $key) {
            array_push($sendEmail, array($key->email, $key->username, $content));
        }

        foreach ($sendEmail as $key) {
            Mail::to($key[0])->send(new BulkMail($key));
        }
        return;
    }

    public function sendMaillAllKop2(Request $request) {
        $content = $request->content;

        $data = DB::table('users_to_vendor as s')
            ->leftjoin('users as u', 'u.id', '=', 's.id_user')
            ->select('u.*', 's.id_vendor')
            ->where('u.status', 1)
            ->get();

        $sendEmail = array();
        foreach ($data as $key) {
            $str = $key->email;
            if (!isset($str) || trim($str) === '' || trim($str) === '-') {
                continue;
            }
            array_push($sendEmail, array($key->email, $key->username, $content, $key->id_kop));
        }

        $failEmail = array();
        $successEmail = array();
        $send = date("Y-m-d H:i:s");
        foreach ($sendEmail as $key) {
            try {
                Mail::to($key[0])->send(new BulkMail($key));
                array_push($successEmail, array($key[0], "success"));
                DB::table('tb_mail_history')->insert(
                    [
                        'id_kop' => $key[3],
                        'body_mail' => $content,
                        'send' => $send,
                        'status' => "1",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            } catch (Exception $e) {
                if (count(Mail::failures()) > 0) {
                    array_push($failEmail, array($key[0], "fail"));
                    DB::table('tb_mail_history')->insert(
                        [
                            'id_kop' => $key[3],
                            'body_mail' => $content,
                            'send' => $send,
                            'status' => "2",
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }
            }
        }

        $failEmail2 = array();
        foreach ($failEmail as $key) {
            array_push($failEmail2, $key[0]);
        }

        return response()->json(['fail' => $failEmail, 'success' => $successEmail, 'emailfail' => implode(", ", $failEmail2)]);
    }

    public function sendMaillAllKopFix($email, $id, Request $request) {
        $content = $request->content;
        $subject = $request->subject;

        $data = DB::table('users_to_vendor as s')
            ->leftjoin('users as u', 'u.id', '=', 's.id_user')
            ->leftjoin('tb_member_premier as p', 's.id_vendor', '=', 'p.id_kop')
            ->select('u.*', 's.id_vendor', 'p.email_komunitas')
            ->where('s.id_kop', $id)
            ->get();

        $sendEmail = array();
        foreach ($data as $key) {
            $str = $key->email;
            if (!isset($str) || trim($str) === '' || trim($str) === '-') {
                continue;
            }
            array_push($sendEmail, array($key->email_komunitas, $key->username, $content, $key->id_kop, $subject));
        }

        $failEmail = array();
        $successEmail = array();
        $send = date("Y-m-d H:i:s");
        foreach ($sendEmail as $key) {
            try {
                Mail::to($key[0])->send(new BulkMail($key));
                array_push($successEmail, array($key[0], "success"));
                DB::table('tb_mail_history')->insert(
                    [
                        'id_kop' => $key[3],
                        'subject' => $subject,
                        'body_mail' => $content,
                        'send' => $send,
                        'status' => "1",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            } catch (Exception $e) {
                if (count(Mail::failures()) > 0) {
                    array_push($failEmail, array($key[0], "fail"));
                    DB::table('tb_mail_history')->insert(
                        [
                            'id_kop' => $key[3],
                            'subject' => $subject,
                            'body_mail' => $content,
                            'send' => $send,
                            'status' => "2",
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }
            }
        }

        return $email;
    }

    public function sendMailltoKop(Request $request) {
        $subject = $request->subject;
        $content = $request->content;
        $email = $request->email;
        $namakop = $request->namakop;

        $data1 = DB::table('tb_member_premier')->where('nama_komunitas', $namakop)->where('email_komunitas', $email)->first();
        $data2 = DB::table('users_to_vendor')->where('id_vendor', $data1->id_kop)->first();
        $data3 = $data = DB::table('users')->where('id', $data2->id_user)->where('status', 1)->first();

        $sendEmail = array();
        array_push($sendEmail, array($content, $data3->username, $subject));

        Mail::to($email)->send(new KopMail($sendEmail));

        $send = date("Y-m-d H:i:s");
        if (count(Mail::failures()) > 0) {
            DB::table('tb_mail_history')->insert(
                [
                    'id_kop' => $data1->id_kop,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "2",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_mail_history')->insert(
                [
                    'id_kop' => $data1->id_kop,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "1",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }
        return;
    }

    public function getHistoryMail(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_mail_history as m')
            ->leftjoin('tb_member_premier as p', 'm.id_kop', '=', 'p.id_kop')
            ->select('m.*', 'p.nama_komunitas')
            ->where('m.body_mail', 'LIKE', '%' . $search_query . '%')
            ->groupBy('m.body_mail', 'm.subject')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }

    public function delHistoryMail($id) {

        $data = DB::table('tb_mail_history')->where('id', $id)->first();

        DB::table('tb_mail_history')->where('subject', $data->subject)->where('body_mail', $data->body_mail)->delete();

        return 204;
    }

    public function getHistoryMailID($id) {
        $data = DB::table('tb_mail_history')->where('id', $id)->first();

        return json_encode($data);
    }

    public function getHistoryMailView($id, Request $request) {
        $data = DB::table('tb_mail_history')->where('id', $id)->first();

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_mail_history as m')
            ->leftjoin('tb_member_premier as p', 'm.id_kop', '=', 'p.id_kop')
            ->select('m.*', 'p.nama_komunitas')
            ->where('m.subject', $data->subject)
            ->where('m.body_mail', $data->body_mail)
            ->where('p.nama_komunitas', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }

}
