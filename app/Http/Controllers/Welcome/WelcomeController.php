<?php

namespace App\Http\Controllers\Welcome;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use App\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class WelcomeController extends Controller
{
	 public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(){
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        if(Session::get('login')){
            return redirect('dashboard');
        }
        else{
            return view('login');
        }
    }
    
    public function dashboard(Request $request){
        if(!Session::get('login')){

            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        if(Session::get('login')){
            return view('content');
        }
        else{
            return view('login');
        }
    }

    public function dashboardIn(){
        return view('content');
    }

    public function comming(){
        return view('comming');
    }

    public function cekInspeksiMobile(Request $request, $id_person) {
        $yyyy = Carbon::now()->year;
        $mm = Carbon::now()->month;
        $start = new Carbon($yyyy."-".$mm."-01");
        $end = new Carbon($yyyy."-".$mm."-01");
        $end = $end->add(1, 'month')->add(-1,'days');
        $end = date ("Y-m-d", strtotime($end));

        $date = array();
        while (strtotime($start) <= strtotime($end)) {
            $start = date ("Y-m-d", strtotime($start));
            $cek = DB::table('tb_inspeksi')->where('tgl_inspeksi',$start)->count();
            $data = DB::table('tb_inspeksi')->select('id_inspeksi')->where('tgl_inspeksi',$start)->where('nama_keeper_tugas',$id_person)->first();
            if($cek > 0){
                if(!empty($data)){
                    array_push($date, array($data->id_inspeksi, $start, "done"));
                } else {
                    array_push($date, array("", $start, "notyet"));
                }
            } else {
                array_push($date, array("", $start, "notyet"));
            }
            $start = date ("Y-m-d", strtotime("+1 day", strtotime($start)));
        }

        return response()->json(
            [   
                'start' => $start,
                'end' => $end,
                'date' => $date,
                'meta' => [
                    'success' => 1,
                    'message' => 'Success',
                ],
            ], 200);

    }
}
