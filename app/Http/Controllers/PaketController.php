<?php
namespace App\Http\Controllers;

use App\Events\PersonEvents
// import file model Vendor
use DateTime;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;

class PaketController extends Controller {
    
    // menambah data vendor
    public function store(Request $request) {
        $image = $request->logo_vendor; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $destinationPath = public_path() . '/uploads/logo_vendor';
        \File::put($destinationPath . '/' . $imageName, base64_decode($image));

        Vendor::insert(
            [
                'logo_vendor' =>  $imageName,
                'nama_vendor' =>  $request->nama_vendor,
                'deskripsi_vendor' =>  $request->deskripsi_vendor,
                'nama_narahubung' =>  $request->nama_narahubung,
                'email_narahubung' =>  $request->email_narahubung,
                'tlp_narahubung' =>  $request->tlp_narahubung,
                'jenis_vendor' =>  $request->jenis_vendor,
                'email_vendor' =>  $request->email_vendor,
                'tlp_vendor' =>  $request->tlp_vendor,
                'fax_vendor' =>  $request->fax_vendor,
                'npwp_vendor' =>  $request->npwp_vendor,
                'pkp_vendor' =>  $request->pkp_vendor,
                'alamat_vendor' =>  $request->alamat_vendor,
                'provinsi_vendor' =>  $request->provinsi_vendor,
                'kota_vendor' =>  $request->kota_vendor,
                'kodepos_vendor' =>  $request->kodepos_vendor,
                'status' => 2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $data = DB::table('tb_vendor_primary')
            ->where('nama_vendor', $request->nama_vendor)
            ->where('jenis_vendor', $request->jenis_vendor)
            ->where('nama_narahubung', $request->nama_narahubung)
            ->first();

        DB::table('tb_vendor_secondary')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'tgl_berdiri_vendor' => $request->tgl_vendor,
                'web_vendor' => $request->web_vendor,
                'facebook_vendor' => $request->facebook_vendor,
                'instagram_vendor' => $request->instagram_vendor,
                'cabang_vendor' =>  $request->cabang_vendor,
                'alamat_pusat' =>  $request->alamat_pusat,
                'email_pusat' =>  $request->email_pusat,
                'tlp_pusat' =>  $request->tlp_pusat,
                'fax_pusat' =>  $request->fax_pusat,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $image2 = $request->photo_pemilik; // your base64 encoded
        $image2 = str_replace('data:image/png;base64,', '', $image2);
        $image2 = str_replace(' ', '+', $image);
        $imageName2 = str_random(10) . '.' . 'png';
        $destinationPath2 = public_path() . '/uploads/photo_owner';
        \File::put($destinationPath2 . '/' . $imageName2, base64_decode($image2));

        DB::table('tb_vendor_third')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'photo_pemilik' => $imageName2,
                'nama_pemilik' => $request->nama_pemilik,
                'alamat_pemilik' => $request->alamat_pemilik,
                'ktp_pemilik' => $request->ktp_pemilik,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

        $file1 = $request->file('file1');
        if(!empty($file1)){
          $imageNameFile = str_random(10).'-pendirian.'.'pdf';
          \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile, $file1);
          move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_akta/".$imageNameFile);
        }

        $file2 = $request->file('file2');
        if(!empty($file2)){
          $imageNameFile2 = str_random(10).'-perubahan.'.'pdf';
          \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile2, $file2);
          move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_akta/".$imageNameFile2);
        }

        DB::table('tb_vendor_dokumen')->insert(
            [
                'id_vendor' => $data->id_vendor,
                'no_akta_diri' => $request->no_akta_diri,
                'notaris_diri' => $request->notaris_diri,
                'tgl_akta_diri' => $request->tgl_akta_diri,
                'file_pendirian' => $imageNameFile,
                'no_akta_ubah' => $request->no_akta_ubah,
                'notaris_ubah' => $request->notaris_ubah,
                'tgl_akta_ubah' => $request->tgl_akta_ubah,
                'file_perubahan' => $imageNameFile2,
                'created_at' => date("Y-m-d H:i:s"),
            ]
        );

                            $firstLast = explode(" ", $request->nama_narahubung);
                            $username = strtolower($firstLast[0]) . rand(10, 10000);

                            $firstLast = explode(" ", $request->nama_narahubung);
                            if (count($firstLast) < 2) {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => "",
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            } else {
                                Person::insert(
                                    [
                                        'photo_profile' => $username . ".png",
                                        'first_name' => $firstLast[0],
                                        'last_name' => $firstLast[1],
                                        'status' => 1,
                                        'status_user' => 1,
                                    ]
                                );
                            }

                            $destinationPath = public_path() . '/uploads/photo_profile';
                            $img = file($destinationPath . "/avatarDefault.png");
                            \File::put($destinationPath . '/' . $username . ".png", $img);

                            $id = DB::getPdo()->lastInsertId();

                            DB::table('users')->insert(
                                [
                                    'id_person' => $id,
                                    'username' => $username,
                                    'password' => bcrypt($username),
                                    'email' => $request->email_narahubung,
                                    'role' => 'R002',
                                    'status' => 1,
                                ]
                            );
                            $dataUser = DB::table('users')
                                ->where('id_person', $id)
                                ->first();
                            $dataMenu = DB::table('tb_role_master')
                                ->where('kd_role', 'R002')
                                ->get();
                            foreach ($dataMenu as $key) {
                                DB::table('tb_role_user_access')->insert(
                                    [
                                        'kd_role' => $key->kd_role,
                                        'UserID' => $dataUser->id,
                                        'MenuID' => $key->MenuID,
                                        'DetailID' => $key->DetailID,
                                        'DetailID2' => $key->DetailID2,
                                        'Link' => $key->Link,
                                        'Action' => $key->Action,
                                    ]
                                );
                            }

                            DB::table('users_to_vendor')->insert(
                                [
                                    'id_user' => $dataUser->id,
                                    'id_vendor' => $data->id_vendor,
                                ]
                            );


        // $objDemo = DB::table('tb_content_email')->where('type', 'Register')->first();

        // $sendEmail = array();
        // array_push($sendEmail, $request->email_narahubung, $request->email_vendor);

        // foreach ($sendEmail as $key) {
        //     Mail::to($key)->send(new ThanksRegister($objDemo));
        // }

        $task = Vendor::count();
        broadcast(new VendorEvents($task));

        $task2 = Person::count();
        broadcast(new PersonEvents($task2));

        return;
    }

    // mengubah data
    public function updateVendor($id, Request $request) {

        if ($request->logo_vendor !== "kosong") {
            $image = $request->logo_vendor; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/logo_vendor';

            if ($destinationPath."/".$request->logo_vendor_old != $destinationPath . "/uploads/logo_vendor/imgDefault.png") {
                if (file_exists($destinationPath."/".$request->logo_vendor_old)) {
                    unlink($destinationPath."/".$request->logo_vendor_old);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));

            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'logo_vendor' =>  $imageName,
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'npwp_vendor' =>  $request->npwp_vendor,
                        'pkp_vendor' =>  $request->pkp_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'tgl_berdiri_vendor' => $request->tgl_vendor,
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_third')->where('id_vendor', $id)
                ->update(
                    [
                        'nama_pemilik' => $request->nama_pemilik,
                        'alamat_pemilik' => $request->alamat_pemilik,
                        'ktp_pemilik' => $request->ktp_pemilik,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );



            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-pendirian.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile, $file1);
              move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_akta/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file1_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file1_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-perubahan.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile2, $file2);
              move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_akta/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file2_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file2_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        } else if ($request->photo_pemilik !== "kosong") {
            $image = $request->photo_pemilik; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/photo_owner';

            if ($destinationPath."/".$request->photo_pemilik_old != $destinationPath . "/uploads/photo_owner/imgDefault.png") {
                if (file_exists($destinationPath."/".$request->photo_pemilik_old)) {
                    unlink($destinationPath."/".$request->photo_pemilik_old);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));

            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'npwp_vendor' =>  $request->npwp_vendor,
                        'pkp_vendor' =>  $request->pkp_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'tgl_berdiri_vendor' => $request->tgl_vendor,
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_third')->where('id_vendor', $id)
                ->update(
                    [
                        'photo_pemilik' => $imageName,
                        'nama_pemilik' => $request->nama_pemilik,
                        'alamat_pemilik' => $request->alamat_pemilik,
                        'ktp_pemilik' => $request->ktp_pemilik,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-pendirian.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile, $file1);
              move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_akta/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file1_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file1_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-perubahan.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile2, $file2);
              move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_akta/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file2_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file2_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        } else if ($request->logo_vendor !== "kosong" && $request->photo_pemilik !== "kosong") {
            $image = $request->logo_vendor; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $destinationPath = public_path() . '/uploads/logo_vendor';

            if ($destinationPath."/".$request->logo_vendor_old != $destinationPath . "/uploads/logo_vendor/imgDefault.png") {
                if (file_exists($destinationPath."/".$request->logo_vendor_old)) {
                    unlink($destinationPath."/".$request->logo_vendor_old);
                }
            }

            $image2 = $request->photo_pemilik; // your base64 encoded
            $image2 = str_replace('data:image/png;base64,', '', $image2);
            $image2 = str_replace(' ', '+', $image2);
            $imageName2 = str_random(10) . '.' . 'png';
            $destinationPath2 = public_path() . '/uploads/photo_owner';

            if ($destinationPath2."/".$request->photo_pemilik_old != $destinationPath2 . "/uploads/photo_owner/imgDefault.png") {
                if (file_exists($destinationPath2."/".$request->photo_pemilik_old)) {
                    unlink($destinationPath2."/".$request->photo_pemilik_old);
                }
            }

            \File::put($destinationPath . '/' . $imageName, base64_decode($image));

            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'logo_vendor' => $imageName,
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'npwp_vendor' =>  $request->npwp_vendor,
                        'pkp_vendor' =>  $request->pkp_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'tgl_berdiri_vendor' => $request->tgl_vendor,
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_third')->where('id_vendor', $id)
                ->update(
                    [
                        'photo_pemilik' => $imageName2,
                        'nama_pemilik' => $request->nama_pemilik,
                        'alamat_pemilik' => $request->alamat_pemilik,
                        'ktp_pemilik' => $request->ktp_pemilik,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-pendirian.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile, $file1);
              move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_akta/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file1_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file1_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-perubahan.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile2, $file2);
              move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_akta/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file2_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file2_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        } else {
            DB::table('tb_vendor_primary')
                ->where('id_vendor', $id)
                ->update(
                    [
                        'nama_vendor' =>  $request->nama_vendor,
                        'deskripsi_vendor' =>  $request->deskripsi_vendor,
                        'nama_narahubung' =>  $request->nama_narahubung,
                        'email_narahubung' =>  $request->email_narahubung,
                        'tlp_narahubung' =>  $request->tlp_narahubung,
                        'jenis_vendor' =>  $request->jenis_vendor,
                        'email_vendor' =>  $request->email_vendor,
                        'tlp_vendor' =>  $request->tlp_vendor,
                        'fax_vendor' =>  $request->fax_vendor,
                        'npwp_vendor' =>  $request->npwp_vendor,
                        'pkp_vendor' =>  $request->pkp_vendor,
                        'alamat_vendor' =>  $request->alamat_vendor,
                        'provinsi_vendor' =>  $request->provinsi_vendor,
                        'kota_vendor' =>  $request->kota_vendor,
                        'kodepos_vendor' =>  $request->kodepos_vendor,
                        'status' => 2,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_secondary')->where('id_vendor', $id)
                ->update(
                    [
                        'tgl_berdiri_vendor' => $request->tgl_vendor,
                        'web_vendor' => $request->web_vendor,
                        'facebook_vendor' => $request->facebook_vendor,
                        'instagram_vendor' => $request->instagram_vendor,
                        'cabang_vendor' =>  $request->cabang_vendor,
                        'alamat_pusat' =>  $request->alamat_pusat,
                        'email_pusat' =>  $request->email_pusat,
                        'tlp_pusat' =>  $request->tlp_pusat,
                        'fax_pusat' =>  $request->fax_pusat,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            DB::table('tb_vendor_third')->where('id_vendor', $id)
                ->update(
                    [
                        'nama_pemilik' => $request->nama_pemilik,
                        'alamat_pemilik' => $request->alamat_pemilik,
                        'ktp_pemilik' => $request->ktp_pemilik,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );

            $file1 = $request->file('file1');
            if(!empty($file1)){
              $imageNameFile = str_random(10).'-pendirian.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile, $file1);
              move_uploaded_file($_FILES['file1']['tmp_name'], "uploads/file_akta/".$imageNameFile);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file1_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file1_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)
                ->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_pendirian' => $imageNameFile,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

            $file2 = $request->file('file2');
            if(!empty($file2)){
              $imageNameFile2 = str_random(10).'-perubahan.'.'pdf';
              \File::put(public_path()."/uploads/file_akta". '/' . $imageNameFile2, $file2);
              move_uploaded_file($_FILES['file2']['tmp_name'], "uploads/file_akta/".$imageNameFile2);

              if (file_exists(public_path()."/uploads/file_akta/".$request->file2_old)) {
                    unlink(public_path()."/uploads/file_akta/".$request->file2_old);
                }

              DB::table('tb_vendor_dokumen')->where('id_vendor', $id)
                ->update(
                  [
                      'no_akta_diri' => $request->no_akta_diri,
                      'notaris_diri' => $request->notaris_diri,
                      'no_akta_ubah' => $request->no_akta_ubah,
                      'notaris_ubah' => $request->notaris_ubah,
                      'tgl_akta_ubah' => $request->tgl_akta_ubah,
                      'tgl_akta_diri' => $request->tgl_akta_diri,
                      'file_perubahan' => $imageNameFile2,
                      'updated_at' => date("Y-m-d H:i:s"),
                  ]
              );
            }

        }

        return;
    }

    // menghapus data
    public function deleteVendor($id) {
        $data = DB::table('tb_vendor_primary as p')
                ->join('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
                ->where('p.id_vendor', $id)
                ->first();
        $data2 = DB::table('tb_vendor_primary as p')
            ->join('users_to_vendor as u', 'p.id_vendor', '=', 'u.id_vendor')
            ->join('users as s', 'u.id_user', '=', 's.id')
            ->join('persons as r', 's.id_person', '=', 'r.id_person')
            ->select('p.id_vendor', 's.id', 'r.id_person', 'r.photo_profile')
            ->where('p.id_vendor', $id)->first();
        $dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->first();
        $cek_sekunder = DB::table('tb_vendor_secondary')->where('id_vendor', $id)->count();
        $cek_tersier = DB::table('tb_vendor_third')->where('id_vendor', $id)->count();
        $cek_dokumen = DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->count();

        if (!empty($data)) {
            $destinationPath2 = public_path() . '/uploads/logo_vendor';
            $img2 = file($destinationPath2."/".$data->logo_vendor);
            if (file_exists($destinationPath2."/".$data->logo_vendor)) {
                $destinationPathOld2 = public_path() . '/uploads/logo_vendor/';
                unlink($destinationPathOld2.$data->logo_vendor);
            }
            $destinationPath22 = public_path() . '/uploads/photo_owner';
            $img22 = file($destinationPath22."/".$data->photo_pemilik);
            if (file_exists($destinationPath22."/".$data->photo_pemilik)) {
                $destinationPathOld22 = public_path() . '/uploads/photo_owner/';
                unlink($destinationPathOld22.$data->photo_pemilik);
            }
        }
        if (!empty($data2)) {
            $destinationPath1 = public_path() . '/uploads/photo_profile';
            $img1 = file($destinationPath1 . "/" . $data2->photo_profile);
            if (!empty($img1)) {
                $destinationPathOld1 = public_path() . '/uploads/photo_profile/';
                unlink($destinationPathOld1 . $data2->photo_profile);
            }
        }
        if (!empty($dokumen)) {
            if ($dokumen->file_pendirian !== null) {
                $destinationPath3 = public_path() . '/uploads/file_akta';
                $img3 = file($destinationPath3 . "/" . $dokumen->file_pendirian);
                if (!empty($img3)) {
                    $destinationPathOld3 = public_path() . '/uploads/file_akta/';
                    unlink($destinationPathOld3 . $dokumen->file_pendirian);
                }
            }
            if ($dokumen->file_perubahan !== null) {
                $destinationPath33 = public_path() . '/uploads/file_akta';
                $img33 = file($destinationPath33 . "/" . $dokumen->file_perubahan);
                if (!empty($img33)) {
                    $destinationPathOld33 = public_path() . '/uploads/file_akta/';
                    unlink($destinationPathOld33 . $dokumen->file_perubahan);
                }
            }
        }

        if ($data->status == 2) {
            DB::table('persons')->where('id_person', $data2->id_person)->delete();
            DB::table('tb_role_user_access')->where('UserID', $data2->id)->delete();
            DB::table('users')->where('id_person', $data2->id_person)->delete();
            DB::table('users_to_vendor')->where('id_user', $data2->id)->where('id_vendor', $data2->id_vendor)->delete();
        }

        if ($cek_sekunder == 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0 && $cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        } else if ($cek_sekunder > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_secondary')->where('id_vendor', $id)->delete();
        } else if ($cek_tersier > 0) {
            DB::table('tb_vendor_primary')->where('id_vendor', $id)->delete();
            DB::table('tb_vendor_third')->where('id_vendor', $id)->delete();
        }

        if ($cek_dokumen == 1) {
            DB::table('tb_vendor_dokumen')->where('id_vendor', $id)->delete();
        }

        $task = Person::count();
        broadcast(new PersonEvents($task));

        return 204;
    }

    public function getVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary')
            ->where('status', 2)
            ->where('nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();

        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            if (filter_var($vendors['data'][$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $vendors['data'][$key]->logo_vendor = str_replace("open", "uc", $vendors['data'][$key]->logo_vendor);
            } else {
                $destinationPath = '/uploads/logo_vendor';
                $path = $destinationPath."/".$vendors['data'][$key]->logo_vendor;
                // $src = str_replace(public_path(), url('/'), $path);

                $vendors['data'][$key]->logo_vendor = $path;
            }
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function getReqVendor(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'first_name');
        $type = $request->input('type', 'asc');
        $vendors = DB::table('tb_vendor_primary')
            ->where('nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->where('status', 1)
            ->orwhere('status', 3)
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $vendors['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $vendors['searchTerm'] = $search_query ? null : '';
        }

        foreach ($vendors['data'] as $key => $value) {
            if (filter_var($vendors['data'][$key]->logo_vendor, FILTER_VALIDATE_URL)) {
                $vendors['data'][$key]->logo_vendor = str_replace("open", "uc", $vendors['data'][$key]->logo_vendor);
            } else {
                $destinationPath = '/uploads/logo_vendor';
                $path = $destinationPath."/".$vendors['data'][$key]->logo_vendor;
                // $src = str_replace(public_path(), url('/'), $path);

                $vendors['data'][$key]->logo_vendor = $path;
            }
        }

        return response()->json([
            'vendors' => $vendors,
        ]);
    }

    public function approveVendor(Request $request) {

        $id_vendor = $request->id_vendor;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $username = $request->username;
        $password = $request->password;
        $catatan = $request->catatan;

        DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->update(
                [
                    'status' => 2,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $firstLast = explode(" ", $request->nama_narahubung);
        if (count($firstLast) < 2) {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => "",
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        } else {
            Person::insert(
                [
                    'photo_profile' => $username . ".png",
                    'first_name' => $firstLast[0],
                    'last_name' => $firstLast[1],
                    'status' => 1,
                    'status_user' => 1,
                ]
            );
        }

        $destinationPath = public_path() . '/uploads/photo_profile';
        $img = file($destinationPath . "/avatarDefault.png");
        \File::put($destinationPath . '/' . $username . ".png", $img);

        $id = DB::getPdo()->lastInsertId();

        DB::table('users')->insert(
            [
                'id_person' => $id,
                'username' => $username,
                'password' => bcrypt($password),
                'email' => $email_narahubung,
                'role' => 'R002',
                'status' => 1,
            ]
        );
        $data = DB::table('users')
            ->where('id_person', $id)
            ->first();
        $dataMenu = DB::table('tb_role_master')
            ->where('kd_role', 'R002')
            ->get();
        foreach ($dataMenu as $key) {
            DB::table('tb_role_user_access')->insert(
                [
                    'kd_role' => $key->kd_role,
                    'UserID' => $data->id,
                    'MenuID' => $key->MenuID,
                    'DetailID' => $key->DetailID,
                    'DetailID2' => $key->DetailID2,
                    'Link' => $key->Link,
                    'Action' => $key->Action,
                ]
            );
        }

        DB::table('users_to_vendor')->insert(
            [
                'id_user' => $data->id,
                'id_vendor' => $id_vendor,
            ]
        );

        $task = Person::count();
        broadcast(new PersonEvents($task));

        $get = DB::table('tb_vendor_primary')->where('id_vendor', $id_vendor)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'Approve')->first();
        array_push($content, $objDemo->content, $username, $password, $email_narahubung, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_vendor);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new ApproveVendor($content));
        }
    }

    public function notapproveVendor(Request $request) {

        $id_vendor = $request->id_vendor;
        $nama_narahubung = $request->nama_narahubung;
        $email_narahubung = $request->email_narahubung;
        $catatan = $request->catatan;

        DB::table('tb_vendor_primary')
            ->where('id_vendor', $id_vendor)
            ->update(
                [
                    'status' => 3,
                    'catatan' => $catatan,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

        $get = DB::table('tb_vendor_primary')->where('id_vendor', $id_vendor)->first();

        $content = array();
        $objDemo = DB::table('tb_content_email')->where('type', 'NotApprove')->first();
        array_push($content, $objDemo->content, $catatan);

        $sendEmail = array();
        array_push($sendEmail, $get->email_narahubung, $get->email_vendor);

        foreach ($sendEmail as $key) {
            Mail::to($key)->send(new NotApproveVendor($content));
        }
    }

    public function sendMaillAllVendorFix($email, $id, Request $request) {
        $content = $request->content;
        $subject = $request->subject;

        $data = DB::table('users_to_vendor as s')
            ->leftjoin('users as u', 'u.id', '=', 's.id_user')
            ->leftjoin('tb_vendor_primary as p', 's.id_vendor', '=', 'p.id_vendor')
            ->select('u.*', 's.id_vendor', 'p.email_vendor')
            ->where('s.id_vendor', $id)
            ->get();

        $sendEmail = array();
        foreach ($data as $key) {
            $str = $key->email;
            if (!isset($str) || trim($str) === '' || trim($str) === '-') {
                continue;
            }
            array_push($sendEmail, array($key->email_vendor, $key->username, $content, $key->id_vendor, $subject));
        }

        $failEmail = array();
        $successEmail = array();
        $send = date("Y-m-d H:i:s");
        foreach ($sendEmail as $key) {
            try {
                Mail::to($key[0])->send(new BulkMail($key));
                array_push($successEmail, array($key[0], "success"));
                DB::table('tb_vendor_mail_history')->insert(
                    [
                        'id_vendor' => $key[3],
                        'subject' => $subject,
                        'body_mail' => $content,
                        'send' => $send,
                        'status' => "1",
                        'created_at' => date("Y-m-d H:i:s"),
                    ]
                );
            } catch (Exception $e) {
                if (count(Mail::failures()) > 0) {
                    array_push($failEmail, array($key[0], "fail"));
                    DB::table('tb_vendor_mail_history')->insert(
                        [
                            'id_vendor' => $key[3],
                            'subject' => $subject,
                            'body_mail' => $content,
                            'send' => $send,
                            'status' => "2",
                            'created_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }
            }
        }

        return $email;
    }

    public function sendMailltoVendor(Request $request) {
        $subject = $request->subject;
        $content = $request->content;
        $email = $request->email;
        $namavendor = $request->namavendor;

        $data1 = DB::table('tb_vendor_primary')->where('nama_vendor', $namavendor)->where('email_vendor', $email)->first();
        $data2 = DB::table('users_to_vendor')->where('id_vendor', $data1->id_vendor)->first();
        $data3 = $data = DB::table('users')->where('id', $data2->id_user)->where('status', 1)->first();

        $sendEmail = array();
        array_push($sendEmail, array($content, $data3->username, $subject));

        Mail::to($email)->send(new VendorMail($sendEmail));

        $send = date("Y-m-d H:i:s");
        if (count(Mail::failures()) > 0) {
            DB::table('tb_vendor_mail_history')->insert(
                [
                    'id_vendor' => $data1->id_vendor,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "2",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        } else {
            DB::table('tb_vendor_mail_history')->insert(
                [
                    'id_vendor' => $data1->id_vendor,
                    'subject' => $subject,
                    'body_mail' => $content,
                    'send' => $send,
                    'status' => "1",
                    'created_at' => date("Y-m-d H:i:s"),
                ]
            );
        }
        return;
    }

    public function getHistoryVendorMail(Request $request) {

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_vendor_mail_history as m')
            ->leftjoin('tb_vendor_primary as p', 'm.id_vendor', '=', 'p.id_vendor')
            ->select('m.*', 'p.nama_vendor')
            ->where('m.body_mail', 'LIKE', '%' . $search_query . '%')
            ->groupBy('m.body_mail', 'm.subject')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }

    public function delHistoryVendorMail($id) {

        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        DB::table('tb_vendor_mail_history')->where('subject', $data->subject)->where('body_mail', $data->body_mail)->delete();

        return 204;
    }

    public function getHistoryVendorMailID($id) {
        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        return json_encode($data);
    }

    public function getHistoryVendorMailView($id, Request $request) {
        $data = DB::table('tb_vendor_mail_history')->where('id', $id)->first();

        $search_query = $request->searchTerm;
        $perPage = $request->per_page;
        $field = $request->input('field', 'body_mail');
        $type = $request->input('type', 'asc');
        $mails = DB::table('tb_vendor_mail_history as m')
            ->leftjoin('tb_vendor_primary as p', 'm.id_vendor', '=', 'p.id_vendor')
            ->select('m.*', 'p.nama_vendor')
            ->where('m.subject', $data->subject)
            ->where('m.body_mail', $data->body_mail)
            ->where('p.nama_vendor', 'LIKE', '%' . $search_query . '%')
            ->orderBy($field, $type)
            ->paginate($perPage)
            ->toArray();
        if ($search_query !== null) {
            $mails['searchTerm'] = $search_query ?: '';
        } else if ($search_query == null) {
            $mails['searchTerm'] = $search_query ? null : '';
        }

        return response()->json([
            'mails' => $mails,
        ]);
    }

}
