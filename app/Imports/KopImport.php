<?php

namespace App\Imports;

use App\Kop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KopImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Kop([
            'nama_komunitas'     => $row[0],
            'nama_narahubung'    => $row[1],
            'email_narahubung' => $row[2],
            'jenis_komunitas' => $row[3],
            'email_komunitas' => $row[4],
            'klaster_kerja' => $row[5],
            'lingkup_komunitas' => $row[6],
            'provinsi_kerja' => $row[7],
            'kota_kerja' => $row[8],
            // 'birth_date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5]),
        ]);
    }
}
