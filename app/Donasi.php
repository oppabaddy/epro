<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $table = 'tb_donasi';
    protected $fillable = ['id_kop','donasi_tahunan','ket_tdk_bersedia','konfirmasi_donasi','created_at'];
}
