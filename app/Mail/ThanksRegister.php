<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThanksRegister extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;

    public function __construct($email) {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject('Thanks for joining!')
            ->view('email/thanksregister')
            ->with(
                [
                    'nama' => 'Semua Murid Semua Guru',
                    'website' => 'https://semuamuridsemuaguru.id',
                ]);
    }
}