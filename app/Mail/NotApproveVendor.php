<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotApproveVendor extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;

    public function __construct($email) {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject('We are sorry')
            ->view('email/notapprovevendor')
            ->with(
                [
                    'nama' => 'PT. Kawasan Industri Wijayakusuma',
                    'website' => 'https://kiw.co.id/',
                ]);
    }
}