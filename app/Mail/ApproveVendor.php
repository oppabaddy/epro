<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApproveVendor extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;

    public function __construct($email) {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject('Approval Joining!')
            ->view('email/approvevendor')
            ->with(
                [
                    'nama' => 'PT. Kawasan Industri Wijayakusuma',
                    'website' => 'https://kiw.co.id/',
                ]);
    }
}