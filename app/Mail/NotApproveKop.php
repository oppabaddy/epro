<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotApproveKop extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;

    public function __construct($email) {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject('We are sorry')
            ->view('email/notapprovekop')
            ->with(
                [
                    'nama' => 'Semua Murid Semua Guru',
                    'website' => 'https://semuamuridsemuaguru.id',
                ]);
    }
}