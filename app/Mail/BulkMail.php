<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BulkMail extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;

    public function __construct($email) {
        $this->email = $email;
        $this->sub = $email[4];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject($this->sub)
            ->view('email/bulkmail')
            ->with(
                [
                    'nama' => 'Semua Murid Semua Guru',
                    'website' => 'https://semuamuridsemuaguru.id',
                ]);
    }
}