<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspeksi extends Model
{
    protected $table = 'inspeksi';
    protected $fillable = ['id_inspeksi','id_satwa','lahir_satwa','ket_lahir_satwa','mati_satwa','ket_mati_satwa','pindah_satwa','ket_pindah_satwa','datang_satwa','ket_datang_satwa','lepas_satwa','ket_lepas_satwa','sakit_satwa','ket_sakit_satwa','observasi_satwa'];
}
