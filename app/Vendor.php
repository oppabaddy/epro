<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Vendor extends Model
{
    protected $table = 'tb_vendor_primary';
    protected $fillable = 	['logo_vendor',
				                'nama_vendor',
				                'deskripsi_vendor',
				                'nama_narahubung',
				                'email_narahubung',
				                'tlp_narahubung',
				                'jenis_vendor',
				                'email_vendor',
				                'tlp_vendor',
				                'fax_vendor',
				                'npwp_vendor',
				                'pkp_vendor',
				                'tgl_vendor',
				                'alamat_vendor',
				                'provinsi_vendor',
				                'kota_vendor',
				                'kodepos_vendor',
				                'web_vendor',
				                'facebook_vendor',
				                'instagram_vendor',
				                'cabang_vendor',
				                'alamat_pusat',
				                'email_pusat',
				                'tlp_pusat',
				                'fax_pusat',
				                'status'
				            ];
}
 