<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kop extends Model
{
    protected $table = 'tb_member_premier';
    protected $fillable = ['nama_komunitas','deskripsi_komunitas','nama_narahubung','email_narahubung','tlp_narahubung','jenis_komunitas','email_komunitas','logo_komunitas','klaster_kerja','lingkup_komunitas','provinsi_kerja','kota_kerja'];
}
 