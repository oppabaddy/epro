<?php

namespace App\Exports;

use App\Donasi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class IuranExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   	
   	use Exportable;

    public function collection()
    {
        return DB::table('tb_donasi as d')
                  ->leftjoin('tb_member_premier as p', 'p.id_kop', '=', 'd.id_kop')
                  ->select('p.nama_komunitas','d.donasi_tahunan','d.ket_tdk_bersedia','d.konfirmasi_donasi','d.created_at')
                  ->orderBy('p.nama_komunitas', 'asc')
                  ->groupBy('p.nama_komunitas')
                  ->get();
    }

    public function headings(): array
    {
        return [
            'Komunitas',
            'Iuran Tahunan',
            'Keterangan',
            'Konfirmasi',
            'Tanggal',
        ];
    }
}
