<?php

namespace App\Exports;

use App\Kop;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class KopExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   	
   	use Exportable;

    public function collection()
    {
        return DB::table('tb_member_premier as p')
                  ->join('tb_member_sekunder as s', 'p.id_kop', '=', 's.id_kop')
                  ->select('p.nama_komunitas','p.deskripsi_komunitas','p.nama_narahubung','p.email_narahubung','p.tlp_narahubung','p.jenis_komunitas','p.email_komunitas','s.tgl_berdiri_kop','p.logo_komunitas','p.klaster_kerja','p.lingkup_komunitas','p.provinsi_kerja','p.kota_kerja','s.web_komunitas','s.twitter_komunitas','s.facebook_komunitas','s.instagram_komunitas')->get();

        // return Kop::select('nama_komunitas','deskripsi_komunitas','nama_narahubung','email_narahubung','tlp_narahubung','jenis_komunitas','email_komunitas','tgl_berdiri_kop','logo_komunitas','klaster_kerja','lingkup_komunitas','provinsi_kerja','kota_kerja','web_komunitas','twitter_komunitas','facebook_komunitas','instagram_komunitas')->get();
        
    }

    public function headings(): array
    {
        return [
            'Nama Komunitas',
            'Deskripsi Komunitas',
            'Nama Narahubung',
            'Email Narahubung',
            'Tlp Narahubung',
            'Berdiri Komunitas',
            'Jenis Komunitas',
            'Email Komunitas',
            'Logo Komunitas',
            'Klaster Kerja',
            'Lingkup Komunitas',
            'Provinsi Kerja',
            'Kota Kerja',
            'Website',
            'Twitter',
            'Facebook',
            'Instagram',
        ];
    }
}
