<?php

namespace App\Exports;

use App\Vendor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class VendorExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   	
   	use Exportable;

    public function collection()
    {
        return DB::table('tb_vendor_primary as p')
                  ->leftjoin('tb_vendor_secondary as s', 'p.id_vendor', '=', 's.id_vendor')
                  ->leftjoin('tb_vendor_third as t', 'p.id_vendor', '=', 't.id_vendor')
                  ->leftjoin('tb_vendor_dokumen as d', 'p.id_vendor', '=', 'd.id_vendor')
                  ->select('p.nama_vendor',
                            'p.deskripsi_vendor',
                            'p.nama_narahubung',
                            'p.email_narahubung',
                            'p.tlp_narahubung',
                            'p.jenis_vendor',
                            'p.email_vendor',
                            'p.tlp_vendor',
                            'p.fax_vendor',
                            'd.npwp_vendor',
                            'd.pkp_vendor',
                            'p.alamat_vendor',
                            'p.provinsi_vendor',
                            'p.kota_vendor',

                            's.tgl_berdiri_vendor',
                            's.web_vendor',
                            's.facebook_vendor',
                            's.instagram_vendor',
                            's.cabang_vendor',
                            's.alamat_pusat',
                            's.email_pusat',
                            's.tlp_pusat',
                            's.fax_pusat',

                            't.nama_pemilik',
                            't.alamat_pemilik',
                            't.ktp_pemilik',
                        )
                  ->get();

        // return Vendor::select('nama_vendor','deskripsi_vendor','nama_narahubung','email_narahubung','tlp_narahubung','jenis_vendor','email_vendor','tgl_berdiri_vendor','logo_vendor','klaster_kerja','lingkup_vendor','provinsi_kerja','kota_kerja','web_vendor','twitter_vendor','facebook_vendor','instagram_vendor')->get();
        
    }

    public function headings(): array
    {
        return [
            'nama_vendor',
            'deskripsi_vendor',
            'nama_narahubung',
            'email_narahubung',
            'tlp_narahubung',
            'jenis_vendor',
            'email_vendor',
            'tlp_vendor',
            'fax_vendor',
            'npwp_vendor',
            'pkp_vendor',
            'alamat_vendor',
            'provinsi_vendor',
            'kota_vendor',

            'tgl_berdiri_vendor',
            'web_vendor',
            'facebook_vendor',
            'instagram_vendor',
            'cabang_vendor',
            'alamat_pusat',
            'email_pusat',
            'tlp_pusat',
            'fax_pusat',

            'nama_pemilik',
            'alamat_pemilik',
            'ktp_pemilik',
        ];
    }
}
