<?php

namespace App\Exports;

use App\Person;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
 
class PersonExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   	
   	use Exportable;

    public function collection()
    {
        // return Person::select('photo_profile','first_name','last_name','gender','birth_place','birth_date','address')->get();
        $persons = DB::table('persons') 
                        ->select('persons.photo_profile','persons.first_name','persons.last_name','persons.gender','persons.birth_place','persons.birth_date','persons.address', 'users.id')
                        ->leftjoin('users', 'users.id_person', '=', 'persons.id_person')
                        ->get();

                           foreach($persons as $key => $value)
                           { 
                              $getKop = DB::table('users_to_kop')
                                            ->where('id_user', $persons[$key]->id)
                                            ->first();

                              if($getKop !== null){
                                $getKopname = DB::table('tb_member_premier')
                                            ->where('id_kop', $getKop->id_kop)
                                            ->first();
                                $persons[$key]->nama_komunitas = $getKopname->nama_komunitas;           
                              } else {
                                $persons[$key]->nama_komunitas = "belum ada";
                              }
                            unset($persons[$key]->id);
                       }
        
        return $persons;
    }

    public function headings(): array
    {
        return [
            'Photo Profile',
            'First Name',
            'Last Name',
            'Gender',
            'Birth Place',
            'Birth Date',
            'Address',
            'KOP',
        ];
    }
}
