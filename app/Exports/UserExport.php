<?php

namespace App\Exports;

use App\Person;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class UserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   	
   	use Exportable;

    public function collection()
    {
        // return Person::select('photo_profile','first_name','last_name','gender','birth_place','birth_date','address')->get();
        $userss = DB::table('persons') 
                        ->select('users.id','users.username','users.email','tb_role.role')
                        ->leftjoin('users', 'users.id_person', '=', 'persons.id_person')
                        ->leftjoin('tb_role', 'users.role', '=', 'tb_role.kd_role')
                        ->get();

                           foreach($userss as $key => $value)
                           { 
                              $getKop = DB::table('users_to_kop')
                                            ->where('id_user', $userss[$key]->id)
                                            ->first();

                              if($getKop !== null){
                                $getKopname = DB::table('tb_member_premier')
                                            ->where('id_kop', $getKop->id_kop)
                                            ->first();
                                $userss[$key]->nama_komunitas = $getKopname->nama_komunitas;           
                              } else {
                                $userss[$key]->nama_komunitas = "belum ada";
                              }
                            unset($userss[$key]->id);
                       }
        
        return $userss;
    }

    public function headings(): array
    {
        return [
            'Username',
            'Email',
            'Role',
            'KOP',
        ];
    }
}
