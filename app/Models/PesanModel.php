<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PesanModel extends Model
{
    protected $table = 'pesan_baik';
    protected $fillable = ['id_kop', 'judul', 'content'];
}
