<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PraktikModel extends Model
{
    protected $table = 'praktik_baik';
    protected $fillable = ['id_kop', 'judul', 'content'];
}
