<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(['middleware' => 'cors'], function () {
Route::middleware('cors')->group(function () {
    Route::get('/getSide/{id}', 'MasterController@getSide');
    Route::get('/getSideName/{link}', 'MasterController@getSideName');
    Route::get('/getNavName/{link}', 'MasterController@getNavName'); 
    Route::get('/getSideCount/{link}/{id}', 'MasterController@getSideCount');
    Route::get('/test', 'MasterController@test');

    // menambahkan route untuk master Role
    Route::get('/menuA', 'MasterController@menuA');
    Route::get('/menuB', 'MasterController@menuB');
    Route::get('/menuC', 'MasterController@menuC');
    Route::get('/action/{id}/{link}', 'MasterController@action');
    Route::get('/sidemenuA/{id}','MasterController@sidemenuA');
    Route::get('/sidemenuB/{user}/{menu}','MasterController@sidemenuB');
    Route::get('/sidemenuBNew/{user}/{menu}','MasterController@sidemenuBNew');
    Route::get('/sidemenuC/{user}/{menu}','MasterController@sidemenuC');
    Route::get('/sidemenuCNew/{user}/{menu}','MasterController@sidemenuCNew');
    Route::post('/addrole', 'MasterController@addRole');
    Route::put('/updaterole/{id}', 'MasterController@updateRole');
    Route::post('/deleterole/{id}', 'MasterController@deleteRole');
    Route::get('/getRole', 'MasterController@getRole');
    Route::get('/role/{id}', 'MasterController@allRoleByID');
    Route::get('/roleByID/{id}', 'MasterController@roleByID');
    Route::get('/actionByID/{id}', 'MasterController@actionByID');
    Route::get('/navmenuA', 'MasterController@navmenuA');
    Route::get('/navmenuB/{menu}', 'MasterController@navmenuB');
    Route::get('/navmenuBNew/{menu}', 'MasterController@navmenuBNew');

    // menambahkan route untuk master MenuLanding
    Route::post('/addmenulanding', 'MasterController@addMenuLanding');
    Route::put('/updatemenulanding/{id}', 'MasterController@updatemenulanding');
    Route::get('/getMenuParent', 'MasterController@getMenuParent');
    Route::get('/getMenuParentChild/{id}', 'MasterController@getMenuParentChild');
    Route::get('/getMenuParentChild2/{id}', 'MasterController@getMenuParentChild2');
    Route::get('/dropdownparents', 'MasterController@dropdownparents');
    Route::post('/addmenuchild', 'MasterController@addMenuChild');
    Route::get('/getMenuChild', 'MasterController@getMenuChild');
    Route::post('/deletemenuparent/{id}', 'MasterController@deleteMenuParent');
    Route::post('/deletemenuchild/{id}', 'MasterController@deleteMenuChild');

    // menambahkan route untuk master Email
    Route::get('/getcontent/{type}', 'MasterController@getContent');
    Route::post('/addContent', 'MasterController@addContent');

    // menambahkan route untuk master Footer
    Route::get('/getcontentFooter', 'MasterController@getcontentFooter');
    Route::post('/addContentFooter', 'MasterController@addContentFooter');

    // menambahkan route untuk master Donasi
    Route::get('/getcontentDonasi', 'MasterController@getcontentDonasi');
    Route::post('/addContentDonasi', 'MasterController@addContentDonasi');

    // menambahkan route untuk master Counter
    Route::get('/getcontentCounter', 'MasterController@getcontentCounter');
    Route::post('/addContentCounter', 'MasterController@addContentCounter');

    // menambahkan route untuk master Tentang Kami
    Route::get('/getcontentMainTentangKami', 'MasterController@getcontentMainTentangKami');
    Route::post('/addContentTentangKami', 'MasterController@addContentTentangKami');

    // menambahkan route untuk master Banner
    Route::get('/banner/{id}', 'MasterController@showBanner');
    Route::get('/getBanner', 'MasterController@getBanner');
    Route::post('/addbanner', 'MasterController@addBanner');
    Route::post('/banner/{id}', 'MasterController@deleteBanner');
    Route::put('/banner/{id}', 'MasterController@updateBanner');
    Route::get('/getcontentBanner', 'MasterController@getcontentBanner');

    // menambahkan route untuk master Divisi
    Route::get('/divisi/{id}', 'MasterController@showDivisi');
    Route::get('/getDivisi', 'MasterController@getDivisi');
    Route::post('/adddivisi', 'MasterController@addDivisi');
    Route::post('/divisi/{id}', 'MasterController@deleteDivisi');
    Route::post('/divisi/{id}', 'MasterController@updateDivisi');
    Route::get('/getcontentDivisi', 'MasterController@getcontentDivisi');
    Route::get('/dropdowndivisi', 'MasterController@dropdownDivisi');

    // menambahkan route untuk master Jenis Vendor
    Route::get('/jenisvendor/{id}', 'MasterController@showJenisVendor');
    Route::get('/getJenisVendor', 'MasterController@getJenisVendor');
    Route::post('/addjenisvendor', 'MasterController@addJenisVendor');
    Route::post('/jenisvendor/{id}', 'MasterController@deleteJenisVendor');
    Route::post('/jenisvendor/{id}', 'MasterController@updateJenisVendor');
    Route::get('/getcontentJenisVendor', 'MasterController@getcontentJenisVendor');
    Route::get('/dropdownJenisVendor', 'MasterController@dropdownJenisVendor');
    
    // menambahkan route untuk master Kategori Pekerjaan
    Route::get('/kategoripekerjaan/{id}', 'MasterController@showKategoriPekerjaan');
    Route::get('/getKategoriPekerjaan', 'MasterController@getKategoriPekerjaan');
    Route::post('/addkategoripekerjaan', 'MasterController@addKategoriPekerjaan');
    Route::post('/kategoripekerjaan/{id}', 'MasterController@deleteKategoriPekerjaan');
    Route::post('/kategoripekerjaan/{id}', 'MasterController@updateKategoriPekerjaan');
    Route::get('/getcontentKategoriPekerjaan', 'MasterController@getcontentKategoriPekerjaan');  

    // menambahkan route untuk master Sub Klasifikasi 1
    Route::get('/subklasifikasi1/{id}', 'MasterController@showSubKlasifikasi1');
    Route::get('/getSubKlasifikasi1', 'MasterController@getSubKlasifikasi1');
    Route::post('/addsubklasifikasi1', 'MasterController@addSubKlasifikasi1');
    Route::post('/subklasifikasi1/{id}', 'MasterController@deleteSubKlasifikasi1');
    Route::post('/subklasifikasi1/{id}', 'MasterController@updateSubKlasifikasi1');
    Route::get('/getcontentSubKlasifikasi1', 'MasterController@getcontentSubKlasifikasi1'); 
    Route::get('/dropdownSubKlasifikasi1', 'MasterController@dropdownSubKlasifikasi1');

    // menambahkan route untuk master Sub Klasifikasi 2
    Route::get('/subklasifikasi2/{id}', 'MasterController@showSubKlasifikasi2');
    Route::get('/getSubKlasifikasi2', 'MasterController@getSubKlasifikasi2');
    Route::post('/addsubklasifikasi2', 'MasterController@addSubKlasifikasi2');
    Route::post('/subklasifikasi2/{id}', 'MasterController@deleteSubKlasifikasi2');
    Route::post('/subklasifikasi2/{id}', 'MasterController@updateSubKlasifikasi2');
    Route::get('/getcontentSubKlasifikasi2', 'MasterController@getcontentSubKlasifikasi2'); 
    Route::get('/dropdownSubKlasifikasi2', 'MasterController@dropdownSubKlasifikasi2');

    // menambahkan route untuk master Jabatan
    Route::get('/jabatan/{id}', 'MasterController@showJabatan');
    Route::get('/getJabatan', 'MasterController@getJabatan');
    Route::post('/addjabatan', 'MasterController@addJabatan');
    Route::post('/jabatan/{id}', 'MasterController@deleteJabatan');
    Route::post('/jabatan/{id}', 'MasterController@updateJabatan');
    Route::get('/getcontentJabatan', 'MasterController@getcontentJabatan');
    Route::get('/dropdownjabatan', 'MasterController@dropdownJabatan');   

    // menambahkan route untuk master Visi Misi
    Route::get('/getcontentVisiMisi', 'MasterController@getcontentVisiMisi');
    Route::post('/addContentVisiMisi', 'MasterController@addContentVisiMisi');

    // menambahkan route untuk master landing page
    Route::get('/getcontentLanding', 'MasterController@getcontentLanding');
    Route::post('/addContentLanding', 'MasterController@addContentLanding');

    // menambahkan route untuk master Sponsor
    Route::get('/sponsor/{id}', 'MasterController@showSponsor');
    Route::get('/getSponsor', 'MasterController@getSponsor');
    Route::post('/addsponsor', 'MasterController@addSponsor');
    Route::delete('/sponsor/{id}', 'MasterController@deleteSponsor');
    Route::put('/sponsor/{id}', 'MasterController@updateSponsor');
    Route::get('/getcontentSponsor', 'MasterController@getcontentSponsor');

    // menambahkan route untuk master AreWe
    Route::get('/getAreWe', 'MasterController@getAreWe');
    Route::post('/addarewe', 'MasterController@addAreWe');
    Route::put('/uparewe/{id}', 'MasterController@updateAreWe');
    Route::post('/arewe/{id}', 'MasterController@deleteAreWe');
    Route::get('/getAreWe/{id}', 'MasterController@getAreWeID');
    Route::get('/getcontentAreWe', 'MasterController@getcontentAreWe');
    Route::post('/upUrutan', 'MasterController@upUrutan');
    Route::post('/downUrutan', 'MasterController@downUrutan');

    // menambahkan route untuk person
    Route::get('/person', 'PersonController@all');
    Route::get('/person/{id}', 'PersonController@show');
    Route::post('/person', 'PersonController@store');
    Route::post('/importPerson', 'PersonController@import');
    Route::post('/importPersonXLXS', 'PersonController@importXLXS');
    Route::get('/downloadExcelPerson', 'PersonController@downloadExcelPerson');
    Route::get('/pdfPerson/{id}', 'PersonController@pdfPerson');
    Route::post('/person2', 'PersonController@store2');
    Route::post('/updateperson/{id}', 'PersonController@update');
    Route::post('/person/{id}', 'PersonController@delete');
    Route::get('/dropdownperson', 'PersonController@dropdownperson');
    Route::get('/getRecords2', 'PersonController@getRecords2');
    Route::post('/getRecords2', 'PersonController@getRecords2');
    Route::get('/getCountPerson', 'PersonController@getCountPerson');

    // menambahkan route untuk user
    Route::get('/dropdownuser', 'UserController@dropdownuser');
    Route::get('/dropdownuserAll', 'UserController@dropdownuserAll');
    Route::get('/dropdownuserPekerjaan', 'UserController@dropdownuserPekerjaan');
    Route::get('/dropdownuser/{id}', 'UserController@dropdownuserbyID');
    Route::get('/dropdownuserto', 'UserController@dropdownuserto');
    Route::get('/dropdownrole', 'UserController@dropdownrole');
    Route::get('/generateuserpass/{id}', 'UserController@generateuserpass');
    Route::post('/user', 'UserController@store');
    Route::post('/usertovendor', 'UserController@storetovendor');
    Route::put('/updateuser/{id}', 'UserController@updateUser');
    Route::post('/user/{id}', 'UserController@delete');
    Route::post('/usertovendor/{id}', 'UserController@deletetovendor');
    Route::get('/getUser', 'UserController@getUser');
    Route::get('/getUsertoVendor', 'UserController@getUsertoVendor');
    Route::get('/user/{id}', 'UserController@user');
    Route::get('/roleByIDuser/{id}', 'UserController@roleByIDuser');
    Route::get('/actionByIDuser/{id}', 'UserController@actionByID');
    Route::get('/menuByIDuser/{id}', 'UserController@menuByIDuser');
    Route::put('/actived/{id}', 'UserController@updateUserActived');
    Route::put('/deactive/{id}', 'UserController@updateUserDeactive');
    Route::put('/updateprofile/{id}', 'UserController@updateProfile');

    //menamahkan route untuk vendor
    Route::get('/dropdownvendor', 'VendorController@dropdownvendor');
    Route::get('/dropdownvendorKlas/{klas1}/{klas2}', 'VendorController@dropdownvendorKlas');
    Route::post('/vendorStoreOnline', 'VendorController@storeOnline');
    Route::post('/vendorStore', 'VendorController@store');
    Route::get('/getVendor', 'VendorController@getVendor');
    Route::post('/getVendor', 'VendorController@getVendor');
    Route::get('/getReqVendor', 'VendorController@getReqVendor');
    Route::get('vendor2', 'VendorController@all2');
    Route::get('/vendor/{id}', 'VendorController@show');
    Route::get('/vendorMy/{id}', 'VendorController@showMy');
    Route::delete('/vendor/{id}', 'VendorController@delete');
    Route::post('/vendordelete/{id}', 'VendorController@delete');
    Route::post('/approveVendor', 'VendorController@approveVendor');
    Route::post('/notapproveVendor', 'VendorController@notapproveVendor');
    Route::post('/sendMaillAllVendorFix/{email}/{id}', 'VendorController@sendMaillAllVendorFix');
    Route::get('/getHistoryVendorMail', 'VendorController@getHistoryVendorMail');
    Route::get('/getHistoryVendorMail/{id}', 'VendorController@getHistoryVendorMailID');
    Route::get('/getHistoryVendorMailView/{id}', 'VendorController@getHistoryVendorMailView');
    Route::post('/delHistoryVendorMail/{id}', 'VendorController@delHistoryVendorMail');
    Route::post('/sendMailltoVendor', 'VendorController@sendMailltoVendor');
    Route::post('/vendorupdate/{id}', 'VendorController@updateVendor');
    Route::post('/vendordelete/{id}', 'VendorController@deleteVendor');
    Route::post('/importVendorXLXS', 'VendorController@importXLXS');
    Route::get('/pdfVendor/{id}', 'VendorController@pdfVendor');
    Route::get('/downloadRarVendor/{id}', 'VendorController@downloadRarVendor');
    Route::get('/getListVendor/{limit}', 'VendorController@getListVendor');
    Route::get('/getListVendorSearch/{search}', 'VendorController@getListVendorBySearch');
    Route::post('/uploadKTP', 'VendorController@uploadKTP');
    Route::post('/uploadIUJK', 'VendorController@uploadIUJK');
    Route::post('/uploadSBU', 'VendorController@uploadSBU');
    Route::post('/updateuploadKTP', 'VendorController@updateuploadKTP');
    Route::post('/updateFileIujkSbu', 'VendorController@updateFileIujkSbu');
    Route::post('/deleteFileIujkSbu', 'VendorController@deleteFileIujkSbu');
    Route::post('/vendorStoreFile', 'VendorController@storeFile');

    Route::post('/getPenilaianVendor', 'VendorController@getPenilaianVendor');
    Route::post('/addRating', 'VendorController@addRating');
    Route::get('/getPenilaianIkutSerta/{vendor}', 'VendorController@getPenilaianIkutSerta');

    Route::post('/getLogActivity', 'VendorController@getLogActivity');

    //menambahkan route untuk manage committee
    Route::get('/dropdownuserCommittee', 'CommitteeController@dropdownuserCommittee');
    Route::post('/committeeStore', 'CommitteeController@store');
    Route::post('/getCommittee', 'CommitteeController@getCommittee');
    Route::post('/committeedelete/{id}', 'CommitteeController@delete');
    Route::get('/committee/{id}', 'CommitteeController@show');
    Route::post('/committeeupdate/{id}', 'CommitteeController@updateCommittee');

    //menamahkan route untuk penunjukanPL
    Route::post('/getPekerjaanAddendum', 'PlPekerjaanController@getPekerjaanAddendum');
    Route::post('/getPenunjukan', 'PlPekerjaanController@getPenunjukan');
    Route::post('/getPenunjukanAPI', 'PlPekerjaanController@getPenunjukanAPI');
    Route::post('/getPenunjukanVendor/{id}', 'PlPekerjaanController@getPenunjukanVendor');
    Route::post('/penunjukanStore', 'PlPekerjaanController@penunjukanStore');
    Route::get('/penunjukan/{id}', 'PlPekerjaanController@showPenunjukan');
    Route::get('/penunjukanMail/{id}', 'PlPekerjaanController@showPenunjukanMail');
    Route::post('/penunjukanupdate/{id}', 'PlPekerjaanController@updatePenunjukan');
    Route::post('/penunjukandelete/{id}', 'PlPekerjaanController@deletePenunjukan');
    Route::get('/dropdownkategoripekerjaan', 'PlPekerjaanController@dropdownKategoriPekerjaan');
    Route::get('/vendorPekerjaan/{id}', 'PlPekerjaanController@showVendorPekerjaan');
    Route::get('/getVendorPekerjaanbyID/{pekerjaan}/{id}/{user}', 'PlPekerjaanController@getVendorPekerjaanbyID');
    Route::get('/getVendorPekerjaan/{pekerjaan}/{id}', 'PlPekerjaanController@getVendorPekerjaan');
    Route::post('/approvePekerjaanPL', 'PlPekerjaanController@approvePekerjaanPL');
    Route::post('/notapprovePekerjaanPL', 'PlPekerjaanController@notapprovePekerjaanPL');
    Route::post('/setJadwalPekerjaanPL', 'PlPekerjaanController@setJadwalPekerjaanPL');
    Route::get('/dropdownJadwalPekerjaan/{id}', 'PlPekerjaanController@dropdownJadwalPekerjaan');
    Route::post('/setJadwalPekerjaanAddendum', 'PlPekerjaanController@setJadwalPekerjaanAddendum');
    Route::get('/viewjadwal/{id}', 'PlPekerjaanController@showViewJadwal');
    Route::get('/getCountPL', 'PlPekerjaanController@getCountPL');
    Route::post('/getPL', 'PlPekerjaanController@getPL');
    Route::get('/getAllTender', 'PlPekerjaanController@getAllTender');
    Route::get('/getFileKontrakbyID/{id}', 'PlPekerjaanController@getFileKontrakbyID');
    Route::post('/getAllTender2', 'PlPekerjaanController@getAllTender2');
    Route::post('/getAllTender2Vendor/{id}', 'PlPekerjaanController@getAllTender2Vendor');
    
    Route::post('/uploads', 'PlPekerjaanController@uploads');
    Route::post('/testStore', 'PlPekerjaanController@testStore');

    //menamahkan route untuk dokumenPL
    Route::post('/getDokumenKualifikasiPL', 'PlPekerjaanController@getDokumenKualifikasiPL');
    Route::post('/getDokumenKualifikasiPLVendor/{id}', 'PlPekerjaanController@getDokumenKualifikasiPLVendor');
    Route::post('/dokumenPekerjaanPL', 'PlPekerjaanController@dokumenPekerjaanPL');
    Route::post('/dokumenKontrak', 'PlPekerjaanController@dokumenKontrak');
    Route::post('/updatedokumenPekerjaanPL', 'PlPekerjaanController@updatedokumenPekerjaanPL');
    Route::get('/getDokumenPL/{id}/{user}/{vendor}', 'PlPekerjaanController@getDokumenPL');
    Route::get('/getDokumenPLbyID/{id}/{user}', 'PlPekerjaanController@getDokumenPLbyID');
    Route::get('/getDokumenKontrakbyID/{id}', 'PlPekerjaanController@getDokumenKontrakbyID');
    Route::get('/getDokumenPLbyID2/{id}/{user}', 'PlPekerjaanController@getDokumenPLbyID2');
    Route::post('/validasiPekerjaanPL', 'PlPekerjaanController@validasiPekerjaanPL');

    //menamahkan route untuk aanwijzingPL
    Route::post('/getAanwijzingPL', 'PlPekerjaanController@getAanwijzingPL');
    Route::post('/getAanwijzingPLVendor/{id}', 'PlPekerjaanController@getAanwijzingPLVendor');
    Route::get('/getmessagesPL/{id_pekerjaan}', 'ChatController@getMessageAddPl');
    Route::post('/insertQNAPL', 'PlPekerjaanController@insertQNAPL');
    Route::post('/updateQNAPL', 'PlPekerjaanController@updateQNAPL');
    Route::post('/deleteQNAPL', 'PlPekerjaanController@deleteQNAPL');
    Route::get('/getQNAPL/{id}/{user}/{vendor}', 'PlPekerjaanController@getQNAPL');
    Route::get('/getQNAPLbyID/{id}/{user}', 'PlPekerjaanController@getQNAPLbyID');
    Route::post('/baAanwijzingPL', 'PlPekerjaanController@baAanwijzingPL');
    Route::get('/getBAAanwijzing/{id}', 'PlPekerjaanController@getBAAanwijzing'); 
    Route::post('/approveBAAanwijzingPL', 'PlPekerjaanController@approveBAAanwijzingPL');
    Route::get('/getPenyedia/{id}', 'PlPekerjaanController@getPenyedia');
    Route::get('/pdfAanwijzing/{id}', 'PlPekerjaanController@pdfAanwijzing');

    //menamahkan route untuk penawaranPL
    Route::post('/getPenawaranPL', 'PlPekerjaanController@getPenawaranPL');
    Route::post('/getPenawaranPLVendor/{id}', 'PlPekerjaanController@getPenawaranPLVendor');
    Route::get('/getPenawaranbyID/{pekerjaan}/{id}/{user}', 'PlPekerjaanController@getPenawaranbyID');
    Route::get('/getPenawaran/{pekerjaan}/{id}', 'PlPekerjaanController@getPenawaran');
    Route::get('/getFilePenawaranPL/{id}/{user}/{vendor}', 'PlPekerjaanController@getFilePenawaranPL');
    Route::get('/getFilePenawaranPLbyID/{id}/{user}', 'PlPekerjaanController@getFilePenawaranPLbyID');
    Route::post('/penawaranStore', 'PlPekerjaanController@penawaranStore');
    Route::get('/getBAPenawaran/{id}', 'PlPekerjaanController@getBAPenawaran');
    Route::post('/baPenawaranPL', 'PlPekerjaanController@baPenawaranPL');
    Route::post('/approveBAPenawaranPL', 'PlPekerjaanController@approveBAPenawaranPL');
    Route::get('/dropdownvendorValidasi/{id}', 'PlPekerjaanController@dropdownvendorValidasi');
    Route::post('/uploadFilePenawaran', 'PlPekerjaanController@uploadFilePenawaran');
    Route::get('/getFilePenawaranPLbyID2/{id}/{user}', 'PlPekerjaanController@getFilePenawaranPLbyID2');
    Route::post('/validasiPenawaranPL', 'PlPekerjaanController@validasiPenawaranPL');
    Route::get('/pdfPenawaran/{id}', 'PlPekerjaanController@pdfPenawaran');

    //menamahkan route untuk evaluasiPL
    Route::post('/getEvaluasiPL', 'PlPekerjaanController@getEvaluasiPL');
    Route::post('/getEvaluasiPLVendor/{id}', 'PlPekerjaanController@getEvaluasiPLVendor');
    Route::post('/evaluasiStore', 'PlPekerjaanController@evaluasiStore');
    Route::get('/getFileNotulenPL/{id}/{user}/{vendor}', 'PlPekerjaanController@getFileNotulenPL');
    Route::get('/getFileNotulenPLbyID/{id}/{user}', 'PlPekerjaanController@getFileNotulenPLbyID');

    //menamahkan route untuk negosiasiPL
    Route::post('/getNegosiasiPL', 'PlPekerjaanController@getNegosiasiPL');
    Route::post('/getNegosiasiPLVendor/{id}', 'PlPekerjaanController@getNegosiasiPLVendor');
    Route::get('/getBANegosiasi/{id}', 'PlPekerjaanController@getBANegosiasi');
    Route::post('/baNegosiasiPL', 'PlPekerjaanController@baNegosiasiPL');
    Route::post('/approveBANegosiasiPL', 'PlPekerjaanController@approveBANegosiasiPL');
    Route::get('/pdfNegosiasi/{id}', 'PlPekerjaanController@pdfNegosiasi');

    //menamahkan route untuk pemenangPL
    Route::post('/getPemenangPL', 'PlPekerjaanController@getPemenangPL');
    Route::post('/getPemenangPLVendor/{id}', 'PlPekerjaanController@getPemenangPLVendor');
    Route::post('/pemenangPL', 'PlPekerjaanController@pemenangPL');
    Route::get('/getPemenang/{id}', 'PlPekerjaanController@getPemenang');
    Route::post('/approvePemenangPL', 'PlPekerjaanController@approvePemenangPL');
    Route::get('/pdfPemenang/{id}', 'PlPekerjaanController@pdfPemenang');



    Route::get('/getIP', 'PtPekerjaanController@getIp');


    //menamahkan route untuk pelelanganPT
    Route::post('/getPelelanganPT', 'PtPekerjaanController@getPelelanganPT');
    Route::post('/getPelelanganVendorPT/{id}', 'PtPekerjaanController@getPelelanganVendorPT');
    Route::post('/pelelanganStorePT', 'PtPekerjaanController@pelelanganStorePT');
    Route::get('/pelelanganPT/{id}', 'PtPekerjaanController@showPelelanganPT');
    Route::get('/pelelanganMailPT/{id}', 'PtPekerjaanController@showPelelanganMailPT');
    Route::post('/pelelanganupdatePT/{id}', 'PtPekerjaanController@updatePelelanganPT');
    Route::post('/pelelangandeletePT/{id}', 'PtPekerjaanController@deletePelelanganPT');
    Route::get('/dropdownkategoripekerjaanPT', 'PtPekerjaanController@dropdownKategoriPekerjaanPT');
    Route::get('/vendorPekerjaanPT/{id}', 'PtPekerjaanController@showVendorPekerjaanPT');
    Route::get('/getVendorPekerjaanbyIDPT/{pekerjaan}/{id}/{user}', 'PtPekerjaanController@getVendorPekerjaanbyIDPT');
    Route::get('/getVendorPekerjaanPT/{pekerjaan}/{id}', 'PtPekerjaanController@getVendorPekerjaanPT');
    Route::post('/approvePekerjaanPT', 'PtPekerjaanController@approvePekerjaanPT');
    Route::post('/notapprovePekerjaanPT', 'PtPekerjaanController@notapprovePekerjaanPT');
    Route::post('/setJadwalPekerjaanPT', 'PtPekerjaanController@setJadwalPekerjaanPT');
    Route::get('/viewjadwalPT/{id}', 'PtPekerjaanController@showViewJadwalPT');
    Route::get('/getCountPT', 'PtPekerjaanController@getCountPT');
    Route::post('/getPT', 'PtPekerjaanController@getPT');
    

    //menamahkan route untuk dokumenPT
    Route::post('/getDokumenKualifikasiPT', 'PtPekerjaanController@getDokumenKualifikasiPT');
    Route::post('/getDokumenKualifikasiPTVendor/{id}', 'PtPekerjaanController@getDokumenKualifikasiPTVendor');
    Route::post('/dokumenPekerjaanPT', 'PtPekerjaanController@dokumenPekerjaanPT');
    Route::post('/updatedokumenPekerjaanPT', 'PtPekerjaanController@updatedokumenPekerjaanPT');
    Route::get('/getDokumenPT/{id}/{user}/{vendor}', 'PtPekerjaanController@getDokumenPT');
    Route::get('/getDokumenPTbyID/{id}/{user}', 'PtPekerjaanController@getDokumenPTbyID');
    Route::get('/getDokumenPTbyID2/{id}/{user}', 'PtPekerjaanController@getDokumenPTbyID2');
    Route::post('/validasiPekerjaanPT', 'PtPekerjaanController@validasiPekerjaanPT');

    //menamahkan route untuk aanwijzingPT
    Route::post('/getAanwijzingPT', 'PtPekerjaanController@getAanwijzingPT');
    Route::post('/getAanwijzingPTVendor/{id}', 'PtPekerjaanController@getAanwijzingPTVendor');
    Route::get('/getmessagesPT/{id_pekerjaan}', 'ChatController@getMessageAddPl');
    Route::post('/insertQNAPT', 'PtPekerjaanController@insertQNAPT');
    Route::post('/updateQNAPT', 'PtPekerjaanController@updateQNAPT');
    Route::post('/deleteQNAPT', 'PtPekerjaanController@deleteQNAPT');
    Route::get('/getQNAPT/{id}/{user}/{vendor}', 'PtPekerjaanController@getQNAPT');
    Route::get('/getQNAPTbyID/{id}/{user}', 'PtPekerjaanController@getQNAPTbyID');
    Route::post('/baAanwijzingPT', 'PtPekerjaanController@baAanwijzingPT');
    Route::get('/getBAAanwijzingPT/{id}', 'PtPekerjaanController@getBAAanwijzingPT'); 
    Route::post('/approveBAAanwijzingPT', 'PtPekerjaanController@approveBAAanwijzingPT');
    Route::get('/getPenyediaPT/{id}', 'PtPekerjaanController@getPenyediaPT');
    Route::get('/pdfAanwijzingPT/{id}', 'PtPekerjaanController@pdfAanwijzingPT');

    //menamahkan route untuk penawaranPT
    Route::post('/getPenawaranPT', 'PtPekerjaanController@getPenawaranPT');
    Route::post('/getPenawaranPTVendor/{id}', 'PtPekerjaanController@getPenawaranPTVendor');
    Route::get('/getPenawaranbyIDPT/{pekerjaan}/{id}/{user}', 'PtPekerjaanController@getPenawaranbyIDPT');
    Route::get('/getPenawaranPT/{pekerjaan}/{id}', 'PtPekerjaanController@getPenawaranPT');
    Route::get('/getFilePenawaranPT/{id}/{user}/{vendor}', 'PtPekerjaanController@getFilePenawaranPT');
    Route::get('/getFilePenawaranPTbyID/{id}/{user}', 'PtPekerjaanController@getFilePenawaranPTbyID');
    Route::post('/penawaranStorePT', 'PtPekerjaanController@penawaranStorePT');
    Route::get('/getBAPenawaranPT/{id}', 'PtPekerjaanController@getBAPenawaranPT');
    Route::post('/baPenawaranPT', 'PtPekerjaanController@baPenawaranPT');
    Route::post('/approveBAPenawaranPT', 'PtPekerjaanController@approveBAPenawaranPT');
    Route::get('/dropdownvendorValidasiPT/{id}', 'PtPekerjaanController@dropdownvendorValidasiPT');
    Route::post('/uploadFilePenawaranPT', 'PtPekerjaanController@uploadFilePenawaranPT');
    Route::get('/getFilePenawaranPTbyID2/{id}/{user}', 'PtPekerjaanController@getFilePenawaranPTbyID2');
    Route::post('/validasiPenawaranPT', 'PtPekerjaanController@validasiPenawaranPT');
    Route::get('/pdfPenawaranPT/{id}', 'PtPekerjaanController@pdfPenawaranPT');

    //menamahkan route untuk evaluasiPT
    Route::post('/getEvaluasiPT', 'PtPekerjaanController@getEvaluasiPT');
    Route::post('/getEvaluasiPTVendor/{id}', 'PtPekerjaanController@getEvaluasiPTVendor');
    Route::post('/evaluasiStorePT', 'PtPekerjaanController@evaluasiStorePT');
    Route::get('/getFileNotulenPT/{id}/{user}/{vendor}', 'PtPekerjaanController@getFileNotulenPT');
    Route::get('/getFileNotulenPTbyIDPT/{id}/{user}', 'PtPekerjaanController@getFileNotulenPTbyIDPT');

    //menamahkan route untuk negosiasiPT
    Route::post('/getNegosiasiPT', 'PtPekerjaanController@getNegosiasiPT');
    Route::post('/getNegosiasiPTVendor/{id}', 'PtPekerjaanController@getNegosiasiPTVendor');
    Route::get('/getBANegosiasiPT/{id}', 'PtPekerjaanController@getBANegosiasiPT');
    Route::post('/baNegosiasiPT', 'PtPekerjaanController@baNegosiasiPT');
    Route::post('/approveBANegosiasiPT', 'PtPekerjaanController@approveBANegosiasiPT');
    Route::get('/pdfNegosiasiPT/{id}', 'PtPekerjaanController@pdfNegosiasiPT');

    //menamahkan route untuk pemenangPT
    Route::post('/getPemenangPT', 'PtPekerjaanController@getPemenangPT');
    Route::post('/getPemenangPTVendor/{id}', 'PtPekerjaanController@getPemenangPTVendor');
    Route::post('/pemenangPT', 'PtPekerjaanController@pemenangPT');
    Route::get('/getPemenangPT/{id}', 'PtPekerjaanController@getPemenangPT');
    Route::post('/approvePemenangPT', 'PtPekerjaanController@approvePemenangPT');
    Route::get('/pdfPemenangPT/{id}', 'PtPekerjaanController@pdfPemenangPT');


    //menamahkan route untuk pelelanganUM
    Route::get('getPelelanganbyID/{id}', 'UmPekerjaanController@getPelelanganbyID');
    Route::post('/applyPelelangan', 'UmPekerjaanController@applyPelelangan');
    Route::get('/getListPelelangan', 'UmPekerjaanController@getListPelelangan');
    Route::get('/getListPelelanganAll/{limit}', 'UmPekerjaanController@getListPelelanganAll');
    Route::get('/getListPelelanganAllCari/{cari}/{kat}', 'UmPekerjaanController@getListPelelanganAllCari');
    Route::post('/getPelelanganUM', 'UmPekerjaanController@getPelelanganUM');
    Route::post('/getPelelanganVendorUM/{id}', 'UmPekerjaanController@getPelelanganVendorUM');
    Route::post('/pelelanganStoreUM', 'UmPekerjaanController@pelelanganStoreUM');
    Route::get('/pelelanganUM/{id}', 'UmPekerjaanController@showPelelanganUM');
    Route::get('/pelelanganMailUM/{id}', 'UmPekerjaanController@showPelelanganMailUM');
    Route::post('/pelelanganupdateUM/{id}', 'UmPekerjaanController@updatePelelanganUM');
    Route::post('/pelelangandeleteUM/{id}', 'UmPekerjaanController@deletePelelanganUM');
    Route::get('/dropdownkategoripekerjaanUM', 'UmPekerjaanController@dropdownKategoriPekerjaanUM');
    Route::get('/vendorPekerjaanUM/{id}', 'UmPekerjaanController@showVendorPekerjaanUM');
    Route::get('/getVendorPekerjaanbyIDUM/{pekerjaan}/{id}/{user}', 'UmPekerjaanController@getVendorPekerjaanbyIDUM');
    Route::get('/getVendorPekerjaanUM/{pekerjaan}/{id}', 'UmPekerjaanController@getVendorPekerjaanUM');
    Route::post('/approvePekerjaanUM', 'UmPekerjaanController@approvePekerjaanUM');
    Route::post('/notapprovePekerjaanUM', 'UmPekerjaanController@notapprovePekerjaanUM');
    Route::post('/setJadwalPekerjaanUM', 'UmPekerjaanController@setJadwalPekerjaanUM');
    Route::get('/viewjadwalUM/{id}', 'UmPekerjaanController@showViewJadwalUM');
    Route::get('/getCountUM', 'UmPekerjaanController@getCountUM');
    Route::post('/getUM', 'UmPekerjaanController@getUM');


    //menamahkan route untuk dokumenUM
    Route::post('/getDokumenKualifikasiUM', 'UmPekerjaanController@getDokumenKualifikasiUM');
    Route::post('/getDokumenKualifikasiUMVendor/{id}', 'UmPekerjaanController@getDokumenKualifikasiUMVendor');
    Route::post('/dokumenPekerjaanUM', 'UmPekerjaanController@dokumenPekerjaanUM');
    Route::post('/updatedokumenPekerjaanUM', 'UmPekerjaanController@updatedokumenPekerjaanUM');
    Route::get('/getDokumenUM/{id}/{user}/{vendor}', 'UmPekerjaanController@getDokumenUM');
    Route::get('/getDokumenUMbyID/{id}/{user}', 'UmPekerjaanController@getDokumenUMbyID');
    Route::get('/getDokumenUMbyID2/{id}/{user}', 'UmPekerjaanController@getDokumenUMbyID2');
    Route::post('/validasiPekerjaanUM', 'UmPekerjaanController@validasiPekerjaanUM');

    //menamahkan route untuk aanwijzingUM
    Route::post('/getAanwijzingUM', 'UmPekerjaanController@getAanwijzingUM');
    Route::post('/getAanwijzingUMVendor/{id}', 'UmPekerjaanController@getAanwijzingUMVendor');
    Route::get('/getmessagesUM/{id_pekerjaan}', 'ChatController@getMessageAddPl');
    Route::post('/insertQNAUM', 'UmPekerjaanController@insertQNAUM');
    Route::post('/updateQNAUM', 'UmPekerjaanController@updateQNAUM');
    Route::post('/deleteQNAUM', 'UmPekerjaanController@deleteQNAUM');
    Route::get('/getQNAUM/{id}/{user}/{vendor}', 'UmPekerjaanController@getQNAUM');
    Route::get('/getQNAUMbyID/{id}/{user}', 'UmPekerjaanController@getQNAUMbyID');
    Route::post('/baAanwijzingUM', 'UmPekerjaanController@baAanwijzingUM');
    Route::get('/getBAAanwijzingUM/{id}', 'UmPekerjaanController@getBAAanwijzingUM'); 
    Route::post('/approveBAAanwijzingUM', 'UmPekerjaanController@approveBAAanwijzingUM');
    Route::get('/getPenyediaUM/{id}', 'UmPekerjaanController@getPenyediaUM');
    Route::get('/pdfAanwijzingUM/{id}', 'UmPekerjaanController@pdfAanwijzingUM');

    //menamahkan route untuk penawaranUM
    Route::post('/getPenawaranUM', 'UmPekerjaanController@getPenawaranUM');
    Route::post('/getPenawaranUMVendor/{id}', 'UmPekerjaanController@getPenawaranUMVendor');
    Route::get('/getPenawaranbyIDUM/{pekerjaan}/{id}/{user}', 'UmPekerjaanController@getPenawaranbyIDUM');
    Route::get('/getPenawaranUM/{pekerjaan}/{id}', 'UmPekerjaanController@getPenawaranUM');
    Route::get('/getFilePenawaranUM/{id}/{user}/{vendor}', 'UmPekerjaanController@getFilePenawaranUM');
    Route::get('/getFilePenawaranUMbyID/{id}/{user}', 'UmPekerjaanController@getFilePenawaranUMbyID');
    Route::post('/penawaranStoreUM', 'UmPekerjaanController@penawaranStoreUM');
    Route::get('/getBAPenawaranUM/{id}', 'UmPekerjaanController@getBAPenawaranUM');
    Route::post('/baPenawaranUM', 'UmPekerjaanController@baPenawaranUM');
    Route::post('/approveBAPenawaranUM', 'UmPekerjaanController@approveBAPenawaranUM');
    Route::get('/dropdownvendorValidasiUM/{id}', 'UmPekerjaanController@dropdownvendorValidasiUM');
    Route::post('/uploadFilePenawaranUM', 'UmPekerjaanController@uploadFilePenawaranUM');
    Route::get('/getFilePenawaranUMbyID2/{id}/{user}', 'UmPekerjaanController@getFilePenawaranUMbyID2');
    Route::post('/validasiPenawaranUM', 'UmPekerjaanController@validasiPenawaranUM');
    Route::get('/pdfPenawaranUM/{id}', 'UmPekerjaanController@pdfPenawaranUM');

    //menamahkan route untuk evaluasiUM
    Route::post('/getEvaluasiUM', 'UmPekerjaanController@getEvaluasiUM');
    Route::post('/getEvaluasiUMVendor/{id}', 'UmPekerjaanController@getEvaluasiUMVendor');
    Route::post('/evaluasiStoreUM', 'UmPekerjaanController@evaluasiStoreUM');
    Route::get('/getFileNotulenUM/{id}/{user}/{vendor}', 'UmPekerjaanController@getFileNotulenUM');
    Route::get('/getFileNotulenUMbyIDUM/{id}/{user}', 'UmPekerjaanController@getFileNotulenUMbyIDUM');

    //menamahkan route untuk negosiasiUM
    Route::post('/getNegosiasiUM', 'UmPekerjaanController@getNegosiasiUM');
    Route::post('/getNegosiasiUMVendor/{id}', 'UmPekerjaanController@getNegosiasiUMVendor');
    Route::get('/getBANegosiasiUM/{id}', 'UmPekerjaanController@getBANegosiasiUM');
    Route::post('/baNegosiasiUM', 'UmPekerjaanController@baNegosiasiUM');
    Route::post('/approveBANegosiasiUM', 'UmPekerjaanController@approveBANegosiasiUM');
    Route::get('/pdfNegosiasiUM/{id}', 'UmPekerjaanController@pdfNegosiasiUM');

    //menamahkan route untuk pemenangUM
    Route::post('/getPemenangUM', 'UmPekerjaanController@getPemenangUM');
    Route::post('/getPemenangUMVendor/{id}', 'UmPekerjaanController@getPemenangUMVendor');
    Route::post('/pemenangUM', 'UmPekerjaanController@pemenangUM');
    Route::get('/getPemenangUM/{id}', 'UmPekerjaanController@getPemenangUM');
    Route::post('/approvePemenangUM', 'UmPekerjaanController@approvePemenangUM');
    Route::get('/pdfPemenangUM/{id}', 'UmPekerjaanController@pdfPemenangUM');







    // untuk profil kop
    Route::get('profil/{id}', 'ProfileController@get_profil');
    Route::post('updatePass', 'ProfileController@updatePass');
    Route::post('updateKop', 'ProfileController@updateKop');
    Route::post('updateAssesment', 'ProfileController@update_assesment');
    Route::post('/updateprofile2/{id}', 'ProfileController@updateProfile'); 

    // untuk berita
    Route::post('insertBerita', 'BeritaPengumumanController@insert_Berita');
    Route::post('updateBerita', 'BeritaPengumumanController@update_Berita');
    Route::get('getBerita/{role}', 'BeritaPengumumanController@getBerita');
    Route::post('prosesBerita', 'BeritaPengumumanController@prosesBerita');
    Route::get('getBeritabyID/{id}', 'BeritaPengumumanController@getBeritabyID');
    Route::post('deleteBerita/{id}', 'BeritaPengumumanController@deleteBerita');
    Route::get('/getListBerita', 'BeritaPengumumanController@getListBerita');
    Route::get('/getListBeritaAll/{limit}', 'BeritaPengumumanController@getListBeritaAll');
    Route::get('/getListBeritaAll/{cari}', 'BeritaPengumumanController@getListBeritaAllCari');
    Route::get('getMap/{param}', 'BeritaPengumumanController@getMaps');
    Route::post('/getCountBerita', 'BeritaPengumumanController@getCountBerita');

    // untuk pengumuman
    Route::post('insertPengumuman', 'BeritaPengumumanController@insert_Pengumuman');
    Route::post('updatePengumuman', 'BeritaPengumumanController@update_Pengumuman');
    Route::get('getPengumuman/{role}', 'BeritaPengumumanController@getPengumuman');
    Route::post('prosesPengumuman', 'BeritaPengumumanController@prosesPengumuman');
    Route::get('getPengumumanbyID/{id}', 'BeritaPengumumanController@getPengumumanbyID');
    Route::post('deletePengumuman/{id}', 'BeritaPengumumanController@deletePengumuman');
    Route::get('/getListPengumuman', 'BeritaPengumumanController@getListPengumuman');
    Route::get('/getListPengumumanAll/{limit}', 'BeritaPengumumanController@getListPengumumanAll');
    Route::get('/getListPengumumanAll/{cari}', 'BeritaPengumumanController@getListPengumumanAllCari');
    Route::post('/getCountPengumuman', 'BeritaPengumumanController@getCountPengumuman');

    // untuk Manage
    Route::get('/getListManageAll/{limit}', 'MasterController@getListManageAll');
    Route::get('/getListManageAllCari/{cari}', 'MasterController@getListManageAllCari');

    //untuk Comment
    Route::post('Comment', 'BeritaPengumumanController@addComment');
    Route::get('getCommentbyID/{id}/{tipe}', 'BeritaPengumumanController@getCommentbyID');
    Route::get('getCommentbyIDAdm/{id}', 'BeritaPengumumanController@getCommentbyIDAdm');
    Route::post('updateComment', 'BeritaPengumumanController@updateComment');
    Route::post('deleteComment/{id}', 'BeritaPengumumanController@deleteComment');
    Route::get('getAllComment', 'BeritaPengumumanController@getAllComment');

    //api untuk Mobile
    Route::post('/loginMobile', 'Auth\LoginController@loginMobile');
    Route::get('/get_pesan_or_praktik_baik/{kategory}/{cari}', 'ApiMobile\ApiMobileController@get_pesan_or_praktik_baik');
    Route::post('/insertDataMobile', 'ApiMobile\ApiMobileController@insert_mobile');
    Route::post('/insertGambar', 'ApiMobile\ApiMobileController@insert_gambar');
    Route::post('/insertPesanBaik', 'ApiMobile\ApiMobileController@insert_pesan_baik');
    Route::get('/listPengumumanMobile', 'ApiMobile\ApiMobileController@getListPengumumanAll');
    Route::get('/listBeritaMobile', 'ApiMobile\ApiMobileController@getListBeritaAll');
    Route::get('/get_MykopMobile/{id}', 'ApiMobile\ApiMobileController@get_Mykop');
    Route::get('/getListProfilKop/{limit}', 'ApiMobile\ApiMobileController@getListKop');
    Route::get('/getListProfilKopCari/{search}', 'ApiMobile\ApiMobileController@getListKopBySearch');
    Route::get('/getListProfilKop/{category}/{cluster}/{scope}', 'ApiMobile\ApiMobileController@getListKopByJenis');
    Route::get('/getcontentBannerMobile', 'ApiMobile\ApiMobileController@getcontentBanner');
    Route::post('/cekPassword', 'ApiMobile\ApiMobileController@check_account');
    Route::post('/gantiPassword', 'ApiMobile\ApiMobileController@userUpdatePass');
    Route::post('/deleteImage', 'ApiMobile\ApiMobileController@deleteImage');

});
