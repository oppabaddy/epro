<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('newPerson', function(){
    return true;
}); 

Broadcast::channel('newVendor', function(){
    return true;
}); 

Broadcast::channel('newNotif', function(){
    return true;
}); 

Broadcast::channel('clearNotif', function(){
    return true;
}); 

Broadcast::channel('refreshEvent', function(){
    return true;
}); 

Broadcast::channel('message', function(){
    return true;
}); 

