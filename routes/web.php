<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//         if(Session::get('login')){
//             return redirect('/dashboard');
//         }
//         else{
//             return view('login');
//         }
// }); 
// 

// mengambil session
Route::get('/session', 'Auth\LoginController@session');
Route::post('/loginPost', 'Auth\LoginController@loginPost');
Route::post('/forgotPost', 'Auth\LoginController@forgotPost');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/downloadExcelPerson', 'PersonController@downloadExcelPerson');
Route::get('/downloadExcelKop', 'KopController@downloadExcelKop');
Route::get('/downloadExcelVendor', 'VendorController@downloadExcelVendor');
Route::get('/downloadRarVendor', 'VendorController@downloadRarVendor2');
Route::get('/downloadExcelIuran', 'KopController@downloadExcelIuran');
Route::get('/downloadExcelUser', 'UserController@downloadExcelUser');
Route::get('/detailKegiatanShare', 'BlogKegiatanController@detailKegiatanShare');
Route::get('/detailBlogShare', 'BlogKegiatanController@detailBlogShare');
Route::get('/detailPesanShare', 'PraktikPesanController@detailPesanShare');
Route::get('/detailPraktikShare', 'PraktikPesanController@detailPraktikShare');
Route::get('/detailPelelanganShare', 'UmPekerjaanController@detailPelelanganShare');
Route::post('/Comment', 'BlogKegiatanController@addComment');

Route::get('messages/{id}', 'ChatController@fetchMessages');
Route::post('messages', 'ChatController@sendMessage');

Route::get('notifikasi/{id}', 'ChatController@fetchNotif');
Route::post('openNotif/{id}', 'ChatController@openNotif');
Route::post('openNotifReqVendor', 'ChatController@openNotifReqVendor');
Route::post('openNotifChat/{id}/{id_pekerjaan}', 'ChatController@openNotifChat');
// Route::post('notifikasi', 'ChatController@sendMessage');


Route::get('/{any}', 'SpaController@admin')->where('any', '.*')->name('spa.admin');

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
